//
//  InstarPicker.swift
//  PlaySongs
//
//  Created by 김영민 on 2020/08/03.
//  Copyright © 2020 김영민. All rights reserved.
//

import Foundation
import YPImagePicker

@objc class InstarPicker : NSObject
{
    @objc var completionHandler: ((UIImage)->Void)?
    @objc var multiCompletionHandler: ((Array<Any>)->Void)?
    
    @objc func getProfilePhoto() -> YPImagePicker {
        
        var config = YPImagePickerConfiguration()
        // [Edit configuration here ...]
        // Build a picker with your configuration
        config.isScrollToChangeModesEnabled = true
        config.onlySquareImagesFromCamera = true
        config.usesFrontCamera = false
        config.showsPhotoFilters = true
        config.showsVideoTrimmer = true
        config.shouldSaveNewPicturesToAlbum = false
        config.albumName = "DefaultYPImagePickerAlbumName"
        config.startOnScreen = YPPickerScreen.library
        config.screens = [.library, .photo]
        config.showsCrop = .rectangle(ratio: 1)
        config.targetImageSize = YPImageSize.original
        config.overlayView = UIView()
        config.hidesStatusBar = false
        config.hidesBottomBar = false
        config.preferredStatusBarStyle = UIStatusBarStyle.default
        //        config.bottomMenuItemSelectedColour = UIColor(red: 38, green: 38, blue: 38, alpha: 255)
        //        config.bottomMenuItemUnSelectedColour = UIColor(red: 153, green: 153, blue: 153, alpha: 255)
        //        config.filters = [DefaultYPFilters...]
        config.maxCameraZoomFactor = 1.0
        
        
        //library
        config.library.options = nil
        config.library.onlySquare = false
        config.library.isSquareByDefault = true
        config.library.minWidthForItem = nil
        //        config.library.mediaType = YPlibraryMediaType.photo
        config.library.defaultMultipleSelection = false
        config.library.maxNumberOfItems = 1
        config.library.minNumberOfItems = 1
        config.library.numberOfItemsInRow = 4
        config.library.spacingBetweenItems = 1.0
        config.library.skipSelectionsGallery = false
        config.library.preselectedItems = nil
        
        
        //        //video
        //        //        config.video.compression = AVAssetExportPresetHighestQuality
        //        config.video.fileType = .mov
        //        config.video.recordingTimeLimit = 60.0
        //        config.video.libraryTimeLimit = 60.0
        //        config.video.minimumTimeLimit = 3.0
        //        config.video.trimmerMaxDuration = 60.0
        //        config.video.trimmerMinDuration = 3.0
        //
        //
        //
        //        //gallery
        //        config.gallery.hidesRemoveButton = false
        
        
        config.library.mediaType = YPlibraryMediaType.photo
        
        YPImagePickerConfiguration.shared = config
        
        let picker = YPImagePicker()
        picker.didFinishPicking { [unowned picker] items, _ in
            if let photo = items.singlePhoto {
                print(photo.fromCamera) // Image source (camera or library)
                print(photo.image) // Final image selected by the user
                print(photo.originalImage) // original image selected by the user, unfiltered
                print(photo.modifiedImage as Any) // Transformed image, can be nil
                print(photo.exifMeta as Any) // Print exif meta data of original image.
                
                if let handler = self.completionHandler {
                    handler(photo.image)
                }
                
            }
            picker.dismiss(animated: true, completion: nil)
        }
        
        return picker
    }
    @objc func getSinglePhoto() -> YPImagePicker {
        
        var config = YPImagePickerConfiguration()
        // [Edit configuration here ...]
        // Build a picker with your configuration
        config.isScrollToChangeModesEnabled = true
        config.onlySquareImagesFromCamera = true
        config.usesFrontCamera = false
        config.showsPhotoFilters = true
        config.showsVideoTrimmer = true
        config.shouldSaveNewPicturesToAlbum = false
        config.albumName = "DefaultYPImagePickerAlbumName"
        config.startOnScreen = YPPickerScreen.library
        config.screens = [.library, .photo]
        config.showsCrop = .rectangle(ratio: 1)
        config.targetImageSize = YPImageSize.original
        config.overlayView = UIView()
        config.hidesStatusBar = false
        config.hidesBottomBar = false
        config.preferredStatusBarStyle = UIStatusBarStyle.default
        config.bottomMenuItemSelectedTextColour = UIColor.red
        config.bottomMenuItemUnSelectedTextColour = UIColor.blue
        //        config.bottomMenuItemSelectedColour = UIColor(red: 38, green: 38, blue: 38, alpha: 255)
        //        config.bottomMenuItemUnSelectedColour = UIColor(red: 153, green: 153, blue: 153, alpha: 255)
        //        config.filters = [DefaultYPFilters...]
        config.maxCameraZoomFactor = 1.0
        
        
        //library
        config.library.options = nil
        config.library.onlySquare = false
        config.library.isSquareByDefault = true
        config.library.minWidthForItem = nil
        //        config.library.mediaType = YPlibraryMediaType.photo
        config.library.defaultMultipleSelection = false
        config.library.maxNumberOfItems = 1
        config.library.minNumberOfItems = 1
        config.library.numberOfItemsInRow = 4
        config.library.spacingBetweenItems = 1.0
        config.library.skipSelectionsGallery = false
        config.library.preselectedItems = nil
        
        
        //        //video
        //        //        config.video.compression = AVAssetExportPresetHighestQuality
        //        config.video.fileType = .mov
        //        config.video.recordingTimeLimit = 60.0
        //        config.video.libraryTimeLimit = 60.0
        //        config.video.minimumTimeLimit = 3.0
        //        config.video.trimmerMaxDuration = 60.0
        //        config.video.trimmerMinDuration = 3.0
        //
        //
        //
        //        //gallery
        //        config.gallery.hidesRemoveButton = false
        
        
        //        config.screens = [.library]
        config.library.mediaType = YPlibraryMediaType.photo
        
        YPImagePickerConfiguration.shared = config
        
        let picker = YPImagePicker()
        picker.didFinishPicking { [unowned picker] items, _ in
            if let photo = items.singlePhoto {
                print(photo.fromCamera) // Image source (camera or library)
                print(photo.image) // Final image selected by the user
                print(photo.originalImage) // original image selected by the user, unfiltered
                print(photo.modifiedImage as Any) // Transformed image, can be nil
                print(photo.exifMeta as Any) // Print exif meta data of original image.
                
                if let handler = self.completionHandler {
                    handler(photo.image)
                }
            }
            picker.dismiss(animated: true, completion: nil)
        }
        
        return picker
    }
    
    @objc func getMultiPhoto(count: Int) -> YPImagePicker {
        
        var config = YPImagePickerConfiguration()
        // [Edit configuration here ...]
        // Build a picker with your configuration
        config.isScrollToChangeModesEnabled = true
        config.onlySquareImagesFromCamera = true
        config.usesFrontCamera = false
        config.showsPhotoFilters = true
        config.showsVideoTrimmer = true
        config.shouldSaveNewPicturesToAlbum = false
        config.albumName = "DefaultYPImagePickerAlbumName"
        config.startOnScreen = YPPickerScreen.library
        //            config.screens = [.library, .photo]
        config.screens = [.library]
        config.showsCrop = .rectangle(ratio: 1)
        config.targetImageSize = YPImageSize.original
        config.overlayView = UIView()
        config.hidesStatusBar = false
        config.hidesBottomBar = false
        config.preferredStatusBarStyle = UIStatusBarStyle.default
        config.bottomMenuItemSelectedTextColour = UIColor.red
        config.bottomMenuItemUnSelectedTextColour = UIColor.blue
        //        config.bottomMenuItemSelectedColour = UIColor(red: 38, green: 38, blue: 38, alpha: 255)
        //        config.bottomMenuItemUnSelectedColour = UIColor(red: 153, green: 153, blue: 153, alpha: 255)
        //        config.filters = [DefaultYPFilters...]
        config.maxCameraZoomFactor = 1.0
        
        
        //library
        config.library.options = nil
        config.library.onlySquare = false
        config.library.isSquareByDefault = true
        config.library.minWidthForItem = nil
        //        config.library.mediaType = YPlibraryMediaType.photo
        config.library.defaultMultipleSelection = false
        config.library.maxNumberOfItems = count
        config.library.minNumberOfItems = 1
        config.library.numberOfItemsInRow = 4
        config.library.spacingBetweenItems = 1.0
        config.library.skipSelectionsGallery = false
        config.library.preselectedItems = nil
        
        
        //video
        //        config.video.compression = AVAssetExportPresetHighestQuality
        config.video.fileType = .mov
        config.video.recordingTimeLimit = 60.0
        config.video.libraryTimeLimit = 60.0
        config.video.minimumTimeLimit = 3.0
        config.video.trimmerMaxDuration = 60.0
        config.video.trimmerMinDuration = 3.0
        
        
        //gallery
        config.gallery.hidesRemoveButton = false
        
        
        config.screens = [.library, .photo, .video]
        config.library.mediaType = YPlibraryMediaType.photoAndVideo
        
        YPImagePickerConfiguration.shared = config
        
        let picker = YPImagePicker()
        picker.didFinishPicking { [unowned picker] items, cancelled in
            
            var array = Array<Dictionary<String, Any>>()
            for item in items {
                switch item {
                case .photo(let photo):
                    print(photo)
                    array.append(["type":"image", "image":photo.image])
                case .video(let video):
                    print(video)
                    if video.asset != nil {
                        array.append(["type":"video", "asset":video.asset as Any, "image":video.thumbnail, "url":video.url])
                    }
                }
            }
            
            picker.dismiss(animated: true) {
                
                if let handler = self.multiCompletionHandler {
                    handler(array)
                }
            }
        }
        
        return picker
    }
    //    @objc(addX:Y:)
    @objc func add(x: Int, y: Int) -> Int {
        return x+y
    }
}
