//
//  ChatListCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/17.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet ThumbImageView *iv_Thumb;
@property (weak, nonatomic) IBOutlet UILabel *lb_Title;
@property (weak, nonatomic) IBOutlet UILabel *lb_Contents;
@property (weak, nonatomic) IBOutlet UILabel *lb_Date;
@property (weak, nonatomic) IBOutlet UIView *v_Badge;
@property (weak, nonatomic) IBOutlet UILabel *lb_Badge;
@end

NS_ASSUME_NONNULL_END
