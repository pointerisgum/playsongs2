//
//  Defines.h
//  Kizzl
//
//  Created by Kim Young-Min on 13. 5. 28..
//  Copyright (c) 2013년 Kim Young-Min. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NSDictionary+JSONValueParsing.h"
#import "WebAPI.h"
#import "Util.h"
#import "Common.h"
#import "MBProgressHUD.h"
#import "UIAlertController+Blocks.h"
#import "ActionSheetPicker.h"
@import HapticHelper;
#import "UIColor+Expanded.h"
#import "UserData.h"
#import "NSObject+PlaySongs.h"
#import "UIImageView+WebCache.h"
#import <Toast/Toast.h>
#import "UIView+Toast.h"
#import "ThumbImageView.h"
#import "SPAlertController.h"

#define kDomain             @"https://www.playhavea.com"

//개발
#define kBaseUrl            [NSString stringWithFormat:@"%@/api/", kDomain]
//#define kWebBaseUrl         @"https://social.playsongs.ai"
#define kSendBirdAppId      @"0F38E766-3619-4009-9B2E-0402062F5C64"
#define kSendBirdApiToken   @"b65f2246f600d68c6e4190ddd5e5f7d75cf60331"
#define kDevText            @"DEV"

//운영




#define kMainBoard            [UIStoryboard storyboardWithName:@"Main" bundle:nil]
#define kLoginBoard           [UIStoryboard storyboardWithName:@"Login" bundle:nil]
#define kFeedBoard            [UIStoryboard storyboardWithName:@"Feed" bundle:nil]
#define kSearchBoard          [UIStoryboard storyboardWithName:@"Search" bundle:nil]
#define kPlayMakeBoard        [UIStoryboard storyboardWithName:@"PlayMake" bundle:nil]
#define kShoppingBoard        [UIStoryboard storyboardWithName:@"Shopping" bundle:nil]
#define kUserBoard            [UIStoryboard storyboardWithName:@"User" bundle:nil]


#define kDeviceWidth                  [Util keyWindow].frame.size.width
#define kDeviceHeight                 [Util keyWindow].frame.size.height

//AppStoreUrl
#define kAppStoreURL            @"https://itunes.apple.com/app/id1526180597"
#define kAppId                  @"1526180597"

#define IsStringEmpty(text)             (text  == nil || [text isStringEmpty])
#define IsDictionaryEmpty(dict)         (dict  == nil || [dict isDictionaryEmpty])
#define IsArrayEmpty(array)             (array == nil || [array isArrayEmpty])
#define IsArrayOutOfBounds(array,row)   (array == nil || [array isArrayOutOfBounds:row])

#define kEnableColor            [UIColor colorWithHexString:@"FE540E"]
#define kDisableColor           [UIColor colorWithHexString:@"9B9B9B"]
#define kErrorColor             [UIColor systemRedColor]
#define kEnableBoarderColor     [UIColor lightGrayColor]

#define degreesToRadian(x) (M_PI * (x) / 180.0)

#ifdef DEBUG
#define DLog(...) NSLog(__VA_ARGS__)
#else
#define DLog(...) /* */
#endif

//GCD
#define AsyncBlock(block)       dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block)
#define SyncBlock(block)        dispatch_sync(dispatch_get_main_queue(), block)
#define AsyncMainBlock(block)   dispatch_async(dispatch_get_main_queue(), block)

#define ALERT(TITLE, MSG, DELEGATE, BTN_TITLE1, BTN_TITLE2){UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:TITLE message:MSG delegate:DELEGATE cancelButtonTitle:nil otherButtonTitles:BTN_TITLE1, BTN_TITLE2, nil];[alertView show];}

#define ALERT_ONE(MSG){UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:MSG delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil];[alertView show];}

#define CREATE_ALERT(TITLE, MSG, BTN_TITLE1, BTN_TITLE2){[[UIAlertView alloc]initWithTitle:TITLE message:MSG delegate:nil cancelButtonTitle:nil otherButtonTitles:BTN_TITLE1, BTN_TITLE2, nil]}

#define BundleImage(IMAGE_NAME) [UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:IMAGE_NAME]]


#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)


//#ifdef DEBUG
////#define NSLog(fmt, ...) NSLog((@"%s[Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
//#define NSLog(fmt, ...) NSLog((fmt), ##__VA_ARGS__)
//#else
//#define NSLog(...)
//#endif


#ifndef popover_PopoverViewCompatibility_h
#define popover_PopoverViewCompatibility_h

#ifdef __IPHONE_6_0

#define UITextAlignmentCenter       NSTextAlignmentCenter
#define UITextAlignmentLeft         NSTextAlignmentLeft
#define UITextAlignmentRight        NSTextAlignmentRight
#define UILineBreakModeTailTruncation   NSLineBreakByTruncatingTail
#define UILineBreakModeMiddleTruncation NSLineBreakByTruncatingMiddle
#define UILineBreakModeWordWrap         NSLineBreakByWordWrapping

#endif

#endif

