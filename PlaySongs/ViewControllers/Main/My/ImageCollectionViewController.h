//
//  ImageCollectionViewController.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/13.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^ItemSelectedBlock)(id completeResult);

@interface ImageCollectionViewController : UIViewController
@property (nonatomic, assign) BOOL isMoreMode;
@property (nonatomic, assign) BOOL isCalled;    //초기 로딩때 불렸는지 여부
@property (nonatomic, strong) NSDictionary *dic_Params;
@property (nonatomic, strong) NSString *str_Path;
@property (nonatomic, copy) ItemSelectedBlock itemSelectedBlock;
- (void)setItemSelectedBlock:(ItemSelectedBlock)itemSelectedBlock;
- (void)updateList;
- (void)refresh;
@end

NS_ASSUME_NONNULL_END
