//
//  PlayJoinViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/27.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "PlayJoinViewController.h"
#import "UITextView+Placeholder.h"
#import "SearchTextCell.h"
#import <Photos/Photos.h>

@interface PlayJoinViewController () <UITextViewDelegate>
@property (nonatomic, assign) BOOL isSearchOn;
@property (nonatomic, assign) NSRange tagStartRange;
@property (nonatomic, strong) NSMutableArray *arM_List;
@property (nonatomic, strong) NSMutableArray *arM_SelectedTagList;
@property (nonatomic, strong) NSArray *ar_UploadResult;
@property (weak, nonatomic) IBOutlet UIButton *btn_Done;
@property (weak, nonatomic) IBOutlet UIImageView *iv_Thumb;
@property (weak, nonatomic) IBOutlet UIImageView *iv_Multi;
@property (weak, nonatomic) IBOutlet UITextView *tv_Contents;
@property (weak, nonatomic) IBOutlet UITableView *tbv_List;

@end

@implementation PlayJoinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tv_Contents.placeholder = @"댓글 및 #tag";
    [_tv_Contents becomeFirstResponder];
    
    _arM_SelectedTagList = [NSMutableArray array];
    
    if( _ar_Images.count > 1 )
    {
        _iv_Multi.hidden = NO;
    }
    else
    {
        _iv_Multi.hidden = YES;
    }
    
    
    
    

//    case .video(let video):
//        print(video)
//        if video.asset != nil {

    
    
    
    if( _ar_Images.count > 0 )
    {
        NSDictionary *dic = _ar_Images.firstObject;
        _iv_Thumb.image = [dic objectForKey:@"image"];
//        array.append(["type":"video", "asset":video.asset as Any, "thumb":video.thumbnail, "url":video.url])
//        array.append(["type":"image", "thumb":photo.image])
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)updateSearch:(NSString *)aSearchTag
{
    __weak __typeof__(self) weakSelf = self;

    NSLog(@"search text: %@", aSearchTag);
    
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:aSearchTag forKey:@"q"];
    [dicM_Params setObject:@"tag" forKey:@"filter"];

    [[WebAPI sharedData] callAsyncWebAPIBlock:@"search/keyword" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        if( resulte != nil )
        {
            NSArray *ar = [NSArray arrayWithArray:resulte];
            if( ar.count > 0 )
            {
                weakSelf.arM_List = [NSMutableArray arrayWithArray:ar];
            }
        }

        [weakSelf.tbv_List reloadData];
    }];

}

- (void)uploadData
{
    _btn_Done.userInteractionEnabled = NO;

    if( _ar_Images.count <= 0 )
    {
        //첨부파일이 없는 경우
        _btn_Done.userInteractionEnabled = YES;
        [self uploadContents];
        return;
    }
    
    __weak __typeof__(self) weakSelf = self;
    NSMutableDictionary *dicM_Datas = [NSMutableDictionary dictionary];
    for( NSInteger i = 0; i < _ar_Images.count; i++ )
    {
        NSString *str_Key = [NSString stringWithFormat:@"file%ld", i+1];
        NSDictionary *dic = _ar_Images[i];
        NSString *str_Type = [dic stringForKey:@"type"];
        NSData *data = nil;
        if( [str_Type isEqualToString:@"image"] )
        {
            UIImage *image = [dic objectForKey:@"image"];
            data = UIImagePNGRepresentation(image);
            if( data != nil )
            {
                [dicM_Datas setObject:@{@"data":data, @"type":@"image"} forKey:str_Key];
            }
        }
        else if( [str_Type isEqualToString:@"video"] )
        {
            NSString *str_VideoLocalUrl = [dic stringForKey:@"url"];
            data = [NSData dataWithContentsOfFile:str_VideoLocalUrl];
            if( data != nil )
            {
                [dicM_Datas setObject:@{@"data":data, @"type":@"video"} forKey:str_Key];
            }
        }
    }

    [[WebAPI sharedData] imageUpload:@"playing/uploader" param:nil withImages:dicM_Datas withBlock:^(id resulte, NSError *error) {
      
        [MBProgressHUD hide];
        weakSelf.btn_Done.userInteractionEnabled = YES;

        if( resulte )
        {
            NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
            if( [str_Success isEqualToString:@"success"] )
            {
                id res = [resulte dictionaryForKey:@"result"];
                if( [res isKindOfClass:[NSArray class]] )
                {
                    weakSelf.ar_UploadResult = [NSArray arrayWithArray:res];
                    [weakSelf uploadContents];
                    return;
                }

            }
            
            [Util showToast:@"데이터 전송에 실패 하였습니다."];
        }
        else
        {
            [Util showToast:@"데이터 전송에 실패 하였습니다."];
        }
    }];
}

- (void)uploadContents
{
    //_arM_SelectedTagList

    _btn_Done.userInteractionEnabled = NO;
    
    __weak __typeof__(self) weakSelf = self;
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:_str_PlayId forKey:@"joinPlayId"];
    [dicM_Params setObject:@([UserData sharedData].userId) forKey:@"userId"];

    NSMutableArray *arM_Body = [NSMutableArray arrayWithCapacity:3];
    NSMutableDictionary *dicM_Body = [NSMutableDictionary dictionary];
    [dicM_Body setObject:@"1" forKey:@"seq"];
    [dicM_Body setObject:@"text" forKey:@"type"];
    [dicM_Body setObject:_tv_Contents.text forKey:@"text"];
    [arM_Body addObject:dicM_Body];
    
    dicM_Body = [NSMutableDictionary dictionary];
    [dicM_Body setObject:@"2" forKey:@"seq"];
    [dicM_Body setObject:@"file" forKey:@"type"];
    [dicM_Body setObject:_ar_UploadResult forKey:@"files"];
    [arM_Body addObject:dicM_Body];
    
    dicM_Body = [NSMutableDictionary dictionary];
    [dicM_Body setObject:@"3" forKey:@"seq"];
    [dicM_Body setObject:@"tag" forKey:@"type"];
    [dicM_Body setObject:_arM_SelectedTagList forKey:@"tags"];
    [arM_Body addObject:dicM_Body];
    
    NSError *err;
    NSData *jsonData = [NSJSONSerialization  dataWithJSONObject:arM_Body options:0 error:&err];
    NSString *str_JsonData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    [dicM_Params setObject:str_JsonData forKey:@"contentBody"];
    
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"playing/save" param:dicM_Params withMethod:@"POST" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        weakSelf.btn_Done.userInteractionEnabled = YES;

        if( error != nil )
        {
            [Util showToast:@"데이터 전송에 실패 하였습니다."];
            return;
        }

        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshFeedListNoti" object:nil];
            [weakSelf dismissViewControllerAnimated:YES completion:^{
            }];
            return;
        }

        [Util showToast:@"데이터 전송에 실패 하였습니다."];

//        if (weakSelf.categoryApiBlock)
//        {
//            weakSelf.categoryApiBlock();
//            weakSelf.categoryApiBlock = nil;
//        }
    }];

}

//MARK: UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    _tagStartRange = range;
    _isSearchOn = YES;

    if( [text isEqualToString:@" "] )
    {
        //검색 종료
        _isSearchOn = NO;
    }

    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    if( _isSearchOn == NO )
    {
        [_arM_List removeAllObjects];
        [_tbv_List reloadData];
        return;
    }
    
    if( textView.text.length < _tagStartRange.location )    return;
    
    NSString *str = [textView.text substringWithRange:NSMakeRange(0, _tagStartRange.location)];
    NSArray *ar = [str componentsSeparatedByString:@" "];
    if( ar.count > 0 )
    {
        NSString *str_Last = ar.lastObject;
        if( [str_Last hasPrefix:@"#"] )
        {
            NSLog(@"%@", str_Last);
            if( [str_Last hasPrefix:@"#"] )
            {
                str_Last = [str_Last stringByReplacingOccurrencesOfString:@"#" withString:@""];
            }

            if( str_Last.length > 0 )
            {
                [self updateSearch:str_Last];
            }
            else
            {
                [_arM_List removeAllObjects];
                [_tbv_List reloadData];
            }
        }
    }
}


//MARK: TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arM_List.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SearchTextCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchTextCell"];

    NSDictionary *dic = _arM_List[indexPath.row];
    
    NSString *str_ImageUrl = [dic stringForKey:@"imgUrl"];
    [cell.iv_Thumb sd_setImageWithURL:[NSURL URLWithString:str_ImageUrl]];
    cell.lb_Title.text = [dic stringForKey:@"name"];
    cell.lb_SubTitle.text = [NSString stringWithFormat:@"%ld 게시물", [dic integerForKey:@"count"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    NSDictionary *dic = _arM_List[indexPath.row];

    if( _tv_Contents.text.length < _tagStartRange.location )
    {
        //여기 들어오면 안됨
        return;
    }
    
    NSString *str = [_tv_Contents.text substringWithRange:NSMakeRange(0, _tv_Contents.selectedRange.location)];
    NSArray *ar = [str componentsSeparatedByString:@" "];
    NSMutableArray *arM = [NSMutableArray arrayWithArray:ar];
    if( ar.count > 0 )
    {
        NSString *str_Last = ar.lastObject;
        if( [str_Last hasPrefix:@"#"] )
        {
            NSLog(@"%@", str_Last);
            if( [str_Last hasPrefix:@"#"] )
            {
                [_arM_SelectedTagList addObject:dic];
                
                [arM replaceObjectAtIndex:ar.count - 1 withObject:[NSString stringWithFormat:@"#%@", [dic stringForKey:@"name"]]];
                
                NSMutableString *strM = [NSMutableString string];
                for( NSString *str in arM )
                {
                    [strM appendString:str];
                    [strM appendString:@" "];
                }
                
                if( [strM hasSuffix:@"  "] )
                {
                    [strM deleteCharactersInRange:NSMakeRange([strM length]-1, 1)];
                }
                
                NSInteger nCursorPosition = strM.length;
                
                NSString *str_Tail = [_tv_Contents.text substringWithRange:NSMakeRange(_tv_Contents.selectedRange.location, _tv_Contents.text.length - _tv_Contents.selectedRange.location)];
                if( [str_Tail isEqualToString:@"#"] == NO && [str_Tail hasPrefix:@" "] )
                {
                    NSMutableString *strM_Temp = [NSMutableString stringWithString:str_Tail];
                    [strM_Temp deleteCharactersInRange:NSMakeRange(0, 1)];
                    str_Tail = strM_Temp;
                }
                [strM appendString:str_Tail];

                _tv_Contents.text = strM;
                _tv_Contents.selectedRange = NSMakeRange(nCursorPosition, 0);
                
                [_arM_List removeAllObjects];
                [_tbv_List reloadData];
            }
        }
    }

}


//MARK: Action
- (IBAction)goDone:(id)sender
{
    NSMutableArray *arM_Temp = [NSMutableArray array];
    NSArray *ar = [_tv_Contents.text componentsSeparatedByString:@" "];
    for( NSString *str in ar )
    {
        if( [str hasPrefix:@"#"] )
        {
            BOOL isMyTag = YES;
            NSString *str_TargetTagName = [str stringByReplacingOccurrencesOfString:@"#" withString:@""];
            for( NSDictionary *dic in _arM_SelectedTagList )
            {
                NSString *str_TagName = [dic stringForKey:@"name"];
                if( [str_TagName isEqualToString:str_TargetTagName] )
                {
                    if( [arM_Temp containsObject:dic] == NO )
                    {
                        isMyTag = NO;
                        [arM_Temp addObject:@{@"tagId":@([dic integerForKey:@"id"]), @"tagName":[dic stringForKey:@"name"]}];
                    }
                    break;
                }
            }
            
            if( isMyTag )
            {
                [arM_Temp addObject:@{@"tagName":str_TargetTagName}];
            }
        }
    }
    
    _arM_SelectedTagList = [NSMutableArray arrayWithArray:arM_Temp];
    
    //중복된거 빼고 삭제된거 빼고 실제 서버에 전송 할 태그들
    NSLog(@"%@", _arM_SelectedTagList);
    
    if( _ar_Images.count <= 0 && _tv_Contents.text.length <= 0 )    return;

    [HapticHelper tap];
    [self uploadData];
}

@end
