//
//  TagPageViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/13.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "TagPageViewController.h"
#import "TagCvCell.h"
#import "SearchThumbCell.h"
#import "UICollectionViewLeftAlignedLayout.h"
#import "FeedDetailViewController.h"

#define kCellMargin 1.5
#define kLimitCount 18

@interface TagPageViewController ()

@property (nonatomic, assign) NSInteger nPage;
@property (nonatomic, assign) BOOL isFinish;
@property (nonatomic, assign) BOOL isLoading;

@property (nonatomic, strong) NSDictionary *dic_TopInfo;
@property (nonatomic, strong) NSMutableArray *arM_TagList;
@property (nonatomic, strong) NSMutableArray *arM_List;

@property (weak, nonatomic) IBOutlet UILabel *lb_Title;
@property (weak, nonatomic) IBOutlet UIImageView *iv_Thumb;
@property (weak, nonatomic) IBOutlet UILabel *lb_PlayCount;
@property (weak, nonatomic) IBOutlet UIButton *btn_Follow;
@property (weak, nonatomic) IBOutlet UILabel *lb_PlayDisc;


@property (weak, nonatomic) IBOutlet UICollectionView *cv_TagList;
@property (weak, nonatomic) IBOutlet UICollectionView *cv_List;



@end

@implementation TagPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _lb_Title.text = [NSString stringWithFormat:@"#%@", _str_TagName];

    _nPage = 1;
    _isFinish = NO;
    _isLoading = NO;
    _arM_List = [NSMutableArray array];

    UICollectionViewLeftAlignedLayout *layout = [[UICollectionViewLeftAlignedLayout alloc] init];
    layout.minimumLineSpacing = kCellMargin;
    layout.minimumInteritemSpacing = kCellMargin;
    _cv_List.collectionViewLayout = layout;

    [self updateFollowCheck];
    [self updateTopList];
    [self updateList];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)updateFollowCheck
{
    __weak __typeof__(self) weakSelf = self;

    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
//    [dicM_Params setObject:_str_TagName forKey:@"tagName"];
    [dicM_Params setObject:@(_nTagId) forKey:@"following"];

    
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"tag/follow/check" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            id followInfo = [resulte objectForKey :@"result"];
            if( [followInfo isKindOfClass:[NSNull class]] )
            {
                //팔로잉 하기
                weakSelf.btn_Follow.selected = YES;
            }
            else
            {
                //팔로우중
                weakSelf.btn_Follow.selected = NO;
            }
        }
    }];
}

- (void)postFollowStatus:(BOOL)isFollow
{
    __weak __typeof__(self) weakSelf = self;

    NSString *str_Path = isFollow ? @"tag/follow": @"tag/unfollow";
    
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@(_nTagId) forKey:@"following"];
    [[WebAPI sharedData] callAsyncWebAPIBlock:str_Path param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            weakSelf.btn_Follow.selected = !weakSelf.btn_Follow.selected;
        }
    }];
}

- (void)updateTopList
{
    __weak __typeof__(self) weakSelf = self;

    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:_str_TagName forKey:@"tagName"];
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"tag/info" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            weakSelf.dic_TopInfo = [resulte valueForDictionaryKey:@"result"];
            [weakSelf updateTopView];
        }
    }];
}

- (void)updateList
{
    if( _isFinish )     return;
    if( _isLoading )    return;
    
    _isLoading = YES;
    
    __weak __typeof__(self) weakSelf = self;

    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setValue:@(_nPage) forKey:@"page"];
    [dicM_Params setValue:@(kLimitCount) forKey:@"limt"];
    [dicM_Params setObject:_str_TagName forKey:@"tagName"];
    [dicM_Params setObject:@"new" forKey:@"sort"];

    [[WebAPI sharedData] callAsyncWebAPIBlock:@"search" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
                
        [MBProgressHUD hide];
        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [weakSelf.refreshControl endRefreshing];
//        });

        if( error != nil )
        {
            weakSelf.isLoading = NO;
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            NSArray *ar = [resulte valueForArrayKey:@"result"];
            if( ar.count > 0 )
            {
                if( weakSelf.arM_List.count > 0 )
                {
                    [weakSelf.arM_List addObjectsFromArray:ar];
                }
                else
                {
                    weakSelf.arM_List = [NSMutableArray arrayWithArray:ar];
                }
            }
            else
            {
                //finish
                weakSelf.isFinish = YES;
            }
        }

        [weakSelf.cv_List reloadData];
        weakSelf.isLoading = NO;
    }];
}

- (void)updateTopView
{
    NSString *str_ImageUrl = [_dic_TopInfo stringForKey:@"coverUrl"];
    [_iv_Thumb sd_setImageWithURL:[NSURL URLWithString:str_ImageUrl] placeholderImage:[UIImage imageNamed:@"ic_no_user"]];
    
    _lb_PlayCount.text = [NSString stringWithFormat:@"%ld", [_dic_TopInfo integerForKey:@"playCount"]];
    
    NSArray *ar = [_dic_TopInfo valueForArrayKey:@"relatedTagList"];
    if( ar.count > 0 )
    {
        _arM_TagList = [NSMutableArray arrayWithArray:ar];
    }
    
    [_cv_TagList reloadData];
}


//MARK: CollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if( collectionView == _cv_TagList )
    {
        return _arM_TagList.count;
    }
    return _arM_List.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if( collectionView == _cv_TagList )
    {
        TagCvCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TagCvCell" forIndexPath:indexPath];
        cell.v_Bg.layer.borderColor = [UIColor clearColor].CGColor;

        if( IsArrayOutOfBounds(_arM_TagList, indexPath.row) )  return cell;
        
        NSDictionary *dic = _arM_TagList[indexPath.row];
        cell.lb_Title.text = [NSString stringWithFormat:@"#%@", [dic stringForKey:@"tagName"]];
        return cell;
    }
    
    SearchThumbCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchThumbCell" forIndexPath:indexPath];
    
    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return cell;
    
    NSDictionary *dic = _arM_List[indexPath.row];
    NSString *str_ImageUrl = [dic stringForKey:@"coverUrl"];
    [cell.iv_Thumb sd_setImageWithURL:[NSURL URLWithString:str_ImageUrl] placeholderImage:[UIImage imageNamed:@"ic_no_image"]];
    
    CGFloat fCellSize = (collectionView.bounds.size.width - (kCellMargin*2))/3;
    cell.lc_Width.constant = fCellSize;
    cell.lc_Height.constant = fCellSize;

    BOOL lastItemReached = [dic isEqual:[_arM_List lastObject]];
    if (lastItemReached && indexPath.row == [_arM_List count] - 1)
    {
        _nPage++;
        [self updateList];
    }

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat fCellSize = (collectionView.bounds.size.width - (kCellMargin*2))/3;
    return CGSizeMake(fCellSize, fCellSize);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if( collectionView == _cv_TagList )
    {
        return UIEdgeInsetsMake(0, 0, 0, 15);
    }
    return UIEdgeInsetsMake(kCellMargin, 0, kCellMargin, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [HapticHelper tap];
    
    if( collectionView == _cv_TagList )
    {
        if( IsArrayOutOfBounds(_arM_TagList, indexPath.row) )  return;
        
        NSDictionary *dic = _arM_TagList[indexPath.row];
        TagPageViewController *vc = [kUserBoard instantiateViewControllerWithIdentifier:@"TagPageViewController"];
        vc.str_TagName = [dic stringForKey:@"tagName"];
        vc.nTagId = [dic integerForKey:@"tagId"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return;
        
        NSDictionary *dic = _arM_List[indexPath.row];
        FeedDetailViewController *vc = [kFeedBoard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
        vc.str_PlayId = [NSString stringWithFormat:@"%ld", [dic integerForKey:@"playId"]];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

//MARK: Action
- (IBAction)goFollow:(id)sender
{
    [HapticHelper tap];

    [self postFollowStatus:_btn_Follow.selected];
}

- (IBAction)goMore:(id)sender
{
    [HapticHelper tap];
    
}

@end
