//
//  TopToastView.m
//  Moon
//
//  Created by 김영민 on 2019/12/27.
//  Copyright © 2019 macpro15. All rights reserved.
//

#import "TopToastView.h"

@interface TopToastView ()
@property (nonatomic, assign) CGFloat fViewY;
@property (nonatomic, assign) CGFloat fPointY;
@end

@implementation TopToastView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.alpha = NO;

    // border radius
    [self.v_Contents.layer setCornerRadius:10.0f];

    // border
    [self.v_Contents.layer setBorderWidth:0.2f];
    [self.v_Contents.layer setBorderColor:[UIColor clearColor].CGColor];
    
    // drop shadow
    [self.v_Contents.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.v_Contents.layer setShadowOpacity:0.4];
    [self.v_Contents.layer setShadowRadius:4.0];
    [self.v_Contents.layer setShadowOffset:CGSizeMake(3.0, 3.0)];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
//    UITouch *touch = [[event touchesForView:self]anyObject];
//    CGPoint point = [touch locationInView:self];
//
//    NSLog(@"%f", self.frame.origin.y);
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event touchesForView:self]anyObject];
    CGPoint point = [touch locationInView:self];
    
    NSLog(@"%f", point.y);

    if( self.fPointY > 0 )
    {
        CGFloat fMoveY = self.fPointY - point.y;
        if( self.frame.origin.y - fMoveY > 0 )
        {
            [UIView animateWithDuration:0.05f animations:^{
                self.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
            }];
        }
        else
        {
            [UIView animateWithDuration:0.05f animations:^{
                self.frame = CGRectMake(0, self.frame.origin.y - fMoveY, self.frame.size.width, self.frame.size.height);
            }];
        }
    }
    
    self.fPointY = point.y;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
//    UITouch *touch = [[event touchesForView:self]anyObject];
//    CGPoint point = [touch locationInView:self];

    if( (self.frame.origin.y * -1) > self.frame.size.height / 6 )
    {
        [UIView animateWithDuration:0.15f animations:^{
            self.frame = CGRectMake(0, -self.frame.size.height, self.frame.size.width, self.frame.size.height);
        }completion:^(BOOL finished) {
            
        }];
    }
    else
    {
        [UIView animateWithDuration:0.15f animations:^{
            self.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        }completion:^(BOOL finished) {
            
        }];
    }
}

@end
