//
//  TopToastView.h
//  Moon
//
//  Created by 김영민 on 2019/12/27.
//  Copyright © 2019 macpro15. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TopToastView : UIView
@property (nonatomic, strong) NSDictionary *dic_Data;
@property (weak, nonatomic) IBOutlet UIView *v_Contents;
@property (weak, nonatomic) IBOutlet UILabel *lb_Msg;
@end

NS_ASSUME_NONNULL_END
