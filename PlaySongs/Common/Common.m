//
//  Common.m
//  KetoPlus
//
//  Created by 김영민 on 2020/03/25.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "Common.h"
#import <SendBirdSDK/SendBirdSDK.h>
#import <KakaoOpenSDK/KakaoOpenSDK.h>

//#import "UserData.h"

@implementation Common

static Common *shared = nil;
static NSMutableDictionary *loginData = nil;
//static UserData *userData = nil;

+ (void)initialize
{
    NSAssert(self == [Common class], @"Singleton is not designed to be subclassed.");
    shared = [Common new];
    loginData = [NSMutableDictionary dictionary];
//    userData = [[UserData alloc] init];
}

+ (Common *)sharedData
{
    return shared;
}

//- (void)logOut:(UIViewController *)vc
//{
////    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
////                                        @"", @"social",
////                                        nil];
////
////    [[WebAPI sharedData] callAsyncWebAPIBlock:@"auth/logout"
////                                        param:dicM_Params
////                                   withMethod:@"POST"
////                                    withBlock:^(id result, NSError *error) {
////
////        [MBProgressHUD hide];
////
////        if( result )
////        {
////            NSDictionary *dic = [NSDictionary dictionaryWithDictionary:result];
////            NSDictionary *dic_Meta = [dic dictionaryForKey:@"meta"];
////            NSDictionary *dic_Data = [dic dictionaryForKey:@"data"];
////            NSInteger nCode = [dic_Meta integerForKey:@"code"];
////            if( nCode == 0 )
////            {
////
////                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"authToken"];
////                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isLogin"];
////                [[NSUserDefaults standardUserDefaults] synchronize];
////
////                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
////                [appDelegate showLoginView];
////            }
////            else
////            {
//////                [Util showMessage:vc withTitle:@"" withMessage:[dic_Data stringForKey:@"message"] withButtonTitle:@"확인"];
////            }
////        }
////    }];
//}
//
//- (UserData *)getUserData
//{
//    return userData;
//}
//
//- (void)setUserData:(NSDictionary *)dic
//{
//    userData.mberId = [dic stringForKey:@"mberId"];
//}

- (void)showMainNavi:(UIWindow *)window
{
    UINavigationController *mainNavi = [kMainBoard instantiateViewControllerWithIdentifier:@"MainTabBar"];

    window.rootViewController = mainNavi;
    [UIView transitionWithView:window
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        window.rootViewController = mainNavi;
                    }completion:nil];
}

- (void)showLoginNavi:(UIWindow *)window
{
    UINavigationController *loginNavi = [kLoginBoard instantiateViewControllerWithIdentifier:@"LoginNavi"];
    window.rootViewController = loginNavi;

}

- (void)logOut
{
    [[UserData sharedData] resetData];
    [self disconnectedSBD];

    [[NSUserDefaults standardUserDefaults] setObject:@(0) forKey:@"IsLogin"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"accessToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[Common sharedData] showLoginNavi:[Util keyWindow]];
    
    [[KOSession sharedSession] logoutAndCloseWithCompletionHandler:^(BOOL success, NSError *error) {
        if (error) {
            NSLog(@"failed to logout. - error: %@", error);
        }
        else {
            NSLog(@"logout succeeded.");
        }
    }];
}

- (void)disconnectedSBD
{
    [SBDMain disconnectWithCompletionHandler:^{
        
    }];
}

- (void)connectedSBD
{
    if( [UserData sharedData].userId <= 0 )  return;
    
    [SBDMain connectWithUserId:[NSString stringWithFormat:@"%ld", [UserData sharedData].userId] completionHandler:^(SBDUser * _Nullable user, SBDError * _Nullable error) {
        if (error != nil)
        {
            
        }
    }];
}

@end
