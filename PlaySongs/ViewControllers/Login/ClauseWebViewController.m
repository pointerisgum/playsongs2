//
//  ClauseWebViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/06.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "ClauseWebViewController.h"
@import WebKit;

@interface ClauseWebViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lb_Title;
@property (weak, nonatomic) IBOutlet WKWebView *webView;

@end

@implementation ClauseWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _lb_Title.text = _str_Title;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.str_Url]];
    [self.webView loadRequest:request];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
