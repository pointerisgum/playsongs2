//
//  ShadowView.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/05.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "ShadowView.h"

@implementation ShadowView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.layer setCornerRadius:8.0f];

    // border
//    [self.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//    [self.layer setBorderWidth:0.5f];

    // drop shadow
    [self.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.layer setShadowOpacity:0.15];
    [self.layer setShadowRadius:2.0];
    [self.layer setShadowOffset:CGSizeMake(0.7, 0.7)];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
