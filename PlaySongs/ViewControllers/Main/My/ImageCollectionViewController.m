//
//  ImageCollectionViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/13.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "ImageCollectionViewController.h"
#import "SearchThumbCell.h"
#import "UICollectionViewLeftAlignedLayout.h"
#import "ODRefreshControl.h"
#import "FeedDetailViewController.h"

#define kCellMargin 1.5
#define kLimitCount 18

@interface ImageCollectionViewController ()
@property (nonatomic, assign) NSInteger nPage;
@property (nonatomic, assign) BOOL isFinish;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, strong) ODRefreshControl *refreshControl;
@property (nonatomic, strong) NSMutableArray *arM_List;
@property (weak, nonatomic) IBOutlet UICollectionView *cv_List;

@end

@implementation ImageCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _nPage = 1;
    _isFinish = NO;
    _isLoading = NO;
    _arM_List = [NSMutableArray array];

    UICollectionViewLeftAlignedLayout *layout = [[UICollectionViewLeftAlignedLayout alloc] init];
    layout.minimumLineSpacing = kCellMargin;
    layout.minimumInteritemSpacing = kCellMargin;
    _cv_List.collectionViewLayout = layout;

    _refreshControl = [[ODRefreshControl alloc] initInScrollView:_cv_List];
    _refreshControl.tintColor = kEnableColor;
    _refreshControl.backgroundColor = [UIColor whiteColor];
    [_refreshControl addTarget:self action:@selector(onRefresh:) forControlEvents:UIControlEventValueChanged];
    [_cv_List addSubview:_refreshControl];

//    [self updateList];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)onRefresh:(ODRefreshControl *)sender
{
    [HapticHelper tap];
    [self refresh];
}

- (void)refresh
{
    _nPage = 1;
    _isFinish = NO;
    _isLoading = NO;
    [_arM_List removeAllObjects];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//        [self.cv_List scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:YES];

        [self updateList];
    });
}

- (void)updateList
{
    _isCalled = YES;
    
    if( _isFinish )     return;
    if( _isLoading )    return;
    
    _isLoading = YES;
    
    __weak __typeof__(self) weakSelf = self;

    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    if( _isMoreMode )
    {
        [dicM_Params setValue:@(_nPage) forKey:@"page"];
        [dicM_Params setValue:@(kLimitCount) forKey:@"limt"];
    }
//    [dicM_Params setObject:_str_TagName forKey:@"tagName"];
//    [dicM_Params setObject:@"new" forKey:@"sort"];

    [dicM_Params addEntriesFromDictionary:_dic_Params];
    [[WebAPI sharedData] callAsyncWebAPIBlock:_str_Path param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
                
        [MBProgressHUD hide];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.refreshControl endRefreshing];
        });

        if( error != nil )
        {
            weakSelf.isLoading = NO;
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            NSArray *ar = [resulte valueForArrayKey:@"result"];
            if( ar.count > 0 )
            {
                [weakSelf.arM_List addObjectsFromArray:ar];
//                if( weakSelf.arM_List.count > 0 )
//                {
//                    [weakSelf.arM_List addObjectsFromArray:ar];
//                }
//                else
//                {
//                    weakSelf.arM_List = [NSMutableArray arrayWithArray:ar];
//                }
            }
            else
            {
                //finish
                weakSelf.isFinish = YES;
            }
        }

        [weakSelf.cv_List reloadData];
        weakSelf.isLoading = NO;
    }];
}

//MARK: CollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _arM_List.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SearchThumbCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchThumbCell" forIndexPath:indexPath];
    
    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return cell;
    
    NSDictionary *dic = _arM_List[indexPath.row];
    NSString *str_ImageUrl = [dic stringForKey:@"coverUrl"];
    [cell.iv_Thumb sd_setImageWithURL:[NSURL URLWithString:str_ImageUrl] placeholderImage:[UIImage imageNamed:@"ic_no_image"]];
    
    CGFloat fCellSize = (collectionView.bounds.size.width - (kCellMargin*2))/3;
    cell.lc_Width.constant = fCellSize;
    cell.lc_Height.constant = fCellSize;

    if( _isMoreMode )
    {
        BOOL lastItemReached = [dic isEqual:[_arM_List lastObject]];
        if (lastItemReached && indexPath.row == [_arM_List count] - 1)
        {
            _nPage++;
            [self updateList];
        }
    }

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat fCellSize = (collectionView.bounds.size.width - (kCellMargin*2))/3;
    return CGSizeMake(fCellSize, fCellSize);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(kCellMargin, 0, kCellMargin, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return;

//    if( _itemSelectedBlock )
//    {
//        _itemSelectedBlock(@{});
//    }

    [HapticHelper tap];
    
    NSDictionary *dic = _arM_List[indexPath.row];
//    NSString *str_PlayType = [dic stringForKey:@"playType"];
    FeedDetailViewController *vc = [kFeedBoard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
//    if( [str_PlayType isEqualToString:@"J"] )
//    {
//        vc.str_PlayId = [NSString stringWithFormat:@"%ld", [dic integerForKey:@"joinPlayId"]];
//    }
//    else
//    {
//        vc.str_PlayId = [NSString stringWithFormat:@"%ld", [dic integerForKey:@"playId"]];
//    }
    vc.str_PlayId = [NSString stringWithFormat:@"%ld", [dic integerForKey:@"playId"]];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
