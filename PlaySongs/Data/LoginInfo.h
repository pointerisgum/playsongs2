//
//  LoginInfo.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/05.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ImageInfo.h"

NS_ASSUME_NONNULL_BEGIN

//@interface ImageInfo : NSObject
//@property (nonatomic, strong) NSString *str_Url;
//@end

@interface LoginInfo : NSObject
@property (nonatomic, assign) NSInteger nUserId;
@property (nonatomic, strong) NSString *str_UserName;
@property (nonatomic, strong) ImageInfo *imageInfo;

- (LoginInfo *)initWithData:(id)data;
@end

NS_ASSUME_NONNULL_END
