//
//  ChatShareCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/17.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatTextCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatShareCell : ChatTextCell
@property (weak, nonatomic) IBOutlet ThumbImageView *iv_Thumb;
@property (weak, nonatomic) IBOutlet UILabel *lb_Name;
@property (weak, nonatomic) IBOutlet UIImageView *iv_Contents;
@property (weak, nonatomic) IBOutlet UILabel *lb_Tag;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_ContentsHeight;

@end

NS_ASSUME_NONNULL_END
