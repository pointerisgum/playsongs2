//
//  ProfileInputViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/06.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "ProfileInputViewController.h"
#import "PlaySongs-Swift.h"
@import ActionSheetPicker_3_0;
#import "ActionSheetPicker.h"
#import "EmailAccView.h"
#import "JoinUserInfo.h"
#import "ClausePopUpViewController.h"

static NSInteger kMaxLength = 10;
static NSInteger kMinLength = 1;

@interface ProfileInputViewController ()

@property (nonatomic, strong) NSMutableArray *arM_Year;
@property (nonatomic, strong) NSMutableArray *arM_Month;
@property (nonatomic, strong) NSMutableArray *arM_Day;
@property (nonatomic, strong) EmailAccView *v_AccNick;
@property (nonatomic, strong) EmailAccView *v_AccBaby;
@property (nonatomic, strong) NSString *str_ImageUrl;

@property (weak, nonatomic) IBOutlet UIScrollView *sv_Main;

//프로필
@property (weak, nonatomic) IBOutlet UIView *v_Profile;
@property (weak, nonatomic) IBOutlet UIImageView *iv_Profile;

//이름 or 닉넴
@property (weak, nonatomic) IBOutlet UIView *v_NickName;
@property (weak, nonatomic) IBOutlet UITextField *tf_NickName;
@property (weak, nonatomic) IBOutlet UIButton *btn_NickNameStatus;

//아이 이름
@property (weak, nonatomic) IBOutlet UIView *v_BabyName;
@property (weak, nonatomic) IBOutlet UITextField *tf_BabyName;
@property (weak, nonatomic) IBOutlet UIButton *btn_BabyStatus;

//성별
@property (weak, nonatomic) IBOutlet UIView *v_Sex;
@property (weak, nonatomic) IBOutlet UIButton *btn_Girl;
@property (weak, nonatomic) IBOutlet UIButton *btn_Boy;

//월령
@property (weak, nonatomic) IBOutlet UIView *v_Year;
@property (weak, nonatomic) IBOutlet UITextField *tf_Year;
@property (weak, nonatomic) IBOutlet UIView *v_Month;
@property (weak, nonatomic) IBOutlet UITextField *tf_Month;
@property (weak, nonatomic) IBOutlet UIView *v_Day;
@property (weak, nonatomic) IBOutlet UITextField *tf_Day;


@property (weak, nonatomic) IBOutlet UIView *v_BottomButton;
@property (weak, nonatomic) IBOutlet UIButton *btn_Next;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_BottomButtonHeight;

@end

@implementation ProfileInputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _btn_Next.enabled = NO;
    _lc_BottomButtonHeight.constant = [Util keyWindow].safeAreaInsets.bottom + 50;

    _v_Profile.layer.cornerRadius = _v_Profile.frame.size.width/2;
    _v_Profile.layer.borderWidth = 0.5f;
    _v_Profile.layer.borderColor = kEnableBoarderColor.CGColor;

    [self setBgRounding:_v_NickName];
    [self setBgRounding:_v_BabyName];
    [self setBgRounding:_v_Year];
    [self setBgRounding:_v_Month];
    [self setBgRounding:_v_Day];
    
    _v_Sex.layer.cornerRadius = 8.0f;
    _v_Sex.layer.borderWidth = 0.5f;
    _v_Sex.layer.borderColor = [UIColor colorWithHexString:@"FE540E"].CGColor;

    _v_AccNick = [[NSBundle mainBundle]loadNibNamed:@"EmailAccView" owner:self options:nil].firstObject;
    [_v_AccNick.btn_Next setTitle:@"다음" forState:UIControlStateNormal];
    _tf_NickName.inputAccessoryView = _v_AccNick;
    [_v_AccNick.btn_Next addTarget:self action:@selector(onNickNext) forControlEvents:UIControlEventTouchUpInside];

    _v_AccBaby = [[NSBundle mainBundle]loadNibNamed:@"EmailAccView" owner:self options:nil].firstObject;
    [_v_AccBaby.btn_Next setTitle:@"다음" forState:UIControlStateNormal];
    _tf_BabyName.inputAccessoryView = _v_AccBaby;
    [_v_AccBaby.btn_Next addTarget:self action:@selector(onBabyNext) forControlEvents:UIControlEventTouchUpInside];

    [self setDates];
    
#ifdef DEBUG
    self.tf_NickName.text = @"김영민";
    self.tf_BabyName.text = @"김준우";
    self.tf_Year.text = @"2015";
    self.tf_Month.text = @"6";
    self.tf_Day.text = @"22";
#endif
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MBProgressHUD hide];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if( [segue.identifier isEqualToString:@"ClausePopUpViewController"] )
    {
        JoinUserInfo *joinUserInfo = [[JoinUserInfo alloc] init];
        joinUserInfo.name = _tf_NickName.text;
        joinUserInfo.email = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserEmail"];
        joinUserInfo.password = [[NSUserDefaults standardUserDefaults] objectForKey:@"TempPw"];
        joinUserInfo.imgUrl = _str_ImageUrl;
        joinUserInfo.regType = @"email";
        joinUserInfo.deviceType = [Util deviceName];
        joinUserInfo.childName = _tf_BabyName.text;
        joinUserInfo.childGender = _btn_Girl.selected ? @"girl" : @"boy";
        joinUserInfo.childBirth = [NSString stringWithFormat:@"%04ld-%02ld-%02ld", [_tf_Year.text integerValue], [_tf_Month.text integerValue], [_tf_Day.text integerValue]];
        joinUserInfo.image = _iv_Profile.image;
        
        ClausePopUpViewController *vc = segue.destinationViewController;
        vc.joinUserInfo = joinUserInfo;
    }
}


//MARK: Custom
- (void)setBgRounding:(UIView *)view
{
    view.layer.cornerRadius = 8.0f;
    view.layer.borderWidth = 0.5f;
    view.layer.borderColor = kEnableBoarderColor.CGColor;
}

- (void)onNickNext
{
    if( [self checkNext] )
    {
        [self next];
    }
    else
    {
        [HapticHelper tap];
        [_tf_BabyName becomeFirstResponder];
    }
}

- (void)onBabyNext
{
    if( [self checkNext] )
    {
        [self next];
    }
    else
    {
        [HapticHelper tap];
        [self.view endEditing:YES];
    }
}

- (BOOL)checkNext
{
    if( _tf_NickName.text.length >= kMinLength &&
       _tf_BabyName.text.length >= kMinLength &&
       (self.btn_Girl.selected || self.btn_Boy.selected) &&
       _tf_Year.text.length > 0 &&
       _tf_Month.text.length > 0 &&
       _tf_Day.text.length > 0 )
    {
        _v_BottomButton.backgroundColor = kEnableColor;
        _btn_Next.enabled = YES;
        return YES;
    }
    else
    {
        _v_BottomButton.backgroundColor = kDisableColor;
        _btn_Next.enabled = NO;
    }
    
    return NO;
}

- (void)setDates
{
    NSDate *date = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
    NSInteger nYear = [components year];

    self.arM_Year = [NSMutableArray array];
    for( NSInteger i = nYear; i > nYear - 100; i-- )
    {
        [self.arM_Year addObject:[NSString stringWithFormat:@"%ld년", i]];
    }

    self.arM_Month = [NSMutableArray array];
    for( NSInteger i = 1; i <= 12; i++ )
    {
        [self.arM_Month addObject:[NSString stringWithFormat:@"%ld월", i]];
    }
    
    self.arM_Day = [NSMutableArray array];
    NSDate *date2 = [calendar dateFromComponents:components];
    NSRange range = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date2];
    for( NSInteger i = range.location; i <= range.length; i++ )
    {
        [self.arM_Day addObject:[NSString stringWithFormat:@"%ld일", i]];
    }
}

- (void)animalWasSelected:(NSNumber *)selectedIndex element:(id)element
{
    NSInteger idx = [selectedIndex integerValue];
    if( element == _tf_Year )
    {
        if( IsArrayOutOfBounds(self.arM_Year, idx) )    return;

        NSString *str = self.arM_Year[idx];
        str = [[str componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        _tf_Year.text = str;

    }
    else if( element == _tf_Month )
    {
        if( IsArrayOutOfBounds(self.arM_Month, idx) )    return;
        
        NSString *str = self.arM_Month[idx];
        str = [[str componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        _tf_Month.text = str;
    }
    else if( element == _tf_Day )
    {
        if( IsArrayOutOfBounds(self.arM_Day, idx) )    return;

        NSString *str = self.arM_Day[idx];
        str = [[str componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        _tf_Day.text = str;
    }
    
    [self checkNext];
}



//MARK: Noti
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    self.sv_Main.contentInset = UIEdgeInsetsMake(self.sv_Main.contentInset.top, self.sv_Main.contentInset.left, keyboardBounds.size.height, self.sv_Main.contentInset.right);
    
    _lc_BottomButtonHeight.constant = 0;
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    self.sv_Main.contentInset = UIEdgeInsetsZero;
    
    self.lc_BottomButtonHeight.constant = [Util keyWindow].safeAreaInsets.bottom + 50;
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}


//MARK: UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if( textField == self.tf_NickName )
    {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        if( newLength > kMaxLength )
        {
            NSString *str_Msg = [NSString stringWithFormat:@"최대 %ld자까지 입력 할 수 있습니다.", kMaxLength];
            [Util showToast:str_Msg];
            [self.view endEditing:YES];
            return NO;
        }
        
        self.btn_NickNameStatus.hidden = NO;

        if( newLength >= kMinLength )
        {
            self.btn_NickNameStatus.selected = YES;
            self.v_NickName.layer.borderColor = kEnableBoarderColor.CGColor;
            self.v_AccNick.btn_Next.backgroundColor = kEnableColor;
            self.v_AccNick.btn_Next.enabled = YES;
        }
        else
        {
            self.btn_NickNameStatus.selected = NO;
            self.v_NickName.layer.borderColor = kErrorColor.CGColor;
            self.v_AccNick.btn_Next.backgroundColor = kDisableColor;
            self.v_AccNick.btn_Next.enabled = NO;
        }
    }
    else if( textField == self.tf_BabyName )
    {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        if( newLength > kMaxLength )
        {
            NSString *str_Msg = [NSString stringWithFormat:@"최대 %ld자까지 입력 할 수 있습니다.", kMaxLength];
            [Util showToast:str_Msg];
            [self.view endEditing:YES];
            return NO;
        }
        
        self.btn_BabyStatus.hidden = NO;

        if( newLength >= kMinLength )
        {
            self.btn_BabyStatus.selected = YES;
            self.v_BabyName.layer.borderColor = kEnableBoarderColor.CGColor;
            self.v_AccBaby.btn_Next.backgroundColor = kEnableColor;
            self.v_AccBaby.btn_Next.enabled = YES;
        }
        else
        {
            self.btn_BabyStatus.selected = NO;
            self.v_BabyName.layer.borderColor = kErrorColor.CGColor;
            self.v_AccBaby.btn_Next.backgroundColor = kDisableColor;
            self.v_AccBaby.btn_Next.enabled = NO;
        }
    }

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self checkNext];
    });
    
    return YES;
}

- (UIView *)inputAccessoryView
{
    return self.v_AccBaby;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if( textField == _tf_Year )
    {
        [self.view endEditing:YES];
        [ActionSheetStringPicker showPickerWithTitle:@"생년" rows:self.arM_Year initialSelection:0 target:self successAction:@selector(animalWasSelected:element:) cancelAction:nil origin:textField];
        return NO;
    }
    else if( textField == _tf_Month )
    {
        [self.view endEditing:YES];
        [ActionSheetStringPicker showPickerWithTitle:@"월" rows:self.arM_Month initialSelection:0 target:self successAction:@selector(animalWasSelected:element:) cancelAction:nil origin:textField];
        return NO;
    }
    else if( textField == _tf_Day )
    {
        [self.view endEditing:YES];
        [ActionSheetStringPicker showPickerWithTitle:@"일" rows:self.arM_Day initialSelection:0 target:self successAction:@selector(animalWasSelected:element:) cancelAction:nil origin:textField];
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
//    [self onNext];
    return YES;
}

//MARK: Action
- (IBAction)goPicture:(id)sender
{
    [HapticHelper tap];
    
    __weak __typeof(&*self)weakSelf = self;

    InstarPicker *instarPicker = [[InstarPicker alloc] init];
    [instarPicker setCompletionHandler:^(UIImage * _Nonnull image) {
        
        weakSelf.iv_Profile.image = image;
    }];
    
    YPImagePicker *pickerNavi = [instarPicker getProfilePhoto];
    [self presentViewController:(UINavigationController *)pickerNavi animated:YES completion:nil];
}

- (IBAction)goTapGesture:(id)sender
{
    [HapticHelper tap];
    [self.view endEditing:YES];
}

- (IBAction)goGirl:(id)sender
{
    [HapticHelper tap];
    
    self.btn_Girl.selected = YES;
    self.btn_Boy.selected = NO;
    
    self.btn_Girl.backgroundColor = kEnableColor;
    self.btn_Boy.backgroundColor = [UIColor whiteColor];
    
    [self checkNext];
}

- (IBAction)goBoy:(id)sender
{
    [HapticHelper tap];
    
    self.btn_Girl.selected = NO;
    self.btn_Boy.selected = YES;
    
    self.btn_Girl.backgroundColor = [UIColor whiteColor];
    self.btn_Boy.backgroundColor = kEnableColor;
    
    [self checkNext];
}

- (void)next
{
    [self.view endEditing:YES];

    [HapticHelper tap];
    
    [self performSegueWithIdentifier:@"ClausePopUpViewController" sender:nil];
}

- (IBAction)goNext:(id)sender
{
    [self next];
}

@end
