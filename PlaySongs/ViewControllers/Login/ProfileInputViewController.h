//
//  ProfileInputViewController.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/06.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProfileInputViewController : UIViewController
- (void)imageSelected:(UIImage *)image;
@end

NS_ASSUME_NONNULL_END
