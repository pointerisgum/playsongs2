//
//  JoinerCell.m
//  PlaySongs
//
//  Created by 김영민 on 2020/09/02.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "JoinerCell.h"
#import "UICollectionViewLeftAlignedLayout.h"

#define kCellMargin 1.5

@implementation JoinerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UICollectionViewLeftAlignedLayout *layout = [[UICollectionViewLeftAlignedLayout alloc] init];
    layout.minimumLineSpacing = kCellMargin;
    layout.minimumInteritemSpacing = kCellMargin;
    self.cv_List.collectionViewLayout = layout;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
