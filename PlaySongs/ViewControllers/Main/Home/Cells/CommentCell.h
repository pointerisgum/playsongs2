//
//  CommentCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/11.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iv_Thumb;
@property (weak, nonatomic) IBOutlet UILabel *lb_Contents;
@property (weak, nonatomic) IBOutlet UILabel *lb_Time;

@end

NS_ASSUME_NONNULL_END
