//
//  TopMainTVCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/07.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^CompletionBlock)(id completeResult);

@interface TopMainTVCell : UITableViewCell
@property (nonatomic, strong) NSArray *ar_List;
@property (weak, nonatomic) IBOutlet UICollectionView *cv_List;
@property (nonatomic, copy) CompletionBlock completionBlock;
- (void)setCompletionBlock:(CompletionBlock)completionBlock;
- (void)initTopCell:(NSArray *)ar;
@end

NS_ASSUME_NONNULL_END
