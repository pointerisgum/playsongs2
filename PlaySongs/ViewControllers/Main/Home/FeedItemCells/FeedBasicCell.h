//
//  FeedBasicCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/23.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FeedBasicCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lb_Contents;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_Top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_Bottom;

@end

NS_ASSUME_NONNULL_END
