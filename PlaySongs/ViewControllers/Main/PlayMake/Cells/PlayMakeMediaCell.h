//
//  PlayMakeMediaCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/29.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PlayMakeMediaCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIImageView *iv_Thumb;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_ImageHeight;
@end

NS_ASSUME_NONNULL_END
