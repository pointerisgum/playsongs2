//
//  TagCvCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/13.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TagCvCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *v_Bg;
@property (weak, nonatomic) IBOutlet UILabel *lb_Title;

@end

NS_ASSUME_NONNULL_END
