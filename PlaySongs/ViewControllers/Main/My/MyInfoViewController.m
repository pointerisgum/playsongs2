//
//  MyInfoViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/09/03.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "MyInfoViewController.h"
#import "MyInfoCell.h"

typedef enum : NSUInteger {
    kProfile,
    kBabyInfo,
    kPassword,
    kPayment,
    kAlram,
    kHelp,
} Type;

@interface MyInfoViewController ()
@property (nonatomic, strong) NSMutableArray *arM_List;
@property (weak, nonatomic) IBOutlet ThumbImageView *iv_Thumb;
@property (weak, nonatomic) IBOutlet UILabel *lb_Name;
@property (weak, nonatomic) IBOutlet UILabel *lb_Bio;
@property (weak, nonatomic) IBOutlet UITableView *tbv_List;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_TbvHeight;

@end

@implementation MyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _arM_List = [NSMutableArray array];
    [_arM_List addObject:@{@"icon":@"", @"title":@"프로필", @"type":@(kProfile)}];
    [_arM_List addObject:@{@"icon":@"", @"title":@"아이정보", @"type":@(kBabyInfo)}];
    [_arM_List addObject:@{@"icon":@"", @"title":@"비밀번호", @"type":@(kPassword)}];
    [_arM_List addObject:@{@"icon":@"", @"title":@"결제정보", @"type":@(kPayment)}];
    [_arM_List addObject:@{@"icon":@"", @"title":@"알림, 설정", @"type":@(kAlram)}];
    [_arM_List addObject:@{@"icon":@"", @"title":@"도움말, PLAYSONGS 정보", @"type":@(kHelp)}];
    [_tbv_List reloadData];
    _lc_TbvHeight.constant = (_arM_List.count + 1) * 50;

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [_iv_Thumb sd_setImageWithURL:[NSURL URLWithString:[UserData sharedData].imgUrl] placeholderImage:[UIImage imageNamed:@"ic_no_user"]];
    _lb_Name.text = [UserData sharedData].name;
    _lb_Bio.text = [UserData sharedData].bio;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//MARK: TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arM_List.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyInfoCell"];

    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return cell;
    
    NSDictionary *dic = _arM_List[indexPath.row];
    NSString *str_Title = [dic stringForKey:@"title"];
    cell.lb_Title.text = str_Title;
    cell.iv_Icon.image = [UIImage imageNamed:[dic stringForKey:@"icon"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    [HapticHelper tap];

    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return;

    NSDictionary *dic = _arM_List[indexPath.row];
    Type type = [dic integerForKey:@"type"];
    switch (type) {
        case kProfile:
            [self performSegueWithIdentifier:@"EditProfileSegue" sender:nil];
            break;
        case kBabyInfo:
            [self performSegueWithIdentifier:@"EditBabyInfoSegue" sender:nil];
            break;
        case kPassword:
            [self performSegueWithIdentifier:@"EditPasswordSegue" sender:nil];
            break;
        case kPayment:
            break;
        case kAlram:
            break;
        case kHelp:
            break;
    }
}



- (IBAction)goLogOut:(id)sender
{
    [UIAlertController showAlertInViewController:self
                                       withTitle:@""
                                         message:@"로그아웃 하시겠습니까?"
                               cancelButtonTitle:@"아니요"
                          destructiveButtonTitle:nil
                               otherButtonTitles:@[@"예"]
                                        tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                            
                                            if (buttonIndex >= controller.firstOtherButtonIndex)
                                            {
                                                [[Common sharedData] logOut];
                                            }
                                        }];

}

@end
