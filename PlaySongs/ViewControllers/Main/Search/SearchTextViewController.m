//
//  SearchTextViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/13.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "SearchTextViewController.h"
#import "SearchTextCell.h"
#import "TagPageViewController.h"
#import "UserPageViewController.h"

#define kMaxSaveRecent 20

@interface SearchTextViewController () <UITextFieldDelegate>
@property (nonatomic, assign) BOOL isRecent;
@property (nonatomic, strong) NSMutableArray *arM_List;
@property (nonatomic, strong) NSMutableArray *arM_Recent;
@property (weak, nonatomic) IBOutlet UIView *v_SearchBg;
@property (weak, nonatomic) IBOutlet UITextField *tf_Search;
@property (weak, nonatomic) IBOutlet UITableView *tbv_List;

@end

@implementation SearchTextViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _v_SearchBg.clipsToBounds = YES;
    _v_SearchBg.layer.cornerRadius = 6.0f;
    
    [_tf_Search becomeFirstResponder];
    
    [_tbv_List reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)updateSearch
{
    __weak __typeof__(self) weakSelf = self;

    NSString *str_SearchKeyword = _tf_Search.text;
    if( [str_SearchKeyword hasPrefix:@"#"] )
    {
        str_SearchKeyword = [str_SearchKeyword stringByReplacingOccurrencesOfString:@"#" withString:@""];
    }
    
    NSLog(@"search key word : %@", str_SearchKeyword);
    
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:str_SearchKeyword forKey:@"q"];
    
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"search/keyword" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        if( resulte != nil )
        {
            NSArray *ar = [NSArray arrayWithArray:resulte];
            if( ar.count > 0 )
            {
                weakSelf.arM_List = [NSMutableArray arrayWithArray:ar];
            }
        }

        [weakSelf.tbv_List reloadData];
    }];

}


//MARK: UITextFieldDelegate
- (void)textFieldDidChangeSelection:(UITextField *)textField
{
    NSLog(@"2 : %@", textField.text);
    [self updateSearch];
}


//MARK: TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if( _arM_List.count > 0 )
    {
        _isRecent = NO;
        return 1;
    }
    
    _arM_Recent = [[NSUserDefaults standardUserDefaults] objectForKey:@"SearchList"];
    if( _arM_Recent.count > 0 )
    {
        _isRecent = YES;
        return 2;
    }
    
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if( _arM_List.count > 0 )
    {
        return _arM_List.count;
    }
    
    _arM_Recent = [[NSUserDefaults standardUserDefaults] objectForKey:@"SearchList"];
    if( _arM_Recent.count > 0 )
    {
        if( section == 0 )
        {
            return 1;
        }
        return _arM_Recent.count;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SearchTextCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchTextCell"];
    cell.userInteractionEnabled = YES;
    cell.lb_Title.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    cell.iv_Thumb.hidden = NO;
    
    NSDictionary *dic = nil;
    if( _arM_List.count > 0 )
    {
        if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return cell;
        
        dic = _arM_List[indexPath.row];
    }
    else
    {
        //최근검색일 경우
        if( indexPath.section == 0 )
        {
            cell.lb_Title.text = @"최근검색";
            cell.iv_Thumb.hidden = YES;
            cell.lb_Title.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
            cell.userInteractionEnabled = NO;
            return cell;
        }
        else
        {
            dic = _arM_Recent[indexPath.row];
        }
    }
    
    cell.lb_Title.text = [dic stringForKey:@"name"];
    
    NSString *str_ImageUrl = [dic stringForKey:@"imgUrl"];
    [cell.iv_Thumb sd_setImageWithURL:[NSURL URLWithString:str_ImageUrl] placeholderImage:[UIImage imageNamed:@"ic_no_user"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if( _isRecent == NO )
    {
        NSDictionary *dic = _arM_List[indexPath.row];
        
        NSArray *ar = [[NSUserDefaults standardUserDefaults] objectForKey:@"SearchList"];
        if( ar == nil || ar.count <= 0 )
        {
            ar = [NSArray array];
        }
        
        NSMutableArray *arM = [NSMutableArray arrayWithArray:ar];
        if( [arM containsObject:dic] == NO )
        {
            [arM insertObject:dic atIndex:0];
            
            if( arM.count > kMaxSaveRecent )
            {
                [arM removeObjectsInRange:NSMakeRange(kMaxSaveRecent-1, arM.count - kMaxSaveRecent)];
            }
            [[NSUserDefaults standardUserDefaults] setObject:arM forKey:@"SearchList"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }

    NSDictionary *dic = nil;
    if( _isRecent )
    {
        dic = _arM_Recent[indexPath.row];
    }
    else
    {
        dic = _arM_List[indexPath.row];
    }
    
    NSString *str_Type = [dic stringForKey:@"type"];
    if( [str_Type isEqualToString:@"tag"] )
    {
        TagPageViewController *vc = [kUserBoard instantiateViewControllerWithIdentifier:@"TagPageViewController"];
        vc.str_TagName = [dic stringForKey:@"name"];
        vc.nTagId = [dic integerForKey:@"id"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if( [str_Type isEqualToString:@"user"] )
    {
        UserPageViewController *vc = [kUserBoard instantiateViewControllerWithIdentifier:@"UserPageViewController"];
        vc.dic_Owner = dic;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

//MARK: Action
- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

@end
