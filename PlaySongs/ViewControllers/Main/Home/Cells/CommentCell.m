//
//  CommentCell.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/11.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "CommentCell.h"

@implementation CommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _iv_Thumb.userInteractionEnabled = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
