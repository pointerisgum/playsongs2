//
//  NSObject+Thoting.h
//  ThoThing
//
//  Created by macpro15 on 2017. 12. 20..
//  Copyright © 2017년 youngmin.kim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (PlaySongs)

- (BOOL)isStringEmpty;
- (BOOL)isDictionaryEmpty;
- (BOOL)isArrayEmpty;
- (BOOL)isArrayOutOfBounds:(NSInteger)row;

@end
