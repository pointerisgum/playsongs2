//
//  FollowListContainerViewController.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/16.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ODRefreshControl.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^FollowingChangeBlock)(id completeResult);
typedef void (^RefreshBlock)(void);

@interface FollowListContainerViewController : UIViewController
@property (nonatomic, copy) FollowingChangeBlock followingChangeBlock;
@property (nonatomic, copy) RefreshBlock refreshBlock;
@property (nonatomic, assign) BOOL isMy;
@property (nonatomic, strong) ODRefreshControl *refreshControl;
@property (nonatomic, strong) NSString *str_Path;
@property (nonatomic, strong) NSDictionary *dic_Params;
@property (nonatomic, strong) NSMutableArray *arM_List;
@property (weak, nonatomic) IBOutlet UITableView *tbv_List;
- (void)setFollowingChangeBlock:(FollowingChangeBlock)followingChangeBlock;
- (void)setRefreshBlock:(RefreshBlock)refreshBlock;
//- (void)updateList;
@end

NS_ASSUME_NONNULL_END
