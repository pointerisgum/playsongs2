//
//  MainTabBarController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/07.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "MainTabBarController.h"
#import "MakePlayViewController.h"
#import "PlaySongs-Swift.h"
#import <Photos/Photos.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "UserPageViewController.h"

@interface MainTabBarController () <UITabBarControllerDelegate>
@property (nonatomic, strong) AVPlayer *avPlayer;
@end

@implementation MainTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.delegate = self;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    if( [viewController isKindOfClass:[UINavigationController class]] )
    {
        UINavigationController *navi = (UINavigationController *)viewController;
        if( [navi.viewControllers.firstObject isKindOfClass:[UserPageViewController class]] )
        {
            UserPageViewController *vc = (UserPageViewController *)navi.viewControllers.firstObject;
            vc.dic_Owner = [UserData sharedData].dic_My;
            vc.isHiddenBackButton = YES;
        }
        else if( [navi.viewControllers.firstObject isKindOfClass:[MakePlayViewController class]] )
        {
            UINavigationController *navi = [kPlayMakeBoard instantiateViewControllerWithIdentifier:@"PlayMakeNavi"];
            [self presentViewController:navi animated:YES completion:^{
                
            }];
            
            return NO;
        }
    }
//    else if( [viewController isKindOfClass:[MakePlayViewController class]] )
//    {
////        [self presentViewController:(UINavigationController *)pickerNavi animated:YES completion:nil];
////        __weak __typeof(&*self)weakSelf = self;
////
////        InstarPicker *instarPicker = [[InstarPicker alloc] init];
////        [instarPicker setMultiCompletionHandler:^(NSArray * _Nonnull array) {
////
////            for( NSDictionary *dic in array )
////            {
////                NSString *str_Type = [dic stringForKey:@"type"];
////                if( [str_Type isEqualToString:@"video"] )
////                {
////                    UIImage *image = (UIImage *)[dic stringForKey:@"thumb"];
////                    PHAsset *asset = [dic stringForKey:@"asset"];
////                    NSString *str_VideoUrl = [dic stringForKey:@"url"];
////                    NSLog(@"str_VideoUrl : %@", str_VideoUrl);
////
////                    [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset* avasset, AVAudioMix* audioMix, NSDictionary* info){
////                        AVURLAsset* myAsset = (AVURLAsset*)avasset;
////                        NSData * data = [NSData dataWithContentsOfFile:myAsset.URL.relativePath];
////                        if (data)
////                        {
////
////                        }
////                    }];
////                }
////            }
////        }];
////
////        YPImagePicker *pickerNavi = [instarPicker getMultiPhotoWithCount:5];
////        [self presentViewController:(UINavigationController *)pickerNavi animated:YES completion:nil];
//
//        return NO;
//    }
    
    //    NSURL *url = [[NSURL alloc] initWithString:@"https://www.radiantmediaplayer.com/media/big-buck-bunny-360p.mp4"]; // url can be remote or local
    //
    //    AVPlayer *player = [AVPlayer playerWithURL:url];
    //    // create a player view controller
    //
    //    AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
    //    [self presentViewController:controller animated:YES completion:nil];
    //    controller.player = player;
    //    [player play];
    
    
    
    
    
    
    
    
    
    
    //    UINavigationController *navi = (UINavigationController *)viewController;
    //    UIViewController *vc = navi.visibleViewController;
    //
    //    AVPlayer *player = [AVPlayer playerWithURL:[NSURL URLWithString:@"https://www.youtube.com/watch?v=8gp4TPF-JI8"]];
    //    AVPlayerLayer *playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
    //    playerLayer.frame = self.view.bounds;
    //    [vc.view.layer addSublayer:playerLayer];
    //    [player play];
    
    return YES;
}

@end


/*
 NSURL *url = [NSURL URLWithString:@"file:///private/var/mobile/Containers/Data/Application/DFBA7978-D4A5-40E9-B5B6-21AD5D95CCAB/tmp/0143679D-2523-4014-8BD4-CDD65472F7A8.mov"];
 AVPlayer *player = [AVPlayer playerWithURL:url];
 AVPlayerLayer *playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
 playerLayer.frame = self.view.bounds;
 [self.view.layer addSublayer:playerLayer];
 [player play];
 */
