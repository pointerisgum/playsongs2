//
//  FeedSubTextMoreCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/23.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlaySongs-Swift.h"
#import "FeedBasicCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FeedSubTextMoreCell : FeedBasicCell
@property (weak, nonatomic) IBOutlet ExpandableLabel *lb_MoreContents;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_ContentsHeight;
@end

NS_ASSUME_NONNULL_END
