//
//  FeedSubJoinCell.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/08.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "FeedSubJoinCell.h"

@implementation FeedSubJoinCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _iv_Thumb.layer.cornerRadius = 2;
//    _iv_Thumb.layer.borderWidth = 0.5f;
//    _iv_Thumb.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
