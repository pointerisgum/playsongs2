//
//  DetailVideoCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/17.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import "DetailImageCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface DetailVideoCell : DetailImageCell
@property (nonatomic, strong) AVPlayerViewController *playerController;
@property (weak, nonatomic) IBOutlet UIView *v_ContentsBg;
@property (weak, nonatomic) IBOutlet UIButton *btn_Play;
@end

NS_ASSUME_NONNULL_END
