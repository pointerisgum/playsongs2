//
//  UIViewController+Extension.h
//  SongDo
//
//  Created by 김영민 on 2020/02/10.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (Extension)

- (IBAction)goBack:(id)sender;

@end

NS_ASSUME_NONNULL_END
