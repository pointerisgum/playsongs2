//
//  EmailAccView.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/05.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EmailAccView : UIView
@property (weak, nonatomic) IBOutlet UIButton *btn_Next;
@end

NS_ASSUME_NONNULL_END
