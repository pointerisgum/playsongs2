//
//  MainTopCell.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/07.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "MainTopCell.h"

@implementation MainTopCell
- (void)awakeFromNib
{
    [super awakeFromNib];
    _iv_Thumb.layer.cornerRadius = 8;
    _iv_Thumb.layer.borderWidth = 0.5;
    _iv_Thumb.layer.borderColor = [UIColor colorWithHexString:@"dbdbdb"].CGColor;
    _iv_Thumb.clipsToBounds = YES;
    
}
@end
