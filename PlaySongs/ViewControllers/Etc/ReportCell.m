//
//  ReportCell.m
//  PlaySongs
//
//  Created by 김영민 on 2020/09/02.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "ReportCell.h"

@implementation ReportCell

- (void)awakeFromNib {
    [super awakeFromNib];
    /*
     case "Stroke":
         self = .stroke
     case "Fill":
         self = .fill
     case "BounceStroke":
         self = .bounce(.stroke)
     case "BounceFill":
         self = .bounce(.fill)
     case "ExpandStroke":
         self = .expand(.stroke)
     case "ExpandFill":
         self = .expand(.fill)
     case "FlatStroke":
         self = .flat(.stroke)
     case "FlatFill":
         self = .flat(.fill)
     case "Spiral":
         self = .spiral
     case "FadeStroke":
         self = .fade(.stroke)
     case "FadeFill":
         self = .fade(.fill)
     case "DotStroke":
         self = .dot(.stroke)
     case "DotFill":
         self = .dot(.fill)
     */
    [self.check set_IBStateChangeAnimation:@"BounceFill"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
