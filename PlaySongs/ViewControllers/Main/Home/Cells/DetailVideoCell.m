//
//  DetailVideoCell.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/17.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "DetailVideoCell.h"

@implementation DetailVideoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _playerController = [[AVPlayerViewController alloc] init];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
