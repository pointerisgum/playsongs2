//
//  EmailLoginPwViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/05.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "EmailLoginPwViewController.h"
#import "NimbusKitAttributedLabel.h"
//#import "NSMutableAttributedString+NimbusKitAttributedLabel.h"
#import "EmailAccView.h"
#import "FindPwViewController.h"

@interface EmailLoginPwViewController ()
@property (weak, nonatomic) IBOutlet NIAttributedLabel *lb_ForgotPw;
@property (nonatomic, strong) EmailAccView *v_AccView;
@property (weak, nonatomic) IBOutlet UITextField *tf_Pw;
@property (weak, nonatomic) IBOutlet UIView *v_Pw;

@end

@implementation EmailLoginPwViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _v_Pw.layer.cornerRadius = 8.0f;
    _v_Pw.layer.borderWidth = 0.5f;
    _v_Pw.layer.borderColor = kEnableBoarderColor.CGColor;

    _lb_ForgotPw.underlineStyle = kCTUnderlineStyleSingle;
    _lb_ForgotPw.underlineStyle = kCTUnderlineStyleSingle;
    _lb_ForgotPw.strokeColor = [UIColor redColor];
    
    _v_AccView = [[NSBundle mainBundle]loadNibNamed:@"EmailAccView" owner:self options:nil].firstObject;
    [_v_AccView.btn_Next setTitle:@"로그인" forState:UIControlStateNormal];
    _tf_Pw.inputAccessoryView = _v_AccView;
    [_v_AccView.btn_Next addTarget:self action:@selector(onNext) forControlEvents:UIControlEventTouchUpInside];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tf_Pw becomeFirstResponder];
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if( [segue.identifier isEqualToString:@"FindPwViewController"] )
    {
        FindPwViewController *vc = (FindPwViewController *) segue.destinationViewController;
        vc.str_Email = self.str_Email;
    }
}




//MARK: UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        if( textField.text.length >= 4 )
        {
            self.v_AccView.btn_Next.backgroundColor = kEnableColor;
            self.v_AccView.btn_Next.enabled = YES;
        }
        else
        {
            self.v_AccView.btn_Next.backgroundColor = kDisableColor;
            self.v_AccView.btn_Next.enabled = NO;
        }
    });
    
//        //글자제한이 있는 경우 백스페이스바 예외처리
//        if( self.nMaxCount > 0 )
//        {
//            const char * _char = [string cStringUsingEncoding:NSUTF8StringEncoding];
//            int isBackSpace = strcmp(_char, "\b");
//            if (isBackSpace != -8)
//            {
//                //이건 바이트인데 기획에선 자릿수로 체크하길 원했음
//    //            NSUInteger bytes = [textField.text lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
//                NSUInteger bytes = [textField.text length];
//                if( bytes > self.nMaxCount - 1 )
//                {
//                    return NO;
//                }
//            }
//        }

    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self onNext];
    return YES;
}

//MARK: Action
- (IBAction)goFindPw:(id)sender
{
    [HapticHelper tap];
    
}

- (void)onNext
{
    if( _tf_Pw.text.length < 4 ) return;
    
    [HapticHelper tap];

    [[NSUserDefaults standardUserDefaults] setObject:_tf_Pw.text forKey:@"UserPw"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setValue:_str_Email forKey:@"email"];
    [dicM_Params setValue:_tf_Pw.text forKey:@"password"];

    [[WebAPI sharedData] callAsyncWebAPIBlock:@"auth/login" param:dicM_Params withMethod:@"POST" withBlock:^(id resulte, NSError *error) {
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            NSDictionary *dic_Result = [resulte dictionaryForKey:@"result"];
            NSString *str_AccToken = [resulte stringForKey:@"accessToken"];
            if( str_AccToken.length > 0 )
            {
                [[NSUserDefaults standardUserDefaults] setObject:str_AccToken forKey:@"accessToken"];
            }

            [[NSUserDefaults standardUserDefaults] setObject:@(1) forKey:@"IsLogin"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSInteger nUserId = [dic_Result integerForKey:@"userId"];
            [[Common sharedData] connectedSBD];

            [[UserData sharedData] initData:dic_Result];
            [[Common sharedData] showMainNavi:[Util keyWindow]];
        }
    }];

}

@end
