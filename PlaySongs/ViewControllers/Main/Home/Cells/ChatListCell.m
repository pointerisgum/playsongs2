//
//  ChatListCell.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/17.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "ChatListCell.h"

@implementation ChatListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _v_Badge.layer.cornerRadius = _v_Badge.frame.size.height/2;
    _v_Badge.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
