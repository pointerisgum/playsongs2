//
//  SharePopUpCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/23.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SharePopUpCell : UITableViewCell
@property (weak, nonatomic) IBOutlet ThumbImageView *iv_Thumb;
@property (weak, nonatomic) IBOutlet UILabel *lb_Title;
@property (weak, nonatomic) IBOutlet UILabel *lb_SubTitle;
@property (weak, nonatomic) IBOutlet UIButton *btn_Check;
@end

NS_ASSUME_NONNULL_END
