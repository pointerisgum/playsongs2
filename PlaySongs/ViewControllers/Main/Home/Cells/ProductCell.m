//
//  ProductCell.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/16.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "ProductCell.h"

@implementation ProductCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _btn_Price.layer.cornerRadius = 2.f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
