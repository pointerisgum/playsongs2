//
//  BabyInfoViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/09/06.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "BabyInfoViewController.h"

static NSInteger kMaxLength = 10;
static NSInteger kMinLength = 1;

@interface BabyInfoViewController () <UITextFieldDelegate>

@property (nonatomic, strong) NSMutableArray *arM_BabyInfo;
@property (nonatomic, strong) NSMutableArray *arM_Year;
@property (nonatomic, strong) NSMutableArray *arM_Month;
@property (nonatomic, strong) NSMutableArray *arM_Day;

@property (weak, nonatomic) IBOutlet UIScrollView *sv_Main;

//아이 이름
@property (weak, nonatomic) IBOutlet UIView *v_BabyName;
@property (weak, nonatomic) IBOutlet UITextField *tf_BabyName;
@property (weak, nonatomic) IBOutlet UIButton *btn_BabyStatus;

//성별
@property (weak, nonatomic) IBOutlet UIView *v_Sex;
@property (weak, nonatomic) IBOutlet UIButton *btn_Girl;
@property (weak, nonatomic) IBOutlet UIButton *btn_Boy;

//월령
@property (weak, nonatomic) IBOutlet UIView *v_Year;
@property (weak, nonatomic) IBOutlet UITextField *tf_Year;
@property (weak, nonatomic) IBOutlet UIView *v_Month;
@property (weak, nonatomic) IBOutlet UITextField *tf_Month;
@property (weak, nonatomic) IBOutlet UIView *v_Day;
@property (weak, nonatomic) IBOutlet UITextField *tf_Day;
@end

@implementation BabyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setBgRounding:_v_BabyName];
    [self setBgRounding:_v_Year];
    [self setBgRounding:_v_Month];
    [self setBgRounding:_v_Day];

    _v_Sex.layer.cornerRadius = 8.0f;
    _v_Sex.layer.borderWidth = 0.5f;
    _v_Sex.layer.borderColor = [UIColor colorWithHexString:@"FE540E"].CGColor;

    [self setDates];

    [self updateList];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MBProgressHUD hide];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)updateList
{
    __weak __typeof(&*self)weakSelf = self;

    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@([UserData sharedData].userId) forKey:@"userId"];
    
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"user/childs" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            weakSelf.arM_BabyInfo = [resulte valueForArrayKey:@"result"];
            [weakSelf updateUI];
        }
    }];
}

- (void)updateUI
{
    NSDictionary *dic_BabyInfo = _arM_BabyInfo.lastObject;
    _tf_BabyName.text = [dic_BabyInfo stringForKey:@"childName"];
    NSString *str_Gender = [dic_BabyInfo stringForKey:@"childGender"];
    if( [str_Gender isEqualToString:@"girl"] )
    {
        _btn_Girl.selected = YES;
        _btn_Girl.backgroundColor = kEnableColor;
    }
    else
    {
        _btn_Boy.selected = YES;
        _btn_Boy.backgroundColor = kEnableColor;
    }
    
    NSString *str_Birth = [dic_BabyInfo stringForKey:@"childBirth"];
    NSArray *ar_Birth = [str_Birth componentsSeparatedByString:@"-"];
    _tf_Year.text = [NSString stringWithFormat:@"%ld", [ar_Birth[0] integerValue]];
    _tf_Month.text = [NSString stringWithFormat:@"%ld", [ar_Birth[1] integerValue]];
    _tf_Day.text = [NSString stringWithFormat:@"%ld", [ar_Birth[2] integerValue]];
}

- (void)setBgRounding:(UIView *)view
{
    view.layer.cornerRadius = 8.0f;
    view.layer.borderWidth = 0.5f;
    view.layer.borderColor = kEnableBoarderColor.CGColor;
}

- (void)setDates
{
    NSDate *date = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
    NSInteger nYear = [components year];

    self.arM_Year = [NSMutableArray array];
    for( NSInteger i = nYear; i > nYear - 100; i-- )
    {
        [self.arM_Year addObject:[NSString stringWithFormat:@"%ld년", i]];
    }

    self.arM_Month = [NSMutableArray array];
    for( NSInteger i = 1; i <= 12; i++ )
    {
        [self.arM_Month addObject:[NSString stringWithFormat:@"%ld월", i]];
    }
    
    self.arM_Day = [NSMutableArray array];
    NSDate *date2 = [calendar dateFromComponents:components];
    NSRange range = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date2];
    for( NSInteger i = range.location; i <= range.length; i++ )
    {
        [self.arM_Day addObject:[NSString stringWithFormat:@"%ld일", i]];
    }
}

- (void)animalWasSelected:(NSNumber *)selectedIndex element:(id)element
{
    NSInteger idx = [selectedIndex integerValue];
    if( element == _tf_Year )
    {
        if( IsArrayOutOfBounds(self.arM_Year, idx) )    return;

        NSString *str = self.arM_Year[idx];
        str = [[str componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        _tf_Year.text = str;

    }
    else if( element == _tf_Month )
    {
        if( IsArrayOutOfBounds(self.arM_Month, idx) )    return;
        
        NSString *str = self.arM_Month[idx];
        str = [[str componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        _tf_Month.text = str;
    }
    else if( element == _tf_Day )
    {
        if( IsArrayOutOfBounds(self.arM_Day, idx) )    return;

        NSString *str = self.arM_Day[idx];
        str = [[str componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        _tf_Day.text = str;
    }
}


//MARK: Noti
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    self.sv_Main.contentInset = UIEdgeInsetsMake(self.sv_Main.contentInset.top, self.sv_Main.contentInset.left, keyboardBounds.size.height, self.sv_Main.contentInset.right);
    
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    self.sv_Main.contentInset = UIEdgeInsetsZero;
    
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}


//MARK: UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if( textField == _tf_Year )
    {
        [self.view endEditing:YES];
        [ActionSheetStringPicker showPickerWithTitle:@"생년" rows:self.arM_Year initialSelection:0 target:self successAction:@selector(animalWasSelected:element:) cancelAction:nil origin:textField];
        return NO;
    }
    else if( textField == _tf_Month )
    {
        [self.view endEditing:YES];
        [ActionSheetStringPicker showPickerWithTitle:@"월" rows:self.arM_Month initialSelection:0 target:self successAction:@selector(animalWasSelected:element:) cancelAction:nil origin:textField];
        return NO;
    }
    else if( textField == _tf_Day )
    {
        [self.view endEditing:YES];
        [ActionSheetStringPicker showPickerWithTitle:@"일" rows:self.arM_Day initialSelection:0 target:self successAction:@selector(animalWasSelected:element:) cancelAction:nil origin:textField];
        return NO;
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if( textField == self.tf_BabyName )
    {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        if( newLength > kMaxLength )
        {
            NSString *str_Msg = [NSString stringWithFormat:@"최대 %ld자까지 입력 할 수 있습니다.", kMaxLength];
            [Util showToast:str_Msg];
            [self.view endEditing:YES];
            return NO;
        }
        
        self.btn_BabyStatus.hidden = NO;

        if( newLength >= kMinLength )
        {
            self.btn_BabyStatus.selected = YES;
            self.v_BabyName.layer.borderColor = kEnableBoarderColor.CGColor;
        }
        else
        {
            self.btn_BabyStatus.selected = NO;
            self.v_BabyName.layer.borderColor = kErrorColor.CGColor;
        }
    }
    
    return YES;
}


//MARK: Action
- (IBAction)goGirl:(id)sender
{
    [HapticHelper tap];
    
    self.btn_Girl.selected = YES;
    self.btn_Boy.selected = NO;
    
    self.btn_Girl.backgroundColor = kEnableColor;
    self.btn_Boy.backgroundColor = [UIColor whiteColor];
}

- (IBAction)goBoy:(id)sender
{
    [HapticHelper tap];
    
    self.btn_Girl.selected = NO;
    self.btn_Boy.selected = YES;
    
    self.btn_Girl.backgroundColor = [UIColor whiteColor];
    self.btn_Boy.backgroundColor = kEnableColor;
}

- (IBAction)goDone:(id)sender
{
    if( _tf_BabyName.text.length <= 0 )
    {
        [UIAlertController showAlertInViewController:self
                                           withTitle:@""
                                             message:@"아이 이름을 입력해 주세요."
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@[@"확인"]
                                            tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                
                                            }];
        return;
    }
    
    if( _tf_Year.text.length <= 0 || _tf_Month.text.length <= 0 || _tf_Day.text.length <= 0 )
    {
        [UIAlertController showAlertInViewController:self
                                           withTitle:@""
                                             message:@"월령을 입력해 주세요."
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@[@"확인"]
                                            tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                
                                            }];
        return;
    }

    if( _btn_Boy.selected == NO && _btn_Girl.selected == NO )
    {
        [UIAlertController showAlertInViewController:self
                                           withTitle:@""
                                             message:@"성병을 입력해 주세요."
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@[@"확인"]
                                            tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                
                                            }];
        return;
    }

    __weak __typeof__(self) weakSelf = self;
    
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@([UserData sharedData].userId) forKey:@"userId"];
    [dicM_Params setObject:_tf_BabyName.text forKey:@"childName"];
    [dicM_Params setObject:_btn_Boy.selected ? @"boy":@"girl" forKey:@"childGender"];
    [dicM_Params setObject:[NSString stringWithFormat:@"%04ld-%02ld-%02ld",
                            [_tf_Year.text integerValue],
                            [_tf_Month.text integerValue],
                            [_tf_Day.text  integerValue]]
                    forKey:@"childBirth"];
    
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"user/child" param:dicM_Params withMethod:@"PATCH" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            [weakSelf dismissViewControllerAnimated:YES completion:^{
               [[Util keyWindow].rootViewController.view makeToast:@"아이정보가 변경 되었습니다."];
            }];
        }
    }];
}

@end
