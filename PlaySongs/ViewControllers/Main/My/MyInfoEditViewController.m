//
//  MyInfoEditViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/09/03.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "MyInfoEditViewController.h"
#import "MyInfoEditCell.h"
#import "MyInfoEditCell2.h"
#import "PlaySongs-Swift.h"
#import "UITextView+Placeholder.h"

@interface MyInfoEditViewController ()
@property (nonatomic, strong) UITextField *tf_Name;
@property (nonatomic, strong) UITextField *tf_UserName;
@property (nonatomic, strong) UITextView *tv_Bio;
@property (nonatomic, strong) NSString *str_UploadImageUrl;
@property (weak, nonatomic) IBOutlet ThumbImageView *iv_Thumb;
@property (weak, nonatomic) IBOutlet UITableView *tbv_List;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_TbvHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_Bottom;
@end

@implementation MyInfoEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tf_Name.placeholder = @"이름을 입력해 주세요.";
    _tf_UserName.placeholder = @"사용자 이름을 입력해 주세요.";
    _tv_Bio.placeholder = @"소개를 입력해 주세요.";
    [_iv_Thumb sd_setImageWithURL:[NSURL URLWithString:[UserData sharedData].imgUrl] placeholderImage:[UIImage imageNamed:@"ic_no_user"]];
    
    //    _lb_Name.text = [UserData sharedData].userName;
    //    _lb_Bio.text = [UserData sharedData].bio;
    
    _lc_TbvHeight.constant = (3*50)+120;
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SBDMain removeChannelDelegateForIdentifier:@"chat"];
    
    [MBProgressHUD hide];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


//MARK: Noti
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    
    //    _tbv_List.contentInset = UIEdgeInsetsMake(_tbv_List.contentInset.top, _tbv_List.contentInset.left, keyboardBounds.size.height, _tbv_List.contentInset.right);
    
    _lc_Bottom.constant = -keyboardBounds.size.height;
    
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    //    _tbv_List.contentInset = UIEdgeInsetsZero;
    
    _lc_Bottom.constant = 0;
    
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}


//MARK: TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyInfoEditCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyInfoEditCell"];
    
    switch (indexPath.row)
    {
        case 0:
            _tf_Name = cell.tf;
            cell.lb_Title.text = @"이름";
            cell.tf.text = [UserData sharedData].name;
            cell.tf.textColor = [UIColor blackColor];
            break;
        case 1:
            _tf_UserName = cell.tf;
            cell.lb_Title.text = @"사용자 이름";
            cell.tf.text = [UserData sharedData].userName;
            cell.tf.textColor = [UIColor blackColor];
            break;
        case 2:
            cell.lb_Title.text = @"이메일";
            cell.tf.text = [UserData sharedData].email;
            cell.tf.enabled = NO;
            cell.tf.textColor = [UIColor lightGrayColor];
            break;
        case 3:
        {
            MyInfoEditCell2 *cell = [tableView dequeueReusableCellWithIdentifier:@"MyInfoEditCell2"];
            _tv_Bio = cell.tv;
            cell.lb_Title.text = @"소개";
            cell.tv.text = [UserData sharedData].bio;
            return cell;
        }
            
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row == 3 )    return 120;
    return 50;
}

- (IBAction)goDone:(id)sender
{
    __weak __typeof__(self) weakSelf = self;
    
    if( _tf_Name.text.length <= 0 )
    {
        [UIAlertController showAlertInViewController:self
                                           withTitle:@""
                                             message:@"이름을 입력해 주세요."
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@[@"확인"]
                                            tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                
                                            }];
        return;
    }
    
    if( _tf_UserName.text.length <= 0 )
    {
        [UIAlertController showAlertInViewController:self
                                           withTitle:@""
                                             message:@"사용자 이름을 입력해 주세요."
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@[@"확인"]
                                            tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                
                                            }];
        return;
    }

    if( _tv_Bio.text.length <= 0 )
    {
        [UIAlertController showAlertInViewController:self
                                           withTitle:@""
                                             message:@"소개를 입력해 주세요."
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@[@"확인"]
                                            tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                
                                            }];
        return;
    }

    [self uploadImage];
}

- (void)uploadImage
{
    __weak __typeof(&*self)weakSelf = self;
    
    NSData *data = UIImageJPEGRepresentation(_iv_Thumb.image, 0.7f);
    NSMutableDictionary *dicM_Datas = [NSMutableDictionary dictionary];
    [dicM_Datas setObject:@{@"data":data, @"type":@"image"} forKey:@"file1"];
    
    [[WebAPI sharedData] imageUpload:@"user/uploader"
                               param:nil
                          withImages:dicM_Datas
                           withBlock:^(id resulte, NSError *error) {
        
        if( resulte )
        {
            NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
            if( [str_Success isEqualToString:@"success"] )
            {
                id res = [resulte dictionaryForKey:@"result"];
                if( [res isKindOfClass:[NSArray class]] )
                {
                    NSDictionary *dic_Result = [res firstObject];
                    weakSelf.str_UploadImageUrl = [dic_Result stringForKey:@"filePath"];
                }
                else
                {
                    NSDictionary *dic_Result = [resulte dictionaryForKey:@"result"];
                    weakSelf.str_UploadImageUrl = [dic_Result stringForKey:@"filePath"];
                }
                
                [weakSelf updateContents];
            }
        }
    }];
    
}

- (void)updateContents
{
    __weak __typeof(&*self)weakSelf = self;

    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@([UserData sharedData].userId) forKey:@"userId"];
    [dicM_Params setObject:_tf_Name.text forKey:@"name"];
    [dicM_Params setObject:_tf_UserName.text forKey:@"username"];
    [dicM_Params setObject:_tv_Bio.text forKey:@"bio"];
    [dicM_Params setObject:[UserData sharedData].email forKey:@"email"];
    [dicM_Params setObject:_str_UploadImageUrl forKey:@"imgUrl"];
    
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"user/profile" param:dicM_Params withMethod:@"PATCH" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            [[UserData sharedData] initData:[resulte valueForDictionaryKey:@"result"]];
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}

//MARK: Action
- (IBAction)goPicture:(id)sender
{
    [HapticHelper tap];
    
    __weak __typeof(&*self)weakSelf = self;

    InstarPicker *instarPicker = [[InstarPicker alloc] init];
    [instarPicker setCompletionHandler:^(UIImage * _Nonnull image) {
        
        weakSelf.iv_Thumb.image = image;
    }];
    
    YPImagePicker *pickerNavi = [instarPicker getProfilePhoto];
    [self presentViewController:(UINavigationController *)pickerNavi animated:YES completion:nil];
}

@end
