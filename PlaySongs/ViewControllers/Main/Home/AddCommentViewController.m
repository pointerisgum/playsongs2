//
//  AddCommentViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/12.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "AddCommentViewController.h"
#import "UITextView+Placeholder.h"

@interface AddCommentViewController ()

@property (weak, nonatomic) IBOutlet UIView *v_MsgBg;
@property (weak, nonatomic) IBOutlet UITextView *tv_Msg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_MsgHeight;
@property (weak, nonatomic) IBOutlet UIImageView *iv_MyThumb;
@property (weak, nonatomic) IBOutlet UIButton *btn_Send;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_AccBottom;
@end

@implementation AddCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _tv_Msg.placeholder = @"댓글 달기...";
    _v_MsgBg.layer.cornerRadius = 12.0f;
    _v_MsgBg.layer.borderWidth = 0.8f;
    _v_MsgBg.layer.borderColor = [UIColor colorWithHexString:@"DCDCDC"].CGColor;
    
    [_iv_MyThumb sd_setImageWithURL:[NSURL URLWithString:[UserData sharedData].imgUrl] placeholderImage:[UIImage imageNamed:@"ic_no_user"]];
    
    [_tv_Msg becomeFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MBProgressHUD hide];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)sendMsg
{
    __weak __typeof__(self) weakSelf = self;
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@([_str_PlayId integerValue]) forKey:@"playId"];
    [dicM_Params setObject:@([UserData sharedData].userId) forKey:@"userId"];
    [dicM_Params setObject:_tv_Msg.text forKey:@"comment"];

    [[WebAPI sharedData] callAsyncWebAPIBlock:@"play/comment" param:dicM_Params withMethod:@"POST" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];

        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            weakSelf.lc_MsgHeight.constant = 33;
            weakSelf.tv_Msg.text = @"";
            weakSelf.btn_Send.selected = NO;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateOneItemNoti" object:weakSelf.str_PlayId];

            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}


//MARK: Noti
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    
    _lc_AccBottom.constant = keyboardBounds.size.height - [Util keyWindow].safeAreaInsets.bottom;
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    _lc_AccBottom.constant = 0;
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}


//MARK: UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    _btn_Send.selected = textView.text.length > 0;
    
    if( textView.contentSize.height > 100 )
    {
        _lc_MsgHeight.constant = 100;
        return;
    }
    
    [textView sizeToFit];
    
    if( textView.contentSize.height < 33 )
    {
        if( _lc_MsgHeight.constant != 33 )
        {
            _lc_MsgHeight.constant = 33;
            [UIView animateWithDuration:0.25f animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    }
    else if( textView.contentSize.height > 100 )
    {
        if( _lc_MsgHeight.constant != 33 )
        {
            _lc_MsgHeight.constant = 100;
            [UIView animateWithDuration:0.25f animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    }
    else
    {
        if( _lc_MsgHeight.constant != textView.contentSize.height )
        {
            _lc_MsgHeight.constant = textView.contentSize.height;
            [UIView animateWithDuration:0.25f animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    }
}


//MARK: Action
- (IBAction)goSend:(id)sender
{
    if( _btn_Send.selected == NO )  return;
    
    [HapticHelper tap];
    
    [self sendMsg];
}

- (IBAction)goDismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
