//
//  SearchViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/07.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "SearchViewController.h"
#import "TagCvCell.h"
#import "SearchThumbCell.h"
#import "ODRefreshControl.h"
#import "UICollectionViewLeftAlignedLayout.h"
#import "TagPageViewController.h"
#import "FeedDetailViewController.h"

#define kLimitCount 18
#define kCellMargin 1.5

@interface SearchViewController () <UITextFieldDelegate>
@property (nonatomic, assign) NSInteger nPage;
@property (nonatomic, assign) BOOL isFinish;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, strong) NSMutableArray *arM_TagList;
@property (nonatomic, strong) NSMutableArray *arM_List;
@property (weak, nonatomic) IBOutlet UICollectionView *cv_TagList;
@property (weak, nonatomic) IBOutlet UICollectionView *cv_List;
@property (weak, nonatomic) IBOutlet UIView *v_SearchBg;
@property (nonatomic, strong) ODRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UITextField *tf_Search;

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;

    UICollectionViewLeftAlignedLayout *layout = [[UICollectionViewLeftAlignedLayout alloc] init];
    layout.minimumLineSpacing = kCellMargin;
    layout.minimumInteritemSpacing = kCellMargin;
    _cv_List.collectionViewLayout = layout;
    
    _v_SearchBg.clipsToBounds = YES;
    _v_SearchBg.layer.cornerRadius = 6.0f;
    
    _nPage = 1;
    _isFinish = NO;
    _isLoading = NO;
    _arM_List = [NSMutableArray array];
    
    _refreshControl = [[ODRefreshControl alloc] initInScrollView:_cv_List];
    _refreshControl.tintColor = kEnableColor;
    _refreshControl.backgroundColor = [UIColor whiteColor];
    [_refreshControl addTarget:self action:@selector(onRefresh:) forControlEvents:UIControlEventValueChanged];
    [_cv_List addSubview:_refreshControl];

    [self updateTagList];
    [self updateList];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)updateTagList
{
    __weak __typeof__(self) weakSelf = self;

    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@(10) forKey:@"limit"];
    
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"tag/recommends" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            weakSelf.arM_TagList = [resulte valueForArrayKey:@"result"];
            [weakSelf.cv_TagList reloadData];
        }
    }];
}

- (void)updateList
{
    if( _isFinish )     return;
    if( _isLoading )    return;
    
    _isLoading = YES;
    
    __weak __typeof__(self) weakSelf = self;

    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setValue:@(_nPage) forKey:@"page"];
    [dicM_Params setValue:@(kLimitCount) forKey:@"limt"];
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"browse/list" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
                
        [MBProgressHUD hide];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.refreshControl endRefreshing];
        });

        if( error != nil )
        {
            weakSelf.isLoading = NO;
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            NSArray *ar = [resulte valueForArrayKey:@"result"];
            if( ar.count > 0 )
            {
                if( weakSelf.arM_List.count > 0 )
                {
                    [weakSelf.arM_List addObjectsFromArray:ar];
                }
                else
                {
                    weakSelf.arM_List = [NSMutableArray arrayWithArray:ar];
                }
            }
            else
            {
                //finish
                weakSelf.isFinish = YES;
            }
        }

        [weakSelf.cv_List reloadData];
        weakSelf.isLoading = NO;
    }];
}

- (void)onRefresh:(ODRefreshControl *)sender
{
    [HapticHelper tap];
    
    _nPage = 1;
    _isFinish = NO;
    _isLoading = NO;
    [_arM_List removeAllObjects];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        [self updateList];
    });
}


//MARK: UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
//    self.tabBarController.tabBar.hidden = YES;
//    [UIView animateWithDuration:1.3f animations:^{
//        self.searchContainer.alpha = YES;
//    }];

    [self performSegueWithIdentifier:@"ShowSearchTextSegue" sender:nil];
//    UIViewController *vc = [kMainBoard instantiateViewControllerWithIdentifier:@"SearchTextViewController"];
//    [self.navigationController pushViewController:vc animated:YES];
    return NO;
}

//MARK: CollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if( collectionView == _cv_TagList )
    {
        return _arM_TagList.count;
    }
    return _arM_List.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if( collectionView == _cv_TagList )
    {
        TagCvCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TagCvCell" forIndexPath:indexPath];
        if( IsArrayOutOfBounds(_arM_TagList, indexPath.row) )  return cell;
        NSDictionary *dic = _arM_TagList[indexPath.row];
        cell.lb_Title.text = [NSString stringWithFormat:@"#%@", [dic stringForKey:@"tagName"]];
        return cell;
    }
    
    SearchThumbCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchThumbCell" forIndexPath:indexPath];
    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return cell;
    NSDictionary *dic = _arM_List[indexPath.row];
    NSString *str_ImageUrl = [dic stringForKey:@"coverUrl"];
    [cell.iv_Thumb sd_setImageWithURL:[NSURL URLWithString:str_ImageUrl] placeholderImage:[UIImage imageNamed:@"ic_no_image"]];
    
    CGFloat fCellSize = (collectionView.bounds.size.width - (kCellMargin*2))/3;
    cell.lc_Width.constant = fCellSize;
    cell.lc_Height.constant = fCellSize;
    
    BOOL lastItemReached = [dic isEqual:[_arM_List lastObject]];
    if (lastItemReached && indexPath.row == [_arM_List count] - 1)
    {
        _nPage++;
        [self updateList];
    }

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    CGFloat minSize = collectionView.frame.size.width * 0.7f;//MIN(collectionView.frame.size.width, collectionView.frame.size.height);
    //    return CGSizeMake(minSize * 1.03, minSize);
    //    //    return CGSizeMake([str_Title sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:14.f]}].width + 30.f, 50.f);
    
    //    NSDictionary *dic = _arM_Corver[indexPath.row];
    //    NSInteger nWidth = [dic integerForKey:@"width"];
    //    NSInteger nHeight = [dic integerForKey:@"height"];
    //    CGFloat fRatio = kDeviceWidth/nWidth;
//    return CGSizeMake(collectionView.bounds.size.width, collectionView.bounds.size.height);
    
    CGFloat fCellSize = (collectionView.bounds.size.width - (kCellMargin*2))/3;
    return CGSizeMake(fCellSize, fCellSize);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if( collectionView == _cv_TagList )
    {
        return UIEdgeInsetsMake(0, 15, 0, 15);
    }
    return UIEdgeInsetsMake(kCellMargin, 0, kCellMargin, 0);
    
    //    CGFloat minSize = collectionView.frame.size.width * 0.7f;//MIN(collectionView.frame.size.width, collectionView.frame.size.height);
    //
    ////    return UIEdgeInsetsMake(20, (collectionView.frame.size.width - minSize) / 2, 20, 16*self.ar_CvEventList.count);
    //    return UIEdgeInsetsMake(20, (collectionView.frame.size.width - minSize) / 2, 20, (16*self.ar_CvEventList.count) + ((collectionView.frame.size.width - minSize) / 2)/2);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [HapticHelper tap];
    
    if( collectionView == _cv_TagList )
    {
        if( IsArrayOutOfBounds(_arM_TagList, indexPath.row) )  return;
        
        NSDictionary *dic = _arM_TagList[indexPath.row];
        TagPageViewController *vc = [kUserBoard instantiateViewControllerWithIdentifier:@"TagPageViewController"];
        vc.str_TagName = [dic stringForKey:@"tagName"];
        vc.nTagId = [dic integerForKey:@"tagId"];
        [self.navigationController pushViewController:vc animated:YES];

    }
    else
    {
        if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return;

        NSDictionary *dic = _arM_List[indexPath.row];
        FeedDetailViewController *vc = [kFeedBoard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
        vc.str_PlayId = [NSString stringWithFormat:@"%ld", [dic integerForKey:@"playId"]];
        [self.navigationController pushViewController:vc animated:YES];
    }
    /*
     tagId : 태그 ID
     tagName : 태그명
     playCount : 해당 태그를 포함하는 놀이 갯수
     */
    
    //    [self performSegueWithIdentifier:@"goShowEvent" sender:[dic stringForKey:@"fesNo"]];
}

//- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
//    *targetContentOffset = scrollView.contentOffset; // set acceleration to 0.0
//    float pageWidth = (float)self.cv_Event.bounds.size.width;
//    int minSpace = 10;
//
//    int cellToSwipe = (scrollView.contentOffset.x)/(pageWidth + minSpace) + 0.5; // cell width + min spacing for lines
//    if (cellToSwipe < 0) {
//        cellToSwipe = 0;
//    } else if (cellToSwipe >= self.ar_CvEventList.count) {
//        cellToSwipe = self.ar_CvEventList.count - 1;
//    }
//    [self.cv_Event scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:cellToSwipe inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
//}

//- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"cell start");
//    collectionView.contentOffset = CGPointMake(_nTopScollX, collectionView.contentOffset.y);
//}
//
//- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"cell end");
//    _nTopScollX = collectionView.contentOffset.x;
//}

@end
