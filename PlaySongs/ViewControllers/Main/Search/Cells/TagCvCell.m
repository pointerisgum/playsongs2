//
//  TagCvCell.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/13.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "TagCvCell.h"

@implementation TagCvCell
- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _v_Bg.clipsToBounds = YES;
    _v_Bg.layer.cornerRadius = 6.0f;
    _v_Bg.layer.borderWidth = 0.5f;
    _v_Bg.layer.borderColor = [UIColor lightGrayColor].CGColor;
}
@end
