//
//  SharePopUpViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/23.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "SharePopUpViewController.h"
#import "SharePopUpCell.h"
#import <SendBirdSDK/SendBirdSDK.h>

@interface SharePopUpViewController ()
@property (nonatomic, assign) CGFloat fBgY;
@property (nonatomic, assign) CGFloat fOldY;
@property (nonatomic, strong) NSMutableArray *arM_List;
@property (nonatomic, strong) NSMutableArray *arM_SelectedList;
@property (weak, nonatomic) IBOutlet UIView *v_Bg;
@property (weak, nonatomic) IBOutlet UITableView *tbv_List;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_ContentsHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_NextButtonBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_BgBottom;
@property (weak, nonatomic) IBOutlet UIButton *btn_Next;

@end

@implementation SharePopUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _fBgY = -1;
    _arM_SelectedList = [NSMutableArray array];
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:_v_Bg.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    _v_Bg.layer.mask = maskLayer;

    _lc_BgBottom.constant = kDeviceHeight * 0.7 * -1;
    _lc_ContentsHeight.constant = kDeviceHeight * 0.7;
    
    [self updateList];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    _lc_NextButtonBottom.constant = 15 + [Util keyWindow].safeAreaInsets.bottom;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    _lc_BgBottom.constant = 0;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)updateList
{
    __weak __typeof__(self) weakSelf = self;

    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@([UserData sharedData].userId) forKey:@"userId"];
    [dicM_Params setObject:@([UserData sharedData].userId) forKey:@"following"];
    
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"user/following/list" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            weakSelf.arM_List = [resulte valueForArrayKey:@"result"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.tbv_List reloadData];
        });
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onMoveGesture:(id)sender
{
//    UIPanGestureRecognizer *gesture = (UIPanGestureRecognizer *)sender;
//    CGPoint transPoint = [gesture translationInView:_v_Bg];
//    NSLog(@"%f", transPoint.y);
//
//    if( _fBgY == -1 )
//    {
//        _fBgY = _v_Bg.center.y;
//    }
//
//
//    switch (gesture.state) {
//        case UIGestureRecognizerStateBegan:
//            NSLog(@"began");
////            _fBgY = _v_Bg.center.y;
//            _v_Bg.center = CGPointMake(_v_Bg.center.x, _fBgY + _fOldY);
//            break;
//
//        case UIGestureRecognizerStateChanged:
//            NSLog(@"move");
//            _fOldY = transPoint.y;
//            _fBgY = _v_Bg.center.y;
//
//            _lc_TbvHeight.constant = (kDeviceHeight * 0.7) - _fOldY;
//            _v_Bg.center = CGPointMake(_v_Bg.center.x, _fBgY + _fOldY);
//
//            break;
//
//        case UIGestureRecognizerStateEnded:
//            NSLog(@"end");
////            _fBgY = _v_Bg.center.y;
//            _v_Bg.center = CGPointMake(_v_Bg.center.x, _fBgY + _fOldY);
//            break;
//
//
//        default:
//            break;
//    }
}

//MARK: TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    _btn_Next.selected = _arM_SelectedList.count > 0;
//    _btn_Next.enabled = _btn_Next.selected;
    _btn_Next.userInteractionEnabled = _btn_Next.selected;
    return _arM_List.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SharePopUpCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SharePopUpCell"];
    
    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return cell;
    
    NSDictionary *dic = _arM_List[indexPath.row];
    
    NSString *str_ImageUrl = [dic stringForKey:@"imgUrl"];
    [cell.iv_Thumb sd_setImageWithURL:[NSURL URLWithString:str_ImageUrl] placeholderImage:[UIImage imageNamed:@"ic_no_user"]];
    cell.lb_Title.text = [dic stringForKey:@"username"];
    cell.lb_SubTitle.text = [dic stringForKey:@"name"];
    
    if( [_arM_SelectedList containsObject:dic] )
    {
        cell.btn_Check.selected = YES;
    }
    else
    {
        cell.btn_Check.selected = NO;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return;
    
    [HapticHelper tap];

    NSDictionary *dic = _arM_List[indexPath.row];
    if( [_arM_SelectedList containsObject:dic] )
    {
        [_arM_SelectedList removeObject:dic];
    }
    else
    {
        [_arM_SelectedList addObject:dic];
    }
    
    [_tbv_List reloadData];
}

- (void)createFinished
{
    self.view.userInteractionEnabled = YES;
    [MBProgressHUD hide];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)nextCreate:(NSDictionary *)dic
{
    [_arM_SelectedList removeObject:dic];
    if( _arM_SelectedList.count > 0 )
    {
        [self createChannel:[_arM_SelectedList firstObject]];
    }
    else
    {
        [self createFinished];
    }
}

- (void)createChannel:(NSDictionary *)dic
{
    __weak __typeof__(self) weakSelf = self;

    NSString *str_Name = [dic stringForKey:@"name"];
    NSString *str_UserId = [NSString stringWithFormat:@"%ld", [dic integerForKey:@"userId"]];
    NSString *str_CoverUrl = [dic stringForKey:@"imgUrl"];
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:str_CoverUrl]];
    
    [SBDGroupChannel createChannelWithName:str_Name isDistinct:YES userIds:@[str_UserId] coverImage:imageData coverImageName:@"cover" data:nil customType:@"" progressHandler:^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        
    } completionHandler:^(SBDGroupChannel * _Nullable channel, SBDError * _Nullable error) {
        
        if( error != nil )
        {
            NSLog(@"채널생성 에러 : %@", error);
            [weakSelf nextCreate:dic];
            return;
        }
        
        NSString *str_PlayType = [weakSelf.dic_Item stringForKey:@"playType"];
        NSString *str_PlayId = @"";
        NSString *str_Title = @"";
        BOOL isJoinPlay = [str_PlayType isEqualToString:@"J"];
        if( [str_PlayType isEqualToString:@"J"] )
        {
//            str_PlayId = [NSString stringWithFormat:@"%ld", [weakSelf.dic_Item integerForKey:@"joinPlayId"]];
            NSDictionary *dic_Join = [weakSelf.dic_Item valueForDictionaryKey:@"playJoin"];
            str_Title = [dic_Join stringForKey:@"title"];
        }
        else
        {
//            str_PlayId = [NSString stringWithFormat:@"%ld", [weakSelf.dic_Item integerForKey:@"playId"]];
            str_Title = [weakSelf.dic_Item stringForKey:@"title"];
        }
        str_PlayId = [NSString stringWithFormat:@"%ld", [weakSelf.dic_Item integerForKey:@"playId"]];

        NSMutableDictionary *dicM_Data = [NSMutableDictionary dictionary];
        [dicM_Data setObject:str_PlayId forKey:@"playId"];
        [dicM_Data setObject:str_Title forKey:@"title"];
        [dicM_Data setObject:[weakSelf.dic_Item stringForKey:@"shareKey"] forKey:@"shareKey"];
        [dicM_Data setObject:[weakSelf.dic_Item stringForKey:@"coverUrl"] forKey:@"coverUrl"];
        [dicM_Data setObject:[weakSelf.dic_Item valueForArrayKey:@"tags"] forKey:@"tags"];
        [dicM_Data setObject:[weakSelf.dic_Item valueForDictionaryKey:@"owner"] forKey:@"owner"];
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dicM_Data options:0 error:&err];
        NSString *str_Data = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        SBDUserMessageParams *params = [[SBDUserMessageParams alloc] initWithMessage:@"new"];
        [params setMessage:str_Title];
        [params setCustomType:isJoinPlay ? @"share-playing" : @"share-play"];
        [params setData:str_Data];
        
        [channel sendUserMessageWithParams:params completionHandler:^(SBDUserMessage * _Nullable userMessage, SBDError * _Nullable error) {
            if (error != nil) {
                NSLog(@"메세지 전송 에러 : %@", error);
                [weakSelf nextCreate:dic];
                return;
            }
            
            [weakSelf nextCreate:dic];
        }];
    }];
    
    NSLog(@"%@과 채널 생성 및 데이터 전송 성공", str_Name);
}

- (IBAction)goNext:(id)sender {
    
    if( _btn_Next.selected == NO )  return;
    if( _arM_SelectedList.count <= 0 )  return;
    
    [HapticHelper tap];
    
    /*
     imgUrl = "https://lh3.googleusercontent.com/a-/AOh14Gg8ps5IMTqk932jPx_P7Xe288vqHhB2elaYYQM=s96-c";
     isFollowing = Y;
     name = "H\Uc96c\Uc96c";
     userId = 81;
     username = "h_jewjew";
     */
    [MBProgressHUD show];
    self.view.userInteractionEnabled = NO;
    [self createChannel:[_arM_SelectedList firstObject]];
}

@end

//가현 피터엄마 기왓장 쥬쥬 솜이엄마 이수아빠
