//
//  PasswordChangeViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/09/06.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "PasswordChangeViewController.h"
#import "NIAttributedLabel.h"

@interface PasswordChangeViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet NIAttributedLabel *lb_ForgotPw;
@property (weak, nonatomic) IBOutlet UITextField *tf_NowPw;
@property (weak, nonatomic) IBOutlet UITextField *tf_NewPw;
@property (weak, nonatomic) IBOutlet UITextField *tf_NewPwConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btn_Done;

@end

@implementation PasswordChangeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _btn_Done.enabled = NO;

    _lb_ForgotPw.underlineStyle = kCTUnderlineStyleSingle;
    _lb_ForgotPw.underlineStyle = kCTUnderlineStyleSingle;
    _lb_ForgotPw.strokeColor = [UIColor redColor];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


//MARK: UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if( textField == _tf_NewPw )
    {
        [_tf_NewPw becomeFirstResponder];
    }
    else if( textField == _tf_NewPw )
    {
        [_tf_NewPwConfirm becomeFirstResponder];
    }
    else if( textField == _tf_NewPwConfirm )
    {
        [self goDone:nil];
    }

    return YES;
}

- (void)textFieldDidChangeSelection:(UITextField *)textField
{
    if( _tf_NowPw.text.length < 4 || _tf_NewPw.text.length < 4 || _tf_NewPwConfirm.text.length < 4 )
    {
        _btn_Done.enabled = NO;
    }
    else
    {
        _btn_Done.enabled = YES;
    }
}



//MARK: Action
- (IBAction)goForgorPw:(id)sender
{
    
}

- (IBAction)goDone:(id)sender
{
    if( _tf_NowPw.text.length <= 0 )
    {
        [UIAlertController showAlertInViewController:self
                                           withTitle:@""
                                             message:@"현재 비밀번호를 입력해 주세요."
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@[@"확인"]
                                            tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                
                                            }];
        return;
    }

    if( _tf_NewPw.text.length <= 0 )
    {
        [UIAlertController showAlertInViewController:self
                                           withTitle:@""
                                             message:@"새 비밀번호를 입력해 주세요."
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@[@"확인"]
                                            tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                
                                            }];
        return;
    }

    if( _tf_NewPwConfirm.text.length <= 0 )
    {
        [UIAlertController showAlertInViewController:self
                                           withTitle:@""
                                             message:@"비밀번호를 확인해 주세요."
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@[@"확인"]
                                            tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                
                                            }];
        return;
    }

    if( [_tf_NewPw.text isEqualToString:_tf_NewPwConfirm.text] == NO )
    {
        [UIAlertController showAlertInViewController:self
                                           withTitle:@""
                                             message:@"비밀번호가 일치하지 않습니다."
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@[@"확인"]
                                            tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                
                                            }];
        return;
    }

    if( _tf_NewPw.text.length < 4 || _tf_NewPwConfirm.text.length < 4 )
    {
        [UIAlertController showAlertInViewController:self
                                           withTitle:@""
                                             message:@"비밀번호는 4자리 이상 입력해 주세요."
                                   cancelButtonTitle:nil
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@[@"확인"]
                                            tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                
                                            }];
        return;
    }

    __weak __typeof__(self) weakSelf = self;
    
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@([UserData sharedData].userId) forKey:@"userId"];
    [dicM_Params setObject:_tf_NowPw.text forKey:@"password"];
    [dicM_Params setObject:_tf_NewPw.text forKey:@"newpassword"];
    
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"user/password" param:dicM_Params withMethod:@"PATCH" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            [[NSUserDefaults standardUserDefaults] setObject:@(0) forKey:@"IsLogin"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            [weakSelf dismissViewControllerAnimated:YES completion:^{
               [[Util keyWindow].rootViewController.view makeToast:@"비밀번호가 변경 되었습니다."];
            }];
        }
    }];

}

@end
