//
//  UserPageViewController.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/13.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserPageViewController : UIViewController
@property (nonatomic, strong) NSDictionary *dic_Owner;
//@property (nonatomic, strong) NSString *str_UserName;
//@property (nonatomic, assign) NSInteger nUserId;
@property (nonatomic, assign) BOOL isHiddenBackButton;
@end

NS_ASSUME_NONNULL_END
