//
//  DetailYouTubeCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/17.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WKYTPlayerView.h"
#import "DetailImageCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface DetailYouTubeCell : DetailImageCell
@property (weak, nonatomic) IBOutlet WKYTPlayerView *v_Youtube;

@end

NS_ASSUME_NONNULL_END
