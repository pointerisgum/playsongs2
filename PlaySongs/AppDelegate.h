//
//  AppDelegate.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/03.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import <SendBirdSDK/SendBirdSDK.h>
#import <KakaoOpenSDK/KakaoOpenSDK.h>
@import GoogleSignIn;
@import Firebase;

@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate, FIRMessagingDelegate>


@end

