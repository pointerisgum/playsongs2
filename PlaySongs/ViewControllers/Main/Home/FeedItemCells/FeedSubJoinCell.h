//
//  FeedSubJoinCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/08.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FeedSubJoinCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iv_Thumb;
@property (weak, nonatomic) IBOutlet UILabel *lb_Title;
@property (weak, nonatomic) IBOutlet UILabel *lb_UserName;
@property (weak, nonatomic) IBOutlet UIButton *btn_Count;

@end

NS_ASSUME_NONNULL_END
