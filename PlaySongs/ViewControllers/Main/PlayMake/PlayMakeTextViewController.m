//
//  PlayMakeTextViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/29.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "PlayMakeTextViewController.h"
#import "UITextView+Placeholder.h"

@interface PlayMakeTextViewController () <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btn_Done;
@property (weak, nonatomic) IBOutlet UITextView *tv_Contents;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_Bottom;

@end

@implementation PlayMakeTextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tv_Contents.placeholder = @"";
    [_tv_Contents becomeFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MBProgressHUD hide];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


//MARK: Noti
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    
    _lc_Bottom.constant = keyboardBounds.size.height + 20;
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    _lc_Bottom.constant = 20;
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}


//MARK: UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    _btn_Done.enabled = textView.text.length > 0;
}


//MARK: Action
- (IBAction)goDone:(id)sender
{
    if( _tv_Contents.text.length > 0 )
    {
        [HapticHelper tap];
        
        if( _completionBlock )
        {
            _completionBlock(_tv_Contents.text);
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
