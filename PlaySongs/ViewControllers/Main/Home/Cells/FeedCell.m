//
//  FeedCell.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/07.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "FeedCell.h"
#import "FeedImageCollectionCell.h"
#import "FeedSubJoinCell.h"
#import "FeedSubButtonCell.h"
#import "TagCvCell.h"
#import "FeedSubTextCell.h"
#import "FeedSubTextMoreCell.h"
#import "AddCommentCell.h"
#import "UICollectionViewLeftAlignedLayout.h"
#import "TagPageViewController.h"

#define kLightColor [UIColor colorWithHexString:@"8e8e8e"]
#define kBlackColor [UIColor blackColor]

@interface FeedCell ()
@property (nonatomic, assign) CGFloat fThumbHeight;
@property (nonatomic, strong) UIViewController *vc_Parent;
@property (nonatomic, strong) NSMutableDictionary *dic_Info;
@property (nonatomic, strong) NSArray *ar_Tags;
@property (nonatomic, strong) NSMutableArray *arM_Items;
@property (nonatomic, strong) NSMutableArray *arM_Corver;
@end

@implementation FeedCell

- (void)awakeFromNib {
    [super awakeFromNib];

    _iv_OwnerThumb.userInteractionEnabled = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)userImageTap:(UIGestureRecognizer *)gestureRecognizer
{
    if( _userImageTapBlock )
    {
        UIView *view = gestureRecognizer.view;
        _userImageTapBlock(@(view.tag));
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    _lc_CvHeight.constant = _fThumbHeight;
    _lc_TagHeight.constant = _cv_Tag.contentSize.height;
    NSLog(@"_lc_TagHeight.constant : %f", _lc_TagHeight.constant);
    [self layoutIfNeeded];
}

- (void)initTagCell:(NSArray *)ar withIndex:(NSInteger)idx withParent:(UIViewController *)vc
{
//    [self.cv_Tag.collectionViewLayout prepareLayout];

    _vc_Parent = vc;
    
    UICollectionViewLeftAlignedLayout *layout = [[UICollectionViewLeftAlignedLayout alloc] init];
    layout.minimumLineSpacing = 5;
    layout.minimumInteritemSpacing = 5;
    layout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize;
    _cv_Tag.collectionViewLayout = layout;

//    self.cv_Tag.collectionViewLayout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize;

//    _lc_TagHeight.constant = 40;
//    [_cv_Tag.collectionViewLayout invalidateLayout];
//    [_cv_Tag.collectionViewLayout prepareLayout];

    _ar_Tags = [NSArray arrayWithArray:ar];
//    [_cv_Tag layoutSubviews];
    [_cv_Tag reloadData];
//    [_cv_Tag sizeToFit];
//    [self sizeToFit];

    _lc_TagHeight.constant = _cv_Tag.contentSize.height;
    [self layoutIfNeeded];
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//
//        [self.cv_Tag reloadData];
//        [self layoutIfNeeded];
//        [self invalidateIntrinsicContentSize];
//    });

//    [_cv_Tag performBatchUpdates:^() {
//
//    } completion:^(BOOL finished) {
//
//    }];


}

- (void)initImageList:(NSDictionary *)dic withIndex:(NSInteger)idx
{
    _dic_Info = [NSMutableDictionary dictionaryWithDictionary:dic];
    
    _arM_Corver = [NSMutableArray array];
    NSArray *ar_Body = [Util jsonStringToArray:[dic stringForKey:@"contentBody"]];
    for( NSDictionary *dic_Body in ar_Body )
    {
        NSString *str_Type = [dic_Body stringForKey:@"type"];
        if( [str_Type isEqualToString:@"file"] )
        {
            NSArray *ar_Files = [dic_Body valueForArrayKey:@"files"];
            for( NSDictionary *dic_File in ar_Files )
            {
                NSString *str_Type = [dic_File stringForKey:@"type"];
                if( [str_Type isEqualToString:@"image"] )
                {
                    NSString *str_ImagePath = [dic_File stringForKey:@"filePath"];
                    if( str_ImagePath.length > 0 )
                    {
                        NSInteger nWidth = [dic_File integerForKey:@"width"];
                        NSInteger nHeight = [dic_File integerForKey:@"height"];
                        [_arM_Corver addObject:@{@"url":str_ImagePath, @"width":@(nWidth), @"height":@(nHeight)}];
                    }
                }
                else if( [str_Type isEqualToString:@"video"] )
                {
                    NSString *str_ImagePath = [dic_File stringForKey:@"thumbnail"];
                    if( str_ImagePath.length > 0 )
                    {
                        NSInteger nWidth = [dic_File integerForKey:@"width"];
                        NSInteger nHeight = [dic_File integerForKey:@"height"];
                        [_arM_Corver addObject:@{@"url":str_ImagePath, @"width":@(nWidth), @"height":@(nHeight)}];
                    }
                }
                
            }
        }
        else if( [str_Type isEqualToString:@"youtube"] )
        {
            NSString *str_VideoId = [dic_Body stringForKey:@"videoId"];
            NSString *str_ImagePath = [NSString stringWithFormat:@"https://img.youtube.com/vi/%@/maxresdefault.jpg", str_VideoId];
            [_arM_Corver addObject:@{@"url":str_ImagePath, @"width":@(1280), @"height":@(720), @"type":@"youtube"}];
        }
    }
    
    if( _arM_Corver.count > 0 )
    {
        NSDictionary *dic = [_arM_Corver firstObject];
        NSInteger nWidth = [dic integerForKey:@"width"];
        NSInteger nHeight = [dic integerForKey:@"height"];
        
        if( nWidth == 0 || nHeight == 0 )
        {
            //이미지 사이즈 정보가 없는 경우 디폴트 값으로 디바이스가로x디바이스가로로 셋팅
            nWidth = nHeight = kDeviceWidth;
        }
        CGFloat fRatio = kDeviceWidth/nWidth;
        _lc_CvHeight.constant = nHeight * fRatio;
        _fThumbHeight = nHeight * fRatio;
    }
    else
    {
        _lc_CvHeight.constant = 0;
        _fThumbHeight = 0;
    }
    [_cv_List reloadData];
    _lc_CvHeight.constant = _fThumbHeight;
    [self layoutIfNeeded];
//    [self.contentView layoutIfNeeded];
    
    _pg.layer.cornerRadius = 8;
    _pg.clipsToBounds = YES;
    if( _arM_Corver.count > 1 )
    {
        _pg.numberOfPages = _arM_Corver.count;
        _pg.hidden = NO;
    }
    else
    {
        _pg.numberOfPages = 0;
        _pg.hidden = YES;
    }

    //==컬렉션뷰 오프셋 설정==//
    CGFloat fOffSetX = [dic integerForKey:@"offset"];
    [_pg setCurrentPageAt:(NSInteger)(fOffSetX / kDeviceWidth) animated:false];
    _cv_List.contentOffset = CGPointMake([dic integerForKey:@"offset"], _cv_List.contentOffset.y);
    //--------------//
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if( scrollView == _cv_List )
    {
        [_pg setProgressWithContentOffsetX:scrollView.contentOffset.x pageWidth:scrollView.bounds.size.width];
    }
}



//MARK: CollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if( collectionView == _cv_List )
    {
        return _arM_Corver.count;
    }
    else if( collectionView == _cv_Tag )
    {
        return _ar_Tags.count;
    }
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if( collectionView == _cv_List )
    {
        FeedImageCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FeedImageCollectionCell" forIndexPath:indexPath];
        
        NSDictionary *dic = _arM_Corver[indexPath.row];
        if( [[dic stringForKey:@"type"] isEqualToString:@"youtube"] )
        {
            cell.iv_Youtube.hidden = NO;
        }
        else
        {
            cell.iv_Youtube.hidden = YES;
        }
        
        NSString *str_ImageUrl = [dic stringForKey:@"url"];
        //    NSInteger nWidth = [dic integerForKey:@"width"];
        //    NSInteger nHeight = [dic integerForKey:@"height"];
        [cell.iv_Thumb sd_setImageWithString:str_ImageUrl];
        
        //    CGFloat fRatio = kDeviceWidth/nWidth;
        //    cell.lc_ImageWidth.constant = kDeviceWidth;
        //    cell.lc_ImageHeight.constant = nHeight * fRatio;
        cell.fImageWidth = collectionView.bounds.size.width;
        cell.fImageHeight = _fThumbHeight;
        cell.lc_ImageWidth.constant = collectionView.bounds.size.width;
        cell.lc_ImageHeight.constant = _fThumbHeight;
//        [cell.contentView layoutIfNeeded];
        return cell;
    }
    
    TagCvCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TagCvCell" forIndexPath:indexPath];
    NSDictionary *dic = _ar_Tags[indexPath.row];
    NSString *str_Title = [dic stringForKey:@"tagName"];
    cell.lb_Title.text = [NSString stringWithFormat:@"#%@", str_Title];
    cell.v_Bg.layer.cornerRadius = 4.0f;
    cell.v_Bg.layer.borderColor = cell.lb_Title.textColor.CGColor;
    cell.v_Bg.layer.borderWidth = 0.7f;
    [self.contentView setNeedsLayout];

    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    CGFloat minSize = collectionView.frame.size.width * 0.7f;//MIN(collectionView.frame.size.width, collectionView.frame.size.height);
    //    return CGSizeMake(minSize * 1.03, minSize);
    //    //    return CGSizeMake([str_Title sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:14.f]}].width + 30.f, 50.f);
    
    //    NSDictionary *dic = _arM_Corver[indexPath.row];
    //    NSInteger nWidth = [dic integerForKey:@"width"];
    //    NSInteger nHeight = [dic integerForKey:@"height"];
    //    CGFloat fRatio = kDeviceWidth/nWidth;
    
    [self.contentView setNeedsLayout];

    if( collectionView == _cv_List )
    {
        return CGSizeMake(collectionView.bounds.size.width, _fThumbHeight);
    }
    

//    return CGSizeZero;
    NSDictionary *dic = _ar_Tags[indexPath.row];
    NSString *str_Title = [dic stringForKey:@"tagName"];
//    return CGSizeMake(10,10);
    return CGSizeMake([str_Title sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14 weight:UIFontWeightMedium]}].width + 20.f, 30.f);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if( collectionView == _cv_List )
    {
        return UIEdgeInsetsZero;
    }
    
    return UIEdgeInsetsMake(0, 15, 5, 15);
    //    CGFloat minSize = collectionView.frame.size.width * 0.7f;//MIN(collectionView.frame.size.width, collectionView.frame.size.height);
    //
    ////    return UIEdgeInsetsMake(20, (collectionView.frame.size.width - minSize) / 2, 20, 16*self.ar_CvEventList.count);
    //    return UIEdgeInsetsMake(20, (collectionView.frame.size.width - minSize) / 2, 20, (16*self.ar_CvEventList.count) + ((collectionView.frame.size.width - minSize) / 2)/2);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [HapticHelper tap];

    if( collectionView == _cv_List )
    {
        NSString *str_PlayType = [_dic_Info stringForKey:@"playType"];
//        NSString *str_PlayId = [NSString stringWithFormat:@"%ld", [str_PlayType isEqualToString:@"J"] ? [_dic_Info integerForKey:@"joinPlayId"] : [_dic_Info integerForKey:@"playId"]];
        NSString *str_PlayId = [NSString stringWithFormat:@"%ld", [_dic_Info integerForKey:@"playId"]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowFeedDetailNoti" object:str_PlayId];
    }
    else
    {
        if( IsArrayOutOfBounds(_ar_Tags, indexPath.row) )  return;

        NSDictionary *dic = _ar_Tags[indexPath.row];
        NSString *str_TagName = [dic stringForKey:@"tagName"];
        NSInteger nTagId = [dic integerForKey:@"tagId"];
        if( str_TagName.length > 0 && nTagId > 0 )
        {
            TagPageViewController *vc = [kUserBoard instantiateViewControllerWithIdentifier:@"TagPageViewController"];
            vc.str_TagName = str_TagName;
            vc.nTagId = nTagId;
            [_vc_Parent.navigationController pushViewController:vc animated:YES];
        }
    }
    
}

//- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
//    *targetContentOffset = scrollView.contentOffset; // set acceleration to 0.0
//    float pageWidth = (float)self.cv_Event.bounds.size.width;
//    int minSpace = 10;
//
//    int cellToSwipe = (scrollView.contentOffset.x)/(pageWidth + minSpace) + 0.5; // cell width + min spacing for lines
//    if (cellToSwipe < 0) {
//        cellToSwipe = 0;
//    } else if (cellToSwipe >= self.ar_CvEventList.count) {
//        cellToSwipe = self.ar_CvEventList.count - 1;
//    }
//    [self.cv_Event scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:cellToSwipe inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
//}

//- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
//{
////    NSLog(@"cell start");
////    collectionView.contentOffset = CGPointMake(_nTopScollX, collectionView.contentOffset.y);
//
//    if( collectionView == _cv_Tag )
//    {
//        _lc_TagHeight.constant = _cv_Tag.contentSize.height;
//        [_cv_Tag layoutSubviews];
//
//    }
//}
//
//- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"cell end");
//    _nTopScollX = collectionView.contentOffset.x;
//}


//MARK: Action
- (void)onLike:(UIButton *)btn
{
    [HapticHelper tap];
    
    __weak __typeof(&*self)weakSelf = self;
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setValue:@([_dic_Info integerForKey:@"playId"]) forKey:@"playId"];
    [dicM_Params setValue:btn.selected?@"N":@"Y" forKey:@"status"];
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"play/heart/save" param:dicM_Params withMethod:@"POST" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            NSDictionary *dic_Result = [resulte dictionaryForKey:@"result"];
            
            //좋아요 토글 상태 업데이트
            NSString *str_Status = [dic_Result stringForKey:@"status"];
            [weakSelf.dic_Info setObject:str_Status forKey:@"isHeart"];
            
            //좋아요 카운트 업데이트
            NSMutableDictionary *dicM_Count = [NSMutableDictionary dictionaryWithDictionary:[weakSelf.dic_Info valueForDictionaryKey:@"count"]];
            NSInteger nHeartCnt = [dicM_Count integerForKey:@"heart"];
            if( [str_Status isEqualToString:@"Y"] )
            {
                nHeartCnt ++;
            }
            else
            {
                nHeartCnt--;
            }
            [dicM_Count setObject:@(nHeartCnt) forKey:@"heart"];
            [weakSelf.dic_Info setObject:dicM_Count forKey:@"count"];

//            [weakSelf.tbv_List reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:btn.tag inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateFeedNoti" object:weakSelf.dic_Info];
        }
    }];
}

- (void)onComment
{
    [HapticHelper tap];

    NSString *str_PlayId = [NSString stringWithFormat:@"%ld", [_dic_Info integerForKey:@"playId"]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowCommentViewNoti" object:str_PlayId];
}

- (void)onAddComment
{
    [HapticHelper tap];

    NSString *str_PlayId = [NSString stringWithFormat:@"%ld", [_dic_Info integerForKey:@"playId"]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowCommentInPutNoti" object:str_PlayId];
}

- (void)onShare
{
    [HapticHelper tap];

    NSString *str_PlayType = [_dic_Info stringForKey:@"playType"];
    NSString *str_PlayId = @"";
    if( [str_PlayType isEqualToString:@"J"] )
    {
        str_PlayId = [NSString stringWithFormat:@"%ld", [_dic_Info integerForKey:@"joinPlayId"]];
    }
    else
    {
        str_PlayId = [NSString stringWithFormat:@"%ld", [_dic_Info integerForKey:@"playId"]];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowShareNoti" object:@{@"playId":str_PlayId, @"obj":_dic_Info}];
}

- (void)onPlay
{
    [HapticHelper tap];

    NSString *str_PlayId = str_PlayId = [NSString stringWithFormat:@"%ld", [_dic_Info integerForKey:@"playId"]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowPlayNoti" object:@{@"playId":str_PlayId, @"obj":_dic_Info}];
}

@end
