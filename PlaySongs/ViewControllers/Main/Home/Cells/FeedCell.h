//
//  FeedCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/07.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlaySongs-Swift.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^UserImageTapBlock)(id completionBlock);

@interface FeedCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btn_More;
@property (nonatomic, copy) UserImageTapBlock userImageTapBlock;
@property (weak, nonatomic) IBOutlet UICollectionView *cv_List;
//@property (weak, nonatomic) IBOutlet UITableView *tbv_List;
@property (weak, nonatomic) IBOutlet FlexiblePageControl *pg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_CvHeight;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_ItemsHeight;
@property (weak, nonatomic) IBOutlet UIImageView *iv_OwnerThumb;
@property (weak, nonatomic) IBOutlet UILabel *lb_OwnerName;
@property (weak, nonatomic) IBOutlet UILabel *lb_OwnerSubTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_PgBottom;

//하단 스택뷰
//참여
@property (weak, nonatomic) IBOutlet UIView *v_Join;
@property (weak, nonatomic) IBOutlet UIImageView *iv_JoinThumb;
@property (weak, nonatomic) IBOutlet UILabel *lb_JoinTitle;
@property (weak, nonatomic) IBOutlet UILabel *lb_JoinSubTitle;
@property (weak, nonatomic) IBOutlet UIButton *btn_JoinPlay;

//버튼들
@property (weak, nonatomic) IBOutlet UIView *v_Button;
@property (weak, nonatomic) IBOutlet UIButton *btn_Heart;
@property (weak, nonatomic) IBOutlet UIButton *btn_Comment;
@property (weak, nonatomic) IBOutlet UIButton *btn_Share;
@property (weak, nonatomic) IBOutlet UIButton *btn_ButtonPlay;

//태그
@property (weak, nonatomic) IBOutlet UICollectionView *cv_Tag;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_TagHeight;

//좋아요
@property (weak, nonatomic) IBOutlet UIView *v_Heart;
@property (weak, nonatomic) IBOutlet UILabel *lb_Heart;

//댓글 모두 보기
@property (weak, nonatomic) IBOutlet UIView *v_CommentAllShow;
@property (weak, nonatomic) IBOutlet UILabel *lb_CommentAllShow;
@property (weak, nonatomic) IBOutlet UIButton *btn_CommentAllShow;

//내용
@property (weak, nonatomic) IBOutlet UIView *v_Contents;
@property (weak, nonatomic) IBOutlet UILabel *lb_Contents;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_ContentsHeight;

//내용 ...더보기
@property (weak, nonatomic) IBOutlet UIView *v_ContentsMore;
@property (weak, nonatomic) IBOutlet ExpandableLabel *lb_ContentsMore;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_ContentsMoreHeight;

//댓글1
@property (weak, nonatomic) IBOutlet UIView *v_Comment1;
@property (weak, nonatomic) IBOutlet UILabel *lb_Comment1;
@property (weak, nonatomic) IBOutlet UIButton *btn_Comment1;

//댓글2
@property (weak, nonatomic) IBOutlet UIView *v_Comment2;
@property (weak, nonatomic) IBOutlet UILabel *lb_Comment2;
@property (weak, nonatomic) IBOutlet UIButton *btn_Comment2;

//댓글쓰기
@property (weak, nonatomic) IBOutlet UIView *v_AddComment;
@property (weak, nonatomic) IBOutlet ThumbImageView *iv_AddCommentThumb;
@property (weak, nonatomic) IBOutlet UIButton *btn_AddComment;

//시간
@property (weak, nonatomic) IBOutlet UIView *v_Time;
@property (weak, nonatomic) IBOutlet UILabel *lb_Time;

@property (weak, nonatomic) IBOutlet UIStackView *stv_Items;

- (void)initImageList:(NSDictionary *)dic withIndex:(NSInteger)idx;
- (void)initTagCell:(NSArray *)ar withIndex:(NSInteger)idx withParent:(UIViewController *)vc;
- (void)setUserImageTapBlock:(UserImageTapBlock)completionBlock;
@end

NS_ASSUME_NONNULL_END
