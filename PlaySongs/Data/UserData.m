//
//  UserData.m
//  ELearning
//
//  Created by Kim Young-Min on 2014. 2. 18..
//  Copyright (c) 2014년 Kim Young-Min. All rights reserved.
//

#import "UserData.h"

static UserData *shared = nil;

@implementation UserData

+ (void)initialize
{
    NSAssert(self == [UserData class], @"Singleton is not designed to be subclassed.");
    shared = [UserData new];
}

+ (UserData *)sharedData
{
    return shared;
}

- (void)initData:(NSDictionary *)dic
{
    self.dic_My = dic;
    self.userId = [dic integerForKey:@"userId"];
    self.userName = [dic stringForKey:@"username"];
    self.email = [dic stringForKey:@"email"];
    self.name = [dic stringForKey:@"name"];
    self.imgUrl = [dic stringForKey:@"imgUrl"];
    self.role = [dic stringForKey:@"role"];
    self.bio = [dic stringForKey:@"bio"];
    self.verified = [dic stringForKey:@"verified"];
}

- (void)resetData
{
    self.userId = -1;
    self.userName = nil;
    self.email = nil;
    self.name = nil;
    self.imgUrl = nil;
    self.role = nil;
    self.bio = nil;
    self.verified = nil;
}

@end
