//
//  ChatViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/17.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "ChatViewController.h"
#import "ChatTextCell.h"
#import "ChatShareCell.h"
#import "UITextView+Placeholder.h"
#import "FeedDetailViewController.h"

@interface ChatViewController () <SBDChannelDelegate>
@property (nonatomic, assign) BOOL isFinish;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, strong) NSMutableArray *arM_List;
@property (weak, nonatomic) IBOutlet UITableView *tbv_List;
@property (weak, nonatomic) IBOutlet ThumbImageView *iv_NaviThumb;
@property (weak, nonatomic) IBOutlet UILabel *lb_NaviTitle;
@property (weak, nonatomic) IBOutlet UILabel *lb_NaviSubTitle;

//msg
@property (weak, nonatomic) IBOutlet UIView *v_MsgBg;
@property (weak, nonatomic) IBOutlet UITextView *tv_Msg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_MsgHeight;
@property (weak, nonatomic) IBOutlet UIImageView *iv_MyThumb;
@property (weak, nonatomic) IBOutlet UIButton *btn_Send;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_AccBottom;

@end

@implementation ChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tbv_List.transform = CGAffineTransformMakeScale(1, -1);
    
    SBDUser *my = [SBDMain getCurrentUser];
    BOOL isAlone = YES;
    for( SBDUser *user in _channel.members )
    {
        if( [user.userId integerValue] != [my.userId integerValue] )
        {
            [_iv_NaviThumb sd_setImageWithString:user.profileUrl];
            _lb_NaviTitle.text = user.nickname;
            if( user.isActive )
            {
                _lb_NaviSubTitle.text = @"활동 중";
            }
            else
            {
                if( user.lastSeenAt > 0 )
                {
                    NSTimeInterval nowTimeStamp = [[NSDate date] timeIntervalSince1970];
                    NSTimeInterval time = nowTimeStamp - user.lastSeenAt;
                    _lb_NaviSubTitle.text = [Util makeAgoTime:time];
                }
                else
                {
                    _lb_NaviSubTitle.hidden = YES;
                }
            }
            isAlone = NO;
            break;
        }
    }

    if( isAlone )
    {
        [_iv_NaviThumb sd_setImageWithString:my.profileUrl];
        _lb_NaviTitle.text = my.nickname;
    }
     
    _tv_Msg.placeholder = @"메세지 보내기...";
    _v_MsgBg.layer.cornerRadius = 12.0f;
    _v_MsgBg.layer.borderWidth = 0.8f;
    _v_MsgBg.layer.borderColor = [UIColor colorWithHexString:@"DCDCDC"].CGColor;
    
    [_iv_MyThumb sd_setImageWithURL:[NSURL URLWithString:[UserData sharedData].imgUrl] placeholderImage:[UIImage imageNamed:@"ic_no_user"]];

    [self updateList];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [SBDMain addChannelDelegate:self identifier:@"chat"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SBDMain removeChannelDelegateForIdentifier:@"chat"];
    
    [MBProgressHUD hide];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)updateList
{
    __weak __typeof(&*self)weakSelf = self;

    _isLoading = YES;
    
    SBDPreviousMessageListQuery *previousMessageQuery = [_channel createPreviousMessageListQuery];
    previousMessageQuery.includeMetaArray = YES;    // Retrieve a list of messages along with their metaarrays.
    previousMessageQuery.includeReactions = YES;    // Retrieve a list of messages along with their reactions.
    [previousMessageQuery loadPreviousMessagesWithLimit:20 reverse:YES completionHandler:^(NSArray<SBDBaseMessage *> * _Nullable messages, SBDError * _Nullable error) {
        if (error != nil) { // Error.
            return;
        }
        
        weakSelf.arM_List = [NSMutableArray arrayWithArray:messages];
        [weakSelf.tbv_List reloadData];
        weakSelf.isLoading = NO;
    }];
}

- (void)updateMore
{
    if( _isFinish )    return;
    if( _isLoading )   return;
    
    _isLoading = YES;

    __weak __typeof(&*self)weakSelf = self;

    SBDBaseMessage *lastMessage = [_arM_List lastObject];
    [_channel getPreviousMessagesByMessageId:lastMessage.messageId limit:20 reverse:YES messageType:SBDMessageTypeFilterAll customType:@"" completionHandler:^(NSArray<SBDBaseMessage *> * _Nullable messages, SBDError * _Nullable error) {
        if (error != nil) { // Error.
            return;
        }

        if( messages.count <= 0 )
        {
            weakSelf.isFinish = YES;
        }
        else
        {
            [weakSelf.arM_List addObjectsFromArray:messages];
        }

        weakSelf.isLoading = NO;
        [weakSelf.tbv_List reloadData];
    }];
}

- (void)sendMsg
{
    __weak __typeof__(self) weakSelf = self;
    SBDUserMessageParams *params = [[SBDUserMessageParams alloc] initWithMessage:_tv_Msg.text];
    [params setCustomType:@"text"];
//    [params setData:DATA];
//    [params setMentionType:SBDMentionTypeUsers];        // Either SBDMentionTypeUsers or SBDMentionTypeChannel
//    [params setMentionedUserIds:@[@"Jeff", @"Julia"]];  // Or setMentionedUsers:LIST_OF_USERS_TO_MENTION
//    [params setMetaArrayKeys:@[@"linkTo", @"itemType"]];
//    [params setTargetLanguages:@[@"fr", @"de"]];        // French and German
//    [params setPushNotificationDeliveryOption:SBDPushNotificationDeliveryOptionDefault];
    [_channel sendUserMessageWithParams:params completionHandler:^(SBDUserMessage * _Nullable userMessage, SBDError * _Nullable error) {
        if (error != nil) { // Error.
            return;
        }
        
        weakSelf.lc_MsgHeight.constant = 33;
        weakSelf.tv_Msg.text = @"";
        weakSelf.btn_Send.selected = NO;

        [weakSelf.arM_List insertObject:userMessage atIndex:0];
        [weakSelf.tbv_List reloadData];
    }];

}

- (void)moreNextMessageIfNeed:(SBDBaseMessage *)message withIdx:(NSInteger)idx
{
    BOOL lastItemReached = [message isEqual:[_arM_List lastObject]];
    if (lastItemReached && idx == [_arM_List count] - 1)
    {
        [self updateMore];
    }
}


//MARK: Noti
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
//    _tbv_List.contentInset = UIEdgeInsetsMake(_tbv_List.contentInset.top, _tbv_List.contentInset.left, keyboardBounds.size.height, _tbv_List.contentInset.right);
    
    _lc_AccBottom.constant = (keyboardBounds.size.height - [Util keyWindow].safeAreaInsets.bottom);
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    _tbv_List.contentInset = UIEdgeInsetsZero;
    
    _lc_AccBottom.constant = 0;
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}


//MARK: UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    _btn_Send.selected = textView.text.length > 0;
    
    if( textView.contentSize.height > 100 )
    {
        _lc_MsgHeight.constant = 100;
        return;
    }
    
    [textView sizeToFit];
    
    if( textView.contentSize.height < 33 )
    {
        if( _lc_MsgHeight.constant != 33 )
        {
            _lc_MsgHeight.constant = 33;
            [UIView animateWithDuration:0.25f animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    }
    else if( textView.contentSize.height > 100 )
    {
        if( _lc_MsgHeight.constant != 33 )
        {
            _lc_MsgHeight.constant = 100;
            [UIView animateWithDuration:0.25f animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    }
    else
    {
        if( _lc_MsgHeight.constant != textView.contentSize.height )
        {
            _lc_MsgHeight.constant = textView.contentSize.height;
            [UIView animateWithDuration:0.25f animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    }
}


//MARK: TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arM_List.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatTextCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatTextCell"];
    cell.contentView.transform = CGAffineTransformMakeScale (1,-1);

    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return cell;
    
    SBDBaseMessage *message = _arM_List[indexPath.row];

    if( [message.customType isEqualToString:@"text"] || [message.customType isEqualToString:@""] )
    {
        [cell setInitUI:message];
        [self moreNextMessageIfNeed:message withIdx:indexPath.row];
        return cell;
    }
    else if( [message.customType isEqualToString:@"share-play"] || [message.customType isEqualToString:@"share-playing"] )
    {
        ChatShareCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatShareCell"];
        cell.contentView.transform = CGAffineTransformMakeScale (1,-1);

        [cell setInitUI:message];
        
        NSDictionary *dic_Data = [NSJSONSerialization JSONObjectWithData:[message.data dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
        NSDictionary *dic_Owner = [dic_Data valueForDictionaryKey:@"owner"];
        
        [cell.iv_Thumb sd_setImageWithString:[dic_Owner stringForKey:@"imgUrl"]];
        cell.lb_Name.text = [dic_Owner stringForKey:@"name"];
        
        [cell.iv_Contents sd_setImageWithString:[dic_Data stringForKey:@"coverUrl"]];

        NSMutableString *strM_Tags = [NSMutableString string];
        NSArray *ar_Tags = [dic_Data valueForArrayKey:@"tags"];
        for( NSDictionary *dic_Tag in ar_Tags )
        {
            NSString *str_TagName = [NSString stringWithFormat:@"#%@", [dic_Tag stringForKey:@"tagName"]];
            [strM_Tags appendString:str_TagName];
            [strM_Tags appendString:@" "];
        }
        if( [strM_Tags hasSuffix:@" "] )
        {
            [strM_Tags deleteCharactersInRange:NSMakeRange([strM_Tags length]-1, 1)];
        }
        cell.lb_Tag.text = strM_Tags;

        [self moreNextMessageIfNeed:message withIdx:indexPath.row];
        return cell;
    }
    else
    {
        NSLog(@"error CustomType : %@", message.customType);
    }
    
    [self moreNextMessageIfNeed:message withIdx:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [_tv_Msg resignFirstResponder];
    
    SBDBaseMessage *message = _arM_List[indexPath.row];
    if( [message.customType isEqualToString:@"share-play"] || [message.customType isEqualToString:@"share-playing"] )
    {
        [HapticHelper tap];
        
        NSDictionary *dic_Data = [NSJSONSerialization JSONObjectWithData:[message.data dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
        NSInteger nPlayId = [dic_Data integerForKey:@"playId"];
        FeedDetailViewController *vc = [kFeedBoard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
        vc.str_PlayId = [NSString stringWithFormat:@"%ld", nPlayId];
        [self.navigationController pushViewController:vc animated:YES];
    }
}


//MARK: Action
- (IBAction)goSend:(id)sender
{
    if( _btn_Send.selected == NO )  return;
    
    [HapticHelper tap];
    
    [self sendMsg];
}


//MARK: SBDChannelDelegate
- (void)channel:(SBDBaseChannel * _Nonnull)sender didReceiveMessage:(SBDBaseMessage * _Nonnull)message {
    
    if (sender == _channel)
    {
        if ([message isKindOfClass:[SBDUserMessage class]]) {
            
        }
        else if ([message isKindOfClass:[SBDFileMessage class]]) {
            
        }
        else if ([message isKindOfClass:[SBDAdminMessage class]]) {
            
        }
        
        [_arM_List insertObject:message atIndex:0];
        [_tbv_List reloadData];
    }
}

@end
