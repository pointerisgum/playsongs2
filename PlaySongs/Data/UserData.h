//
//  UserData.h
//  ELearning
//
//  Created by Kim Young-Min on 2014. 2. 18..
//  Copyright (c) 2014년 Kim Young-Min. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserData : NSObject
@property (nonatomic, strong) NSDictionary *dic_My;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *imgUrl;
@property (nonatomic, strong) NSString *role;
@property (nonatomic, strong) NSString *bio;
@property (nonatomic, strong) NSString *verified;
+ (UserData *)sharedData;
- (void)initData:(NSDictionary *)dic;
- (void)resetData;
@end
