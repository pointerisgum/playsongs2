//
//  PlayMakeBottomView.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/29.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "PlayMakeBottomView.h"

@implementation PlayMakeBottomView

- (void)awakeFromNib
{
    [super awakeFromNib];

}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.frame.size.width/2;
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor = [UIColor blackColor].CGColor;
    self.clipsToBounds = YES;

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
