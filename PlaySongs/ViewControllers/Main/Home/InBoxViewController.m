//
//  InBoxViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/08.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "InBoxViewController.h"
#import <SendBirdSDK/SendBirdSDK.h>
#import "ChatListCell.h"
#import "ChatViewController.h"
#import "ODRefreshControl.h"

@interface InBoxViewController () <SBDChannelDelegate>
@property (nonatomic, strong) NSMutableArray *arM_List;
@property (nonatomic, strong) ODRefreshControl *refreshControl;
@property (nonatomic, strong) SBDGroupChannelListQuery *query;
@property (weak, nonatomic) IBOutlet UITableView *tbv_List;
@end

@implementation InBoxViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SBDMain addChannelDelegate:self identifier:@"list"];

    _arM_List = [NSMutableArray array];
    
    _refreshControl = [[ODRefreshControl alloc] initInScrollView:_tbv_List];
    _refreshControl.tintColor = kEnableColor;
    _refreshControl.backgroundColor = [UIColor whiteColor];
    [_refreshControl addTarget:self action:@selector(onRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tbv_List addSubview:_refreshControl];
    
    [self updateList];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if( [segue.identifier isEqualToString:@"detail"] )
    {
        ChatViewController *vc = segue.destinationViewController;
        vc.channel = sender;
    }
}

- (void)updateList
{
    __weak __typeof(&*self)weakSelf = self;
    
    _query = [SBDGroupChannel createMyGroupChannelListQuery];
    _query.includeEmptyChannel = NO;
    _query.order = SBDGroupChannelListOrderLatestLastMessage;  // SBDGroupChannelListOrderChronological, SBDGroupChannelListOrderLatestLastMessage, SBDGroupChannelListOrderChannelNameAlphabetical, and SBDGroupChannelListOrderChannelMetaDataValueAlphabetical
    _query.limit = 100;

    [_query loadNextPageWithCompletionHandler:^(NSArray<SBDGroupChannel *> * _Nullable groupChannels, SBDError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.refreshControl endRefreshing];
        });

        if (error != nil)
        {
            return;
        }
        
        [weakSelf.arM_List addObjectsFromArray:groupChannels];
        [weakSelf.tbv_List reloadData];
    }];
}

- (void)updateMore
{
    __weak __typeof(&*self)weakSelf = self;
    
    if ([_query hasNext] == NO)
    {
        return;
    }

    [_query loadNextPageWithCompletionHandler:^(NSArray<SBDGroupChannel *> * _Nullable groupChannels, SBDError * _Nullable error) {
        
        if (error != nil)
        {
            return;
        }
        
        [weakSelf.arM_List addObjectsFromArray:groupChannels];
        [weakSelf.tbv_List reloadData];
    }];
}

- (void)onRefresh:(ODRefreshControl *)sender
{
    [HapticHelper tap];
    
    [_arM_List removeAllObjects];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        [self updateList];
    });
}


//MARK: TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arM_List.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatListCell"];
    cell.lb_Contents.hidden = NO;
    cell.lb_Date.hidden = NO;
    cell.v_Badge.hidden = YES;

    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return cell;

    SBDGroupChannel *channel = _arM_List[indexPath.row];
    SBDUser *my = [SBDMain getCurrentUser];
    BOOL isAlone = YES;
    for( SBDUser *user in channel.members )
    {
        if( [user.userId integerValue] != [my.userId integerValue] )
        {
            [cell.iv_Thumb sd_setImageWithString:user.profileUrl];
            cell.lb_Title.text = user.nickname;
            isAlone = NO;
            break;
        }
    }

    if( isAlone )
    {
        [cell.iv_Thumb sd_setImageWithString:my.profileUrl];
        cell.lb_Title.text = my.nickname;
    }
    
    SBDUserMessage *lastMessage = (SBDUserMessage *)channel.lastMessage;
    if( lastMessage.message.length > 0 )
    {
        cell.lb_Contents.text = lastMessage.message;
    }
    else
    {
        cell.lb_Contents.hidden = YES;
        cell.lb_Date.hidden = YES;
    }

    NSDate *lastMessageDate = nil;
    if( lastMessage.createdAt <= 0 )
    {
        //마지막 메세지가 없을때
        lastMessageDate = [NSDate dateWithTimeIntervalSince1970:(double)channel.createdAt];
    }
    else
    {
        //마지막 메세지가 있을때
        lastMessageDate = [NSDate dateWithTimeIntervalSince1970:(double)lastMessage.createdAt / 1000.0f];
    }

    cell.lb_Date.text = [Util getChatDate:lastMessageDate];
    
    if( channel.unreadMessageCount > 0 )
    {
        cell.v_Badge.hidden = NO;
        if( channel.unreadMessageCount > 99 )
        {
            cell.lb_Badge.text = @"99+";
        }
        else
        {
            cell.lb_Badge.text = [NSString stringWithFormat:@"%ld", channel.unreadMessageCount];
        }
    }
    
    
    BOOL lastItemReached = [channel isEqual:[_arM_List lastObject]];
    if (lastItemReached && indexPath.row == [_arM_List count] - 1)
    {
        [self updateMore];
    }

    return cell;
}

- (NSString *)getDday:(NSString *)aDay
{
    aDay = [aDay stringByReplacingOccurrencesOfString:@"-" withString:@""];
    aDay = [aDay stringByReplacingOccurrencesOfString:@" " withString:@""];
    aDay = [aDay stringByReplacingOccurrencesOfString:@":" withString:@""];
    
    NSString *str_Year = [aDay substringWithRange:NSMakeRange(0, 4)];
    NSString *str_Month = [aDay substringWithRange:NSMakeRange(4, 2)];
    NSString *str_Day = [aDay substringWithRange:NSMakeRange(6, 2)];
    NSString *str_Hour = [aDay substringWithRange:NSMakeRange(8, 2)];
    NSString *str_Minute = [aDay substringWithRange:NSMakeRange(10, 2)];
    NSString *str_Second = [aDay substringWithRange:NSMakeRange(12, 2)];
    NSString *str_Date = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@", str_Year, str_Month, str_Day, str_Hour, str_Minute, str_Second];
    
    NSDateFormatter *format1 = [[NSDateFormatter alloc] init];
    [format1 setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    
    NSDate *ddayDate = [format1 dateFromString:str_Date];
    
    NSDate *date = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:date];
    NSInteger nYear = [components year];
    NSInteger nMonth = [components month];
    NSInteger nDay = [components day];
    NSInteger nHour = [components hour];
    NSInteger nMinute = [components minute];
    NSInteger nSecond = [components second];
    
    NSDate *currentTime = [format1 dateFromString:[NSString stringWithFormat:@"%04ld-%02ld-%02ld %02ld:%02ld:%02ld", nYear, nMonth, nDay, nHour, nMinute, nSecond]];
    
    NSTimeInterval diff = [currentTime timeIntervalSinceDate:ddayDate];
    
    NSTimeInterval nWriteTime = diff;
    
    
    
    
    if( nWriteTime > (60 * 60 * 24) )
    {
//        return [NSString stringWithFormat:@"%@-%@-%@", str_Year, str_Month, str_Day];
        return [NSString stringWithFormat:@"%@월 %@일", str_Month, str_Day];
    }
    else
    {
        if( nWriteTime <= 0 )
        {
            return @"1초전";
        }
        else if( nWriteTime < 60 )
        {
            //1분보다 작을 경우
            return [NSString stringWithFormat:@"%.0f초전", nWriteTime];
        }
        else if( nWriteTime < (60 * 60) )
        {
            //1시간보다 작을 경우
            return [NSString stringWithFormat:@"%.0f분전", nWriteTime / 60];
        }
        else
        {
            return [NSString stringWithFormat:@"%.0f시간전", ((nWriteTime / 60) / 60)];
        }
    }
    
    
    return @"";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return;

    [HapticHelper tap];
    
    SBDGroupChannel *channel = _arM_List[indexPath.row];
    [channel markAsRead];
    [self performSegueWithIdentifier:@"detail" sender:channel];
}


//MARK: SBDChannelDelegate
- (void)channel:(SBDBaseChannel * _Nonnull)sender didReceiveMessage:(SBDBaseMessage * _Nonnull)message
{
    if ([sender isKindOfClass:[SBDGroupChannel class]])
    {
        SBDGroupChannel *messageReceivedChannel = (SBDGroupChannel *)sender;
        if ([_arM_List indexOfObject:messageReceivedChannel] != NSNotFound) {
            [_arM_List removeObject:messageReceivedChannel];
        }
        [_arM_List insertObject:messageReceivedChannel atIndex:0];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tbv_List reloadData];
        });
    }
}

- (void)channelWasChanged:(SBDBaseChannel * _Nonnull)sender
{
    if ([sender isKindOfClass:[SBDGroupChannel class]])
    {
        SBDGroupChannel *messageReceivedChannel = (SBDGroupChannel *)sender;
        
        if ([_arM_List indexOfObject:messageReceivedChannel] != NSNotFound)
        {
            [_arM_List removeObject:messageReceivedChannel];
            [_arM_List insertObject:messageReceivedChannel atIndex:0];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tbv_List reloadData];
            });
        }
    }
}

@end
