//
//  PlayJoinViewController.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/27.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PlayJoinViewController : UIViewController
@property (nonatomic, strong) NSDictionary *dic_Item;
@property (nonatomic, strong) NSString *str_PlayId;
@property (nonatomic, strong) NSArray *ar_Images;
@end

NS_ASSUME_NONNULL_END
