//
//  FollowListViewController.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/16.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum {
    kFollower = 0,
    kFollowing = 1
} FollowMode;

@interface FollowListViewController : UIViewController
@property (nonatomic, assign) FollowMode followMode;
@property (nonatomic, assign) NSInteger nUserId;
@property (nonatomic, strong) NSString *str_UserName;
@end

NS_ASSUME_NONNULL_END
