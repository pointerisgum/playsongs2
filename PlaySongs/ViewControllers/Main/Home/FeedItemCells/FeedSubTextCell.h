//
//  FeedSubTextCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/08.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlaySongs-Swift.h"
#import "FeedBasicCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface FeedSubTextCell : FeedBasicCell

@end

NS_ASSUME_NONNULL_END
