//
//  PlayMakeYoutubeViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/29.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "PlayMakeYoutubeViewController.h"
#import "PlaySongs-Swift.h"

@interface PlayMakeYoutubeViewController () <UITextFieldDelegate>
@property (nonatomic, strong) SwiftUtil *swiftUtil;
@property (nonatomic, strong) NSString *str_VedioId;
@property (weak, nonatomic) IBOutlet UIButton *btn_Done;
@property (weak, nonatomic) IBOutlet UITextField *tf_Url;
@property (weak, nonatomic) IBOutlet UIImageView *iv_Thumb;
@property (weak, nonatomic) IBOutlet UIView *v_TfBg;
@end

@implementation PlayMakeYoutubeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _v_TfBg.layer.cornerRadius = 4.0f;
    _v_TfBg.layer.borderWidth = 1.0f;
    _v_TfBg.layer.borderColor = [UIColor colorWithHexString:@"DCDCDC"].CGColor;
    
    _swiftUtil = [[SwiftUtil alloc] init];
    
    //https://www.youtube.com/watch?v=ZddfRmDMjz4
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//MARK: UITextFieldDelegate
- (void)textFieldDidChangeSelection:(UITextField *)textField
{
    _str_VedioId = [_swiftUtil getYoutubeVideoIdFromLink:textField.text];
    NSLog(@"%@", _str_VedioId);
    if( _str_VedioId.length > 0 )
    {
        _btn_Done.enabled = YES;
        NSString *str_ImagePath = [NSString stringWithFormat:@"https://img.youtube.com/vi/%@/maxresdefault.jpg", _str_VedioId];
        [_iv_Thumb sd_setImageWithURL:[NSURL URLWithString:str_ImagePath] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
            if( image )
            {
                
            }
        }];
        
    }
    else
    {
        _btn_Done.enabled = NO;
    }
}


//MARK: Action
- (IBAction)goDone:(id)sender
{
    if( _str_VedioId.length > 0 )
    {
        [HapticHelper tap];
        
        __weak __typeof(&*self)weakSelf = self;
        if( _iv_Thumb.image == nil )
        {
            [UIAlertController showAlertInViewController:self
                                               withTitle:@""
                                                 message:@"입력하신 URL에 대한\n섬네일을 찾지 못했습니다.\n\n그래도 올리시겠습니까?"
                                       cancelButtonTitle:@"아니요"
                                  destructiveButtonTitle:nil
                                       otherButtonTitles:@[@"예"]
                                                tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                    
                                                    if (buttonIndex >= controller.firstOtherButtonIndex)
                                                    {
                                                        if( weakSelf.completionBlock )
                                                        {
                                                            weakSelf.completionBlock(weakSelf.str_VedioId);
                                                        }
                                                        [self dismissViewControllerAnimated:YES completion:nil];
                                                    }
                                                }];
        }
        else
        {
            if( _completionBlock )
            {
                _completionBlock(_str_VedioId);
            }
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

@end
