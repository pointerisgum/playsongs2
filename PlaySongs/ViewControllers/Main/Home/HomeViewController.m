//
//  HomeViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/07.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "HomeViewController.h"
#import "FeedCell.h"
#import "TopMainTVCell.h"
#import "ODRefreshControl.h"
#import "PlaySongs-Swift.h"
#import "CommentViewController.h"
#import "AddCommentViewController.h"
#import "UserPageViewController.h"
#import "FeedDetailViewController.h"
#import "SharePopUpViewController.h"
#import "PlayJoinViewController.h"
#import "ReportViewController.h"
#import "MakePlayViewController.h"

#define kLimitCount 10

@interface HomeViewController ()
typedef void (^CategoryApiBlock)(void);
@property (nonatomic, strong) CategoryApiBlock categoryApiBlock;

@property (nonatomic, assign) NSInteger nTopScollX;
@property (nonatomic, assign) NSInteger nPage;
@property (nonatomic, assign) BOOL isFirstLoad;
@property (nonatomic, assign) BOOL isFinish;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, strong) NSMutableArray *arM_RecommendList;
@property (nonatomic, strong) NSMutableArray *arM_List;
@property (nonatomic, strong) ODRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UITableView *tbv_List;
- (void)categoryApiRequestWithCompletion:(CategoryApiBlock)block;

@end

@implementation HomeViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //    [self.view makeToast:@"This is a piece of toast."];


    _isFirstLoad = YES;
    _tbv_List.alpha = NO;

    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;

    _nPage = 1;
    _isFinish = NO;
    _isLoading = NO;
    _arM_List = [NSMutableArray array];
    
    _refreshControl = [[ODRefreshControl alloc] initInScrollView:_tbv_List];
    _refreshControl.tintColor = kEnableColor;
    _refreshControl.backgroundColor = [UIColor whiteColor];
    [_refreshControl addTarget:self action:@selector(onRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tbv_List addSubview:_refreshControl];

    [self categoryApiRequestWithCompletion:^() {
        
        [self updateList];
    }];
    [self updateCategoryList];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFeedListNoti) name:@"RefreshFeedListNoti" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFeedNoti:) name:@"UpdateFeedNoti" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showCommentViewNoti:) name:@"ShowCommentViewNoti" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showCommentInPutNoti:) name:@"ShowCommentInPutNoti" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showShareNoti:) name:@"ShowShareNoti" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateOneItemNoti:) name:@"UpdateOneItemNoti" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showFeedDetailNoti:) name:@"ShowFeedDetailNoti" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showPlayNoti:) name:@"ShowPlayNoti" object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UpdateFeedNoti" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ShowCommentViewNoti" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ShowCommentInPutNoti" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ShowShareNoti" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UpdateOneItemNoti" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ShowFeedDetailNoti" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ShowPlayNoti" object:nil];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if( [segue.identifier isEqualToString:@"InBoxViewController"] )
    {
        [HapticHelper tap];
    }
    else if( [segue.identifier isEqualToString:@"CommentSegue"] )
    {
        CommentViewController *vc = segue.destinationViewController;
        vc.str_PlayId = sender;
    }
    else if( [segue.identifier isEqualToString:@"AddCommentSegue"] )
    {
        AddCommentViewController *vc = segue.destinationViewController;
        vc.str_PlayId = sender;
    }
    else if( [segue.identifier isEqualToString:@"DetailSegue"] )
    {
        FeedDetailViewController *vc = segue.destinationViewController;
        vc.str_PlayId = sender;
    }
    else if( [segue.identifier isEqualToString:@"ShareSegue"] )
    {
        SharePopUpViewController *vc = segue.destinationViewController;
        vc.str_PlayId = [sender stringForKey:@"playId"];
        vc.dic_Item = [sender valueForDictionaryKey:@"obj"];
    }
}


- (NSInteger)getItemIndex:(NSInteger)playId
{
    for( NSInteger i = 0; i < _arM_List.count; i++ )
    {
        NSDictionary *dic_Sub = _arM_List[i];
        if( playId == [dic_Sub integerForKey:@"playId"] )
        {
            return i;
        }
    }
    
    return -1;
}

- (void)updateFeedNoti:(NSNotification *)noti
{
    NSDictionary *dic = noti.object;
    NSInteger nTargetPlayId = [dic integerForKey:@"playId"];
    NSInteger nFindIdx = [self getItemIndex:nTargetPlayId];
    
//    [_tbv_List reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:nFindIdx inSection:1]] withRowAnimation:UITableViewRowAnimationNone];
    [_arM_List replaceObjectAtIndex:nFindIdx withObject:dic];
    [_tbv_List reloadData];
}

- (void)showCommentViewNoti:(NSNotification *)noti
{
    [self performSegueWithIdentifier:@"CommentSegue" sender:noti.object];
}

- (void)showCommentInPutNoti:(NSNotification *)noti
{
    [self performSegueWithIdentifier:@"AddCommentSegue" sender:noti.object];
}

- (void)showShareNoti:(NSNotification *)noti
{
    [self performSegueWithIdentifier:@"ShareSegue" sender:noti.object];
}


- (void)updateOneItemNoti:(NSNotification *)noti
{
    NSString *str_PlayId = noti.object;
    NSInteger nFindIdx = [self getItemIndex:[str_PlayId integerValue]];
    [self updateOneItem:str_PlayId withIdx:nFindIdx];
}

- (void)updateOneItem:(NSString *)str_PlayId withIdx:(NSInteger)nIdx
{
    __weak __typeof__(self) weakSelf = self;
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [[WebAPI sharedData] callAsyncWebAPIBlock:[NSString stringWithFormat:@"feed/%@", str_PlayId] param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            NSDictionary *dic_Result = [resulte valueForDictionaryKey:@"result"];
            
            if( IsArrayOutOfBounds(weakSelf.arM_List, nIdx) == NO )
            {
                [weakSelf.arM_List replaceObjectAtIndex:nIdx withObject:dic_Result];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf.tbv_List reloadData];
                });
            }
        }
    }];
}

- (void)showFeedDetailNoti:(NSNotification *)noti
{
    [self performSegueWithIdentifier:@"DetailSegue" sender:noti.object];
}

- (void)showPlayNoti:(NSNotification *)noti
{
    __weak __typeof__(self) weakSelf = self;
    __block NSString *str_PlayId = [noti.object stringForKey:@"playId"];
    __block NSDictionary *dic = [noti.object valueForDictionaryKey:@"obj"];

    InstarPicker *instarPicker = [[InstarPicker alloc] init];
    [instarPicker setMultiCompletionHandler:^(NSArray * _Nonnull array) {
        
        if( array.count > 0 )
        {
            PlayJoinViewController *vc = [kFeedBoard instantiateViewControllerWithIdentifier:@"PlayJoinViewController"];;
            vc.str_PlayId = str_PlayId;
            vc.dic_Item = dic;
            vc.ar_Images = array;
            [weakSelf presentViewController:vc animated:YES completion:nil];
        }
    }];
    
    YPImagePicker *pickerNavi = [instarPicker getMultiPhotoWithCount:5];
    [self presentViewController:(UINavigationController *)pickerNavi animated:YES completion:nil];
}

- (void)refreshFeedListNoti
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [_tbv_List scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];

    _nPage = 1;
    _isFinish = NO;
    _isLoading = NO;
    [_arM_List removeAllObjects];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        [self updateList];
    });
}

- (void)onRefresh:(ODRefreshControl *)sender
{
    [HapticHelper tap];
    
//    [WebAPI sharedData] cancell
    _nPage = 1;
    _isFinish = NO;
    _isLoading = NO;
    [_arM_List removeAllObjects];
    [_arM_RecommendList removeAllObjects];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        [self categoryApiRequestWithCompletion:^() {
            
            [self updateList];
        }];
        [self updateCategoryList];
    });
}

//MARK: Block
- (void)categoryApiRequestWithCompletion:(CategoryApiBlock)block
{
    self.categoryApiBlock = block;
}

- (void)updateCategoryList
{
    __weak __typeof__(self) weakSelf = self;
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"recommend/list" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            NSArray *ar = [resulte valueForArrayKey:@"result"];
            if( IsArrayEmpty(ar) == NO )
            {
                weakSelf.arM_RecommendList = [NSMutableArray arrayWithArray:ar];
            }
        }
        
        if (weakSelf.categoryApiBlock)
        {
            weakSelf.categoryApiBlock();
            weakSelf.categoryApiBlock = nil;
        }
    }];
}

- (void)updateList
{
    if( _isFinish )     return;
    if( _isLoading )    return;
    
    _isLoading = YES;
    
    __weak __typeof__(self) weakSelf = self;

    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setValue:@(_nPage) forKey:@"page"];
//    [dicM_Params setValue:@(9) forKey:@"page"];
    [dicM_Params setValue:@(kLimitCount) forKey:@"limt"];
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"feed/list" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
                
        [MBProgressHUD hide];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.refreshControl endRefreshing];
        });

        if( error != nil )
        {
            weakSelf.isLoading = NO;
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            NSArray *ar = [resulte valueForArrayKey:@"result"];
            if( ar.count > 0 )
            {
                if( weakSelf.arM_List.count > 0 )
                {
                    [weakSelf.arM_List addObjectsFromArray:ar];
                }
                else
                {
                    weakSelf.arM_List = [NSMutableArray arrayWithArray:ar];
                }
            }
            else
            {
                //finish
                weakSelf.isFinish = YES;
            }
        }
        
        
//        NSRange range = NSMakeRange(1, 1);
//        NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if( weakSelf.isFirstLoad )
            {
                weakSelf.isFirstLoad = NO;
                [weakSelf.tbv_List reloadData];
                [weakSelf.view layoutIfNeeded];

                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [UIView animateWithDuration:0.1f animations:^{
                        weakSelf.tbv_List.alpha = YES;
                    } completion:^(BOOL finished) {
                        [weakSelf.indicator stopAnimating];
                    }];
                });
            }
            else
            {
                [weakSelf.tbv_List reloadData];
                [weakSelf.view layoutIfNeeded];
            }
        });
        weakSelf.isLoading = NO;
    }];
}


//MARK: TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if( section == 0 )
    {
        return 1;
    }
    return _arM_List.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.section == 0 )
    {
        __weak __typeof__(self) weakSelf = self;

        TopMainTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TopMainTVCell"];
        [cell initTopCell:self.arM_RecommendList];
        [cell setCompletionBlock:^(id  _Nonnull completeResult) {
           
            NSString *str_PlayId = [NSString stringWithFormat:@"%ld", [completeResult integerForKey:@"playId"]];
            [weakSelf performSegueWithIdentifier:@"DetailSegue" sender:str_PlayId];
        }];

        return cell;
    }

    FeedCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FeedCell"];
    
    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return cell;
    
    NSDictionary *dic = _arM_List[indexPath.row];
    if( dic == nil )
    {
        return cell;
    }
    
    if( [dic isKindOfClass:[NSDictionary class]] == NO )
    {
        return cell;
    }
    
    [cell initImageList:dic withIndex:indexPath.row];

    NSString *str_PlayType = [dic stringForKey:@"playType"];
    BOOL isJoinType = [str_PlayType isEqualToString:@"J"];

    //오너 정보
    NSDictionary *dic_Owner = [dic valueForDictionaryKey:@"owner"];
    NSString *str_OwnerImageUrl = [dic_Owner stringForKey:@"imgUrl"];
    [cell.iv_OwnerThumb sd_setImageWithString:str_OwnerImageUrl];
    cell.iv_OwnerThumb.tag = indexPath.row;
    UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userImageTap:)];
    [imageTap setNumberOfTapsRequired:1];
    [cell.iv_OwnerThumb addGestureRecognizer:imageTap];
    cell.lb_OwnerName.text = [dic_Owner stringForKey:@"name"];
        
    cell.btn_More.tag = indexPath.row;
    [cell.btn_More removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
    [cell.btn_More addTarget:self action:@selector(onMoreMenu:) forControlEvents:UIControlEventTouchUpInside];

    
    if( isJoinType )
    {
        cell.lb_OwnerSubTitle.hidden = YES;
    }
    else
    {
        cell.lb_OwnerSubTitle.hidden = NO;
        cell.lb_OwnerSubTitle.text = [dic stringForKey:@"title"];
    }

    
    //조인뷰
    if( isJoinType )
    {
        cell.v_Join.hidden = NO;
        
        cell.v_Join.tag = indexPath.row;
        UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playDetailTap:)];
        [imageTap setNumberOfTapsRequired:1];
        [cell.v_Join addGestureRecognizer:imageTap];

//        cell.iv_JoinThumb.tag = indexPath.row;
//        imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playUserTap:)];
//        [imageTap setNumberOfTapsRequired:1];
//        [cell.iv_JoinThumb addGestureRecognizer:imageTap];
        
        NSDictionary *dic_JoinInfo = [dic valueForDictionaryKey:@"playJoin"];
        NSString *str_ImageUrl = [dic_JoinInfo stringForKey:@"coverUrl"];
        [cell.iv_JoinThumb sd_setImageWithString:str_ImageUrl];
        cell.lb_JoinTitle.text = [dic_JoinInfo stringForKey:@"title"];
        cell.lb_JoinSubTitle.text = [dic_JoinInfo stringForKey:@"name"];
        
        NSString *str_JoinCnt = [NSString stringWithFormat:@"%ld", [dic_JoinInfo integerForKey:@"playing"]];
        [cell.btn_JoinPlay setTitle:str_JoinCnt forState:UIControlStateNormal];
        cell.btn_JoinPlay.selected = YES;
        
        cell.lc_PgBottom.constant = 90;
    }
    else
    {
        cell.v_Join.hidden = YES;
        
        cell.lc_PgBottom.constant = 30;
    }

    
    //버튼뷰
    NSString *str_IsHeart = [dic stringForKey:@"isHeart"];
    cell.btn_Heart.selected = [str_IsHeart isEqualToString:@"Y"];
    
    cell.btn_ButtonPlay.hidden = isJoinType;
    NSDictionary *dic_Count = [dic valueForDictionaryKey:@"count"];
    NSInteger nPlayingCnt = [dic_Count integerForKey:@"playing"];
    [cell.btn_ButtonPlay setTitle:[NSString stringWithFormat:@"%ld", nPlayingCnt] forState:UIControlStateNormal];
    cell.btn_ButtonPlay.selected = [[dic stringForKey:@"isPlaying"] isEqualToString:@"Y"] ? YES : NO;

    //actions
    cell.btn_Heart.tag = indexPath.row;
    [cell.btn_Heart removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
    [cell.btn_Heart addTarget:self action:@selector(onLike:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btn_Comment.tag = indexPath.row;
    [cell.btn_Comment removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
    [cell.btn_Comment addTarget:self action:@selector(onComment:) forControlEvents:UIControlEventTouchUpInside];

    cell.btn_Share.tag = indexPath.row;
    [cell.btn_Share removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
    [cell.btn_Share addTarget:self action:@selector(onShare:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btn_ButtonPlay.tag = indexPath.row;
    [cell.btn_ButtonPlay removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
    if( cell.btn_ButtonPlay.selected == NO )
    {
        //참여하지 않은 놀이만 참여 할 수 있게
        [cell.btn_ButtonPlay addTarget:self action:@selector(onPlay:) forControlEvents:UIControlEventTouchUpInside];
    }

    
    //하트
    NSInteger nHeartCnt = [dic_Count integerForKey:@"heart"];
    if( nHeartCnt > 0 )
    {
        //총 하트수가 0보다 클 경우에만 표시
        cell.v_Heart.hidden = NO;
        cell.lb_Heart.text = [NSString stringWithFormat:@"좋아요 %ld개", nHeartCnt];
    }
    else
    {
        cell.v_Heart.hidden = YES;
    }

    
    //태그
    if( isJoinType == NO )
    {
        NSArray *ar_Tags = [Util jsonStringToArray:[dic stringForKey:@"tags"]];
        if( ar_Tags.count > 0 )
        {
            cell.cv_Tag.hidden = NO;
            cell.lc_TagHeight.constant = 0;
            [cell initTagCell:ar_Tags withIndex:indexPath.row withParent:self];
//            [cell layoutIfNeeded];
//            [cell updateConstraintsIfNeeded];
//            [cell.contentView layoutIfNeeded];
//            [cell.contentView updateConstraintsIfNeeded];
        }
        else
        {
            cell.cv_Tag.hidden = YES;
        }
    }
    else
    {
        cell.cv_Tag.hidden = YES;
    }
    
    
    //내용
    NSString *str_Contents = [dic stringForKey:@"summary"];
    if( str_Contents.length > 0 )
    {
        if( isJoinType )
        {
            cell.v_Contents.hidden = NO;
            cell.v_ContentsMore.hidden = YES;
            cell.lb_Contents.text = [dic stringForKey:@"summary"];
            
            NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        cell.lb_Contents.font, NSFontAttributeName, nil];
            
            CGRect textRect = [cell.lb_Contents.text boundingRectWithSize:CGSizeMake(kDeviceWidth - 30, FLT_MAX)
                                                                  options:NSLineBreakByClipping | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                                               attributes:attributes
                                                                  context:nil];
            cell.lc_ContentsHeight.constant = textRect.size.height + 2;

        }
        else
        {
            cell.v_Contents.hidden = YES;
            cell.v_ContentsMore.hidden = NO;
            cell.lb_ContentsMore.text = [dic stringForKey:@"summary"];
            cell.lb_ContentsMore.collapsed = true;
            
            NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        cell.lb_ContentsMore.font, NSFontAttributeName, nil];
            
            CGRect textRect = [cell.lb_ContentsMore.text boundingRectWithSize:CGSizeMake(kDeviceWidth - 30, FLT_MAX)
                                                                  options:NSLineBreakByClipping | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                                               attributes:attributes
                                                                  context:nil];
            cell.lc_ContentsMoreHeight.constant = textRect.size.height > 17 ? textRect.size.height + 2 : textRect.size.height + 2;
        }
    }
    else
    {
        cell.v_Contents.hidden = YES;
        cell.v_ContentsMore.hidden = YES;
    }
    
    
    //댓글 모두 보기
    NSInteger nCommentCnt = [dic_Count integerForKey:@"comment"];
    if( nCommentCnt > 2 )
    {
        cell.v_CommentAllShow.hidden = NO;
        cell.lb_CommentAllShow.text = [NSString stringWithFormat:@"댓글 %ld개 모두 보기", nCommentCnt];
        
        cell.btn_CommentAllShow.tag = indexPath.row;
        [cell.btn_CommentAllShow removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
        [cell.btn_CommentAllShow addTarget:self action:@selector(onComment:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        cell.v_CommentAllShow.hidden = YES;
    }
        
    
    //댓글
    cell.v_Comment1.hidden = YES;
    cell.v_Comment2.hidden = YES;
    NSArray *ar_CommentList = [dic valueForArrayKey:@"commentList"];
    NSInteger nShowCommentCnt = ar_CommentList.count;
    if( nShowCommentCnt > 2 )
    {
        nShowCommentCnt = 2;
    }
    for( NSInteger i = 0; i < nShowCommentCnt; i++ )
    {
        NSDictionary *dic_Comment = ar_CommentList[i];
        NSString *str_Name = [dic_Comment stringForKey:@"name"];
        NSString *str_Contents = [NSString stringWithFormat:@"  %@", [dic_Comment stringForKey:@"comment"]];
        NSMutableAttributedString *attM = [[NSMutableAttributedString alloc] initWithString:str_Name attributes:@{NSForegroundColorAttributeName:[UIColor blackColor],
                                                                                                                  NSFontAttributeName:[UIFont systemFontOfSize:14 weight:UIFontWeightBold]}];
        NSAttributedString *att = [[NSAttributedString alloc] initWithString:str_Contents attributes:@{ NSForegroundColorAttributeName:[UIColor darkGrayColor],
                                                                                                                     NSFontAttributeName:[UIFont systemFontOfSize:14 weight:UIFontWeightMedium]}];
        [attM appendAttributedString:att];
        if( i == 0 )
        {
            cell.v_Comment1.hidden = NO;
            cell.lb_Comment1.attributedText = attM;
            
            cell.btn_Comment1.tag = indexPath.row;
            [cell.btn_Comment1 removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
            [cell.btn_Comment1 addTarget:self action:@selector(onComment:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            cell.v_Comment2.hidden = NO;
            cell.lb_Comment2.attributedText = attM;
            
            cell.btn_Comment2.tag = indexPath.row;
            [cell.btn_Comment2 removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
            [cell.btn_Comment2 addTarget:self action:@selector(onComment:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    
    //댓글 달기
    [cell.iv_AddCommentThumb sd_setImageWithString:[UserData sharedData].imgUrl];
    cell.btn_AddComment.tag = indexPath.row;
    [cell.btn_AddComment removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
    [cell.btn_AddComment addTarget:self action:@selector(onAddComment:) forControlEvents:UIControlEventTouchUpInside];
    
    
    //시간
    NSString *str_Date = [NSString stringWithFormat:@"%@", [dic stringForKey:@"createDate"]];
    cell.lb_Time.text = [Util getCommentAfterTime:str_Date];
    
    
    
//
//    [cell setUserImageTapBlock:^(id  _Nonnull completionBlock) {
//
//        [HapticHelper tap];
//
//        NSInteger nIdx = [completionBlock integerValue];
//        NSDictionary *dic_Temp = self.arM_List[nIdx];
//        NSDictionary *dic_Owner = [dic_Temp valueForDictionaryKey:@"owner"];
//        UserPageViewController *vc = [kMainBoard instantiateViewControllerWithIdentifier:@"UserPageViewController"];
//        vc.dic_Owner = dic_Owner;
//        [self.navigationController pushViewController:vc animated:YES];
//    }];

    BOOL lastItemReached = [dic isEqual:[_arM_List lastObject]];
    if (lastItemReached && indexPath.row == [_arM_List count] - 1)
    {
        _nPage++;
        [self updateList];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    [HapticHelper tap];
    
    NSDictionary *dic = self.arM_List[indexPath.row];
//    NSString *str_PlayType = [dic stringForKey:@"playType"];
//    NSString *str_PlayId = [NSString stringWithFormat:@"%ld", [str_PlayType isEqualToString:@"J"] ? [dic integerForKey:@"joinPlayId"] : [dic integerForKey:@"playId"]];
    NSString *str_PlayId = [NSString stringWithFormat:@"%ld", [dic integerForKey:@"playId"]];
    [self performSegueWithIdentifier:@"DetailSegue" sender:str_PlayId];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.section == 0 )
    {
        UICollectionView *cv = [cell.contentView viewWithTag:10];
        if( cv )
        {
            cv.contentOffset = CGPointMake(_nTopScollX, cv.contentOffset.y);
        }
    }
    else if( indexPath.section == 1 )
    {
        if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return;
        
        NSDictionary *dic = _arM_List[indexPath.row];
        FeedCell *feedCell = (FeedCell *)cell;
        feedCell.cv_List.contentOffset = CGPointMake([dic integerForKey:@"offset"], feedCell.cv_List.contentOffset.y);
    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    if( indexPath.section == 0 )
    {
        UICollectionView *cv = [cell.contentView viewWithTag:10];
        if( cv )
        {
            _nTopScollX = cv.contentOffset.x;
        }
    }
    else if( indexPath.section == 1 )
    {
        if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return;
        
        FeedCell *feedCell = (FeedCell *)cell;
        NSMutableDictionary *dicM = _arM_List[indexPath.row];
        [dicM setObject:@(feedCell.cv_List.contentOffset.x) forKey:@"offset"];
        [_arM_List replaceObjectAtIndex:indexPath.row withObject:dicM];
    }
}


//MARK: Action
- (void)playDetailTap:(UIGestureRecognizer *)gestureRecognizer
{
    [HapticHelper tap];
    
    UIView *view = gestureRecognizer.view;
    NSDictionary *dic_Temp = _arM_List[view.tag];
    NSDictionary *dic_Join = [dic_Temp valueForDictionaryKey:@"playJoin"];
    FeedDetailViewController *vc = [kFeedBoard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
    vc.str_PlayId = [NSString stringWithFormat:@"%ld", [dic_Join integerForKey:@"playId"]];
    [self.navigationController pushViewController:vc animated:YES];
}

//- (void)playUserTap:(UIGestureRecognizer *)gestureRecognizer
//{
//    [HapticHelper tap];
//
//    UIView *view = gestureRecognizer.view;
//    NSDictionary *dic_Temp = _arM_List[view.tag];
//    NSDictionary *dic_Owner = [dic_Temp valueForDictionaryKey:@"owner"];
//    UserPageViewController *vc = [kUserBoard instantiateViewControllerWithIdentifier:@"UserPageViewController"];
//    vc.dic_Owner = dic_Owner;
//    [self.navigationController pushViewController:vc animated:YES];
//}

- (void)userImageTap:(UIGestureRecognizer *)gestureRecognizer
{
    [HapticHelper tap];
    
    UIView *view = gestureRecognizer.view;
    NSDictionary *dic_Temp = _arM_List[view.tag];
    NSDictionary *dic_Owner = [dic_Temp valueForDictionaryKey:@"owner"];
    UserPageViewController *vc = [kUserBoard instantiateViewControllerWithIdentifier:@"UserPageViewController"];
    vc.dic_Owner = dic_Owner;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onLike:(UIButton *)btn
{
    [HapticHelper tap];
    
    NSDictionary *dic_Temp = _arM_List[btn.tag];
    __block NSMutableDictionary *dicM = [NSMutableDictionary dictionaryWithDictionary:dic_Temp];
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setValue:@([dicM integerForKey:@"playId"]) forKey:@"playId"];
    [dicM_Params setValue:btn.selected?@"N":@"Y" forKey:@"status"];
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"play/heart/save" param:dicM_Params withMethod:@"POST" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            NSDictionary *dic_Result = [resulte dictionaryForKey:@"result"];

            //좋아요 토글 상태 업데이트
            NSString *str_Status = [dic_Result stringForKey:@"status"];
            [dicM setObject:str_Status forKey:@"isHeart"];

            //좋아요 카운트 업데이트
            NSMutableDictionary *dicM_Count = [NSMutableDictionary dictionaryWithDictionary:[dicM valueForDictionaryKey:@"count"]];
            NSInteger nHeartCnt = [dicM_Count integerForKey:@"heart"];
            if( [str_Status isEqualToString:@"Y"] )
            {
                nHeartCnt ++;
            }
            else
            {
                nHeartCnt--;
            }
            [dicM_Count setObject:@(nHeartCnt) forKey:@"heart"];
            [dicM setObject:dicM_Count forKey:@"count"];

//            [weakSelf.tbv_List reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:btn.tag inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateFeedNoti" object:dicM];
        }
    }];
}

- (void)onComment:(UIButton *)btn
{
    [HapticHelper tap];

    NSDictionary *dic = _arM_List[btn.tag];
    NSString *str_PlayId = [NSString stringWithFormat:@"%ld", [dic integerForKey:@"playId"]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowCommentViewNoti" object:str_PlayId];
}

- (void)onAddComment:(UIButton *)btn
{
    [HapticHelper tap];

    NSDictionary *dic = _arM_List[btn.tag];
    NSString *str_PlayId = [NSString stringWithFormat:@"%ld", [dic integerForKey:@"playId"]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowCommentInPutNoti" object:str_PlayId];
}

- (void)onShare:(UIButton *)btn
{
    [HapticHelper tap];

    NSDictionary *dic = _arM_List[btn.tag];
    NSString *str_PlayType = [dic stringForKey:@"playType"];
    NSString *str_PlayId = @"";
    if( [str_PlayType isEqualToString:@"J"] )
    {
        str_PlayId = [NSString stringWithFormat:@"%ld", [dic integerForKey:@"joinPlayId"]];
    }
    else
    {
        str_PlayId = [NSString stringWithFormat:@"%ld", [dic integerForKey:@"playId"]];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowShareNoti" object:@{@"playId":str_PlayId, @"obj":dic}];
}

- (void)onPlay:(UIButton *)btn
{
    [HapticHelper tap];

    NSDictionary *dic = _arM_List[btn.tag];
    NSString *str_PlayId = str_PlayId = [NSString stringWithFormat:@"%ld", [dic integerForKey:@"playId"]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowPlayNoti" object:@{@"playId":str_PlayId, @"obj":dic}];
}

- (IBAction)goLogoTap:(id)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [_tbv_List scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)onMoreMenu:(UIButton *)btn
{
    [HapticHelper tap];
    
    __weak __typeof__(self) weakSelf = self;

    __block NSDictionary *dic = _arM_List[btn.tag];
    NSDictionary *dic_Owner = [dic valueForDictionaryKey:@"owner"];
    NSInteger nOwnerId = [dic_Owner integerForKey:@"userId"];
    BOOL isMy = NO;
    if( nOwnerId == [UserData sharedData].userId )
    {
        isMy = YES;
    }
    
    SPAlertController *alert = [SPAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:SPAlertControllerStyleActionSheet];
    
    if( isMy )
    {
        SPAlertAction *action = [SPAlertAction actionWithTitle:@"삭제" style:SPAlertActionStyleDestructive
                                         handler:^(SPAlertAction * _Nonnull action) {
            [weakSelf deleteItem:btn];
        }];
        [alert addAction:action];

        NSString *str_PlayType = [dic stringForKey:@"playType"];
        BOOL isJoinType = [str_PlayType isEqualToString:@"J"];
        if( isJoinType == NO )
        {
            action = [SPAlertAction actionWithTitle:@"수정" style:SPAlertActionStyleDefault
                                            handler:^(SPAlertAction * _Nonnull action) {
                [weakSelf modify:btn];
            }];
            [alert addAction:action];
        }
    }
    else
    {
        SPAlertAction *action = [SPAlertAction actionWithTitle:@"신고" style:SPAlertActionStyleDestructive
                                         handler:^(SPAlertAction * _Nonnull action) {
            [self reportItem:btn];
        }];
        [alert addAction:action];
    }
    
    SPAlertAction *action = [SPAlertAction actionWithTitle:@"링크복사" style:SPAlertActionStyleDefault
                                                    handler:^(SPAlertAction * _Nonnull action) {

        [HapticHelper tap];
        NSString *str_ShareKey = [dic stringForKey:@"shareKey"];
        //https://www.playhavea.com/p/SqqmghLk9OPWD32f?utm_source=web_button_share
        //https://www.playhavea.com/p/aHQPFNARpDuYdsug?utm_source=web_button_share
        NSString *str_Link = [NSString stringWithFormat:@"%@/p/%@?utm_source=web_button_share", kDomain, str_ShareKey];
        [UIPasteboard generalPasteboard].string = str_Link;
        [weakSelf dismissViewControllerAnimated:YES completion:^{
            [weakSelf.navigationController.view makeToast:@"링크를 클립보드에 복사했습니다."];
        }];
    }];
    [alert addAction:action];

    
    action = [SPAlertAction actionWithTitle:@"공유대상..." style:SPAlertActionStyleDefault
                                                    handler:^(SPAlertAction * _Nonnull action) {
        
        [HapticHelper tap];
        NSString *str_ShareKey = [dic stringForKey:@"shareKey"];
        NSString *str_Link = [NSString stringWithFormat:@"%@/p/%@?utm_source=web_button_share", kDomain, str_ShareKey];

        NSArray* sharedObjects=[NSArray arrayWithObjects:str_Link, nil];
        UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:sharedObjects applicationActivities:nil];
        activityViewController.popoverPresentationController.sourceView = self.view;
        [weakSelf presentViewController:activityViewController animated:YES completion:nil];
    }];
    [alert addAction:action];

    
    if( isMy )
    {
//        SPAlertAction *action = [SPAlertAction actionWithTitle:@"공유" style:SPAlertActionStyleDefault
//                                         handler:^(SPAlertAction * _Nonnull action) {
//            [HapticHelper tap];
//            [weakSelf onShare:btn];
//        }];
//        [alert addAction:action];
    }
    else
    {
        if( [[dic stringForKey:@"isFollowing"] isEqualToString:@"Y"] )
        {
            SPAlertAction *action = [SPAlertAction actionWithTitle:@"팔로우 취소" style:SPAlertActionStyleDefault
                                             handler:^(SPAlertAction * _Nonnull action) {[self reqFollow:NO withIdx:btn.tag];}];
            [alert addAction:action];
        }
        else
        {
            SPAlertAction *action = [SPAlertAction actionWithTitle:@"팔로우" style:SPAlertActionStyleDefault
                                             handler:^(SPAlertAction * _Nonnull action) {[self reqFollow:YES withIdx:btn.tag];}];
            [alert addAction:action];
        }
    }
    
    action = [SPAlertAction actionWithTitle:@"취소" style:SPAlertActionStyleCancel
                                                    handler:^(SPAlertAction * _Nonnull action) {}];
    [alert addAction:action];
    [self presentViewController: alert animated:YES completion:^{}];
}

- (void)deleteItem:(UIButton *)btn
{
    __weak __typeof__(self) weakSelf = self;
    __block NSDictionary *dic = _arM_List[btn.tag];

    [HapticHelper tap];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        [UIAlertController showAlertInViewController:self
                                           withTitle:@""
                                             message:@"삭제 하시겠습니까?"
                                   cancelButtonTitle:@"아니요"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@[@"예"]
                                            tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                
                                                if (buttonIndex >= controller.firstOtherButtonIndex)
                                                {
                                                    NSString *str_Path = [NSString stringWithFormat:@"play/%ld", [dic integerForKey:@"playId"]];
                                                    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
                                                    [dicM_Params setObject:@([UserData sharedData].userId) forKey:@"userId"];
                                                    
                                                    [[WebAPI sharedData] callAsyncWebAPIBlock:str_Path param:dicM_Params withMethod:@"DELETE" withBlock:^(id resulte, NSError *error) {
                                                        
                                                        [MBProgressHUD hide];
                                                        
                                                        if( error != nil )
                                                        {
                                                            return;
                                                        }
                                                        
                                                        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
                                                        if( [str_Success isEqualToString:@"success"] )
                                                        {
                                                            [weakSelf onRefresh:nil];
                                                        }
                                                    }];
                                                }
                                            }];
    });
}

- (void)reportItem:(UIButton *)btn
{
    [HapticHelper tap];

    NSDictionary *dic = self.arM_List[btn.tag];
    NSString *str_PlayId = [NSString stringWithFormat:@"%ld", [dic integerForKey:@"playId"]];
    
    ReportViewController *vc = [kFeedBoard instantiateViewControllerWithIdentifier:@"ReportViewController"];
    vc.str_PlayId = str_PlayId;
    [vc setCompletionBlock:^(id  _Nonnull completeResult) {
       
        [self.arM_List removeObject:dic];
        [self.tbv_List reloadData];
    }];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)reqFollow:(BOOL)isFollow withIdx:(NSInteger)nIdx
{
    __weak __typeof__(self) weakSelf = self;
    __block NSDictionary *dic = _arM_List[nIdx];
    NSDictionary *dic_Owner = [dic valueForDictionaryKey:@"owner"];
    NSString *str_Path = isFollow ? @"user/follow": @"user/unfollow";
    
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@([dic_Owner integerForKey:@"userId"]) forKey:@"following"];
    [[WebAPI sharedData] callAsyncWebAPIBlock:str_Path param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            [weakSelf.view makeToast:isFollow?@"팔로우":@"팔로우 취소함"];
            
            NSString *str_PlayId = [NSString stringWithFormat:@"%ld", [dic integerForKey:@"playId"]];
            [weakSelf updateOneItem:str_PlayId withIdx:nIdx];
            [weakSelf onRefresh:nil];
        }
    }];
}

- (void)modify:(UIButton *)btn
{
    NSDictionary *dic = _arM_List[btn.tag];
    MakePlayViewController *vc = [kPlayMakeBoard instantiateViewControllerWithIdentifier:@"MakePlayViewController"];
    vc.dic_Modify = dic;
    [self presentViewController:vc animated:YES completion:^{
        
    }];
}

@end
