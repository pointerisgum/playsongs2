//
//  FeedSubButtonCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/08.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FeedSubButtonCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIStackView *stv;
@property (weak, nonatomic) IBOutlet UIButton *btn_Heart;
@property (weak, nonatomic) IBOutlet UIButton *btn_Comment;
@property (weak, nonatomic) IBOutlet UIButton *btn_Share;
@property (weak, nonatomic) IBOutlet UIButton *btn_Play;

@end

NS_ASSUME_NONNULL_END
