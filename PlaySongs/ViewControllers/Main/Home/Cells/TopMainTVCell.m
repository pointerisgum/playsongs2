//
//  TopMainTVCell.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/07.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "TopMainTVCell.h"
#import "MainTopCell.h"

@implementation TopMainTVCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initTopCell:(NSArray *)ar
{
    _ar_List = ar;
    [_cv_List reloadData];
}

//- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    collectionView.contentOffset = CGPointMake(100, collectionView.contentOffset.y);
//}
//
//- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//}

//MARK: CollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _ar_List.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MainTopCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MainTopCell" forIndexPath:indexPath];

    NSDictionary *dic = _ar_List[indexPath.row];
    cell.lb_Title.text = [dic stringForKey:@"title"];
    NSString *str_ThumbUrl = [dic stringForKey:@"coverUrl"];
    cell.iv_Thumb.image = BundleImage(@"");
    [cell.iv_Thumb sd_setImageWithURL:[NSURL URLWithString:str_ThumbUrl] placeholderImage:[UIImage imageNamed:@"ic_no_image"]];
////    http://social.playsongs.ai/upload/33ff3cf1-4b3a-4f18-be3f-14c6f0eaa531.jpg
//    
//    [cell.iv_Thumb sd_setImageWithURL:[NSURL URLWithString:@"http://social.playsongs.ai/upload/33ff3cf1-4b3a-4f18-be3f-14c6f0eaa531.jpg"] placeholderImage:nil options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//        if (image)
//        {
//            // Set your image over here
//           }else{
//               //something went wrong
//           }
//
//    }];


    return cell;
}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
////    CGFloat minSize = collectionView.frame.size.width * 0.7f;//MIN(collectionView.frame.size.width, collectionView.frame.size.height);
////    return CGSizeMake(minSize * 1.03, minSize);
////    //    return CGSizeMake([str_Title sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:14.f]}].width + 30.f, 50.f);
//
//    return CGSizeMake(collectionView.frame.size.width, collectionView.frame.size.height);
//}
//
//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsZero;
////    CGFloat minSize = collectionView.frame.size.width * 0.7f;//MIN(collectionView.frame.size.width, collectionView.frame.size.height);
////
//////    return UIEdgeInsetsMake(20, (collectionView.frame.size.width - minSize) / 2, 20, 16*self.ar_CvEventList.count);
////    return UIEdgeInsetsMake(20, (collectionView.frame.size.width - minSize) / 2, 20, (16*self.ar_CvEventList.count) + ((collectionView.frame.size.width - minSize) / 2)/2);
//}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = _ar_List[indexPath.row];
    if( _completionBlock )
    {
        _completionBlock(dic);
    }
//    NSDictionary *dic = self.ar_CvEventList[indexPath.row];
//    [self performSegueWithIdentifier:@"goShowEvent" sender:[dic stringForKey:@"fesNo"]];
}

@end
