//
//  ClausePopUpViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/06.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "ClausePopUpViewController.h"
#import "TLRadioButton.h"
#import "ClauseWebViewController.h"

@interface ClausePopUpViewController ()

@property (weak, nonatomic) IBOutlet UIView *v_Contents;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_ContentsHeight;
@property (weak, nonatomic) IBOutlet TLRadioButton *btn_All;
@property (weak, nonatomic) IBOutlet TLRadioButton *btn_RadioReq1;
@property (weak, nonatomic) IBOutlet TLRadioButton *btn_RadioReq2;
@property (weak, nonatomic) IBOutlet TLRadioButton *btn_RadioOpt1;
@property (weak, nonatomic) IBOutlet TLRadioButton *btn_RadioOptSub1;
@property (weak, nonatomic) IBOutlet TLRadioButton *btn_RadioOptSub2;
@property (weak, nonatomic) IBOutlet TLRadioButton *btn_RadioOptSub3;
@property (weak, nonatomic) IBOutlet UIButton *btn_Next;

@end

@implementation ClausePopUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _btn_Next.enabled = NO;
    _lc_ContentsHeight.constant = 0;
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:_v_Contents.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    _v_Contents.layer.mask = maskLayer;

    _btn_Next.layer.cornerRadius = 4.0f;
    _btn_Next.clipsToBounds = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
 
    _lc_ContentsHeight.constant = [Util keyWindow].safeAreaInsets.bottom + (349 + 15);
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if( [NSStringFromClass(segue.destinationViewController.class) isEqualToString:@"ClauseWebViewController"] )
    {
        [HapticHelper tap];
        
        ClauseWebViewController *vc = segue.destinationViewController;
        if( [segue.identifier isEqualToString:@"Req1Segue"] )
        {
            vc.str_Title = @"이용약관";
            vc.str_Url = @"https://m.naver.com";
        }
        else if( [segue.identifier isEqualToString:@"Req2Segue"] )
        {
            vc.str_Title = @"개인정보 처리방침";
            vc.str_Url = @"https://www.google.co.kr";
        }
        else if( [segue.identifier isEqualToString:@"Opt1Segue"] )
        {
            vc.str_Title = @"마케팅 정보 수신동의";
            vc.str_Url = @"https://www.apple.com/kr";
        }
    }
}

- (void)checkButtons
{
    if( _btn_RadioOptSub1.isChecked &&
    _btn_RadioOptSub2.isChecked &&
    _btn_RadioOptSub3.isChecked )
    {
        if( _btn_RadioOpt1.isChecked == NO )
        {
            _btn_RadioOpt1.isChecked = YES;
        }
    }
    else
    {
        if( _btn_RadioOpt1.isChecked == YES )
        {
            _btn_RadioOpt1.isChecked = NO;
        }
    }

    if( _btn_RadioReq1.isChecked &&
       _btn_RadioReq2.isChecked &&
       _btn_RadioOpt1.isChecked &&
       _btn_RadioOptSub1.isChecked &&
       _btn_RadioOptSub2.isChecked &&
       _btn_RadioOptSub3.isChecked )
    {
        if( _btn_All.isChecked == NO )
        {
            _btn_All.isChecked = YES;
        }
    }
    else
    {
        if( _btn_All.isChecked == YES )
        {
            _btn_All.isChecked = NO;
        }
    }
    
    
    if( _btn_RadioReq1.isChecked == NO || _btn_RadioReq2.isChecked == NO )
    {
        self.btn_Next.enabled = NO;
        self.btn_Next.backgroundColor = kDisableColor;
    }
    else
    {
        //필수약관 동의 완료 스텝
        self.btn_Next.enabled = YES;
        self.btn_Next.backgroundColor = kEnableColor;
    }
}

- (IBAction)goAllCheck:(id)sender
{
    [_btn_All toggleRadioButton];
    
    if( _btn_All.isChecked )
    {
        _btn_RadioReq1.isChecked = _btn_RadioReq2.isChecked =  _btn_RadioOptSub1.isChecked = _btn_RadioOptSub2.isChecked = _btn_RadioOptSub3.isChecked = _btn_RadioOpt1.isChecked = _btn_RadioOptSub1.isChecked = _btn_RadioOptSub2.isChecked = _btn_RadioOptSub3.isChecked = YES;
    }
    else
    {
        _btn_RadioReq1.isChecked = _btn_RadioReq2.isChecked =  _btn_RadioOptSub1.isChecked = _btn_RadioOptSub2.isChecked = _btn_RadioOptSub3.isChecked = _btn_RadioOpt1.isChecked = _btn_RadioOptSub1.isChecked = _btn_RadioOptSub2.isChecked = _btn_RadioOptSub3.isChecked = NO;
    }
    
    [self checkButtons];
}

- (IBAction)goReq1:(id)sender
{
    [_btn_RadioReq1 toggleRadioButton];
    
    [self checkButtons];
}

- (IBAction)goReq2:(id)sender
{
    [_btn_RadioReq2 toggleRadioButton];
    
    [self checkButtons];
}

- (IBAction)goOpt1:(id)sender
{
    [_btn_RadioOpt1 toggleRadioButton];
    
    if( _btn_RadioOpt1.isChecked )
    {
        _btn_RadioOptSub1.isChecked = _btn_RadioOptSub2.isChecked = _btn_RadioOptSub3.isChecked = YES;
    }
    else
    {
        _btn_RadioOptSub1.isChecked = _btn_RadioOptSub2.isChecked = _btn_RadioOptSub3.isChecked = NO;
    }
    
    [self checkButtons];
}

- (IBAction)goOptSub1:(id)sender
{
    [_btn_RadioOptSub1 toggleRadioButton];
    
    [self checkButtons];
}

- (IBAction)goOptSub2:(id)sender
{
    [_btn_RadioOptSub2 toggleRadioButton];
    
    [self checkButtons];
}

- (IBAction)goOptSub3:(id)sender
{
    [_btn_RadioOptSub3 toggleRadioButton];
    
    [self checkButtons];
}

- (IBAction)goClose:(id)sender
{
    [HapticHelper tap];
    
    _lc_ContentsHeight.constant = 0;
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }];
}

- (IBAction)goNext:(id)sender
{
    [HapticHelper tap];
    
    if( _joinUserInfo.image )
    {
        __weak __typeof(&*self)weakSelf = self;

//        NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
        NSData *data = UIImageJPEGRepresentation(_joinUserInfo.image, 0.7f);
        
        NSMutableDictionary *dicM_Datas = [NSMutableDictionary dictionary];
        [dicM_Datas setObject:@{@"data":data, @"type":@"image"} forKey:@"file1"];

        [[WebAPI sharedData] imageUpload:@"user/uploader"
                                   param:nil
                              withImages:dicM_Datas
                               withBlock:^(id resulte, NSError *error) {
            
            if( resulte )
            {
                NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
                if( [str_Success isEqualToString:@"success"] )
                {
                    NSString *str_ImageUrl = @"";
                    id res = [resulte dictionaryForKey:@"result"];
                    if( [res isKindOfClass:[NSArray class]] )
                    {
                        NSDictionary *dic_Result = [res firstObject];
                        str_ImageUrl = [dic_Result stringForKey:@"filePath"];
                    }
                    else
                    {
                        NSDictionary *dic_Result = [resulte dictionaryForKey:@"result"];
                        str_ImageUrl = [dic_Result stringForKey:@"filePath"];
                    }
                    
                    weakSelf.joinUserInfo.imgUrl = str_ImageUrl;
                    [weakSelf join];
                }
            }
        }];
    }
    else
    {
        [self join];
    }
}

- (void)join
{
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setValue:_joinUserInfo.name forKey:@"name"];
    [dicM_Params setValue:_joinUserInfo.email forKey:@"email"];
    [dicM_Params setValue:_joinUserInfo.password forKey:@"password"];
    [dicM_Params setValue:_joinUserInfo.imgUrl forKey:@"imgUrl"];
    [dicM_Params setValue:_joinUserInfo.regOs forKey:@"regOs"];
    [dicM_Params setValue:_joinUserInfo.regType forKey:@"regType"];
    [dicM_Params setValue:_joinUserInfo.deviceType forKey:@"deviceType"];
    [dicM_Params setValue:_joinUserInfo.childName forKey:@"childName"];
    [dicM_Params setValue:_joinUserInfo.childGender forKey:@"childGender"];
    [dicM_Params setValue:_joinUserInfo.childBirth forKey:@"childBirth"];
    [dicM_Params setValue:_btn_RadioOptSub1.isChecked ? @"Y": @"N" forKey:@"mirPush"];
    [dicM_Params setValue:_btn_RadioOptSub2.isChecked ? @"Y": @"N" forKey:@"mirSms"];
    [dicM_Params setValue:_btn_RadioOptSub3.isChecked ? @"Y": @"N" forKey:@"mirEmail"];

    [[WebAPI sharedData] callAsyncWebAPIBlock:@"auth/register" param:dicM_Params withMethod:@"POST" withBlock:^(id resulte, NSError *error) {
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            [[NSUserDefaults standardUserDefaults] setObject:self.joinUserInfo.password forKey:@"UserPw"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            NSDictionary *dic_Result = [resulte dictionaryForKey:@"result"];
            NSString *str_AccToken = [resulte stringForKey:@"accessToken"];
            if( str_AccToken.length > 0 )
            {
                [[NSUserDefaults standardUserDefaults] setObject:str_AccToken forKey:@"accessToken"];
            }

            [[NSUserDefaults standardUserDefaults] setObject:@(1) forKey:@"IsLogin"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            [[UserData sharedData] initData:dic_Result];
            [[Common sharedData] connectedSBD];
            [[Common sharedData] showMainNavi:[Util keyWindow]];
        }
    }];
}

@end
