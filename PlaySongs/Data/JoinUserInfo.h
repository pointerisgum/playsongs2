//
//  JoinUserInfo.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/06.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JoinUserInfo : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *imgUrl;
@property (nonatomic, strong) NSString *regOs;
@property (nonatomic, strong) NSString *regType;
@property (nonatomic, strong) NSString *deviceType;
@property (nonatomic, strong) NSString *childName;
@property (nonatomic, strong) NSString *childGender;
@property (nonatomic, strong) NSString *childBirth;
@property (nonatomic, strong) NSString *mirPush;
@property (nonatomic, strong) NSString *mirSms;
@property (nonatomic, strong) NSString *mirEmail;
@property (nonatomic, strong) UIImage *image;
@end

NS_ASSUME_NONNULL_END
