//
//  EmailJoinPwViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/05.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "EmailJoinPwViewController.h"
#import "EmailAccView.h"

static NSInteger kMinPwLength = 4;

@interface EmailJoinPwViewController ()
@property (nonatomic, strong) EmailAccView *v_PwAccView;
@property (nonatomic, strong) EmailAccView *v_PwConfirmAccView;
@property (weak, nonatomic) IBOutlet UIView *v_Pw;
@property (weak, nonatomic) IBOutlet UITextField *tf_Pw;
@property (weak, nonatomic) IBOutlet UIButton *btn_PwStatus;
@property (weak, nonatomic) IBOutlet UILabel *lb_PwStatus;

@property (weak, nonatomic) IBOutlet UIView *v_PwConfirm;
@property (weak, nonatomic) IBOutlet UITextField *tf_PwConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btn_PwConfirmStatus;
@property (weak, nonatomic) IBOutlet UILabel *lb_PwConfirmStatus;
@end

@implementation EmailJoinPwViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _v_Pw.layer.cornerRadius = 8.0f;
    _v_Pw.layer.borderWidth = 0.5f;
    _v_Pw.layer.borderColor = kEnableBoarderColor.CGColor;

    _v_PwAccView = [[NSBundle mainBundle]loadNibNamed:@"EmailAccView" owner:self options:nil].firstObject;
    [_v_PwAccView.btn_Next setTitle:@"다음" forState:UIControlStateNormal];
    _tf_Pw.inputAccessoryView = _v_PwAccView;
    [_v_PwAccView.btn_Next addTarget:self action:@selector(onShowConfirmKeyboard) forControlEvents:UIControlEventTouchUpInside];

    
    
    _v_PwConfirm.layer.cornerRadius = 8.0f;
    _v_PwConfirm.layer.borderWidth = 0.5f;
    _v_PwConfirm.layer.borderColor = kEnableBoarderColor.CGColor;

    _v_PwConfirmAccView = [[NSBundle mainBundle]loadNibNamed:@"EmailAccView" owner:self options:nil].firstObject;
    [_v_PwConfirmAccView.btn_Next setTitle:@"다음" forState:UIControlStateNormal];
    _tf_PwConfirm.inputAccessoryView = _v_PwConfirmAccView;
    [_v_PwConfirmAccView.btn_Next addTarget:self action:@selector(onNext) forControlEvents:UIControlEventTouchUpInside];

    
    [_tf_Pw becomeFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)validPwCheck
{
    self.btn_PwStatus.hidden = _tf_Pw.text.length <= 0;

    if( _tf_Pw.text.length < kMinPwLength )
    {
        self.btn_PwStatus.selected = NO;
        self.v_Pw.layer.borderColor = kErrorColor.CGColor;
        self.v_PwAccView.btn_Next.backgroundColor = kDisableColor;
        self.v_PwAccView.btn_Next.enabled = NO;
        [UIView animateWithDuration:0.25 animations:^{
            self.lb_PwStatus.alpha = YES;
        }];
    }
    else
    {
        self.btn_PwStatus.selected = YES;
        self.v_Pw.layer.borderColor = kEnableBoarderColor.CGColor;
        self.v_PwAccView.btn_Next.backgroundColor = kEnableColor;
        self.v_PwAccView.btn_Next.enabled = YES;
        [UIView animateWithDuration:0.25 animations:^{
            self.lb_PwStatus.alpha = NO;
        }];
    }
}

- (void)validPwConfirmCheck
{
    self.btn_PwConfirmStatus.hidden = _tf_PwConfirm.text.length <= 0;

    if( [_tf_Pw.text isEqualToString:_tf_PwConfirm.text] )
    {
        self.btn_PwConfirmStatus.selected = YES;
        self.v_PwConfirm.layer.borderColor = kEnableBoarderColor.CGColor;
        self.v_PwConfirmAccView.btn_Next.backgroundColor = kEnableColor;
        self.v_PwConfirmAccView.btn_Next.enabled = YES;
        [UIView animateWithDuration:0.25 animations:^{
            self.lb_PwConfirmStatus.alpha = NO;
        }];
    }
    else
    {
        self.btn_PwConfirmStatus.selected = NO;
        self.v_PwConfirm.layer.borderColor = kErrorColor.CGColor;
        self.v_PwConfirmAccView.btn_Next.backgroundColor = kDisableColor;
        self.v_PwConfirmAccView.btn_Next.enabled = NO;
        [UIView animateWithDuration:0.25 animations:^{
            self.lb_PwConfirmStatus.alpha = YES;
        }];
    }
}

//MARK: UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        if( textField == self.tf_Pw )
        {
            [self validPwCheck];
        }
        else if( textField == self.tf_PwConfirm )
        {
            [self validPwConfirmCheck];
        }
    });

    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if( textField == _tf_Pw )
    {
        [HapticHelper tap];
        [_tf_PwConfirm becomeFirstResponder];
    }
    else if( textField == _tf_PwConfirm )
    {
        [self onNext];
    }
    
    return YES;
}


//MARK: Action
- (void)onShowConfirmKeyboard
{
    [HapticHelper tap];
    
    [_tf_PwConfirm becomeFirstResponder];
}

- (void)onNext
{
    if( [_tf_Pw.text isEqualToString:_tf_PwConfirm.text] == NO || _tf_Pw.text.length < kMinPwLength )  return;
   
    [[NSUserDefaults standardUserDefaults] setObject:_tf_Pw.text forKey:@"TempPw"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [HapticHelper tap];
    
    [self performSegueWithIdentifier:@"ProfileInputViewController" sender:nil];
}

@end
