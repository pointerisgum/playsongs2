//
//  SearchThumbCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/13.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchThumbCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iv_Thumb;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_Width;

@end

NS_ASSUME_NONNULL_END
