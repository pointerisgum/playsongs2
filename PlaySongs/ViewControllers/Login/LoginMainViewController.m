//
//  LoginMainViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/04.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "LoginMainViewController.h"
#import <AuthenticationServices/AuthenticationServices.h>
#import <KakaoOpenSDK/KakaoOpenSDK.h>
@import GoogleSignIn;
#import "OHParagraphStyle.h"
#import "LoginInfo.h"

typedef enum {
    kKakaoLogin      = 1,
    kFaceBookLogin   = 2,
    kGoogleLogin     = 3,
    kAppleLogin      = 4,
    kEmailLogin      = 5,
} LoginType;


API_AVAILABLE(ios(13.0))
@interface LoginMainViewController () <GIDSignInDelegate, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding, UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet ASAuthorizationAppleIDButton *v_AppleLoginButton;
@property (weak, nonatomic) IBOutlet UIView *v_AppleLogin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_AppleLoginHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_AppleLoginBottom;
@property (weak, nonatomic) IBOutlet UIButton *tttt;
@property (weak, nonatomic) IBOutlet UILabel *lb_Title;

@end

@implementation LoginMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tttt.tintColor = [UIColor whiteColor];

    _lb_Title.attributedText = [Util setLabelLineSpacing:_lb_Title withLineSpacing:15];

    [self setGoogleLogin];
    [self setFaceBookLogin];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)setGoogleLogin
{
    [GIDSignIn sharedInstance].shouldFetchBasicProfile = YES;
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].presentingViewController = self;
}

- (void)setFaceBookLogin
{
    
}

- (void)googleJoin
{
    NSString *str_UserName = [GIDSignIn sharedInstance].currentUser.profile.name;
    NSString *str_Email = [GIDSignIn sharedInstance].currentUser.profile.email;

    if( [GIDSignIn sharedInstance].currentUser.profile.hasImage )
    {
        //유저 이미지가 있는 경우
        CGFloat fImageWidth = 100;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSUInteger dimension = round(fImageWidth * [[UIScreen mainScreen] scale]);
            NSURL *imageURL = [[GIDSignIn sharedInstance].currentUser.profile imageURLWithDimension:dimension];
            NSData *avatarData = [NSData dataWithContentsOfURL:imageURL];
            
            if (avatarData) {
                // Update UI from the main thread when available
                dispatch_async(dispatch_get_main_queue(), ^{

                    UIImage *userImage = [UIImage imageWithData:avatarData];
                });
            }
        });
    }
    else
    {
        //유저 이미지가 없는 경우
        
    }
}

- (void)login:(LoginType)type
{
    switch (type) {
        case kKakaoLogin:
            break;
            
        default:
            break;
    }
}

- (void)vaildEmail:(NSString *)aEmail
{
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setValue:aEmail forKey:@"email"];
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"auth/validate/email" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        if( error != nil )
        {
            return;
        }
        
//        NSDictionary *dic_Result = [resulte dictionaryForKey:@"result"];
//        LoginInfo *loginInfo = [[LoginInfo alloc] initWithData:dic_Result];
//        NSLog(@"%ld", loginInfo.nUserId);
//        NSLog(@"%@", loginInfo.str_UserName);
    }];
}

#pragma mark - GoogleSignUp
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    //    [myActivityIndicator stopAnimating];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - GIDSignInDelegate
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if (error)
    {
        NSLog(@"%@", error);
        return;
    }
    
    NSLog(@"Success");
    [self googleJoin];
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if (error)
    {
        NSLog(@"%@", error);
    }
    else
    {
        NSLog(@"Success");
    }
    
    //TODO: 에러 얼렛 띄우기
}


//MARK:AppleIdDelegate
 - (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithAuthorization:(ASAuthorization *)authorization  API_AVAILABLE(ios(13.0))
{
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"%@", controller);
    NSLog(@"%@", authorization);
    
    NSLog(@"authorization.credential：%@", authorization.credential);
    
    NSMutableString *mStr = [NSMutableString string];
    
    if ([authorization.credential isKindOfClass:[ASAuthorizationAppleIDCredential class]])
    {
        // ASAuthorizationAppleIDCredential
        ASAuthorizationAppleIDCredential *appleIDCredential = authorization.credential;
        NSString *user = appleIDCredential.user;    //001223.aa2e5a700a0448fa8ae9434879a5f6f0.0403
        [[NSUserDefaults standardUserDefaults] setValue:user forKey:@"setCurrentIdentifier"];
        [mStr appendString:user?:@""];
        NSString *familyName = appleIDCredential.fullName.familyName;
        [mStr appendString:familyName?:@""];
        NSString *givenName = appleIDCredential.fullName.givenName;
        [mStr appendString:givenName?:@""];
        NSString *email = appleIDCredential.email;  //iuzzxycgvm@privaterelay.appleid.com
        [mStr appendString:email?:@""];
        NSLog(@"mStr：%@", mStr);
        [mStr appendString:@"\n"];
        //001223.aa2e5a700a0448fa8ae9434879a5f6f0.0403
        
        NSMutableString *strM_UserNameTmp = [NSMutableString stringWithString:@""];
        [strM_UserNameTmp appendString:familyName?:@""];
        [strM_UserNameTmp appendString:givenName?:@""];
        NSString *str_UserName = [strM_UserNameTmp stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSLog(@"email : %@", email);    //iuzzxycgvm@privaterelay.appleid.com
        NSLog(@"userName : %@", str_UserName);
        
//        email = @"pointerisgum9@gmail.com";
//        user = @"024223.aa2e5a711a0448fa8ae9434879a5f6f0.0403";
//        str_UserName = @"kym";
        
        if( email.length > 0 )
        {
            //토큰 정보 저장
            
        }
        else
        {
            //TODO: 토큰 정보로 이메일 계정을 가져와서 로그인을 태운다.

            
        }
            
        
    }
}
 

- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithError:(NSError *)error  API_AVAILABLE(ios(13.0)){
    
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"error ：%@", error);
    NSString *errorMsg = nil;
    switch (error.code) {
        case ASAuthorizationErrorCanceled:
            errorMsg = @"ASAuthorizationErrorCanceled";
            break;
        case ASAuthorizationErrorFailed:
            errorMsg = @"ASAuthorizationErrorFailed";
            break;
        case ASAuthorizationErrorInvalidResponse:
            errorMsg = @"ASAuthorizationErrorInvalidResponse";
            break;
        case ASAuthorizationErrorNotHandled:
            errorMsg = @"ASAuthorizationErrorNotHandled";
            break;
        case ASAuthorizationErrorUnknown:
            errorMsg = @"ASAuthorizationErrorUnknown";
            break;
    }
    
    NSLog(@"errorMsg：%@", errorMsg);

    if (errorMsg) {
        return;
    }
    
    if (error.localizedDescription) {
        NSLog(@"error description：%@", error.localizedDescription);
    }
    NSLog(@"controller requests：%@", controller.authorizationRequests);
    /*
     ((ASAuthorizationAppleIDRequest *)(controller.authorizationRequests[0])).requestedScopes
     <__NSArrayI 0x2821e2520>(
     full_name,
     email
     )
     */
}
 
//! Tells the delegate from which window it should present content to the user.
 - (ASPresentationAnchor)presentationAnchorForAuthorizationController:(ASAuthorizationController *)controller  API_AVAILABLE(ios(13.0)){
    
    NSLog(@"window：%s", __FUNCTION__);
    return self.view.window;
}



//MARK: Action
- (IBAction)goAppleLogin:(id)sender
{
//    [HapticHelper generateFeedback:FeedbackType_Impact_Medium];
    [HapticHelper tap];
    
    if (@available(iOS 13.0, *))
    {
        // A mechanism for generating requests to authenticate users based on their Apple ID.
        ASAuthorizationAppleIDProvider *appleIDProvider = [ASAuthorizationAppleIDProvider new];
        
        // Creates a new Apple ID authorization request.
        ASAuthorizationAppleIDRequest *request = appleIDProvider.createRequest;
        // The contact information to be requested from the user during authentication.
        request.requestedScopes = @[ASAuthorizationScopeFullName, ASAuthorizationScopeEmail];
        
        // A controller that manages authorization requests created by a provider.
        ASAuthorizationController *controller = [[ASAuthorizationController alloc] initWithAuthorizationRequests:@[request]];
        
        // A delegate that the authorization controller informs about the success or failure of an authorization attempt.
        controller.delegate = self;
        
        // A delegate that provides a display context in which the system can present an authorization interface to the user.
        controller.presentationContextProvider = self;
        
        // starts the authorization flows named during controller initialization.
        [controller performRequests];
    }
}

- (IBAction)goGoogleJoin:(id)sender
{
    [HapticHelper tap];

    if ([GIDSignIn sharedInstance].currentUser.authentication == nil)
    {
        [[GIDSignIn sharedInstance] signIn];
    }
    else
    {
        [self googleJoin];
    }
    
}

- (IBAction)goKakao:(id)sender
{
    [HapticHelper tap];
    
    NSString *str_KakaoToken = [KOSession sharedSession].token.accessToken;
    if( str_KakaoToken == nil || str_KakaoToken.length <= 0 )
    {
        //로그인 필요
            [[KOSession sharedSession] close];

            [[KOSession sharedSession] openWithCompletionHandler:^(NSError *error) {
                
                [KOSessionTask userMeTaskWithCompletion:^(NSError * _Nullable error, KOUserMe * _Nullable me) {
                    
                    NSLog(@"%@", [KOSession sharedSession].token.accessToken);

                    if (me)
                    {
        //                JoinViewController *vc = [kMainBoard instantiateViewControllerWithIdentifier:@"JoinViewController"];
        //                vc.kakaoInfo = me;
        //                vc.isKakaoJoin = YES;
        //                [self.navigationController pushViewController:vc animated:YES];
                    }
                }];
                
            } authType:(KOAuthType)KOAuthTypeTalk];
    }
    else
    {
//        [self login:]
    }


}

- (IBAction)goFaceBook:(id)sender
{
    
    
}

- (IBAction)goStartEmail:(id)sender
{
    [HapticHelper tap];

//    [self vaildEmail:@"asdasd@asdad.ccc"];
}

/*
     //api/auth/validate/email
     NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
     [dicM_Params setValue:@"p001@t.com" forKey:@"email"];
     [dicM_Params setValue:@"1234" forKey:@"password"];
     [[WebAPI sharedData] callAsyncWebAPIBlock:@"api/auth/login" param:dicM_Params withMethod:@"POST" withBlock:^(id resulte, NSError *error) {
        
 //        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte objectForKey_YM:@"success"]];

         if( error != nil )
         {
             [Util showAlert:@"통신 상태가 원활하지 않거나\n데이터가 잘못되었습니다." withVc:self];
             return;
         }
         
         NSDictionary *dic_Result = [resulte dictionaryForKey:@"result"];
         LoginInfo *loginInfo = [[LoginInfo alloc] initWithData:dic_Result];
         NSLog(@"%ld", loginInfo.nUserId);
         NSLog(@"%@", loginInfo.str_UserName);
     }];

 */
@end
