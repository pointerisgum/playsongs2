//
//  Common.h
//  KetoPlus
//
//  Created by 김영민 on 2020/03/25.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Common : NSObject

+ (Common *)sharedData;
//- (void)logOut:(UIViewController *)vc;
//- (UserData *)getUserData;
//- (void)setUserData:(NSDictionary *)dic;

- (void)showLoginNavi:(UIWindow *)window;
- (void)showMainNavi:(UIWindow *)window;
- (void)connectedSBD;
- (void)disconnectedSBD;
- (void)logOut;

@end

NS_ASSUME_NONNULL_END
