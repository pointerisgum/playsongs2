//
//  ReportViewController.h
//  PlaySongs
//
//  Created by 김영민 on 2020/09/02.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^CompletionBlock)(id completeResult);

@interface ReportViewController : UIViewController
@property (nonatomic, copy) CompletionBlock completionBlock;
@property (nonatomic, strong) NSString *str_PlayId;
- (void)setCompletionBlock:(CompletionBlock)completionBlock;
@end

NS_ASSUME_NONNULL_END
