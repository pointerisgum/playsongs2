//
//  FollowListViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/16.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "FollowListViewController.h"
#import "FollowListContainerViewController.h"

@interface FollowListViewController ()
@property (nonatomic, strong) FollowListContainerViewController *vc_Follower;
@property (nonatomic, strong) FollowListContainerViewController *vc_Following;
@property (nonatomic, assign) NSInteger nFollowerCnt;
@property (nonatomic, assign) BOOL isMy;
@property (weak, nonatomic) IBOutlet UILabel *lb_Title;
@property (weak, nonatomic) IBOutlet UIButton *btn_Follower;
@property (weak, nonatomic) IBOutlet UIButton *btn_Following;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_BarLeading;
@property (weak, nonatomic) IBOutlet UIScrollView *sv_Main;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_ContentsWidth;

@end

@implementation FollowListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     
    _lb_Title.text = _str_UserName;
    
    switch (_followMode)
    {
        case kFollower:
            _btn_Follower.selected = YES;
            _btn_Following.selected = NO;
            break;
        case kFollowing:
            _btn_Follower.selected = NO;
            _btn_Following.selected = YES;
            break;
        default:
            break;
    }

    _lc_BarLeading.constant = _followMode * (kDeviceWidth/2);
    _sv_Main.contentOffset = CGPointMake(_followMode * self.sv_Main.bounds.size.width, 0);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if( [_str_UserName isEqualToString:[UserData sharedData].userName] && _nUserId == [UserData sharedData].userId )
    {
        _isMy = YES;
    }
    else
    {
        _isMy = NO;
    }
    
    [self updateFollower];
    [self updateFollowing];
}

- (void)viewDidLayoutSubviews
{
    _sv_Main.contentSize = CGSizeMake(_sv_Main.frame.size.width * 2, _sv_Main.frame.size.height);
    _lc_ContentsWidth.constant = _sv_Main.contentSize.width;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if( [segue.identifier isEqualToString:@"follower"] )
    {
        __weak __typeof__(self) weakSelf = self;
        _vc_Follower = segue.destinationViewController;
        _vc_Follower.isMy = _isMy;
        [_vc_Follower setFollowingChangeBlock:^(id  _Nonnull completeResult) {
            
            if( weakSelf.isMy )
            {
                BOOL isFind = NO;
                for( NSInteger i = 0; i < weakSelf.vc_Following.arM_List.count; i++ )
                {
                    NSDictionary *dic = weakSelf.vc_Following.arM_List[i];
                    NSInteger nId = [[dic stringForKey:@"userId"] integerValue];
                    NSInteger nTargetId = [[completeResult stringForKey:@"userId"] integerValue];
                    if( nId == nTargetId )
                    {
                        isFind = YES;
                        NSMutableDictionary *dicM = [NSMutableDictionary dictionaryWithDictionary:dic];
                        [dicM setObject:[completeResult stringForKey:@"isFollowing"] forKey:@"isFollowing"];
                        [weakSelf.vc_Following.arM_List replaceObjectAtIndex:i withObject:dicM];
                        break;
                    }
                }
                
                if( isFind == NO )
                {
                    //신규 데이터
                    [weakSelf.vc_Following.arM_List addObject:completeResult];
                }
                
                [weakSelf updateFollowingCount];
                [weakSelf.vc_Following.tbv_List reloadData];

                
                for( NSInteger i = 0; i < weakSelf.vc_Follower.arM_List.count; i++ )
                {
                    NSDictionary *dic = weakSelf.vc_Follower.arM_List[i];
                    NSInteger nId = [[dic stringForKey:@"userId"] integerValue];
                    NSInteger nTargetId = [[completeResult stringForKey:@"userId"] integerValue];
                    if( nId == nTargetId )
                    {
                        NSMutableDictionary *dicM = [NSMutableDictionary dictionaryWithDictionary:dic];
                        [dicM setObject:[completeResult stringForKey:@"isFollowing"] forKey:@"isFollowing"];
                        [weakSelf.vc_Follower.arM_List replaceObjectAtIndex:i withObject:dicM];
                        break;
                    }
                }
                [weakSelf.vc_Follower.tbv_List reloadData];
            }
            else
            {
                [weakSelf updateFollower];
                [weakSelf updateFollowing];
            }
        }];
        
        [_vc_Follower setRefreshBlock:^{
            [weakSelf updateFollower];
        }];
    }
    else if( [segue.identifier isEqualToString:@"following"] )
    {
        __weak __typeof__(self) weakSelf = self;
        _vc_Following = segue.destinationViewController;
        _vc_Following.isMy = _isMy;
        [_vc_Following setFollowingChangeBlock:^(id  _Nonnull completeResult) {
            
            if( weakSelf.isMy )
            {
                for( NSInteger i = 0; i < weakSelf.vc_Follower.arM_List.count; i++ )
                {
                    NSDictionary *dic = weakSelf.vc_Follower.arM_List[i];
                    NSInteger nId = [[dic stringForKey:@"userId"] integerValue];
                    NSInteger nTargetId = [[completeResult stringForKey:@"userId"] integerValue];
                    if( nId == nTargetId )
                    {
                        NSMutableDictionary *dicM = [NSMutableDictionary dictionaryWithDictionary:dic];
                        [dicM setObject:[completeResult stringForKey:@"isFollowing"] forKey:@"isFollowing"];
                        [weakSelf.vc_Follower.arM_List replaceObjectAtIndex:i withObject:dicM];
                        break;
                    }
                }

                [weakSelf.vc_Follower.tbv_List reloadData];
                [weakSelf updateFollowingCount];
            }
            else
            {
                [weakSelf updateFollower];
                [weakSelf updateFollowing];
            }
        }];
        
        [_vc_Following setRefreshBlock:^{
            [weakSelf updateFollowing];
        }];
    }
}

- (void)updateFollowingCount
{
    NSInteger nCnt = 0;
    for( NSDictionary *dic in _vc_Following.arM_List )
    {
        if( [[dic stringForKey:@"isFollowing"] isEqualToString:@"Y"] )
        {
            nCnt++;
        }
    }
    
    [_btn_Following setTitle:[NSString stringWithFormat:@"%ld 팔로잉", nCnt] forState:UIControlStateNormal];
}

- (void)updateFollower
{
    __weak __typeof__(self) weakSelf = self;

    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@(_nUserId) forKey:@"userId"];
    [dicM_Params setObject:@([UserData sharedData].userId) forKey:@"following"];
    
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"user/follower/list" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.vc_Follower.refreshControl endRefreshing];
        });

        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            weakSelf.vc_Follower.arM_List = [resulte valueForArrayKey:@"result"];
        }
        
        [weakSelf.btn_Follower setTitle:[NSString stringWithFormat:@"%ld 팔로워", weakSelf.vc_Follower.arM_List.count] forState:UIControlStateNormal];
        [weakSelf.vc_Follower.tbv_List reloadData];
    }];
}

- (void)updateFollowing
{
    __weak __typeof__(self) weakSelf = self;

    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@(_nUserId) forKey:@"userId"];
    [dicM_Params setObject:@([UserData sharedData].userId) forKey:@"following"];
    
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"user/following/list" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            weakSelf.vc_Following.arM_List = [resulte valueForArrayKey:@"result"];
        }
        
        [weakSelf updateTagFollow];
    }];
}

- (void)updateTagFollow
{
    __weak __typeof__(self) weakSelf = self;

    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@(_nUserId) forKey:@"userId"];
//    [dicM_Params setObject:@(_nUserId) forKey:@"following"];
    
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"tag/following/list" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.vc_Following.refreshControl endRefreshing];
        });

        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            NSArray *ar_Tags = [resulte valueForArrayKey:@"result"];
            
            NSMutableArray *arM_temp = [NSMutableArray arrayWithCapacity:ar_Tags.count];
            NSMutableArray *arM = [NSMutableArray arrayWithArray:ar_Tags];
            for( NSMutableDictionary *dicM in arM )
            {
                [dicM setObject:@"tag" forKey:@"type"];
                [arM_temp addObject:dicM];
            }
            [arM_temp addObjectsFromArray:weakSelf.vc_Following.arM_List];
            weakSelf.vc_Following.arM_List = arM_temp;
//            [weakSelf.vc_Following.arM_List addObjectsFromArray:ar_Tags];
//            weakSelf.vc_Following.arM_List = [resulte valueForArrayKey:@"result"];
        }
        
        weakSelf.nFollowerCnt = weakSelf.vc_Following.arM_List.count;
        [weakSelf updateFollowingCount];
        [weakSelf.vc_Following.tbv_List reloadData];
    }];
}


//MARK: UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSInteger nPage = scrollView.contentOffset.x / scrollView.frame.size.width;
    if( scrollView == _sv_Main )
    {
        [self chooseMenu:nPage];
    }
}

- (IBAction)goScrollMenu:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [self chooseMenu:btn.tag];
}

- (void)chooseMenu:(NSInteger)idx
{
    self.lc_BarLeading.constant = idx * (self.sv_Main.bounds.size.width/2);
    [UIView animateWithDuration:0.3f animations:^{
        self.sv_Main.contentOffset = CGPointMake(idx * self.sv_Main.bounds.size.width, 0);
        [self.view layoutIfNeeded];
    }];
    
    switch (idx)
    {
        case 0:
            _btn_Follower.selected = YES;
            _btn_Following.selected = NO;
            break;
        case 1:
            _btn_Follower.selected = NO;
            _btn_Following.selected = YES;
            break;
        default:
            break;
    }
}

@end
