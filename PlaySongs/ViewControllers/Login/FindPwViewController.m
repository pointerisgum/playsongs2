//
//  FindPwViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/06.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "FindPwViewController.h"
#import "EmailAccView.h"

@interface FindPwViewController ()
@property (nonatomic, strong) EmailAccView *v_AccView;
@property (weak, nonatomic) IBOutlet UIView *v_Email;
@property (weak, nonatomic) IBOutlet UIButton *btn_Status;
@property (weak, nonatomic) IBOutlet UITextField *tf_Email;
@property (weak, nonatomic) IBOutlet UILabel *lb_WarningMsg;

@end

@implementation FindPwViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _v_Email.layer.cornerRadius = 8.0f;
    _v_Email.layer.borderWidth = 0.5f;
    _v_Email.layer.borderColor = kEnableBoarderColor.CGColor;

    _v_AccView = [[NSBundle mainBundle]loadNibNamed:@"EmailAccView" owner:self options:nil].firstObject;
    [_v_AccView.btn_Next setTitle:@"임시 비밀번호 발급받기" forState:UIControlStateNormal];
    _tf_Email.inputAccessoryView = _v_AccView;
    [_v_AccView.btn_Next addTarget:self action:@selector(onNext) forControlEvents:UIControlEventTouchUpInside];
    
    _tf_Email.text = self.str_Email;
    
    [self validCheck];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tf_Email becomeFirstResponder];
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {


}


- (void)validCheck
{
    self.btn_Status.hidden = self.tf_Email.text.length <= 0;
    
    if( [Util isUsableEmail:self.tf_Email.text] )
    {
        //이메일 형식일 경우
        self.btn_Status.selected = YES;
        self.v_Email.layer.borderColor = kEnableBoarderColor.CGColor;
        self.v_AccView.btn_Next.backgroundColor = kEnableColor;
        self.v_AccView.btn_Next.enabled = YES;
        [UIView animateWithDuration:0.25 animations:^{
            self.lb_WarningMsg.alpha = NO;
        }];
    }
    else
    {
        //이메일 형식이 아닐 경우
        self.btn_Status.selected = NO;
        self.v_Email.layer.borderColor = kErrorColor.CGColor;
        self.v_AccView.btn_Next.backgroundColor = kDisableColor;
        self.v_AccView.btn_Next.enabled = NO;
        [UIView animateWithDuration:0.25 animations:^{
            self.lb_WarningMsg.alpha = YES;
        }];
    }
}

//MARK: UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        [self validCheck];
    });
    
//        //글자제한이 있는 경우 백스페이스바 예외처리
//        if( self.nMaxCount > 0 )
//        {
//            const char * _char = [string cStringUsingEncoding:NSUTF8StringEncoding];
//            int isBackSpace = strcmp(_char, "\b");
//            if (isBackSpace != -8)
//            {
//                //이건 바이트인데 기획에선 자릿수로 체크하길 원했음
//    //            NSUInteger bytes = [textField.text lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
//                NSUInteger bytes = [textField.text length];
//                if( bytes > self.nMaxCount - 1 )
//                {
//                    return NO;
//                }
//            }
//        }

    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self onNext];
    return YES;
}


//MARK: Action
- (void)onNext
{
    if( _tf_Email.text.length <= 0 )    return;
    
    [HapticHelper tap];
  
    //TODO: 임시 비밀번호 발급 받는거 API 나오면 작업 할 것
//    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
//    [dicM_Params setValue:_tf_Email.text forKey:@"email"];
//    [[WebAPI sharedData] callAsyncWebAPIBlock:@"auth/validate/email" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
//
//        if( error != nil )
//        {
//            return;
//        }
//
//    }];
}

@end
