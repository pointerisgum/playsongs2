//
//  NSObject+Thoting.m
//  ThoThing
//
//  Created by macpro15 on 2017. 12. 20..
//  Copyright © 2017년 youngmin.kim. All rights reserved.
//

#import "NSObject+PlaySongs.h"

@implementation NSObject (PlaySongs)

- (BOOL)isStringEmpty {
    if (self == nil) {
        return YES;
    }
    
    if ([self isKindOfClass:[NSNull class]]) {
        return YES;
    }
    
    if (self == (id)[NSNull null]) {
        return YES;
    }
    
    if ([self isKindOfClass:[NSString class]] == NO) {
        return YES;
    }
    if ([(NSString *)self length] == 0) {
        return YES;
    }
    if ([(NSString *)self isEqualToString:@"(null)"]) {
        return YES;
        
    }
    return NO;
}

- (BOOL)isDictionaryEmpty {
    if (self == nil) {
        return YES;
    }
    
    if ([self isKindOfClass:[NSNull class]]) {
        return YES;
    }
    
    if (self == (id)[NSNull null]) {
        return YES;
    }
    
    if ([self isKindOfClass:[NSDictionary class]] == NO) {
        return YES;
    }
    if ([(NSDictionary *)self count] == 0) {
        return YES;
    }
    return NO;
}

- (BOOL)isArrayEmpty {
    if (self == nil) {
        return YES;
    }
    
    if ([self isKindOfClass:[NSNull class]]) {
        return YES;
    }
    
    if (self == (id)[NSNull null]) {
        return YES;
    }
    
    if ([self isKindOfClass:[NSArray class]] == NO) {
        return YES;
    }
    if ([(NSArray *)self count] == 0) {
        return YES;
    }
    return NO;
}

- (BOOL)isArrayOutOfBounds:(NSInteger)row {
    if ([self isArrayEmpty]) {
        return YES;
    }
    
    if ([(NSArray *)self count] <= row) {
        return YES;
    }
    
    return NO;
}

@end
