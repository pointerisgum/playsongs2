//
//  MakePlayViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/28.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "MakePlayViewController.h"
#import "PlayMakeTextCell.h"
#import "PlayMakeMediaCell.h"
#import "PlayMakeYouTubeCell.h"
#import "ProductCell.h"
#import "PlaySongs-Swift.h"
#import "PlayMakeTextViewController.h"
#import "PlayMakeYoutubeViewController.h"
#import "PlayMakeProductViewController.h"
#import "SearchTextCell.h"

@interface MakePlayViewController () <UITextFieldDelegate>
@property (nonatomic, assign) BOOL isTagMode;
@property (nonatomic, assign) BOOL isDoneOk;
@property (nonatomic, strong) NSMutableArray *arM_List;
@property (nonatomic, strong) NSMutableArray *arM_TagList;
@property (nonatomic, strong) NSMutableArray *arM_SelectedTag;
@property (nonatomic, strong) NSMutableArray *arM_SendData;
@property (nonatomic, strong) NSMutableArray *arM_SendTags;
@property (nonatomic, strong) NSArray *ar_UploadResult;
@property (weak, nonatomic) IBOutlet UILabel *lb_Title;
@property (weak, nonatomic) IBOutlet UITableView *tbv_List;
@property (weak, nonatomic) IBOutlet UIStackView *stv_BottomMenu;
@property (weak, nonatomic) IBOutlet UIButton *btn_Modify;
@property (weak, nonatomic) IBOutlet UIButton *btn_ModifyDone;
@property (weak, nonatomic) IBOutlet UIButton *btn_Done;
@property (weak, nonatomic) IBOutlet UITextField *tf_Title;
@property (weak, nonatomic) IBOutlet UITextField *tf_Tag;
@end

@implementation MakePlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _arM_List = [NSMutableArray array];
    _arM_SelectedTag = [NSMutableArray array];

    if( _dic_Modify )
    {
        _lb_Title.text = @"놀이 수정하기";
        [self setModifyData];
    }
    else
    {
        _lb_Title.text = @"놀이 올리기";
    }
}

//- (void)viewDidLayoutSubviews
//{
//    [super viewDidLayoutSubviews];
//
//    [_stv_BottomMenu updateConstraints];
//    [_stv_BottomMenu layoutIfNeeded];
//    [_stv_BottomMenu updateConstraintsIfNeeded];
//
//}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [self.view layoutIfNeeded];
//    [self.stv_BottomMenu layoutIfNeeded];

}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    __weak __typeof(&*self)weakSelf = self;

    _btn_Modify.hidden = NO;
    _btn_ModifyDone.hidden = YES;
    [_tbv_List setEditing:NO animated:NO];

    if( [segue.identifier isEqualToString:@"InPutTextSegue"] )
    {
        [HapticHelper tap];
        
        PlayMakeTextViewController *vc = segue.destinationViewController;
        [vc setCompletionBlock:^(id  _Nonnull completeResult) {
           
            [weakSelf.arM_List addObject:@{@"type":@"text", @"text":completeResult}];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.tbv_List reloadData];
            });
        }];
    }
    else if( [segue.identifier isEqualToString:@"InPutYouTubeSegue"] )
    {
        [HapticHelper tap];
        
        PlayMakeYoutubeViewController *vc = segue.destinationViewController;
        [vc setCompletionBlock:^(id  _Nonnull completeResult) {
           
            //https://www.youtube.com/watch?v=ZddfRmDMjz4
            [weakSelf.arM_List addObject:@{@"type":@"youtube", @"text":completeResult}];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.tbv_List reloadData];
            });
        }];
    }
    else if( [segue.identifier isEqualToString:@"InPutProductSegue"] )
    {
        [HapticHelper tap];
        
        PlayMakeProductViewController *vc = segue.destinationViewController;
        [vc setCompletionBlock:^(id  _Nonnull completeResult) {
           
            [weakSelf.arM_List addObject:@{@"type":@"product", @"obj":completeResult}];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.tbv_List reloadData];
            });
        }];
    }
}

- (void)setModifyData
{
    _tf_Title.text = [_dic_Modify stringForKey:@"title"];
    
    NSArray *ar_Tags = [Util jsonStringToArray:[_dic_Modify stringForKey:@"tags"]];
    _arM_SelectedTag = [NSMutableArray arrayWithArray:ar_Tags];
    
    NSMutableString *strM_Tag = [NSMutableString string];
    for( NSDictionary *dic_Tag in _arM_SelectedTag )
    {
        NSString *str_Tag = [NSString stringWithFormat:@"#%@ ", [dic_Tag stringForKey:@"tagName"]];
        [strM_Tag appendString:str_Tag];
    }
    
    _tf_Tag.text = strM_Tag;
    
    
    NSArray *ar_Body = [Util jsonStringToArray:[_dic_Modify stringForKey:@"contentBody"]];
    for( NSDictionary *dic_Body in ar_Body )
    {
        NSString *str_Type = [dic_Body stringForKey:@"type"];
        if( [str_Type isEqualToString:@"text"] )
        {
            [_arM_List addObject:@{@"type":str_Type, @"text":[dic_Body stringForKey:@"text"]}];
        }
        else if( [str_Type isEqualToString:@"file"] )
        {
            NSArray *ar_Files = [dic_Body valueForArrayKey:@"files"];
            if( ar_Files.count <= 0 )   {continue;}
            NSDictionary *dic_File = ar_Files.firstObject;
            NSString *str_SubType = [dic_File stringForKey:@"type"];
            NSString *str_ImageUrl = [dic_File stringForKey:@"thumbnail"];
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:str_ImageUrl]];
            UIImage *image = [UIImage imageWithData:imageData];
            if( [str_SubType isEqualToString:@"image"] )
            {
                [_arM_List addObject:@{@"type":@"file", @"obj":@{@"type":str_SubType, @"image":image}}];
            }
            else if( [str_SubType isEqualToString:@"video"] )
            {
                [_arM_List addObject:@{@"type":@"file", @"obj":@{@"type":str_SubType, @"image":image, @"url":[dic_File stringForKey:@"filePath"]}}];
            }
            else
            {
                continue;
            }
        }
        else if( [str_Type isEqualToString:@"youtube"] )
        {
            [_arM_List addObject:@{@"type":str_Type, @"text":[dic_Body stringForKey:@"videoId"]}];
        }
        else if( [str_Type isEqualToString:@"product"] )
        {
            NSArray *ar_Files = [dic_Body valueForArrayKey:@"product"];
            if( ar_Files.count <= 0 )   {continue;}
            NSDictionary *dic_Data = ar_Files.firstObject;
            NSDictionary *dic_File = [dic_Data valueForDictionaryKey:@"file"];
            NSString *str_ImageUrl = [dic_File stringForKey:@"thumbnail"];
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:str_ImageUrl]];
            UIImage *image = [UIImage imageWithData:imageData];
            NSString *str_Title = [dic_Data stringForKey:@"productName"];
            NSString *str_SubTitle = [dic_Data stringForKey:@"productDesc"];
            NSInteger nPrice = [dic_Data integerForKey:@"price"];
            [_arM_List addObject:@{@"type":str_Type, @"obj":@{@"type":str_Type, @"image":image, @"price":@(nPrice), @"subTitle":str_SubTitle, @"title":str_Title}}];
        }
    }
    
    [_tbv_List reloadData];
}

- (void)updateSearch:(NSString *)aTag
{
    __weak __typeof__(self) weakSelf = self;

    if( [aTag hasPrefix:@"#"] )
    {
        aTag = [aTag stringByReplacingOccurrencesOfString:@"#" withString:@""];
    }
    
    NSLog(@"search key word : %@", aTag);
    
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:aTag forKey:@"q"];
    [dicM_Params setObject:@"tag" forKey:@"filter"];

    [[WebAPI sharedData] callAsyncWebAPIBlock:@"search/keyword" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        if( resulte != nil )
        {
            NSArray *ar = [NSArray arrayWithArray:resulte];
            if( ar.count > 0 )
            {
                weakSelf.arM_TagList = [NSMutableArray arrayWithArray:ar];
            }
        }

        if( weakSelf.arM_TagList.count > 0 )
        {
            [weakSelf.tbv_List reloadData];
        }
    }];
}

- (void)uploadData
{
    _btn_Done.userInteractionEnabled = NO;

    _arM_SendData = [NSMutableArray arrayWithArray:_arM_List];
    
    NSMutableArray *arM_Files = [NSMutableArray array];
//    for( NSDictionary *dic in _arM_List )
    for( NSInteger i = 0; i < _arM_List.count; i++ )
    {
        NSDictionary *dic = _arM_List[i];
        if( [[dic stringForKey:@"type"] isEqualToString:@"file"] )
        {
            NSMutableDictionary *dicM = [NSMutableDictionary dictionaryWithDictionary:dic];
            [dicM setObject:@(i) forKey:@"idx"];
            [arM_Files addObject:dicM];
        }
        else if( [[dic stringForKey:@"type"] isEqualToString:@"product"] )
        {
            NSMutableDictionary *dicM = [NSMutableDictionary dictionaryWithDictionary:dic];
            [dicM setObject:@(i) forKey:@"idx"];
            [arM_Files addObject:dicM];
        }
    }
    
    if( arM_Files.count <= 0 )
    {
        //첨부파일이 없는 경우
        _btn_Done.userInteractionEnabled = YES;
        [self uploadContents];
        return;
    }
    
    __weak __typeof__(self) weakSelf = self;
    NSMutableDictionary *dicM_Datas = [NSMutableDictionary dictionary];
    for( NSInteger i = 0; i < arM_Files.count; i++ )
    {
        NSString *str_Key = [NSString stringWithFormat:@"file%ld", i+1];
        NSDictionary *dic_Temp = arM_Files[i];
        NSDictionary *dic_Obj = [dic_Temp objectForKey:@"obj"];
        NSString *str_Type = [dic_Obj stringForKey:@"type"];
        NSData *data = nil;
        if( [str_Type isEqualToString:@"image"] )
        {
            UIImage *image = [dic_Obj objectForKey:@"image"];
            data = UIImagePNGRepresentation(image);
            if( data != nil )
            {
                [dicM_Datas setObject:@{@"data":data, @"type":@"image", @"idx":@([dic_Temp integerForKey:@"idx"])} forKey:str_Key];
            }
        }
        else if( [str_Type isEqualToString:@"video"] )
        {
            NSString *str_VideoLocalUrl = [dic_Obj stringForKey:@"url"];
            if( _dic_Modify )
            {
                //서버에 있는 동영상일 경우
                data = [NSData dataWithContentsOfURL:[NSURL URLWithString:str_VideoLocalUrl]];
            }
            else
            {
                data = [NSData dataWithContentsOfFile:str_VideoLocalUrl];
            }
            
            if( data != nil )
            {
                [dicM_Datas setObject:@{@"data":data, @"type":@"video", @"idx":@([dic_Temp integerForKey:@"idx"])} forKey:str_Key];
            }
        }
        else if( [str_Type isEqualToString:@"product"] )
        {
            UIImage *image = [dic_Obj objectForKey:@"image"];
            data = UIImagePNGRepresentation(image);
            if( data != nil )
            {
                [dicM_Datas setObject:@{@"data":data, @"type":@"image", @"idx":@([dic_Temp integerForKey:@"idx"]), @"product":@"product"} forKey:str_Key];
            }
        }
    }
    
    [[WebAPI sharedData] imageUpload:@"play/uploader" param:nil withImages:dicM_Datas withBlock:^(id resulte, NSError *error) {
      
        [MBProgressHUD hide];
        weakSelf.btn_Done.userInteractionEnabled = YES;

        if( resulte )
        {
            NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
            if( [str_Success isEqualToString:@"success"] )
            {
                id res = [resulte dictionaryForKey:@"result"];
                if( [res isKindOfClass:[NSArray class]] )
                {
                    for( NSDictionary *dic_Result in res )
                    {
                        NSString *str_FileName = [dic_Result objectForKey:@"orgFileName"];
                        NSArray *ar = [str_FileName componentsSeparatedByString:@"_"];
                        if( ar.count > 0 )
                        {
                            NSInteger nIdx = [ar.firstObject integerValue];
                            NSMutableDictionary *dicM = [NSMutableDictionary dictionary];
                            if( [str_FileName rangeOfString:@"product"].location != NSNotFound )
                            {
                                NSDictionary *dic_Temp = weakSelf.arM_SendData[nIdx];
                                [dicM setObject:@"product" forKey:@"type"];
                                [dicM addEntriesFromDictionary:[dic_Temp objectForKey:@"obj"]];
                            }
                            else
                            {
                                [dicM setObject:@"file" forKey:@"type"];
                            }
                            [dicM setObject:dic_Result forKey:@"obj"];
                            [weakSelf.arM_SendData replaceObjectAtIndex:nIdx withObject:dicM];
                        }
                    }
//                    weakSelf.ar_UploadResult = [NSArray arrayWithArray:res];
                    [weakSelf uploadContents];
                    return;
                }

            }
            
            [Util showToast:@"데이터 전송에 실패 하였습니다."];
        }
        else
        {
            [Util showToast:@"데이터 전송에 실패 하였습니다."];
        }
    }];
}

- (void)uploadContents
{
    //_arM_SelectedTagList

    _btn_Done.userInteractionEnabled = NO;

    __weak __typeof__(self) weakSelf = self;

    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@([UserData sharedData].userId) forKey:@"userId"];
    [dicM_Params setObject:_tf_Title.text forKey:@"title"];
    
    //컨텐츠 바디 만들기
    NSMutableArray *arM_Body = [NSMutableArray array];
    for( NSInteger i = 0; i < _arM_SendData.count; i++ )
    {
        NSDictionary *dic = _arM_SendData[i];
        NSString *str_Type = [dic stringForKey:@"type"];
        if( [str_Type isEqualToString:@"text"] )
        {
            NSMutableDictionary *dicM = [NSMutableDictionary dictionary];
            [dicM setObject:@(i+1) forKey:@"seq"];
            [dicM setObject:@"text" forKey:@"type"];
            [dicM setObject:[dic stringForKey:@"text"] forKey:@"text"];
            [arM_Body addObject:dicM];
        }
        else if( [str_Type isEqualToString:@"file"] )
        {
            NSMutableDictionary *dicM = [NSMutableDictionary dictionary];
            [dicM setObject:@(i+1) forKey:@"seq"];
            [dicM setObject:@"file" forKey:@"type"];
            [dicM setObject:@[[dic objectForKey:@"obj"]] forKey:@"files"];
            [arM_Body addObject:dicM];
        }
        else if( [str_Type isEqualToString:@"youtube"] )
        {
            NSMutableDictionary *dicM = [NSMutableDictionary dictionary];
            NSString *str_VideoId = [dic stringForKey:@"text"];
            NSString *str_ImagePath = [NSString stringWithFormat:@"https://img.youtube.com/vi/%@/maxresdefault.jpg", str_VideoId];
            [dicM setObject:@(i+1) forKey:@"seq"];
            [dicM setObject:@"youtube" forKey:@"type"];
            [dicM setObject:str_VideoId forKey:@"videoId"];
            [dicM setObject:str_ImagePath forKey:@"thumbnail"];
            [arM_Body addObject:dicM];
        }
        else if( [str_Type isEqualToString:@"product"] )
        {
            NSMutableDictionary *dicM = [NSMutableDictionary dictionary];
//            [dicM setObject:@(i+1) forKey:@"seq"];
            [dicM setObject:[dic stringForKey:@"title"] forKey:@"productName"];
            [dicM setObject:[dic stringForKey:@"subTitle"] forKey:@"productDesc"];
            [dicM setObject:[dic stringForKey:@"price"] forKey:@"price"];
//            [dicM setObject:@"product" forKey:@"type"];
            [dicM setObject:[dic objectForKey:@"obj"] forKey:@"file"];
            [arM_Body addObject:@{@"product":@[dicM], @"seq":@(i+1), @"type":@"product"}];

        }
    }
    
    NSMutableDictionary *dicM_Tags = [NSMutableDictionary dictionary];
    [dicM_Tags setObject:@(_arM_SendData.count+1) forKey:@"seq"];
    [dicM_Tags setObject:@"tag" forKey:@"type"];
    [dicM_Tags setObject:_arM_SendTags forKey:@"tags"];
    [arM_Body addObject:dicM_Tags];

    NSError *err;
    NSData *jsonData = [NSJSONSerialization  dataWithJSONObject:arM_Body options:0 error:&err];
    NSString *str_JsonData = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [dicM_Params setObject:str_JsonData forKey:@"contentBody"];

    NSString *str_Path = @"play/save";
    NSString *str_Method = @"POST";
    if( _dic_Modify )
    {
//        NSString *str_PlayId = [NSString stringWithFormat:@"%ld", [_dic_Modify integerForKey:@"playId"]];
        str_Path = [NSString stringWithFormat:@"play/%ld", [_dic_Modify integerForKey:@"playId"]];
        str_Method = @"PATCH";
    }
    [[WebAPI sharedData] callAsyncWebAPIBlock:str_Path param:dicM_Params withMethod:str_Method withBlock:^(id resulte, NSError *error) {

        [MBProgressHUD hide];
        weakSelf.btn_Done.userInteractionEnabled = YES;
        
        if( error != nil )
        {
            [Util showToast:@"데이터 전송에 실패 하였습니다."];
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            NSString *str_PlayId = [NSString stringWithFormat:@"%ld", [weakSelf.dic_Modify integerForKey:@"playId"]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateOneItemNoti" object:str_PlayId];
            [weakSelf dismissViewControllerAnimated:YES completion:^{
            }];
            return;
        }

        [Util showToast:@"데이터 전송에 실패 하였습니다."];
    }];
}


//MARK: UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if( textField == _tf_Title )  {return YES;}
    
    if( [string isEqualToString:@"#"] )
    {
        _isTagMode = YES;
        [self updateSearch:@""];
    }
    else if( [string isEqualToString:@" "] )
    {
        [self endInputTag];
    }
    return YES;
}

- (void)textFieldDidChangeSelection:(UITextField *)textField
{
    if( textField == _tf_Title )  {return;}
    
    NSInteger nStart = [textField offsetFromPosition:textField.beginningOfDocument toPosition:textField.selectedTextRange.start];
    NSInteger nEnd = [textField offsetFromPosition:textField.beginningOfDocument toPosition:textField.selectedTextRange.end];
    NSString *str_Front = [textField.text substringToIndex:nStart]; //커서 앞의 글자
    NSString *str_Back = [textField.text substringFromIndex:nEnd];   //커서 뒤의 글자

    NSLog(@"%@", str_Front);
    NSLog(@"%@", str_Back);

    if( [str_Front hasSuffix:@" "] )
    {
        //#을 지웠을 경우
        [self endInputTag];
        return;
    }
    
    NSArray *ar = [str_Front componentsSeparatedByString:@" "];
    if( ar.count > 0 )
    {
        NSString *str_Last = ar.lastObject;
        if( [str_Last hasPrefix:@"#"] )
        {
            NSLog(@"%@", str_Last);
            if( [str_Last hasPrefix:@"#"] )
            {
                str_Last = [str_Last stringByReplacingOccurrencesOfString:@"#" withString:@""];
            }

            _isTagMode = YES;
            [self updateSearch:str_Last];
        }
        else
        {
            _isTagMode = YES;
            [self updateSearch:str_Last];
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if( textField == _tf_Title )  {return;}
    
    if( textField == _tf_Tag )
    {
        [self endInputTag];
    }
}

- (void)endInputTag
{
    [_arM_TagList removeAllObjects];
    _isTagMode = NO;
    [_tbv_List reloadData];
}

//MARK: UITableviewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    _btn_Done.enabled = NO;
    _btn_Modify.enabled = _arM_List.count > 0;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if( _isTagMode )
    {
        _btn_Modify.hidden = NO;
        _btn_ModifyDone.hidden = YES;
        [_tbv_List setEditing:NO animated:NO];

        return _arM_TagList.count;
    }
    else
    {
        return _arM_List.count;
    }

    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( _isTagMode )
    {
        SearchTextCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchTextCell"];
        
        if( IsArrayOutOfBounds(_arM_TagList, indexPath.row) )  return cell;
        
        NSDictionary *dic = _arM_TagList[indexPath.row];
        cell.lb_Title.text = [dic stringForKey:@"name"];
        NSString *str_ImageUrl = [dic stringForKey:@"imgUrl"];
        [cell.iv_Thumb sd_setImageWithURL:[NSURL URLWithString:str_ImageUrl] placeholderImage:[UIImage imageNamed:@"ic_no_user"]];
        return cell;
    }
    
    NSDictionary *dic = _arM_List[indexPath.row];
    NSString *str_Type = [dic stringForKey:@"type"];
    if( [str_Type isEqualToString:@"text"] )
    {
        PlayMakeTextCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlayMakeTextCell"];
        cell.lb_Title.text = [dic stringForKey:@"text"];
        return cell;
    }
    else if( [str_Type isEqualToString:@"file"] )
    {
        _btn_Done.enabled = YES;
        NSDictionary *dic_File = [dic objectForKey:@"obj"];
        UIImage *image = [dic_File objectForKey:@"image"];
        PlayMakeMediaCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlayMakeMediaCell"];
        cell.iv_Thumb.image = image;
        
        CGFloat fRatio = (kDeviceWidth-30)/image.size.width;
        cell.lc_ImageHeight.constant = image.size.height * fRatio;
        return cell;
    }
    else if( [str_Type isEqualToString:@"youtube"] )
    {
        _btn_Done.enabled = YES;
        PlayMakeYouTubeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlayMakeYouTubeCell"];
        NSString *str_VideoId = [dic stringForKey:@"text"];
        NSString *str_ImagePath = [NSString stringWithFormat:@"https://img.youtube.com/vi/%@/maxresdefault.jpg", str_VideoId];
        [cell.iv_Thumb sd_setImageWithString:str_ImagePath];
        return cell;
    }
    else if( [str_Type isEqualToString:@"product"] )
    {
        ProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProductCell"];
        NSDictionary *dic_File = [dic objectForKey:@"obj"];
        UIImage *image = [dic_File objectForKey:@"image"];
        cell.iv_Thumb.image = image;
        cell.lb_Title.text = [dic_File stringForKey:@"title"];
        cell.lb_Contents.text = [dic_File stringForKey:@"subTitle"];
        NSInteger nPrice = [dic_File integerForKey:@"price"];
        NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
        format.numberStyle = NSNumberFormatterDecimalStyle;
        NSString *str_Price = [format stringFromNumber:@(nPrice)];
        [cell.btn_Price setTitle:[NSString stringWithFormat:@"%@원", str_Price] forState:UIControlStateNormal];
        return cell;

    }
//    _completionBlock(@{@"image":_iv_Thumb.image, @"title":_tf_Title.text, @"subTitle":_tf_SubTitle.text, @"price":_tf_Price.text});

    
    PlayMakeTextCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlayMakeTextCell"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if( _isTagMode )
    {
        NSDictionary *dic = _arM_TagList[indexPath.row];
//        if( [_arM_SelectedTag containsObject:dic] == NO )
//        {
            [_arM_SelectedTag addObject:dic];
            
            NSInteger nStart = [_tf_Tag offsetFromPosition:_tf_Tag.beginningOfDocument toPosition:_tf_Tag.selectedTextRange.start];
            NSInteger nEnd = [_tf_Tag offsetFromPosition:_tf_Tag.beginningOfDocument toPosition:_tf_Tag.selectedTextRange.end];
            NSString *str_Front = [_tf_Tag.text substringToIndex:nStart]; //커서 앞의 글자
            NSString *str_Back = [_tf_Tag.text substringFromIndex:nEnd];   //커서 뒤의 글자
            
            NSMutableArray *arM = [NSMutableArray arrayWithArray:[str_Front componentsSeparatedByString:@" "]];
            if( arM.count > 0 )
            {
                [arM removeLastObject];
                [arM addObject:[NSString stringWithFormat:@"#%@", [dic stringForKey:@"name"]]];
                if( str_Back.length > 0 )
                {
                    [arM addObject:str_Back];
                }
//                else
//                {
//                    [arM addObject:@" "];
//                }
                
                NSMutableString *strM = [NSMutableString string];
                for( NSInteger i = 0; i < arM.count; i++ )
                {
                    [strM appendString:arM[i]];
                    [strM appendString:@" "];
                }
                
                _tf_Tag.text = strM;
            }
//        }
        
        [self endInputTag];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( _isTagMode )
    {
        return NO;
    }
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if( _isTagMode )
    {
        return NO;
    }
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    NSDictionary *dic = [_arM_List[sourceIndexPath.row] copy];
    [_arM_List removeObjectAtIndex:sourceIndexPath.row];
    [_arM_List insertObject:dic atIndex:destinationIndexPath.row];
    [_tbv_List reloadData];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        if( _arM_List.count <= 0 )  {return;}
        
        if( _arM_List.count == 1 )
        {
            _btn_Modify.hidden = NO;
            _btn_ModifyDone.hidden = YES;
            [_tbv_List setEditing:NO animated:NO];
        }

        [_arM_List removeObjectAtIndex:indexPath.row];
        [tableView reloadData];
    }
}

- (IBAction)goMedia:(id)sender
{
    [HapticHelper tap];

    __weak __typeof(&*self)weakSelf = self;

    InstarPicker *instarPicker = [[InstarPicker alloc] init];
    [instarPicker setMultiCompletionHandler:^(NSArray * _Nonnull array) {
        
        if( array.count > 0 )
        {
            //        array.append(["type":"video", "asset":video.asset as Any, "thumb":video.thumbnail, "url":video.url])
            //        array.append(["type":"image", "thumb":photo.image])
            for( NSInteger i = 0; i < array.count; i++ )
            {
                NSDictionary *dic = array[i];
                [weakSelf.arM_List addObject:@{@"type":@"file", @"obj":dic}];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf.tbv_List reloadData];
                });

//                NSString *str_Type = [dic stringForKey:@"type"];
//                if( [str_Type isEqualToString:@"image"] )
//                {
//
//                }
//                else if( [str_Type isEqualToString:@"video"] )
//                {
//
//                }
            }

        }
    }];
    
    YPImagePicker *pickerNavi = [instarPicker getMultiPhotoWithCount:5];
    [self presentViewController:(UINavigationController *)pickerNavi animated:YES completion:nil];
}


- (IBAction)goEdit:(id)sender
{
    if( _isTagMode && _arM_TagList.count > 0 )
    {
        return;
    }

    [HapticHelper tap];

    if( _btn_Modify.hidden == YES )
    {
        _btn_Modify.hidden = NO;
        _btn_ModifyDone.hidden = YES;
        [_tbv_List setEditing:NO animated:YES];
    }
    else
    {
        _btn_Modify.hidden = YES;
        _btn_ModifyDone.hidden = NO;
        [_tbv_List setEditing:YES animated:YES];
    }
//    _btn_Modify.hidden = _btn_ModifyDone.hidden;
//    _btn_ModifyDone.hidden = !_btn_Modify.hidden;
//    [_tbv_List reloadData];
}


- (IBAction)goDone:(id)sender
{
    BOOL isOk = NO;
    for( NSDictionary *dic in _arM_List )
    {
        if( [[dic stringForKey:@"type"] isEqualToString:@"file"] || [[dic stringForKey:@"type"] isEqualToString:@"youtube"] )
        {
            isOk = YES;
            break;;
        }
    }

    if( _btn_Done.enabled == NO )
    {
        [HapticHelper tap];
        
        if( isOk == NO )
        {
            [UIAlertController showAlertInViewController:self
                                               withTitle:@""
                                                 message:@"이미지, 동영상 또는 Youtube 링크를\n 한개 이상 올려주세요."
                                       cancelButtonTitle:nil
                                  destructiveButtonTitle:nil
                                       otherButtonTitles:@[@"예"]
                                                tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                    
                                                }];
        }
        return;
    }

    if( _btn_Done.enabled && isOk )
    {
        [HapticHelper tap];

        _btn_Modify.hidden = NO;
        _btn_ModifyDone.hidden = YES;
        [_tbv_List setEditing:NO animated:YES];
        
        _arM_SendTags = [NSMutableArray array];

        NSArray *ar_AllTags = [_tf_Tag.text componentsSeparatedByString:@" "];
        for( NSString *tag in ar_AllTags )
        {
            if( tag.length <= 0 ) {continue;}
            
            NSString *newTag = [tag stringByReplacingOccurrencesOfString:@"#" withString:@""];

            BOOL isServerTag = NO;
            for( NSDictionary *dic in _arM_SelectedTag )
            {
                NSString *str_TagName = [dic stringForKey:@"name"];
                if( [str_TagName isEqualToString:newTag] )
                {
                    isServerTag = YES;
                    [_arM_SendTags addObject:@{@"tagName":str_TagName, @"tagId":@([dic integerForKey:@"id"])}];
                    break;
                }
            }
            
            if( isServerTag == NO )
            {
                [_arM_SendTags addObject:@{@"tagName":newTag}];
            }

        }

        NSLog(@"%@", _arM_SendTags);
        [self uploadData];
    }
    
}

@end
