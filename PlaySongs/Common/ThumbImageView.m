//
//  ThumbImageView.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/11.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "ThumbImageView.h"

@implementation ThumbImageView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.clipsToBounds = YES;
    self.layer.cornerRadius = self.frame.size.width / 2;
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

@end
