//
//  TagCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/08.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TagCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UICollectionView *cv_List;

@end

NS_ASSUME_NONNULL_END
