//
//  ReportCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/09/02.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlaySongs-Swift.h"

//#import "M13Checkbox.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btn_Title;
@property (weak, nonatomic) IBOutlet M13Checkbox *check;

@end

NS_ASSUME_NONNULL_END
