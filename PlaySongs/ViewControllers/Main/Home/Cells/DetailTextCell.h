//
//  DetailTextCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/16.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DetailTextCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lb_Contents;

@end

NS_ASSUME_NONNULL_END
