//
//  CommentViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/11.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "CommentViewController.h"
#import "UITextView+Placeholder.h"
#import "CommentCell.h"
#import "ODRefreshControl.h"
#import "UserPageViewController.h"

#define kLimitCount 10

@interface CommentViewController () <UITextViewDelegate>
@property (nonatomic, assign) BOOL isFinish;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, strong) NSNumber *index;
@property (nonatomic, strong) NSMutableArray *arM_List;
@property (nonatomic, strong) ODRefreshControl *refreshControl;

@property (weak, nonatomic) IBOutlet UIView *v_MsgBg;
@property (weak, nonatomic) IBOutlet UITextView *tv_Msg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_MsgHeight;
@property (weak, nonatomic) IBOutlet UIImageView *iv_MyThumb;
@property (weak, nonatomic) IBOutlet UIButton *btn_Send;
@property (weak, nonatomic) IBOutlet UITableView *tbv_List;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_AccBottom;
@end

@implementation CommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _index = @(0);
    _arM_List = [NSMutableArray array];
    
    _tv_Msg.placeholder = @"댓글 달기...";
    _v_MsgBg.layer.cornerRadius = 12.0f;
    _v_MsgBg.layer.borderWidth = 0.8f;
    _v_MsgBg.layer.borderColor = [UIColor colorWithHexString:@"DCDCDC"].CGColor;
    
    [_iv_MyThumb sd_setImageWithURL:[NSURL URLWithString:[UserData sharedData].imgUrl] placeholderImage:[UIImage imageNamed:@"ic_no_user"]];
    
    _refreshControl = [[ODRefreshControl alloc] initInScrollView:_tbv_List];
    _refreshControl.tintColor = kEnableColor;
    _refreshControl.backgroundColor = [UIColor whiteColor];
    [_refreshControl addTarget:self action:@selector(onRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tbv_List addSubview:_refreshControl];
    
    [self updateList];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [MBProgressHUD hide];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateOneItemNoti" object:_str_PlayId];

    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
//- (void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//    
//}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)updateList
{
    if( _isFinish )     return;
    if( _isLoading )    return;
    
    _isLoading = YES;
    
    __weak __typeof__(self) weakSelf = self;
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@([_str_PlayId integerValue]) forKey:@"playId"];
    [dicM_Params setObject:@(_arM_List.count) forKey:@"start"];
    [dicM_Params setObject:@(kLimitCount) forKey:@"limit"];
    
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"play/comment" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.refreshControl endRefreshing];
        });
        
        if( error != nil )
        {
            weakSelf.isLoading = NO;
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            NSArray *ar = [resulte valueForArrayKey:@"result"];
            if( ar.count > 0 )
            {
                if( weakSelf.arM_List.count > 0 )
                {
                    [weakSelf.arM_List addObjectsFromArray:ar];
                }
                else
                {
                    weakSelf.arM_List = [NSMutableArray arrayWithArray:ar];
                }
            }
            else
            {
                //finish
                weakSelf.isFinish = YES;
            }
        }
        
        [weakSelf.tbv_List reloadData];
        weakSelf.isLoading = NO;
    }];
}

- (void)sendMsg
{
    __weak __typeof__(self) weakSelf = self;
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@([_str_PlayId integerValue]) forKey:@"playId"];
    [dicM_Params setObject:@([UserData sharedData].userId) forKey:@"userId"];
    [dicM_Params setObject:_tv_Msg.text forKey:@"comment"];

    [[WebAPI sharedData] callAsyncWebAPIBlock:@"play/comment" param:dicM_Params withMethod:@"POST" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];

        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            weakSelf.lc_MsgHeight.constant = 33;
            weakSelf.tv_Msg.text = @"";
            weakSelf.btn_Send.selected = NO;

            NSDictionary *dic_Result = [resulte valueForDictionaryKey:@"result"];
            if( weakSelf.arM_List.count > 0 )
            {
                [weakSelf.arM_List insertObject:dic_Result atIndex:0];
            }
            else
            {
                weakSelf.arM_List = [NSMutableArray arrayWithObject:dic_Result];
            }
            
            [weakSelf.tbv_List reloadData];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            [weakSelf.tbv_List scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
    }];
}

- (void)onRefresh:(ODRefreshControl *)sender
{
    [HapticHelper tap];
    
    //    [WebAPI sharedData] cancell
    _index = @(0);
    _isFinish = NO;
    _isLoading = NO;
    [_arM_List removeAllObjects];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self updateList];
    });
}


//MARK: Noti
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    _tbv_List.contentInset = UIEdgeInsetsMake(_tbv_List.contentInset.top, _tbv_List.contentInset.left, keyboardBounds.size.height, _tbv_List.contentInset.right);
    
    _lc_AccBottom.constant = -(keyboardBounds.size.height - [Util keyWindow].safeAreaInsets.bottom);
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    _tbv_List.contentInset = UIEdgeInsetsZero;
    
    _lc_AccBottom.constant = 0;
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}


//MARK: UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    _btn_Send.selected = textView.text.length > 0;
    
    if( textView.contentSize.height > 100 )
    {
        _lc_MsgHeight.constant = 100;
        return;
    }
    
    [textView sizeToFit];
    
    if( textView.contentSize.height < 33 )
    {
        if( _lc_MsgHeight.constant != 33 )
        {
            _lc_MsgHeight.constant = 33;
            [UIView animateWithDuration:0.25f animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    }
    else if( textView.contentSize.height > 100 )
    {
        if( _lc_MsgHeight.constant != 33 )
        {
            _lc_MsgHeight.constant = 100;
            [UIView animateWithDuration:0.25f animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    }
    else
    {
        if( _lc_MsgHeight.constant != textView.contentSize.height )
        {
            _lc_MsgHeight.constant = textView.contentSize.height;
            [UIView animateWithDuration:0.25f animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    }
}


//MARK: TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arM_List.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentCell"];
    
    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return cell;
    
    NSDictionary *dic = _arM_List[indexPath.row];
    
    NSString *str_ImageUrl = [dic stringForKey:@"imgUrl"];
    [cell.iv_Thumb sd_setImageWithURL:[NSURL URLWithString:str_ImageUrl] placeholderImage:[UIImage imageNamed:@"ic_no_user"]];
    
    NSString *str_Name = [dic stringForKey:@"name"];
    NSString *str_Contents = [NSString stringWithFormat:@"  %@", [dic stringForKey:@"comment"]];
    NSMutableAttributedString *attM = [[NSMutableAttributedString alloc] initWithString:str_Name attributes:@{NSForegroundColorAttributeName:[UIColor blackColor],
                                                                                                              NSFontAttributeName:[UIFont systemFontOfSize:14 weight:UIFontWeightBold]}];
    NSAttributedString *att = [[NSAttributedString alloc] initWithString:str_Contents attributes:@{ NSForegroundColorAttributeName:[UIColor darkGrayColor],
                                                                                                                 NSFontAttributeName:[UIFont systemFontOfSize:14 weight:UIFontWeightMedium]}];
    [attM appendAttributedString:att];
    cell.lb_Contents.attributedText = attM;
    
    NSString *str_Date = [NSString stringWithFormat:@"%@", [dic stringForKey:@"createDate"]];
    cell.lb_Time.text = [Util getCommentAfterTime:str_Date];

    cell.iv_Thumb.tag = indexPath.row;
    UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userImageTap:)];
    [imageTap setNumberOfTapsRequired:1];
    [cell.iv_Thumb addGestureRecognizer:imageTap];

    
    BOOL lastItemReached = [dic isEqual:[_arM_List lastObject]];
    if (lastItemReached && indexPath.row == [_arM_List count] - 1)
    {
        [self updateList];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [_tv_Msg resignFirstResponder];
}

- (void)userImageTap:(UIGestureRecognizer *)gestureRecognizer
{
    [HapticHelper tap];

    [_tv_Msg resignFirstResponder];

    UIView *view = gestureRecognizer.view;
    NSDictionary *dic = _arM_List[view.tag];
    UserPageViewController *vc = [kUserBoard instantiateViewControllerWithIdentifier:@"UserPageViewController"];
    vc.dic_Owner = dic;
    [self.navigationController pushViewController:vc animated:YES];
}

//MARK: Action
- (IBAction)goSend:(id)sender
{
    if( _btn_Send.selected == NO )  return;
    
    [HapticHelper tap];
    
    [self sendMsg];
}

@end
