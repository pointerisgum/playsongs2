//
//  IBExtensions.swift
//  PPUser
//
//  Created by Mac on 2017. 12. 22..
//  Copyright © 2017년 pplus. All rights reserved.
//

import UIKit

extension UIView {
	
	var bundle: Bundle { return Bundle(for: type(of: self)) }
	
	func ib_image(named: String) -> UIImage? {
		#if TARGET_INTERFACE_BUILDER
			return UIImage(named: named, in: self.bundle, compatibleWith: self.traitCollection)
		#else
			return UIImage(named: named)
		#endif
	}
	
	func ib_localizeString(_ key: String) -> String {
		#if TARGET_INTERFACE_BUILDER
			return self.bundle.localizedString(forKey: key, value: "", table: nil)
		#else
			return NSLocalizedString(key, comment: "")
		#endif
	}
	
}
