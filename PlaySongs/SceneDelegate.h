//
//  SceneDelegate.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/03.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

