//
//  PlayMakeProductViewController.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/29.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^CompletionBlock)(id completeResult);
@interface PlayMakeProductViewController : UIViewController
@property (nonatomic, copy) CompletionBlock completionBlock;
- (void)setCompletionBlock:(CompletionBlock)completionBlock;

@end

NS_ASSUME_NONNULL_END
