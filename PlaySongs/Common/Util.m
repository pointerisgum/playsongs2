//
//  Util.m
//  FoodView
//
//  Created by Kim Young-Min on 13. 3. 13..
//  Copyright (c) 2013년 bencrow. All rights reserved.
//

#import "Util.h"
#import <QuartzCore/QuartzCore.h>
#include <sys/xattr.h>
#import <SystemConfiguration/SystemConfiguration.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
//#import "UIImageView+AFNetworking.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import "Defines.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#import <sys/utsname.h>
#import <CommonCrypto/CommonDigest.h>
#import "NSString+URLEncode.h"
//#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Photos/Photos.h>
#import "TopToastView.h"
#import "YM_KeychainItemWrapper.h"
#import "ALToastView.h"

static Util *shared = nil;
static UIView *v_Indi = nil;
static CGFloat kTopToastViewTime = 3.f;

@implementation Util

+ (void)initialize
{
    NSAssert(self == [Util class], @"Singleton is not designed to be subclassed.");
    shared = [Util new];
    
    //로딩용 인디케이터 뷰 만들기
    v_Indi = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [v_Indi setBackgroundColor:[UIColor clearColor]];
    v_Indi.userInteractionEnabled = YES;
    UIImageView *iv_BG = [[UIImageView alloc]initWithFrame:v_Indi.frame];
    [iv_BG setBackgroundColor:[UIColor blackColor]];
    iv_BG.alpha = 0.6f;
    [v_Indi addSubview:iv_BG];
    
    UIImageView *iv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 60, 50)];
    iv.center = v_Indi.center;
    [iv setBackgroundColor:[UIColor blackColor]];
    [iv setAlpha:0.7f];
    [Util imageRounding:iv];
    [v_Indi addSubview:iv];
    
    UIActivityIndicatorView *indi = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleMedium];
    indi.center = CGPointMake(v_Indi.frame.size.width/2, v_Indi.frame.size.height/2);
    [indi setHidesWhenStopped:YES];
    [v_Indi addSubview:indi];
}

+ (Util *)sharedData
{
    return shared;
}

- (void)addIndicator
{
    [self performSelectorInBackground:@selector(onAddIndicator) withObject:nil];
}

- (void)onAddIndicator
{
    [v_Indi removeFromSuperview];
    
    UIWindow *window = [Util keyWindow];
    v_Indi.alpha = NO;
    [window addSubview:v_Indi];
    
    for( id subView in [v_Indi subviews] )
    {
        if( [subView isKindOfClass:[UIActivityIndicatorView class]] )
        {
            UIActivityIndicatorView *indi = (UIActivityIndicatorView *)subView;
            [indi startAnimating];
        }
    }
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         v_Indi.alpha = YES;
                     }];
}

- (void)removeIndicator
{
    if( !v_Indi ) return;
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         v_Indi.alpha = NO;
                     } completion:^(BOOL finished) {
                         [v_Indi removeFromSuperview];
                     }];
}

+ (BOOL)isNetworkCheckAlert
{
    NSString *str_Status = [Util getNetworkSatatus];
    if( str_Status == nil )
    {
//        ALERT(nil, @"네트워크에 접속할 수 없습니다.\n3G 및 Wifi 연결상태를\n확인해주세요.", nil, @"확인", nil);
        return NO;
    }
    
    return YES;
}

+ (NSString *)getNetworkSatatus
{
    NSString *str_ReturnSatatus = nil;
    
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    
    if (!didRetrieveFlags)
    {
        printf("Error. Could not recover network reachability flags\n");
        return str_ReturnSatatus;
    }
    
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    
    BOOL isNetworkStatus = (isReachable && !needsConnection) ? YES : NO;
    
    if(!isNetworkStatus)    return str_ReturnSatatus;
    
    zeroAddress.sin_addr.s_addr = htonl(IN_LINKLOCALNETNUM);
    
    SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    
    CFRelease(defaultRouteReachability);
    
    if( flags & kSCNetworkReachabilityFlagsIsWWAN )
        str_ReturnSatatus = @"3G";
    else
        str_ReturnSatatus = @"Wifi";
    
    return str_ReturnSatatus;
}

+ (BOOL)isOffLine
{
    NSString *str_NetworkStatus = [Util getNetworkSatatus];
    if( str_NetworkStatus == nil || str_NetworkStatus.length <= 0 )
    {
        return YES;
    }
    
    return NO;
}

+ (void)imageRounding:(UIView *)v
{
    v.layer.cornerRadius = 5.0;
    v.layer.masksToBounds = YES;
}

+ (void)imageRoundingAndBorder:(UIView *)v
{
    v.layer.cornerRadius = 5.0;
    v.layer.masksToBounds = YES;
    v.layer.borderColor = [UIColor colorWithRed:208.0f/255.0f green:180.0f/255.0f blue:216.0f/255.0f alpha:1].CGColor;//[UIColor colorWithRed:211.0f/255.0f green:211.0f/255.0f blue:211.0f/255.0f alpha:1].CGColor;
    v.layer.borderWidth = 1.0;
}

+ (void)spinLayer:(CALayer *)inLayer duration:(CFTimeInterval)inDuration direction:(int)direction
{
    CABasicAnimation* rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.repeatCount = 1;
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 1 * direction];
    rotationAnimation.duration = inDuration;
    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    [inLayer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

+ (void)rotationImage:(UIView *)view withRadian:(int)radian
{
    [UIView animateWithDuration:3.3f
                     animations:^{
                         view.transform = CGAffineTransformMakeRotation(degreesToRadian(radian));
                     }];
}

+ (NSString*)getOnlyNumber:(NSString *)aString
{
    NSMutableString *strippedString = [NSMutableString stringWithCapacity:[aString length]];
    NSScanner *scanner = [NSScanner scannerWithString:aString];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    while ([scanner isAtEnd] == NO)
    {
        NSString *buffer;
        if ([scanner scanCharactersFromSet:numbers intoString:&buffer]) {
            [strippedString appendString:buffer];
        } else {
            [scanner setScanLocation:([scanner scanLocation] + 1)];
        }
    }
    
    return [NSString stringWithString:strippedString];
}

+ (BOOL)createFile:(NSString *)filePath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
//    if( [fileManager fileExistsAtPath:filePath] )
//    {
//        [fileManager removeItemAtPath:filePath error:nil];
//    }

    BOOL isSuccess = [fileManager createFileAtPath:filePath contents:[NSData data] attributes:nil];
    if( !isSuccess )
    {
        NSLog(@"File Make Fail");
        return NO;
    }
    
    NSLog(@"Make File Success");
    return YES;
}

+ (BOOL)isUsableEmail:(NSString *)str
{
    //1차로 한글이 있는지 검색
    const char *tmp = [str cStringUsingEncoding:NSUTF8StringEncoding];

    if (str.length != strlen(tmp))
    {
        return NO;
    }

    //2차로 이메일 형식인지 검색
    NSString *check = @"([0-9a-zA-Z_-]+)@([0-9a-zA-Z_-]+)(\\.[0-9a-zA-Z_-]+){1,2}";
    NSRange match = [str rangeOfString:check options:NSRegularExpressionSearch];
    if (NSNotFound == match.location)
    {
        return NO;
    }

    return YES;
}

//+ (BOOL)isUsableEmail:(NSString *)str
//{
//    NSString *ptn = @"^[a-zA-Z0-9]+@[a-zA-Z0-9]+$  or  ^[_0-9a-zA-Z-]+@[0-9a-zA-Z-]+(.[_0-9a-zA-Z-]+)*$";
//    //    NSString *ptn = @"^(?:\\w+\\.?)*\\w+@(?:\\w+\\.)+\\w+$";
//
//    NSRange range = [str rangeOfString:ptn options:NSRegularExpressionSearch];
//
//    if( (range.length != NSNotFound) && (range.length > 5) )
//    {
//        return YES;
//    }
//
//    return NO;
//}

+ (BOOL)addSkipBackupAttributeToiTemAtURL:(NSURL *)URL
{
    const char *filePath = [[URL path] fileSystemRepresentation];
    const char *attrName = "com.apple.MobileBackup";
    
    u_int8_t attrValue = 1;
    
    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    
    return result == 0;
}

+ (UIImage *)thumbnailFromVideoAtURL:(NSString *)path
{
    UIImage *theImage = nil;
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:path] options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generator copyCGImageAtTime:time actualTime:NULL error:&err];
    
    theImage = [[UIImage alloc] initWithCGImage:imgRef];
    
    CGImageRelease(imgRef);
    
    return theImage;
}

+ (BOOL)isUsablePhoneNumber:(NSString *)str
{
    str = [str stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    NSString *ptn = @"(010|011|016|017|018|019)([0-9]{3,4})([0-9]{4})";
    NSRange range = [str rangeOfString:ptn options:NSRegularExpressionSearch];
    
    if( ![str hasPrefix:@"0"] )
    {
        return NO;
    }
    
    if( (range.length != NSNotFound) && (range.length > 8) )
    {
        return YES;
    }
    
    return NO;
}

+ (BOOL)isOnlyNumber:(NSString *)str
{
    NSString *ptn = @"^[0-9]*$";
    NSRange range = [str rangeOfString:ptn options:NSRegularExpressionSearch];

    if( range.length != NSNotFound && (range.length > 0) )
    {
        return YES;
    }
    
    return NO;
}

+ (BOOL)isOnlyEnglish:(NSString *)str
{
    NSString *ptn = @"^[A-Za-z]*$";
    NSRange range = [str rangeOfString:ptn options:NSRegularExpressionSearch];
    
    if( range.length != NSNotFound && (range.length > 0) )
    {
        return YES;
    }
    
    return NO;
}

+ (void)makeCircleImage:(UIView *)iv withBorderWidth:(float)border
{
    iv.layer.cornerRadius = iv.frame.size.width/2;
    iv.layer.masksToBounds = YES;
    iv.layer.borderColor = [UIColor lightGrayColor].CGColor;
    iv.layer.borderWidth = border;
}

+ (void)addTextShodow:(UILabel *)text
{
    text.textColor = [UIColor whiteColor];
    text.layer.shadowOffset = CGSizeMake(0.5f, 0.5f);
    text.layer.shadowOpacity = 1.0f;
    text.layer.shadowRadius = 1.0f;
}

+ (UIView *)createNavigationTitleView:(UIView *)view withTitle:(NSString *)title
{
    UILabel *titleView = (UILabel *)view;
    if (!titleView)
    {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont boldSystemFontOfSize:20.0];
        titleView.textColor = [UIColor whiteColor];
        titleView.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
        titleView.layer.shadowOpacity = 0.1f;
        titleView.layer.shadowRadius = 1.0f;
        
        titleView.textColor = [UIColor colorWithRed:115.0f/255.0f green:115.0f/255.0f blue:115.0f/255.0f alpha:1];
        titleView.text = title;
        [titleView sizeToFit];
    }
    
    return titleView;
}

+ (NSString *)makeThumbNailUrlString:(NSString *)aUrlString withSize:(NSString *)aSize;
{
    NSMutableString *strM_ImageUrl = [NSMutableString string];
    NSArray *ar_Sep = [aUrlString componentsSeparatedByString:@"_"];
    for( int i = 0; i < [ar_Sep count] - 1; i++ )
    {
        [strM_ImageUrl appendString:[ar_Sep objectAtIndex:i]];
        [strM_ImageUrl appendString:@"_"];
    }
    
    NSString *str_LastObj = [ar_Sep lastObject];
    NSString *str_Extension = [str_LastObj pathExtension];
    
    NSArray *ar_LastSep = [str_LastObj componentsSeparatedByString:@"."];
    if( [ar_LastSep count] > 1 )
    {
        NSString *str_BeforeSize = [ar_LastSep objectAtIndex:0];
        if( [str_BeforeSize longLongValue] < [aSize longLongValue] || [str_BeforeSize longLongValue] > 1280 )
        {
            //요청 사이즈보다 원본이 작거나 1280보다 클 경우
            return aUrlString;
        }
        else
        {
            [strM_ImageUrl appendString:[NSString stringWithFormat:@"%@.%@", aSize, str_Extension]];
            return strM_ImageUrl;
        }
    }
    else
    {
        return aUrlString;
    }
    
    return nil;
}

+ (BOOL)isStringCheck:(NSString *)aString
{
    NSString *ptn = @"^[A-Za-z0-9]*$";
    NSRange range = [aString rangeOfString:ptn options:NSRegularExpressionSearch];
    if( range.length != NSNotFound && (range.length > 0) )
    {
        return YES;
    }
    
    return NO;
}

//빠바 비번체크(문자 숫자 조합 6자리 이상) //영문과 숫자의 조합
+ (BOOL)isPariPwCheck:(NSString *)aString
{
    //한글은 영문과 특수문자만 입력되게 키보드 설정이 되어 있음. keyboardType = ASKII
    
    if( [Util isOnlyNumber:aString] )
    {
        //숫자만 있다면
        return NO;
    }

    if( [Util isStringCheck:aString] == NO )
    {
        //영문과 숫자외에 다른 케릭터가 들어가 있다면
        return NO;
    }
    
    if( [Util isOnlyEnglish:aString] )
    {
        //영문만 있다면
        return NO;
    }
    
    return YES;
}


+ (void)setRound:(const UIView *)view withCorners:(UIRectCorner)corners
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(5.0f, 5.0f)];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
}

+ (void)setRound:(const UIView *)view withCornerSize:(CGSize)size
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:size];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
}


//특수문자 검사 (특수문자가 있으면 YES, 없으면 NO)
+ (BOOL)isSpecialCharacter:(NSString *)str
{
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *ptn = @"^[ㄱ-힣0-9a-zA-Z]*$";
    NSRange range = [str rangeOfString:ptn options:NSRegularExpressionSearch];
    
    if( range.length != NSNotFound && (range.length > 0) )
    {
        return NO;
    }
    
    return YES;
}

+ (void)setMainNaviBar:(UINavigationBar *)naviBar
{
    [[UINavigationBar appearance] setBackgroundImage:BundleImage(@"navi128.png") forBarMetrics:UIBarMetricsDefault];
    
    [naviBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                     [UIColor whiteColor], NSForegroundColorAttributeName,
                                     [UIFont fontWithName:@"Helvetica-bold" size:18], NSFontAttributeName,
                                     nil]];
}

+ (void)setLoginNaviBar:(UINavigationBar *)naviBar
{
    [[UINavigationBar appearance] setBackgroundImage:BundleImage(@"navi_white128.png") forBarMetrics:UIBarMetricsDefault];
//    [[UINavigationBar appearance] setBackgroundColor:[UIColor whiteColor]];
    
    [naviBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                     [UIColor blackColor], NSForegroundColorAttributeName,
                                     [UIFont fontWithName:@"Helvetica-bold" size:18], NSFontAttributeName,
                                     nil]];
}

+ (void)setSearchNaviBar:(UINavigationBar *)naviBar
{
//    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
    
    [[UINavigationBar appearance] setBackgroundImage:BundleImage(@"searchNavi.png") forBarMetrics:UIBarMetricsDefault];

    [naviBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                     [UIColor whiteColor], NSForegroundColorAttributeName,
                                     [UIFont fontWithName:@"Helvetica-bold" size:18], NSFontAttributeName,
                                     nil]];
}


+ (CGSize)getTextSize:(UILabel *)lb
{
    CGRect textRect = CGRectZero;
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                lb.font, NSFontAttributeName, nil];

    textRect = [lb.text boundingRectWithSize:CGSizeMake(lb.frame.size.width, FLT_MAX)
                                     options:NSLineBreakByClipping | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                  attributes:attributes
                                     context:nil];

    return textRect.size;
}

////채팅용
//+ (CGSize)getTextSize2:(UILabel *)lb
//{
//    CGRect textRect = CGRectZero;
//    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
//                                lb.font, NSFontAttributeName, nil];
//
//    //        NSLog(@"lb.frame.size.width : %f", lb.frame.size.width);
//
//    UIWindow *window = [Util keyWindow];
//    textRect = [lb.text boundingRectWithSize:CGSizeMake(window.frame.size.width - 100, FLT_MAX)
//                                     options:NSLineBreakByClipping | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
//                                  attributes:attributes
//                                     context:nil];
//
//    return textRect.size;
//}

+ (UIWindow *)keyWindow
{
    UIWindow *foundWindow = nil;
    NSArray* windows = [[UIApplication sharedApplication]windows];
    for (UIWindow* window in windows) {
        if (window.isKeyWindow) {
            foundWindow = window;
            break;
        }
    }
    return foundWindow;
}

+ (void)setSvContentsSize:(UIScrollView *)sv withTargetObj:(UIView *)lastObj
{
    sv.contentSize = CGSizeMake(sv.contentSize.width, lastObj.frame.origin.y + lastObj.frame.size.height);
}


//가로, 세로 맞춰 이미지 리사이징하기
+ (UIImage*)imageWithImage:(UIImage *)image convertToWidth:(float)width covertToHeight:(float)height
{
    CGSize size = CGSizeMake(width, height);
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage * newimage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimage;
}

//세로 맞춰 이미지 리사이징하기
+ (UIImage*)imageWithImage:(UIImage *)image convertToHeight:(float)height
{
    float ratio = image.size.height / height;
    float width = image.size.width / ratio;
    CGSize size = CGSizeMake(width, height);
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage * newimage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimage;
}

//가로 맞춰 이미지 리사이징하기
+ (UIImage*)imageWithImage:(UIImage *)image convertToWidth:(float)width
{
    float ratio = image.size.width / width;
    float height = image.size.height / ratio;
    CGSize size = CGSizeMake(width, height);
//    UIGraphicsBeginImageContext(size);
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);

    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage * newimage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimage;
}

+ (CGFloat)getTextViewHeight:(UITextView *)textView
{
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
    {
        // This is the code for iOS 7. contentSize no longer returns the correct value, so
        // we have to calculate it.
        //
        // This is partly borrowed from HPGrowingTextView, but I've replaced the
        // magic fudge factors with the calculated values (having worked out where
        // they came from)

        CGRect frame = textView.bounds;

        // Take account of the padding added around the text.

        UIEdgeInsets textContainerInsets = textView.textContainerInset;
        UIEdgeInsets contentInsets = textView.contentInset;

        CGFloat leftRightPadding = textContainerInsets.left + textContainerInsets.right + textView.textContainer.lineFragmentPadding * 2 + contentInsets.left + contentInsets.right;
        CGFloat topBottomPadding = textContainerInsets.top + textContainerInsets.bottom + contentInsets.top + contentInsets.bottom;

        frame.size.width -= leftRightPadding;
        frame.size.height -= topBottomPadding;

        NSString *textToMeasure = textView.text;
        if ([textToMeasure hasSuffix:@"\n"])
        {
            textToMeasure = [NSString stringWithFormat:@"%@-", textView.text];
        }

        // NSString class method: boundingRectWithSize:options:attributes:context is
        // available only on ios7.0 sdk.

        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];

        NSDictionary *attributes = @{ NSFontAttributeName: textView.font, NSParagraphStyleAttributeName : paragraphStyle };

        CGRect size = [textToMeasure boundingRectWithSize:CGSizeMake(CGRectGetWidth(frame), MAXFLOAT)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:attributes
                                                  context:nil];

        CGFloat measuredHeight = ceilf(CGRectGetHeight(size) + topBottomPadding);
        return measuredHeight;
    }
    else
    {
        return textView.contentSize.height;
    }
}


//버전 비교 (앞이 현재버전, 뒤가 최신버전 리턴값이 NSOrderedAscending면 업데이트 필요)
+ (NSComparisonResult)compareVersion:(NSString*)versionOne toVersion:(NSString*)versionTwo
{
    NSArray* versionOneComp = [versionOne componentsSeparatedByString:@"."];
    NSArray* versionTwoComp = [versionTwo componentsSeparatedByString:@"."];
    
    NSInteger pos = 0;
    
    while ([versionOneComp count] > pos || [versionTwoComp count] > pos)
    {
        NSInteger v1 = [versionOneComp count] > pos ? [[versionOneComp objectAtIndex:pos] integerValue] : 0;
        NSInteger v2 = [versionTwoComp count] > pos ? [[versionTwoComp objectAtIndex:pos] integerValue] : 0;
        if (v1 < v2)
        {
            return NSOrderedAscending;
        }
        else if (v1 > v2)
        {
            return NSOrderedDescending;
        }
        pos++;
    }
    
    return NSOrderedSame;
}


//+ (CGSize)getTextSize:(NSString *)str withAttr:(NSAttributedString *)attbStr
//{
////    NSAttributedString *at = lb.attributedText;
//
//}


//NSString *ptn = @"^[0-9]*$";
//
//NSRange range = [str rangeOfString:ptn options:NSRegularExpressionSearch];
//NSRange range1 = [str1 rangeOfString:ptn options:NSRegularExpressionSearch];
//
//NSLog(@"%d, %d", range.length, range1.length); // 4, 0을 출력
//
//
//
//
//2.영문자[특수문자(-,_,&)포함]인지 체크
//NSString *str = @"1234";
//NSString *str1 = @"asdfb-sdf";
//NSString *ptn = @"^[a-zA-Z\-\_\&]*$";
//[출처] ios nsstring 정규식|작성자 Sainteyes


+ (NSString *)getIPAddress
{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0)
    {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL)
        {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    
#if TARGET_IPHONE_SIMULATOR
    address = @"121.136.77.167";
#endif

    return address;
    
}

+ (NSString *)stringByStrippingHTML:(NSString*)str
{
    NSRange r;
    while ((r = [str rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
    {
        str = [str stringByReplacingCharactersInRange:r withString:@""];
    }
    return str;
}

+ (void)printDictionaryLog:(NSDictionary *)dic
{
    NSArray *ar_AllKeys = dic.allKeys;
    for( NSInteger i = 0; i < ar_AllKeys.count; i++ )
    {
        NSString *str_Key = ar_AllKeys[i];
        NSLog(@"key : %@ // value : %@", str_Key, [dic objectForKey:str_Key]);
    }
}

+ (NSString *)getUUID
{
    NSString *uuid = [YM_KeychainItemWrapper loadValueForKey:@"DeviceId"];

    if( uuid == nil || uuid.length == 0)
    {
        CFUUIDRef uuidRef = CFUUIDCreate(NULL);
        CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
        CFRelease(uuidRef);
        uuid = [NSString stringWithString:(__bridge NSString *) uuidStringRef];
        CFRelease(uuidStringRef);

        [YM_KeychainItemWrapper saveValue:uuid forKey:@"DeviceId"];
    }

    return uuid;
}


+ (CGFloat)getTextWith:(UILabel *)lb
{
    CGRect textRect = CGRectZero;
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                lb.font, NSFontAttributeName, nil];
    
    textRect = [lb.text boundingRectWithSize:CGSizeMake(FLT_MAX, lb.frame.size.height)
                                     options:NSLineBreakByClipping | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                  attributes:attributes
                                     context:nil];

    return textRect.size.width;
}

+ (CGFloat)getTextHeight:(UILabel *)lb withWidth:(CGFloat)fWidth withHeight:(CGFloat)fHeight
{
    CGRect textRect = CGRectZero;
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                lb.font, NSFontAttributeName, nil];
    
    textRect = [lb.text boundingRectWithSize:CGSizeMake(fWidth, fHeight)
                                     options:NSLineBreakByClipping | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                  attributes:attributes
                                     context:nil];

    return textRect.size.height;
}

+ (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(320-130, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

//+ (NSString *)getDeviceName
//{
//    struct utsname systemInfo;
//    uname(&systemInfo);
//
//    NSString *str_Model = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
//
//    if( [str_Model isEqualToString:@"iPhone3,1"] || [str_Model isEqualToString:@"iPhone3,3"] )
//    {
//        str_Model = @"iPhone4";
//    }
//    else if( [str_Model isEqualToString:@"iPhone4,1"] )
//    {
//        str_Model = @"iPhone4s";
//    }
//    else if( [str_Model isEqualToString:@"iPhone5,1"] || [str_Model isEqualToString:@"iPhone5,2"] )
//    {
//        str_Model = @"iPhone5";
//    }
//    else if( [str_Model isEqualToString:@"iPhone5,3"] || [str_Model isEqualToString:@"iPhone5,4"] )
//    {
//        str_Model = @"iPhone5c";
//    }
//    else if( [str_Model isEqualToString:@"iPhone6,1"] || [str_Model isEqualToString:@"iPhone6,2"] )
//    {
//        str_Model = @"iPhone5s";
//    }
//    else if( [str_Model isEqualToString:@"iPhone7,1"] )
//    {
//        str_Model = @"iPhone6Plus";
//    }
//    else if( [str_Model isEqualToString:@"iPhone7,2"] )
//    {
//        str_Model = @"iPhone6";
//    }
//    else if( [str_Model isEqualToString:@"iPhone8,1"] )
//    {
//        str_Model = @"iPhone6S";
//    }
//    else if( [str_Model isEqualToString:@"iPhone8,2"] )
//    {
//        str_Model = @"iPhone6SPlus";
//    }
//    else if( [str_Model isEqualToString:@"iPhone8,4"] )
//    {
//        str_Model = @"iPhoneSE";
//    }
//
//    return str_Model;
//}

+ (NSURL *)createImageUrl:(NSString *)aHeader withFooter:(NSString *)aFooter
{
    if( [aFooter isKindOfClass:[NSString class]] == NO )
    {
        return [NSURL URLWithString:@""];
    }
    
    if( [aFooter hasPrefix:@"http"] || [aFooter hasPrefix:@"https"] )
    {
        return [NSURL URLWithString:aFooter];
    }
    
    if( aHeader == nil || aHeader.length <= 0 )
    {
        aHeader = [[NSUserDefaults standardUserDefaults] objectForKey:@"img_prefix"];
    }
    
    if( aHeader == nil || aHeader.length <= 0 )
    {
        aHeader = [[NSUserDefaults standardUserDefaults] objectForKey:@"userImg_prefix"];
    }
    
    NSString *str_ImageUrl = [NSString stringWithFormat:@"%@%@", aHeader, aFooter];
    NSArray *ar_Sep = [str_ImageUrl componentsSeparatedByString:@"://"];
    if( ar_Sep.count > 1 )
    {
        NSString *str_Footer = ar_Sep[1];
        str_Footer = [str_Footer stringByReplacingOccurrencesOfString:@"//" withString:@"/"];
        str_ImageUrl = [NSString stringWithFormat:@"%@://%@", ar_Sep[0], str_Footer];
    }
    
//    NSLog(@"str_ImageUrl : %@", str_ImageUrl);
    return [NSURL URLWithString:str_ImageUrl];
}

+ (NSString *)transIntToString:(id)obj
{
    if( [obj isEqual:[NSNull null]] )
    {
        return @"";
    }
    NSInteger nObj = [obj integerValue];
    return [NSString stringWithFormat:@"%ld", nObj];
}

+ (UIImage *)makeNinePatchImage:(UIImage *)image
{
    //Clear the black 9patch regions
    CGRect imageRect = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    UIGraphicsBeginImageContextWithOptions(image.size, NO, [UIScreen mainScreen].scale);
    [image drawInRect:imageRect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextClearRect(context, CGRectMake(0, 0, image.size.width, 1));
    CGContextClearRect(context, CGRectMake(0, 0, 1, image.size.height));
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIEdgeInsets insets;
    
    //hard coded for now, could easily read the black regions if necessary
    insets.left = insets.right = image.size.width / 2 - 1;
    insets.top = insets.bottom = image.size.height / 2 - 1;
    
    UIImage *nineImage = [image resizableImageWithCapInsets:insets
                                               resizingMode:UIImageResizingModeStretch];
    
    return nineImage;
    
}

//+ (void)showToast:(NSString *)aMsg
//{
//    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
//    [window makeToast:aMsg withPosition:kPositionCenter];
//}



+ (NSString *)getThotingChatDate:(NSString *)aDay
{
    if( aDay.length < 12 )
    {
        return aDay;
    }
    
    NSDate *date = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:date];
    NSInteger nYear = [components year];
    NSInteger nMonth = [components month];
    NSInteger nDay = [components day];
    NSInteger nHour = [components hour];
    NSInteger nMinute = [components minute];
    NSInteger nSecond = [components second];
    NSString *str_Current = [NSString stringWithFormat:@"%04ld%02ld%02ld%02ld%02ld%02ld", nYear, nMonth, nDay, nHour, nMinute, nSecond];

    NSString *str_Date = [NSString stringWithFormat:@"%@", aDay];
    NSString *str_Year = [str_Date substringWithRange:NSMakeRange(0, 4)];
    NSString *str_Month = [str_Date substringWithRange:NSMakeRange(4, 2)];
    NSString *str_Day = [str_Date substringWithRange:NSMakeRange(6, 2)];
    
    if( nYear > [str_Year integerValue] )
    {
        return [NSString stringWithFormat:@"%@년 %ld월 %ld일", str_Year, [str_Month integerValue], [str_Day integerValue]];
//        return [NSString stringWithFormat:@"%ld년전", nYear - [str_Year integerValue]];
    }
    else if( nMonth > [str_Month integerValue] )
    {
        return [NSString stringWithFormat:@"%ld월 %ld일", [str_Month integerValue], [str_Day integerValue]];
//        return [NSString stringWithFormat:@"%ld개월전", nMonth - [str_Month integerValue]];
    }
    else if( nDay > [str_Day integerValue] )
    {
        return [NSString stringWithFormat:@"%ld월 %ld일", [str_Month integerValue], [str_Day integerValue]];
//        return [NSString stringWithFormat:@"%ld일전", nDay - [str_Day integerValue]];
    }
    else
    {
        NSString *str_Hour = [str_Date substringWithRange:NSMakeRange(8, 2)];
        NSString *str_Minute = [str_Date substringWithRange:NSMakeRange(10, 2)];
        str_Date = [NSString stringWithFormat:@"%@ %ld:%02ld",
                    [str_Hour integerValue] > 12 ? @"오후" : @"오전",
                    ([str_Hour integerValue] > 12) ? [str_Hour integerValue] - 12 : [str_Hour integerValue] == 0 ? 12 : [str_Hour integerValue], [str_Minute integerValue]];
        
        return str_Date;
    }

    return @"";
}

+ (NSString *)getDetailDate:(NSString *)aDay
{
    if( aDay.length < 12 )
    {
        return aDay;
    }
    
    NSDate *date = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:date];
    NSInteger nYear = [components year];
    NSInteger nMonth = [components month];
    NSInteger nDay = [components day];
    NSInteger nHour = [components hour];
    NSInteger nMinute = [components minute];
    NSInteger nSecond = [components second];
    NSString *str_Current = [NSString stringWithFormat:@"%04ld%02ld%02ld%02ld%02ld%02ld", nYear, nMonth, nDay, nHour, nMinute, nSecond];
    
    NSString *str_Date = [NSString stringWithFormat:@"%@", aDay];
    NSString *str_Year = [str_Date substringWithRange:NSMakeRange(0, 4)];
    NSString *str_Month = [str_Date substringWithRange:NSMakeRange(4, 2)];
    NSString *str_Day = [str_Date substringWithRange:NSMakeRange(6, 2)];
    
    NSString *str_Hour = [str_Date substringWithRange:NSMakeRange(8, 2)];
    NSString *str_Minute = [str_Date substringWithRange:NSMakeRange(10, 2)];
    str_Date = [NSString stringWithFormat:@"%@ %ld:%02ld",
                [str_Hour integerValue] > 12 ? @"오후" : @"오전",
                ([str_Hour integerValue] > 12) ? [str_Hour integerValue] - 12 : [str_Hour integerValue] == 0 ? 12 : [str_Hour integerValue], [str_Minute integerValue]];
    
    return str_Date;
}

+ (NSString *)getMainThotingChatDate:(NSString *)aDay
{
    NSDate *date = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:date];
    NSInteger nYear = [components year];
    NSInteger nMonth = [components month];
    NSInteger nDay = [components day];
//    NSInteger nHour = [components hour];
//    NSInteger nMinute = [components minute];

    
    
    
    
    
    NSString *str_Date = [NSString stringWithFormat:@"%@", aDay];
    NSString *str_Year = [str_Date substringWithRange:NSMakeRange(0, 4)];
    NSString *str_Month = [str_Date substringWithRange:NSMakeRange(4, 2)];
    NSString *str_Day = [str_Date substringWithRange:NSMakeRange(6, 2)];
    NSString *str_Hour = [str_Date substringWithRange:NSMakeRange(8, 2)];
    NSString *str_Minute = [str_Date substringWithRange:NSMakeRange(10, 2)];
    
    
    
    //오늘자
    NSString *str_Current = [NSString stringWithFormat:@"%04ld%02ld%02ld", nYear, nMonth, nDay];
    NSString *str_Target = [NSString stringWithFormat:@"%04ld%02ld%02ld", [str_Year integerValue], [str_Month integerValue], [str_Day integerValue]];
    if( [str_Current integerValue] == [str_Target integerValue] )
    {
        NSString *str_Time = [NSString stringWithFormat:@"%@ %ld:%02ld",
                              [str_Hour integerValue] > 12 ? @"오후" : @"오전",
                              ([str_Hour integerValue] > 12) ? [str_Hour integerValue] - 12 : [str_Hour integerValue] == 0 ? 12 : [str_Hour integerValue], [str_Minute integerValue]];

        return str_Time;
    }

    //하루가 지난것
    return [NSString stringWithFormat:@"%ld월 %ld일", [str_Month integerValue], [str_Day integerValue]];
}

+ (void)addChannelUrl:(NSString *)aUrl withRId:(NSString *)aRId
{
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        [[NSUserDefaults standardUserDefaults] objectForKey:@"apiToken"], @"apiToken",
                                        [Util getUUID], @"uuid",
                                        aUrl, @"channelUrl",
                                        aRId, @"rId",
                                        nil];
    
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"v1/update/chat/room/channel/url"
                                        param:dicM_Params
                                   withMethod:@"GET"
                                    withBlock:^(id resulte, NSError *error) {
                                        
                                        [MBProgressHUD hide];
                                        
                                        if( resulte )
                                        {

                                        }
                                    }];
}

+ (void)addOpenChannelUrl:(NSString *)aUrl withRId:(NSString *)aRId
{
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        [[NSUserDefaults standardUserDefaults] objectForKey:@"apiToken"], @"apiToken",
                                        [Util getUUID], @"uuid",
                                        aUrl, @"channelUrl",
                                        aRId, @"rId",
                                        @"opengroup", @"channelType",
                                        nil];
    
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"v1/update/chat/room/channel/url"
                                        param:dicM_Params
                                   withMethod:@"GET"
                                    withBlock:^(id resulte, NSError *error) {
                                        
                                        [MBProgressHUD hide];
                                        
                                        if( resulte )
                                        {
                                            
                                        }
                                    }];
}

+ (nullable NSArray<SBDBaseMessage *> *)loadMessagesInChannel:(NSString * _Nonnull)channelUrl
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appIdDirectory = [documentsDirectory stringByAppendingPathComponent:[SBDMain getApplicationId]];
    NSString *messageFileNamePrefix = [[self class] sha256:[NSString stringWithFormat:@"%@_%@", [SBDMain getCurrentUser].userId, channelUrl]];
    NSString *dumpFileName = [NSString stringWithFormat:@"%@.data", messageFileNamePrefix];
    NSString *dumpFilePath = [appIdDirectory stringByAppendingPathComponent:dumpFileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dumpFilePath]) {
        return nil;
    }
    
    NSError *errorReadDump;
    NSString *messageDump = [NSString stringWithContentsOfFile:dumpFilePath encoding:NSUTF8StringEncoding error:&errorReadDump];
    
    if (messageDump.length > 0) {
        NSArray *loadMessages = [messageDump componentsSeparatedByString:@"\n"];
        
        if (loadMessages.count > 0) {
            NSMutableArray<SBDBaseMessage *> *messages = [[NSMutableArray alloc] init];
            for (NSString *msgString in loadMessages) {
                NSData *msgData = [[NSData alloc] initWithBase64EncodedString:msgString options:0];
                
                
                SBDBaseMessage *message = [SBDBaseMessage buildFromSerializedData:msgData];
                [messages addObject:message];
            }
            
            return messages;
        }
    }
    
    return nil;
}

+ (nonnull NSString *)sha256:(NSString * _Nonnull)src
{
    const char* str = [src UTF8String];
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(str, (uint32_t)strlen(str), result);
    
    NSMutableString *sha256hash = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
        [sha256hash appendFormat:@"%02x", result[i]];
    }
    
    if (sha256hash == nil) {
        return @"";
    }
    
    return sha256hash;
}

+ (void)showToast:(NSString *)aMsg
{
    [ALToastView toastInView:[Util keyWindow] withText:aMsg];
}

+ (void)showToastCenter:(NSString *)aMsg
{
    [ALToastView toastInViewCenter:[Util keyWindow] withText:aMsg];
}

+ (NSString *)contentTypeForImageData:(NSData *)data
{
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"jpeg";
            break;
        case 0x89:
            return @"png";
            break;
        case 0x47:
            return @"gif";
            break;
        case 0x49:
        case 0x4D:
            return @"tiff";
            break;
        case 0x25:
            return @"pdf";
            break;
        case 0xD0:
            return @"vnd";
            break;
        case 0x46:
            return @"plain";
            break;
            //        case 0x52:
            //            // R as RIFF for WEBP
            //            if ([data length] < 12) {
            //                return nil;
            //            }
            //
            //            NSString *testString = [[NSString alloc] initWithData:[data subdataWithRange:NSMakeRange(0, 12)] encoding:NSASCIIStringEncoding];
            //            if ([testString hasPrefix:@"RIFF"] && [testString hasSuffix:@"WEBP"]) {
            //                return @"image/webp";
            //            }
            //
            //            return nil;
            
        default:
            //            return @"application/octet-stream";
            return @"jpg";
    }
    
    return nil;
}

+ (NSString *)getSharpName:(NSString *)aName
{
    if( [aName hasPrefix:@"#"] == NO )
    {
        aName = [NSString stringWithFormat:@"#%@", aName];
    }
    
    return aName;
}

+ (NSString*) deviceName
{
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    static NSDictionary* deviceNamesByCode = nil;
    
    if (!deviceNamesByCode) {
        
        deviceNamesByCode = @{
            @"i386"      : @"Simulator",
            @"x86_64"    : @"Simulator",
            @"iPod1,1"   : @"iPod Touch",        // (Original)
            @"iPod2,1"   : @"iPod Touch",        // (Second Generation)
            @"iPod3,1"   : @"iPod Touch",        // (Third Generation)
            @"iPod4,1"   : @"iPod Touch",        // (Fourth Generation)
            @"iPod7,1"   : @"iPod Touch",        // (6th Generation)
            @"iPhone1,1" : @"iPhone",            // (Original)
            @"iPhone1,2" : @"iPhone",            // (3G)
            @"iPhone2,1" : @"iPhone",            // (3GS)
            @"iPad1,1"   : @"iPad",              // (Original)
            @"iPad2,1"   : @"iPad 2",            //
            @"iPad3,1"   : @"iPad",              // (3rd Generation)
            @"iPhone3,1" : @"iPhone 4",          // (GSM)
            @"iPhone3,3" : @"iPhone 4",          // (CDMA/Verizon/Sprint)
            @"iPhone4,1" : @"iPhone 4S",         //
            @"iPhone5,1" : @"iPhone 5",          // (model A1428, AT&T/Canada)
            @"iPhone5,2" : @"iPhone 5",          // (model A1429, everything else)
            @"iPad3,4"   : @"iPad",              // (4th Generation)
            @"iPad2,5"   : @"iPad Mini",         // (Original)
            @"iPhone5,3" : @"iPhone 5c",         // (model A1456, A1532 | GSM)
            @"iPhone5,4" : @"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
            @"iPhone6,1" : @"iPhone 5s",         // (model A1433, A1533 | GSM)
            @"iPhone6,2" : @"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
            @"iPhone7,1" : @"iPhone 6 Plus",     //
            @"iPhone7,2" : @"iPhone 6",          //
            @"iPhone8,1" : @"iPhone 6S",         //
            @"iPhone8,2" : @"iPhone 6S Plus",    //
            @"iPhone8,4" : @"iPhone SE",         //
            @"iPhone9,1" : @"iPhone 7",          //
            @"iPhone9,3" : @"iPhone 7",          //
            @"iPhone9,2" : @"iPhone 7 Plus",     //
            @"iPhone9,4" : @"iPhone 7 Plus",     //
            @"iPhone10,1": @"iPhone 8",          // CDMA
            @"iPhone10,4": @"iPhone 8",          // GSM
            @"iPhone10,2": @"iPhone 8 Plus",     // CDMA
            @"iPhone10,5": @"iPhone 8 Plus",     // GSM
            @"iPhone10,3": @"iPhone X",          // CDMA
            @"iPhone10,6": @"iPhone X",          // GSM
            @"iPhone11,2": @"iPhone XS",
            @"iPhone11,4": @"iPhone XS Max China",
            @"iPhone11,6": @"iPhone XS Max",
            @"iPhone11,8": @"iPhone XR",
            @"iPhone12,1": @"iPhone 11",
            @"iPhone12,3": @"iPhone 11 Pro",
            @"iPhone12,5": @"iPhone 11 Pro Max",
            
            @"iPod1,1" : @"1st Gen iPod",
            @"iPod2,1" : @"2nd Gen iPod",
            @"iPod3,1" : @"3rd Gen iPod",
            @"iPod4,1" : @"4th Gen iPod",
            @"iPod5,1" : @"5th Gen iPod",
            @"iPod7,1" : @"6th Gen iPod",
            @"iPod9,1" : @"7th Gen iPod",
            
            @"iPad4,1"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
            @"iPad4,2"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
            @"iPad4,4"   : @"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
            @"iPad4,5"   : @"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
            @"iPad4,7"   : @"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
            @"iPad6,7"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1584)
            @"iPad6,8"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1652)
            @"iPad6,3"   : @"iPad Pro (9.7\")",  // iPad Pro 9.7 inches - (model A1673)
            @"iPad6,4"   : @"iPad Pro (9.7\")",   // iPad Pro 9.7 inches - (models A1674 and A1675)
            @"iPad6,7" : @"iPad Pro (12.9 inch, WiFi)",
            @"iPad6,8" : @"iPad Pro (12.9 inch, WiFi+LTE)",
            @"iPad6,11" : @"iPad (2017)",
            @"iPad6,12" : @"iPad (2017)",
            @"iPad7,1" : @"iPad Pro 2nd Gen (WiFi)",
            @"iPad7,2" : @"iPad Pro 2nd Gen (WiFi+Cellular)",
            @"iPad7,3" : @"iPad Pro 10.5-inch",
            @"iPad7,4" : @"iPad Pro 10.5-inch",
            @"iPad7,5" : @"iPad 6th Gen (WiFi)",
            @"iPad7,6" : @"iPad 6th Gen (WiFi+Cellular)",
            @"iPad8,1" : @"iPad Pro 3rd Gen (11 inch, WiFi)",
            @"iPad8,2" : @"iPad Pro 3rd Gen (11 inch, 1TB, WiFi)",
            @"iPad8,3" : @"iPad Pro 3rd Gen (11 inch, WiFi+Cellular)",
            @"iPad8,4" : @"iPad Pro 3rd Gen (11 inch, 1TB, WiFi+Cellular)",
            @"iPad8,5" : @"iPad Pro 3rd Gen (12.9 inch, WiFi)",
            @"iPad8,6" : @"iPad Pro 3rd Gen (12.9 inch, 1TB, WiFi)",
            @"iPad8,7" : @"iPad Pro 3rd Gen (12.9 inch, WiFi+Cellular)",
            @"iPad8,8" : @"iPad Pro 3rd Gen (12.9 inch, 1TB, WiFi+Cellular)",
            @"iPad11,1" : @"iPad mini 5th Gen (WiFi)",
            @"iPad11,2" : @"iPad mini 5th Gen",
            @"iPad11,3" : @"iPad Air 3rd Gen (WiFi)",
            @"iPad11,4" : @"iPad Air 3rd Gen",
            
            @"Watch1,1" : @"Apple Watch 38mm case",
            @"Watch1,2" : @"Apple Watch 38mm case",
            @"Watch2,6" : @"Apple Watch Series 1 38mm case",
            @"Watch2,7" : @"Apple Watch Series 1 42mm case",
            @"Watch2,3" : @"Apple Watch Series 2 38mm case",
            @"Watch2,4" : @"Apple Watch Series 2 42mm case",
            @"Watch3,1" : @"Apple Watch Series 3 38mm case (GPS+Cellular)",
            @"Watch3,2" : @"Apple Watch Series 3 42mm case (GPS+Cellular)",
            @"Watch3,3" : @"Apple Watch Series 3 38mm case (GPS)",
            @"Watch3,4" : @"Apple Watch Series 3 42mm case (GPS)",
            @"Watch4,1" : @"Apple Watch Series 4 40mm case (GPS)",
            @"Watch4,2" : @"Apple Watch Series 4 44mm case (GPS)",
            @"Watch4,3" : @"Apple Watch Series 4 40mm case (GPS+Cellular)",
            @"Watch4,4" : @"Apple Watch Series 4 44mm case (GPS+Cellular)",
            @"Watch5,1" : @"Apple Watch Series 5 40mm case (GPS)",
            @"Watch5,2" : @"Apple Watch Series 5 44mm case (GPS)",
            @"Watch5,3" : @"Apple Watch Series 5 40mm case (GPS+Cellular)",
            @"Watch5,4" : @"Apple Watch Series 5 44mm case (GPS+Cellular)"
        };
    }
    
    NSString* deviceName = [deviceNamesByCode objectForKey:code];
    
    if (!deviceName) {
        // Not found on database. At least guess main device type from string contents:
        
        if ([code rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        }
        else if([code rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        }
        else if([code rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = @"iPhone";
        }
        else {
            deviceName = @"Unknown";
        }
    }
    
    return deviceName;
}

+ (void)showAlert:(NSString *)aMsg withVc:(UIViewController *)vc
{
    [UIAlertController showAlertInViewController:vc
                                       withTitle:@""
                                         message:aMsg
                               cancelButtonTitle:@"확인"
                          destructiveButtonTitle:nil
                               otherButtonTitles:nil
                                        tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                            
                                        }];

}

+ (void)showAlertWindow:(NSString *)aMsg
{
    UIViewController *topController = [Util keyWindow].rootViewController;

    while (topController.presentedViewController)
    {
        topController = topController.presentedViewController;
    }

    [UIAlertController showAlertInViewController:topController
                                       withTitle:@""
                                         message:aMsg
                               cancelButtonTitle:@"확인"
                          destructiveButtonTitle:nil
                               otherButtonTitles:nil
                                        tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                            
                                        }];

}

+ (void)showAlertServerError
{
    UIViewController *topController = [Util keyWindow].rootViewController;

    while (topController.presentedViewController)
    {
        topController = topController.presentedViewController;
    }

    [UIAlertController showAlertInViewController:topController
                                       withTitle:@""
                                         message:@"통신 상태가 원활하지 않거나\n데이터가 잘못되었습니다."
                               cancelButtonTitle:@"확인"
                          destructiveButtonTitle:nil
                               otherButtonTitles:nil
                                        tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                            
                                        }];
}

+ (BOOL)canNextStep:(UIViewController *)vc
{
    if( [Util isOffLine] )
    {
        [UIAlertController showAlertInViewController:vc
                                           withTitle:@""
                                             message:@"통신 상태가 원활 하지 않습니다.\n네트웍 상태를 확인 하세요."
                                   cancelButtonTitle:@"확인"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:nil
                                            tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                
                                                if (buttonIndex == controller.cancelButtonIndex)
                                                {
                                                    NSLog(@"Cancel Tapped");
                                                }
                                                else if (buttonIndex == controller.destructiveButtonIndex)
                                                {
                                                    NSLog(@"Delete Tapped");
                                                }
                                                else if (buttonIndex >= controller.firstOtherButtonIndex)
                                                {
                                                    
                                                }
                                            }];
        
        return NO;
    }
    
    return YES;
}

+ (void)dumpMessages:(NSArray<SBDBaseMessage *> * _Nonnull)messages resendableMessages:(NSDictionary<NSString *, SBDBaseMessage *> * _Nullable)resendableMessages resendableFileData:(NSDictionary<NSString *, NSDictionary<NSString *, NSObject *> *> * _Nullable)resendableFileData preSendMessages:(NSDictionary<NSString *, SBDBaseMessage *> * _Nullable)preSendMessages channelUrl:(NSString * _Nonnull)channelUrl{
    // Serialize messages
    NSUInteger startIndex = 0;
    
    if (messages.count == 0) {
        return;
    }
    
    if (messages.count > 15) {
        startIndex = messages.count - 15;
    }
    
    NSMutableArray<NSString *> *serializedMessages = [[NSMutableArray alloc] init];
    for (; startIndex < messages.count; startIndex++)
    {
        NSString *requestId = nil;
        if ([messages[startIndex] isKindOfClass:[SBDUserMessage class]])
        {
            requestId = ((SBDUserMessage *)messages[startIndex]).requestId;
        }
        else
        {
            continue;
        }
//        else if ([messages[startIndex] isKindOfClass:[SBDFileMessage class]]) {
//            requestId = ((SBDFileMessage *)messages[startIndex]).requestId;
//        }
//
//        if (requestId != nil && requestId.length > 0) {
//            if (resendableMessages[requestId] != nil) {
//                continue;
//            }
//
//            if (preSendMessages[requestId] != nil) {
//                continue;
//            }
//
//            if (resendableFileData[requestId] != nil) {
//                continue;
//            }
//        }
        
        NSData *messageData = [messages[startIndex] serialize];
        NSString *messageString = [messageData base64EncodedStringWithOptions:0];
        [serializedMessages addObject:messageString];
    }
    
    NSString *dumpedMessages = [serializedMessages componentsJoinedByString:@"\n"];
    NSString *dumpedMessagesHash = [[self class] sha256:dumpedMessages];
    
    // Save messages to temp file.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appIdDirectory = [documentsDirectory stringByAppendingPathComponent:[SBDMain getApplicationId]];
    
    NSString *uniqueTempFileNamePrefix = [[NSUUID UUID] UUIDString];
    NSString *tempMessageDumpFileName = [NSString stringWithFormat:@"%@.data", uniqueTempFileNamePrefix];
    NSString *tempMessageHashFileName = [NSString stringWithFormat:@"%@.hash", uniqueTempFileNamePrefix];
    
    NSString *tempMessageDumpFilePath = [appIdDirectory stringByAppendingPathComponent:tempMessageDumpFileName];
    NSString *tempMessageHashFilePath = [appIdDirectory stringByAppendingPathComponent:tempMessageHashFileName];
    
    NSError *errorCreateDirectory = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath:appIdDirectory] == NO) {
        [[NSFileManager defaultManager] createDirectoryAtPath:appIdDirectory withIntermediateDirectories:NO attributes:nil error:&errorCreateDirectory];
    }
    
    if (errorCreateDirectory != nil) {
        return;
    }
    
    NSString *messageFileNamePrefix = [[self class] sha256:[NSString stringWithFormat:@"%@_%@", [[SBDMain getCurrentUser].userId urlencoding], channelUrl]];
    NSString *messageDumpFileName = [NSString stringWithFormat:@"%@.data", messageFileNamePrefix];
    NSString *messageHashFileName = [NSString stringWithFormat:@"%@.hash", messageFileNamePrefix];
    
    NSString *messageDumpFilePath = [appIdDirectory stringByAppendingPathComponent:messageDumpFileName];
    NSString *messageHashFilePath = [appIdDirectory stringByAppendingPathComponent:messageHashFileName];
    
    // Check hash.
    NSString *previousHash;
    if (![[NSFileManager defaultManager] fileExistsAtPath:messageDumpFilePath]) {
        [[NSFileManager defaultManager] createFileAtPath:messageDumpFilePath contents:nil attributes:nil];
    }
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:messageHashFilePath]) {
        [[NSFileManager defaultManager] createFileAtPath:messageHashFilePath contents:nil attributes:nil];
    }
    else {
        previousHash = [NSString stringWithContentsOfFile:messageHashFilePath encoding:NSUTF8StringEncoding error:nil];
    }
    
    if (previousHash != nil && [previousHash isEqualToString:dumpedMessagesHash]) {
        return;
    }
    
    // Write temp file.
    NSError *errorDump = nil;
    NSError *errorHash = nil;
    [dumpedMessages writeToFile:tempMessageDumpFilePath atomically:NO encoding:NSUTF8StringEncoding error:&errorDump];
    [dumpedMessagesHash writeToFile:tempMessageHashFilePath atomically:NO encoding:NSUTF8StringEncoding error:&errorHash];
    
    // Move temp to real file.
    if (errorDump == nil && errorHash == nil) {
        NSError *errorMoveDumpFile;
        NSError *errorMoveHashFile;
        
        [[NSFileManager defaultManager] removeItemAtPath:messageDumpFilePath error:nil];
        [[NSFileManager defaultManager] moveItemAtPath:tempMessageDumpFilePath toPath:messageDumpFilePath error:&errorMoveDumpFile];
        
        [[NSFileManager defaultManager] removeItemAtPath:messageHashFilePath error:nil];
        [[NSFileManager defaultManager] moveItemAtPath:tempMessageHashFilePath toPath:messageHashFilePath error:&errorMoveHashFile];
        
        if (errorMoveDumpFile != nil || errorMoveHashFile != nil) {
            [[NSFileManager defaultManager] removeItemAtPath:tempMessageDumpFilePath error:nil];
            [[NSFileManager defaultManager] removeItemAtPath:tempMessageHashFilePath error:nil];
            [[NSFileManager defaultManager] removeItemAtPath:messageDumpFilePath error:nil];
            [[NSFileManager defaultManager] removeItemAtPath:messageHashFilePath error:nil];
        }
    }
}

+ (TimeStruct *)makeTimeWithTimeStamp:(double)timestamp
{
    double nowTimeStamp = [[NSDate date] timeIntervalSince1970];
    if( timestamp > (nowTimeStamp * 10) )
    {
        timestamp = timestamp / 1000;
    }
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:date];
    //    NSInteger nYear = [components year];
    //    NSInteger nMonth = [components month];
    //    NSInteger nDay = [components day];
    //    NSInteger nHour = [components hour];
    //    NSInteger nMinute = [components minute];
    //    NSInteger nSecond = [components second];
    
    TimeStruct *time = [[TimeStruct alloc] init];
    time.nYear = [components year];
    time.nMonth = [components month];
    time.nDay = [components day];
    time.nHour = [components hour];
    time.nMinute = [components minute];
    time.nSecond = [components second];
    
    return time;
}

+ (NSString *)setWebViewAddUuidAndToken:(NSString *)aUrl
{
    NSMutableString *strM = [NSMutableString stringWithString:aUrl];
    if( [strM rangeOfString:@"uuid="].location == NSNotFound )
    {
        if( [strM rangeOfString:@"?"].location == NSNotFound )
        {
            [strM appendString:[NSString stringWithFormat:@"?uuid=%@", [Util getUUID]]];
        }
        else
        {
            [strM appendString:[NSString stringWithFormat:@"&uuid=%@", [Util getUUID]]];
        }
    }
    
    if( [strM rangeOfString:@"apiToken="].location == NSNotFound )
    {
        NSString *str_ApiToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"apiToken"];
        if( str_ApiToken && str_ApiToken.length > 0 )
        {
            if( [strM rangeOfString:@"?"].location == NSNotFound )
            {
                [strM appendString:[NSString stringWithFormat:@"?apiToken=%@", str_ApiToken]];
            }
            else
            {
                [strM appendString:[NSString stringWithFormat:@"&apiToken=%@", str_ApiToken]];
            }
        }
    }
    
    return [NSString stringWithString:strM];
}

+ (void)showCameraAndPermission:(UIViewController *_Nonnull)vc
{
    switch ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo])
    {
        case AVAuthorizationStatusAuthorized:
        {
            //권한 부여
            [Util showCamera:vc];
            break;
        }
        case AVAuthorizationStatusNotDetermined:
        {
            //엑세스 요청 전
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if (granted)
                {
                    //수락
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [Util showCamera:vc];
                    });
                }
                else
                {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        
                        NSString *str_Msg = [NSString stringWithFormat:@"카메라 허용을 거부하셨습니다.\n설정->%@->카메라\n를 켜주세요.", [Util getAppName]];
                        [Util showOneMsg:str_Msg withVc:vc];
                    });
                }
            }];
            break;
        }
        case AVAuthorizationStatusDenied:
        {
            //권한 거부
            NSString *str_Msg = [NSString stringWithFormat:@"카메라 허용을 거부하셨습니다.\n설정->%@->카메라\n를 켜주세요.", [Util getAppName]];
            [Util showOneMsg:str_Msg withVc:vc];
        }
        case AVAuthorizationStatusRestricted:
        {
            //권한 부여 불가
        }
    }
}

+ (void)showCamera:(UIViewController *_Nonnull)vc
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.delegate = vc;
    imagePickerController.allowsEditing = NO;
    imagePickerController.modalPresentationStyle = UIModalPresentationOverFullScreen;

    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [vc presentViewController:imagePickerController animated:YES completion:nil];
    }];
}

+ (void)showVideoAndPermission:(UIViewController *_Nonnull)vc
{
    switch ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo])
    {
        case AVAuthorizationStatusAuthorized:
        {
            //권한 부여
            [Util showVideo:vc];
            break;
        }
        case AVAuthorizationStatusNotDetermined:
        {
            //엑세스 요청 전
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if (granted)
                {
                    //수락
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [Util showVideo:vc];
                    });
                }
                else
                {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        
                        NSString *str_Msg = [NSString stringWithFormat:@"카메라 허용을 거부하셨습니다.\n설정->%@->카메라\n를 켜주세요.", [Util getAppName]];
                        [Util showOneMsg:str_Msg withVc:vc];
                    });
                }
            }];
            break;
        }
        case AVAuthorizationStatusDenied:
        {
            //권한 거부
            NSString *str_Msg = [NSString stringWithFormat:@"카메라 허용을 거부하셨습니다.\n설정->%@->카메라\n를 켜주세요.", [Util getAppName]];
            [Util showOneMsg:str_Msg withVc:vc];
        }
        case AVAuthorizationStatusRestricted:
        {
            //권한 부여 불가
        }
    }
}

+ (void)showVideo:(UIViewController *_Nonnull)vc
{
    UIImagePickerController* imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.videoQuality = UIImagePickerControllerQualityTypeHigh;
    imagePickerController.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeMovie];
    imagePickerController.delegate = vc;
    imagePickerController.modalPresentationStyle = UIModalPresentationOverFullScreen;

    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [vc presentViewController:imagePickerController animated:YES completion:nil];
    }];
}

+ (void)showAlbumLibraryAndPermission:(UIViewController *_Nonnull)vc
{
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if (status == PHAuthorizationStatusAuthorized)
    {
         // Access has been granted.
        [Util showAlbumLibrary:vc];
    }
    else if (status == PHAuthorizationStatusDenied)
    {
         // Access has been denied.
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {

            if (status == PHAuthorizationStatusAuthorized)
            {
                // Access has been granted.
                dispatch_sync(dispatch_get_main_queue(), ^{

                    [Util showAlbumLibrary:vc];
                });
            }
            else
            {
                // Access has been denied.
                dispatch_sync(dispatch_get_main_queue(), ^{

                    NSString *str_Msg = [NSString stringWithFormat:@"앨범 허용을 거부하셨습니다.\n설정->%@->사진\n에서 권한을 켜주세요.", [Util getAppName]];
                    [Util showOneMsg:str_Msg withVc:vc];
                });
            }
        }];
    }
    else if (status == PHAuthorizationStatusNotDetermined)
    {
        //결정중
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus authorizationStatus){
            
            if( authorizationStatus == PHAuthorizationStatusAuthorized )
            {
                // 사용가능
                dispatch_sync(dispatch_get_main_queue(), ^{

                    [Util showAlbumLibrary:vc];
                });
            }
            else
            {
                // 사용불가
                dispatch_sync(dispatch_get_main_queue(), ^{

                    NSString *str_Msg = [NSString stringWithFormat:@"앨범 허용을 거부하셨습니다.\n설정->%@->사진\n에서 권한을 켜주세요.", [Util getAppName]];
                    [Util showOneMsg:str_Msg withVc:vc];
                });
            }
        }];
    }
    else if (status == PHAuthorizationStatusRestricted)
    {
         // Restricted access - normally won't happen.
    }
}

+ (void)showAlbumLibrary:(UIViewController *_Nonnull)vc
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    //             imagePickerController.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, kUTTypeImage, nil];
    imagePickerController.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeImage, nil];
    imagePickerController.delegate = vc;
    imagePickerController.allowsEditing = NO;
    imagePickerController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [vc presentViewController:imagePickerController animated:YES completion:nil];
    }];
}

+ (void)showOneMsg:(NSString *)aMsg withVc:(UIViewController *)vc
{
    [UIAlertController showAlertInViewController:vc
                                       withTitle:@""
                                         message:aMsg
                               cancelButtonTitle:@"확인"
                          destructiveButtonTitle:nil
                               otherButtonTitles:nil
                                        tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                            
                                        }];
}

+ (NSString *)getAppName
{
    NSDictionary *bundleInfo = [[NSBundle mainBundle] infoDictionary];
    NSString *appName = [bundleInfo objectForKey:@"CFBundleDisplayName"];
    return appName;
}

+ (NSDictionary *)urlStringToDictionary:(NSString *)aUrl
{
//    NSString *decodeUrl = [aUrl stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *decodeUrl = [aUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];

    NSMutableDictionary *dicM = [NSMutableDictionary dictionary];
    
    NSArray *ar_IdentiSep = [decodeUrl componentsSeparatedByString:@"?"];
    if( ar_IdentiSep.count != 2 )
    {
        return dicM;
    }
    
    NSString *str_Identi = ar_IdentiSep[0];
    [dicM setObject:str_Identi forKey:@"identi"];
    
    NSString *str_Params = ar_IdentiSep[1];
    NSArray *ar_Params = [str_Params componentsSeparatedByString:@"&"];
    for( NSInteger i = 0; i < ar_Params.count; i++ )
    {
        NSString *str_Param = ar_Params[i];
        NSArray *ar_Param = [str_Param componentsSeparatedByString:@"="];
        if( ar_Param.count == 2 )
        {
            [dicM setObject:ar_Param[1] forKey:ar_Param[0]];
        }
    }
    
    return dicM;
}

+ (void)showTopToastMsg:(NSString *)aMsg withData:(NSDictionary *)dic_Data
{
    NSArray *topLevelObjects = [[NSBundle mainBundle]loadNibNamed:@"TopToastView" owner:self options:nil];
    TopToastView *v_Item = [topLevelObjects objectAtIndex:0];
    v_Item.alpha = NO;
    v_Item.lb_Msg.text = aMsg;
    v_Item.dic_Data = dic_Data;

    UIWindow *window = [Util keyWindow];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                v_Item.lb_Msg.font, NSFontAttributeName, nil];
    CGRect textRect = [v_Item.lb_Msg.text boundingRectWithSize:CGSizeMake(window.frame.size.width - 80, FLT_MAX)
                                     options:NSLineBreakByClipping | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                  attributes:attributes
                                     context:nil];
    v_Item.frame = CGRectMake(0, 0, kDeviceWidth, textRect.size.height + 100);
    [window addSubview:v_Item];
    
    [UIView animateWithDuration:0.3f animations:^{
        v_Item.alpha = YES;
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kTopToastViewTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [UIView animateWithDuration:0.3f animations:^{
            v_Item.alpha = NO;
        }completion:^(BOOL finished) {
           [v_Item removeFromSuperview];
        }];
    });
}

+ (NSAttributedString *)setLabelLineSpacing:(UILabel *)lb withLineSpacing:(CGFloat)spacing
{
//    UIFont *font = [UIFont systemFontOfSize:20 weight:UIFontWeightSemibold];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:lb.text attributes:@{NSFontAttributeName:lb.font}];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.lineSpacing = spacing;
    [attrStr addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, attrStr.length)];

    return attrStr;
}

+ (NSString *)getCommentAfterTime:(NSString *)aTime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"];
    NSDate *date = [formatter dateFromString:aTime];
    NSTimeInterval time = date.timeIntervalSinceNow;
    if( time < 0 )
    {
        time *= -1;
    }
    
    return [Util makeAgoTime:time];
}

+ (NSString *)makeAgoTime:(NSTimeInterval)time
{
    if( time > (60 * 60 * 24 * 7) )
    {
        //1주일보다 크면
        return [NSString stringWithFormat:@"%0.f주 전", time/(60 * 60 * 24 * 7)];
    }
    else if( time > (60 * 60 * 24) )
    {
        //하루보다 크면
        return [NSString stringWithFormat:@"%0.f일 전", time/(60 * 60 * 24)];
    }
    else
    {
        if( time <= 1 )
        {
            return @"지금 막";
        }
        else if( time < 60 )
        {
            return [NSString stringWithFormat:@"%0.f초 전", time];
        }
        else if( time < (60 * 60) )
        {
            //1시간보다 작을 경우
            return [NSString stringWithFormat:@"%0.f분 전", time / 60.0];
        }
        else
        {
            return [NSString stringWithFormat:@"%.0f시간 전", ((time / 60.0) / 60.0)];
        }
    }
    
    
    return @"";
}

+ (NSArray *)jsonStringToArray:(NSString *)str
{
    NSData *jsonData = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *ar = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
    return ar;
}

+ (NSString *)getChatDate:(NSDate *)date
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:date];
    NSInteger nYear = [components year];
    NSInteger nMonth = [components month];
    NSInteger nDay = [components day];
    NSInteger nHour = [components hour];
    NSInteger nMinute = [components minute];
    NSInteger nSecond = [components second];
    NSString *str_Date = [NSString stringWithFormat:@"%04ld-%02ld-%02ld %02ld:%02ld:%02ld", nYear, nMonth, nDay, nHour, nMinute, nSecond];
    NSDateFormatter *format1 = [[NSDateFormatter alloc] init];
    [format1 setDateFormat:@"yyyy-MM-dd HH:mm:ss +0000"];
    NSDate *ddayDate = [format1 dateFromString:str_Date];

    NSDate *nowDate = [NSDate date];
    components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:nowDate];
    nYear = [components year];
    nMonth = [components month];
    nDay = [components day];
    nHour = [components hour];
    nMinute = [components minute];
    nSecond = [components second];
    
    NSDate *currentTime = [format1 dateFromString:[NSString stringWithFormat:@"%04ld-%02ld-%02ld %02ld:%02ld:%02ld", nYear, nMonth, nDay, nHour, nMinute, nSecond]];
    NSTimeInterval diff = [currentTime timeIntervalSinceDate:ddayDate];
    return [Util makeAgoTime:diff];
}

@end
