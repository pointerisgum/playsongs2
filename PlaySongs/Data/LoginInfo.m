//
//  LoginInfo.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/05.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "LoginInfo.h"

@implementation LoginInfo
- (LoginInfo *)initWithData:(id)data
{
    _nUserId = [data integerForKey:@"userId"];
    _str_UserName = [data stringForKey:@"username"];
    return self;
}
@end
