//
//  AppDelegate.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/03.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "AppDelegate.h"
#import <SendBirdSDK/SendBirdSDK.h>

NSString *const kGCMMessageIDKey = @"gcm.message_id";
static NSString * const kClientID = @"1088173325537-ik5fr7gmlm59j8mumcev7m38erf4kemk.apps.googleusercontent.com";

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    

    
    
    [FIRApp configure];
    [FIRMessaging messaging].delegate = self;
    
    [GIDSignIn sharedInstance].clientID = kClientID;
    
    if ([UNUserNotificationCenter class] != nil)
    {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
            // ...
            NSLog(@"%@", error);
        }];
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    
    [SBDMain initWithApplicationId:kSendBirdAppId];
    [SBDMain setLogLevel:SBDLogLevelNone];
    [SBDOptions setUseMemberAsMessageSender:YES];
    
    BOOL isLogin = [[[NSUserDefaults standardUserDefaults] objectForKey:@"IsLogin"] boolValue];
    if( launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey] )
    {
        NSDictionary *userInfo = launchOptions;
        
        // Print full message.
        NSLog(@"%@", userInfo);
        
        
        //푸시를 타고 들어왔을때
        NSString *str_ChannelUrl = [userInfo stringForKey:@"gcm.notification.channelUrl"];
        NSString *str_BotType = [userInfo stringForKey:@"gcm.notification.botType"];

    }
    
    return YES;
}



#pragma mark - UISceneSession lifecycle
- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options  API_AVAILABLE(ios(13.0)){
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions  API_AVAILABLE(ios(13.0)){
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
}



//MARK: PUSH
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"APNs device token retrieved: %@", deviceToken);
    NSString *fcmToken = [FIRMessaging messaging].FCMToken;
    NSLog(@"FCM registration token: %@", fcmToken);
    [FIRMessaging messaging].APNSToken = deviceToken;

    [[NSUserDefaults standardUserDefaults] setObject:fcmToken forKey:@"PushToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    /*****************SB***************/
    [SBDMain registerDevicePushToken:deviceToken unique:YES completionHandler:^(SBDPushTokenRegistrationStatus status, SBDError * _Nullable error) {
        if (error == nil)
        {
            NSLog(@"%ld", status);
            if (status == SBDPushTokenRegistrationStatusPending)
            {
                // Registration is pending.
                // If you get this status, invoke `+ registerDevicePushToken:unique:completionHandler:` with `[SBDMain getPendingPushToken]` after connection.
            }
            else
            {
                // Registration succeeded.
            }
        }
        else
        {
            // Registration failed.
        }
    }];
    /**********************************/
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    //iOS10 이상
    //앱 실행중에 푸시가 왔을때 또는 앱 백그라운드에서 푸시를 타고 들어왔을때

//    ALERT_ONE(@"@@@@@@@");
    
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    completionHandler(UIBackgroundFetchResultNewData);

    if (userInfo[kGCMMessageIDKey])
    {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
//    completionHandler();
    
    //푸시를 타고 들어왔을때
    //sendbird_group_channel_56284320_9b391b974ecb5274f7f594f10fb30918dbabceea
    NSString *str_ChannelUrl = [userInfo stringForKey:@"gcm.notification.channelUrl"];
    
    //input
    NSString *str_BotType = [userInfo stringForKey:@"gcm.notification.botType"];
    
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler
{
    //포그라운드에서 푸시가 왔을때
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateDashBoard" object:nil];

    NSDictionary *userInfo = notification.request.content.userInfo;
    
    if (userInfo[kGCMMessageIDKey])
    {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    NSLog(@"%@", userInfo);
    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionNone);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void(^)(void))completionHandler
{
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    if (userInfo[kGCMMessageIDKey]) {
      NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }

    // Print full message.
    NSLog(@"%@", userInfo);

    completionHandler();

    //    //푸시를 타고 들어왔을때
    //    NSDictionary *userInfo = response.notification.request.content.userInfo;
    //    if (userInfo[kGCMMessageIDKey])
    //    {
    //        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    //    }
    //
    //    // Print full message.
    //    NSLog(@"%@", userInfo);
    //
    //    completionHandler();
    //
    //    //sendbird_group_channel_56284320_9b391b974ecb5274f7f594f10fb30918dbabceea
    //    NSString *str_ChannelUrl = [userInfo objectForKey_YM:@"gcm.notification.channelUrl"];
    //
    //    //input
    //    NSString *str_BotType = [userInfo objectForKey_YM:@"gcm.notification.botType"];
    //
    //
    //    NSDictionary *dic_APS = userInfo[@"aps"];
    //    NSDictionary *dic_Alert = dic_APS[@"alert"];
    //    NSString *str_Body = [dic_Alert stringForKey:@"body"];
    //    NSLog(@"body : %@", str_Body);
    
    
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Unable to register for remote notifications: %@", error);
}


//MARK: FireBase Push
- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    
    
}

@end
