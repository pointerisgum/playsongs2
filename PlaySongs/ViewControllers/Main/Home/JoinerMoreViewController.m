//
//  JoinerMoreViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/09/02.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "JoinerMoreViewController.h"
#import "ImageCollectionViewController.h"

@interface JoinerMoreViewController ()
@property (nonatomic, strong) ImageCollectionViewController *vc_List;
@end

@implementation JoinerMoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_vc_List refresh];
    
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if( [segue.destinationViewController isKindOfClass:[ImageCollectionViewController class]] )
    {
        _vc_List = segue.destinationViewController;
        _vc_List.str_Path = @"playing/list";
        _vc_List.dic_Params = @{@"playId":_str_PlayId};
    }
}

@end
