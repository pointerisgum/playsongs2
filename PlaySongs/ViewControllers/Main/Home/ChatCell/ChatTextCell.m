//
//  ChatTextCell.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/17.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "ChatTextCell.h"

@implementation ChatTextCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _v_ContentsBg.layer.cornerRadius = 8.0f;
    _v_ContentsBg.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (void)layoutSubviews
//{
//    [super layoutSubviews];
//    [self layoutIfNeeded];
//}

- (void)setInitUI:(SBDBaseMessage *)message
{
    _lc_BgLeading.active = NO;
    _lc_BgTail.active = NO;

    BOOL isMy = [message.sender.userId integerValue] == [UserData sharedData].userId;

    _lb_Contents.text = message.message;
    
    if( isMy )
//    if( 1 )
    {
        _v_ContentsBg.backgroundColor = [UIColor colorWithHexString:@"DCDCDC"];


        //leading
        [self removeConstraint:_lc_BgLeading];
        _lc_BgLeading = [NSLayoutConstraint constraintWithItem:_v_ContentsBg
                                                     attribute:NSLayoutAttributeLeading
                                                     relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                        toItem:self attribute:NSLayoutAttributeLeading multiplier:1 constant:52];
        _lc_BgLeading.priority = 750;
        [self addConstraint:_lc_BgLeading];

        
        //tail
        [self removeConstraint:_lc_BgTail];
        _lc_BgTail = [NSLayoutConstraint constraintWithItem:self
                                                           attribute:NSLayoutAttributeTrailing
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:_v_ContentsBg attribute:NSLayoutAttributeTrailing multiplier:1 constant:15];
        _lc_BgTail.priority = 1000;
        [self addConstraint:_lc_BgTail];


        _lc_BgTop.constant = 5;
        _iv_OtherUserThumb.hidden = YES;
        _lb_OtherUserName.hidden = YES;
    }
    else
    {
        _v_ContentsBg.backgroundColor = [UIColor colorWithHexString:@"D8E1DA"];
        
        //leading
        [self removeConstraint:_lc_BgLeading];
        _lc_BgLeading = [NSLayoutConstraint constraintWithItem:_v_ContentsBg
                                                     attribute:NSLayoutAttributeLeading
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self attribute:NSLayoutAttributeLeading multiplier:1 constant:52];
        _lc_BgLeading.priority = 1000;
        [self addConstraint:_lc_BgLeading];

        
        
        //tail
        [self removeConstraint:_lc_BgTail];
        _lc_BgTail = [NSLayoutConstraint constraintWithItem:self
                                                           attribute:NSLayoutAttributeTrailing
                                                           relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                              toItem:_v_ContentsBg attribute:NSLayoutAttributeTrailing multiplier:1 constant:15];
        _lc_BgTail.priority = 750;
        [self addConstraint:_lc_BgTail];

        
        
        //top
        _lc_BgTop.constant = 22;
        
        
        _iv_OtherUserThumb.hidden = NO;
        _lb_OtherUserName.hidden = NO;

        [_iv_OtherUserThumb sd_setImageWithString:message.sender.profileUrl];
        _lb_OtherUserName.text = message.sender.nickname;
    }
    
//    _lb_Contents.text = @"asdasdklasdlkj  asdjkadjskla jkds jkld ajkld ajkld jkalds jklads jklads kjlasdkjlasdjkl ads kjsdkl sdkjlaskdjalsd";
    [self layoutIfNeeded];
}

@end
