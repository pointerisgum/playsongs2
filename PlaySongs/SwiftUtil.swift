//
//  SwiftUtil.swift
//  PlaySongs
//
//  Created by 김영민 on 2020/08/09.
//  Copyright © 2020 김영민. All rights reserved.
//

import Foundation

@objc class SwiftUtil : NSObject
{
    @objc func addSeeMore(str: String, maxLength: Int) -> NSAttributedString {
        var attributedString = NSAttributedString()
        let index: String.Index = str.index(str.startIndex, offsetBy: maxLength)
        let editedText = String(str.prefix(upTo: index)) + "... 더보기"
        attributedString = NSAttributedString(string: editedText)

        return attributedString
    }

    @objc func getYoutubeVideoId(fromLink link: String) -> String {
        
        let regexString: String = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        let regExp = try? NSRegularExpression(pattern: regexString, options: .caseInsensitive)
        let array: [Any] = (regExp?.matches(in: link, options: [], range: NSRange(location: 0, length: (link.count ))))!
        if array.count > 0 {
            let result: NSTextCheckingResult? = array.first as? NSTextCheckingResult
            return (link as NSString).substring(with: (result?.range)!)
        }

        return ""
    }
}

