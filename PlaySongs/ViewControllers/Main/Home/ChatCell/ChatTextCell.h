//
//  ChatTextCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/17.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatTextCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *v_ContentsBg;
@property (weak, nonatomic) IBOutlet UILabel *lb_Contents;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_BgLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_BgTail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_BgTop;
@property (weak, nonatomic) IBOutlet ThumbImageView *iv_OtherUserThumb;
@property (weak, nonatomic) IBOutlet UILabel *lb_OtherUserName;
- (void)setInitUI:(SBDBaseMessage *)message;
@end

NS_ASSUME_NONNULL_END
