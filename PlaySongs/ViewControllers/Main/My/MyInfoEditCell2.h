//
//  MyInfoEditCell2.h
//  PlaySongs
//
//  Created by 김영민 on 2020/09/03.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyInfoEditCell2 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lb_Title;
@property (weak, nonatomic) IBOutlet UITextView *tv;
@end

NS_ASSUME_NONNULL_END
