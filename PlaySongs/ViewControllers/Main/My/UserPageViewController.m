//
//  UserPageViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/13.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "UserPageViewController.h"
#import "ImageCollectionViewController.h"
#import "FollowListViewController.h"
#import "ChatViewController.h"

@interface UserPageViewController () <UIScrollViewDelegate>

@property (nonatomic, assign) BOOL isMy;
@property (nonatomic, strong) NSDictionary *dic_TopInfo;
@property (nonatomic, strong) ImageCollectionViewController *vc_MakePlay;
@property (nonatomic, strong) ImageCollectionViewController *vc_JoinPlay;
@property (nonatomic, strong) ImageCollectionViewController *vc_HeartPlay;

@property (weak, nonatomic) IBOutlet UILabel *lb_Title;
@property (weak, nonatomic) IBOutlet ThumbImageView *iv_Thumb;
@property (weak, nonatomic) IBOutlet UILabel *lb_Name;
@property (weak, nonatomic) IBOutlet UILabel *lb_Bio;
@property (weak, nonatomic) IBOutlet UIButton *btn_Back;

@property (weak, nonatomic) IBOutlet UILabel *lb_MakePlayCount;
@property (weak, nonatomic) IBOutlet UILabel *lb_FollowerCount;
@property (weak, nonatomic) IBOutlet UILabel *lb_FollowingCount;

@property (weak, nonatomic) IBOutlet UIView *v_TopButton;
@property (weak, nonatomic) IBOutlet UIButton *btn_Follow;
@property (weak, nonatomic) IBOutlet UIButton *btn_Msg;

@property (weak, nonatomic) IBOutlet UIButton *btn_MakePlay;
@property (weak, nonatomic) IBOutlet UIButton *btn_JoinPlay;
@property (weak, nonatomic) IBOutlet UIButton *btn_HeartPlay;

@property (weak, nonatomic) IBOutlet UIImageView *iv_Bar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_BarLeading;

@property (weak, nonatomic) IBOutlet UIScrollView *sv_Main;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_ContentsWidth;

@property (weak, nonatomic) IBOutlet UIButton *btn_MyMenu;

@end

@implementation UserPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;

    _lb_Title.text = [_dic_Owner stringForKey:@"username"];
    
    _btn_Msg.layer.cornerRadius = 6.f;
    _btn_Msg.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _btn_Msg.layer.borderWidth = 0.5f;

    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _btn_Back.hidden = _isHiddenBackButton;
    _lb_Bio.hidden = YES;

    if( [[_dic_Owner stringForKey:@"username"] isEqualToString:[UserData sharedData].userName] &&
       [_dic_Owner integerForKey:@"userId"] == [UserData sharedData].userId )
    {
        _isMy = YES;
        _v_TopButton.hidden = YES;
        _btn_MyMenu.hidden = NO;
        
        if( [UserData sharedData].bio.length > 0 )
        {
            _lb_Bio.text = [UserData sharedData].bio;
            _lb_Bio.hidden = NO;
        }
    }
    else
    {
        _isMy = NO;
        _v_TopButton.hidden = NO;
        _btn_Back.hidden = NO;
        _btn_MyMenu.hidden = YES;
    }

    [self updateFollowCheck];
    [self updateTopList];
    
    if( _btn_MakePlay.selected )
    {
        [_vc_MakePlay refresh];
    }
    else if( _btn_JoinPlay.selected )
    {
        [_vc_JoinPlay refresh];
    }
    else if( _btn_HeartPlay.selected )
    {
        [_vc_HeartPlay refresh];
    }
}

- (void)viewDidLayoutSubviews
{
    _sv_Main.contentSize = CGSizeMake(_sv_Main.frame.size.width * 3, _sv_Main.frame.size.height);
    _lc_ContentsWidth.constant = _sv_Main.contentSize.width;
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSString *str_UserId = [NSString stringWithFormat:@"%ld", [_dic_Owner integerForKey:@"userId"]];
    
    if( [segue.identifier isEqualToString:@"make"] )
    {
        _vc_MakePlay = segue.destinationViewController;
        _vc_MakePlay.isMoreMode = YES;
        _vc_MakePlay.str_Path = @"my/play/list";
        _vc_MakePlay.dic_Params = @{@"filter":@"upload", @"userId":str_UserId};
    }
    else if( [segue.identifier isEqualToString:@"join"] )
    {
        _vc_JoinPlay = segue.destinationViewController;
        _vc_JoinPlay.isMoreMode = YES;
        _vc_JoinPlay.str_Path = @"my/play/list";
        _vc_JoinPlay.dic_Params = @{@"filter":@"playing", @"userId":str_UserId};
    }
    else if( [segue.identifier isEqualToString:@"heart"] )
    {
        _vc_HeartPlay = segue.destinationViewController;
        _vc_HeartPlay.isMoreMode = YES;
        _vc_HeartPlay.str_Path = @"my/play/list";
        _vc_HeartPlay.dic_Params = @{@"filter":@"heart", @"userId":str_UserId};
    }
    else if( [segue.identifier isEqualToString:@"follower"] )
    {
        [HapticHelper tap];
        FollowListViewController *vc = segue.destinationViewController;
        vc.nUserId = [_dic_Owner integerForKey:@"userId"];
        vc.str_UserName = [_dic_Owner stringForKey:@"username"];
        vc.followMode = kFollower;
    }
    else if( [segue.identifier isEqualToString:@"following"] )
    {
        [HapticHelper tap];
        FollowListViewController *vc = segue.destinationViewController;
        vc.nUserId = [_dic_Owner integerForKey:@"userId"];
        vc.str_UserName = [_dic_Owner stringForKey:@"username"];
        vc.followMode = kFollowing;
    }
}

- (void)updateFollowCheck
{
    __weak __typeof__(self) weakSelf = self;

    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
//    [dicM_Params setObject:_str_TagName forKey:@"tagName"];
    [dicM_Params setObject:@([_dic_Owner integerForKey:@"userId"]) forKey:@"following"];

    
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"user/follow/check" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            id followInfo = [resulte objectForKey :@"result"];
            if( [followInfo isKindOfClass:[NSNull class]] )
            {
                //팔로잉 하기
                weakSelf.btn_Follow.selected = YES;
                weakSelf.btn_Msg.hidden = YES;
            }
            else
            {
                //팔로우중
                weakSelf.btn_Follow.selected = NO;
                weakSelf.btn_Msg.hidden = NO;
            }
        }
    }];
}

- (void)updateTopList
{
    if( [_dic_Owner stringForKey:@"username"].length <= 0 )
    {
        //여기 들어오면 안됨
        return;
    }
    
    __weak __typeof__(self) weakSelf = self;
    
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setValue:[_dic_Owner stringForKey:@"username"] forKey:@"username"];
    
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"user/info" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            weakSelf.dic_TopInfo = [resulte dictionaryForKey:@"result"];
            [weakSelf updateTopUI];
        }
    }];
}

- (void)updateTopUI
{
    NSString *str_ImageUrl = [_dic_TopInfo stringForKey:@"imgUrl"];
    [_iv_Thumb sd_setImageWithURL:[NSURL URLWithString:str_ImageUrl] placeholderImage:[UIImage imageNamed:@"ic_no_user"]];

    _lb_Name.text = [_dic_TopInfo stringForKey:@"name"];
    
    NSDictionary *dic_Count = [_dic_TopInfo valueForDictionaryKey:@"activity"];
    _lb_MakePlayCount.text = [NSString stringWithFormat:@"%ld", [dic_Count integerForKey:@"upload"]];
    _lb_FollowerCount.text = [NSString stringWithFormat:@"%ld", [dic_Count integerForKey:@"follower"]];
    _lb_FollowingCount.text = [NSString stringWithFormat:@"%ld", [dic_Count integerForKey:@"following"]];
}

- (void)postFollowStatus:(BOOL)isFollow
{
    __weak __typeof__(self) weakSelf = self;

    NSString *str_Path = isFollow ? @"user/follow": @"user/unfollow";
    
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@([_dic_Owner integerForKey:@"userId"]) forKey:@"following"];
    [[WebAPI sharedData] callAsyncWebAPIBlock:str_Path param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            weakSelf.btn_Follow.selected = !weakSelf.btn_Follow.selected;
            weakSelf.btn_Msg.hidden = weakSelf.btn_Follow.selected;
        }
    }];
}


//MARK: UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSInteger nPage = scrollView.contentOffset.x / scrollView.frame.size.width;
    if( scrollView == _sv_Main )
    {
        [self chooseMenu:nPage];
    }
}


//MARK: Action
- (IBAction)goMakePlay:(id)sender
{
    
}

- (IBAction)goFollower:(id)sender
{
    [HapticHelper tap];
    [self postFollowStatus:_btn_Follow.selected];
}

- (IBAction)goMessage:(id)sender
{
    __weak __typeof__(self) weakSelf = self;

    NSString *str_UserId = [NSString stringWithFormat:@"%ld", [_dic_Owner integerForKey:@"userId"]];
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[_dic_Owner stringForKey:@"imgUrl"]]];
    
    [SBDGroupChannel createChannelWithName:[_dic_Owner stringForKey:@"username"] isDistinct:YES userIds:@[str_UserId] coverImage:imageData coverImageName:@"cover" data:nil customType:@"" progressHandler:^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        
    } completionHandler:^(SBDGroupChannel * _Nullable channel, SBDError * _Nullable error) {
        
        if( error != nil )
        {
            NSLog(@"채널생성 에러 : %@", error);
            return;
        }

        ChatViewController *vc = [kFeedBoard instantiateViewControllerWithIdentifier:@"ChatViewController"];
        vc.channel = channel;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];

}

- (IBAction)goScrollMenu:(id)sender
{
    [HapticHelper tap];
    UIButton *btn = (UIButton *)sender;
    [self chooseMenu:btn.tag];
}

- (void)chooseMenu:(NSInteger)idx
{
    self.lc_BarLeading.constant = idx * (self.sv_Main.bounds.size.width/3);
    [UIView animateWithDuration:0.3f animations:^{
        self.sv_Main.contentOffset = CGPointMake(idx * self.sv_Main.bounds.size.width, 0);
        [self.view layoutIfNeeded];
    }];
    
    switch (idx)
    {
        case 0:
            if( _vc_MakePlay.isCalled == NO )
            {
                [_vc_MakePlay updateList];
            }
            _btn_MakePlay.selected = YES;
            _btn_JoinPlay.selected = NO;
            _btn_HeartPlay.selected = NO;
            break;
        case 1:
            if( _vc_JoinPlay.isCalled == NO )
            {
                [_vc_JoinPlay updateList];
            }
            _btn_MakePlay.selected = NO;
            _btn_JoinPlay.selected = YES;
            _btn_HeartPlay.selected = NO;
            break;
        case 2:
            if( _vc_HeartPlay.isCalled == NO )
            {
                [_vc_HeartPlay updateList];
            }
            _btn_MakePlay.selected = NO;
            _btn_JoinPlay.selected = NO;
            _btn_HeartPlay.selected = YES;
            break;
            
        default:
            break;
    }
}


@end
