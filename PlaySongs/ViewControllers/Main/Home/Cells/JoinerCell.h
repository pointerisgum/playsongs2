//
//  JoinerCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/09/02.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JoinerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_Height;
@property (weak, nonatomic) IBOutlet UICollectionView *cv_List;
@property (weak, nonatomic) IBOutlet UIButton *btn_More;
@end

NS_ASSUME_NONNULL_END
