//
//  FeedImageCollectionCell.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/07.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlaySongs-Swift.h"

NS_ASSUME_NONNULL_BEGIN

@interface FeedImageCollectionCell : UICollectionViewCell
@property (nonatomic, assign) CGFloat fImageWidth;
@property (nonatomic, assign) CGFloat fImageHeight;
@property (weak, nonatomic) IBOutlet UIImageView *iv_Thumb;
@property (weak, nonatomic) IBOutlet UIImageView *iv_Video;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_ImageWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_ImageHeight;
@property (weak, nonatomic) IBOutlet UIImageView *iv_Youtube;

@end

NS_ASSUME_NONNULL_END
