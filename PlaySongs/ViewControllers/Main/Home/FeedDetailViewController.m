//
//  FeedDetailViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/16.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "FeedDetailViewController.h"
#import "CommentViewController.h"
#import "UserPageViewController.h"
#import "OwnerInfoCell.h"
#import "FeedSubButtonCell.h"
#import "DetailImageCell.h"
#import "DetailYouTubeCell.h"
#import "DetailVideoCell.h"
#import "DetailTextCell.h"
#import "TagCvCell.h"
//#import "CommentCell.h"
//#import "AddCommentCell.h"
#import "ProductCell.h"
#import "AddCommentViewController.h"
#import "WKYTPlayerView.h"
#import "SharePopUpViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import "FeedCell.h"
#import "PlayJoinViewController.h"
#import "JoinerCell.h"
#import "SearchThumbCell.h"
#import "JoinerMoreViewController.h"
#import "ReportViewController.h"
#import "MakePlayViewController.h"

#define kCellMargin 1.5
#define kMaxJoinerCount 9

#define kLightColor [UIColor colorWithHexString:@"8e8e8e"]
#define kBlackColor [UIColor blackColor]

@interface FeedDetailViewController () <WKYTPlayerViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, assign) BOOL isFirstLoad;
@property (nonatomic, assign) BOOL isJoinType;
@property (nonatomic, strong) NSMutableArray *arM_List;
@property (nonatomic, strong) NSMutableArray *arM_Corver;
@property (nonatomic, strong) NSMutableArray *arM_JoinList;
@property (nonatomic, strong) NSMutableDictionary *dic_Info;

@property (weak, nonatomic) IBOutlet UILabel *lb_Title;
@property (weak, nonatomic) IBOutlet UIImageView *iv_OwnerThumb;
@property (weak, nonatomic) IBOutlet UILabel *lb_OwnerName;
@property (weak, nonatomic) IBOutlet UITableView *tbv_List;

@end

@implementation FeedDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isFirstLoad = YES;
//    _str_PlayId = @"247";
//    _str_PlayId = @"178";
//    _str_PlayId = @"243";
//    _str_PlayId = @"181";
    //157 : 데이터 많은거
//    _str_PlayId = @"316";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateOneItemNoti:) name:@"UpdateOneItemNoti" object:nil];

    [self updateJoinList];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [_tbv_List layoutIfNeeded];
    [self.view layoutIfNeeded];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MBProgressHUD hide];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UpdateOneItemNoti" object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [MBProgressHUD hide];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateOneItemNoti" object:_str_PlayId];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if( [segue.identifier isEqualToString:@"CommentSegue"] )
    {
        CommentViewController *vc = segue.destinationViewController;
        vc.str_PlayId = _str_PlayId;
    }
    else if( [segue.identifier isEqualToString:@"AddCommentSegue"] )
    {
        AddCommentViewController *vc = segue.destinationViewController;
        vc.str_PlayId = _str_PlayId;
    }
    else if( [segue.identifier isEqualToString:@"ShareSegue"] )
    {
        SharePopUpViewController *vc = segue.destinationViewController;
        vc.str_PlayId = [sender stringForKey:@"playId"];
        vc.dic_Item = [sender valueForDictionaryKey:@"obj"];
    }
    else if( [segue.identifier isEqualToString:@"JoinerSegue"] )
    {
        JoinerMoreViewController *vc = segue.destinationViewController;
        vc.str_PlayId = _str_PlayId;
    }
}


- (void)updateOneItemNoti:(NSNotification *)noti
{
    [self updateJoinList];
}

- (void)updateList
{
    __weak __typeof__(self) weakSelf = self;
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@([_str_PlayId integerValue]) forKey:@"playId"];
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"play/page" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            weakSelf.dic_Info = [NSMutableDictionary dictionaryWithDictionary:[resulte valueForDictionaryKey:@"result"]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf updateUI];
                
                if( weakSelf.isFirstLoad )
                {
                    weakSelf.isFirstLoad = NO;
                    weakSelf.tbv_List.contentOffset = CGPointMake(0, -10000);
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        weakSelf.tbv_List.contentOffset = CGPointMake(0, 0);
                    });
                }
            });
        }
    }];
}

- (void)updateJoinList
{
    __weak __typeof__(self) weakSelf = self;
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@([_str_PlayId integerValue]) forKey:@"playId"];
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"playing/feeds" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            weakSelf.arM_JoinList = [NSMutableArray arrayWithArray:[resulte valueForArrayKey:@"result"]];
            
            if( weakSelf.arM_JoinList.count > kMaxJoinerCount )
            {
                NSArray *ar_Cut = [weakSelf.arM_JoinList subarrayWithRange:NSMakeRange(0, kMaxJoinerCount)];
                weakSelf.arM_JoinList = [NSMutableArray arrayWithArray:ar_Cut];
            }
            [weakSelf updateList];
        }
    }];
}

- (void)updateUI
{
    _arM_List = [NSMutableArray array];

    NSString *str_PlayType = [_dic_Info stringForKey:@"playType"];
    _isJoinType = [str_PlayType isEqualToString:@"J"];
    if( _isJoinType )
    {
        _lb_Title.text = @"놀이 참여";
    }
    else
    {
        _lb_Title.text = [_dic_Info stringForKey:@"title"];
    }
    
    [_arM_List addObject:@{@"type":@"owner"}];

    NSArray *ar_Body = [Util jsonStringToArray:[_dic_Info stringForKey:@"contentBody"]];
    for( NSDictionary *dic_Body in ar_Body )
    {
        NSString *str_Type = [dic_Body stringForKey:@"type"];
        if( [str_Type isEqualToString:@"text"] )
        {
            [_arM_List addObject:@{@"type":@"text", @"contents":[dic_Body stringForKey:@"text"]}];
        }
        else if( [str_Type isEqualToString:@"file"] )
        {
            NSArray *ar_Files = [dic_Body valueForArrayKey:@"files"];
            for( NSDictionary *dic_File in ar_Files )
            {
                NSString *str_Type = [dic_File stringForKey:@"type"];
                if( [str_Type isEqualToString:@"image"] )
                {
                    NSString *str_ImagePath = [dic_File stringForKey:@"filePath"];
                    if( str_ImagePath.length > 0 )
                    {
                        NSInteger nWidth = [dic_File integerForKey:@"width"];
                        NSInteger nHeight = [dic_File integerForKey:@"height"];
                        [_arM_List addObject:@{@"type":@"image", @"url":str_ImagePath, @"width":@(nWidth), @"height":@(nHeight)}];
                    }
                }
                else if( [str_Type isEqualToString:@"video"] )
                {
                    NSString *str_ImagePath = [dic_File stringForKey:@"thumbnail"];
                    if( str_ImagePath.length > 0 )
                    {
                        NSInteger nWidth = [dic_File integerForKey:@"width"];
                        NSInteger nHeight = [dic_File integerForKey:@"height"];
                        [_arM_List addObject:@{@"type":@"video", @"url":str_ImagePath, @"width":@(nWidth), @"height":@(nHeight), @"filePath":[dic_File stringForKey:@"filePath"]}];
                    }
                }
            }
        }
        else if( [str_Type isEqualToString:@"youtube"] )
        {
            NSString *str_VideoId = [dic_Body stringForKey:@"videoId"];
            NSString *str_ImagePath = [NSString stringWithFormat:@"https://img.youtube.com/vi/%@/maxresdefault.jpg", str_VideoId];
            [_arM_List addObject:@{@"type":@"youtube", @"url":str_ImagePath, @"width":@(1280), @"height":@(720), @"type":@"youtube", @"videoId":str_VideoId}];
        }
    }
    
    [_arM_List addObject:@{@"type":@"menu"}];

    
    //==교구재 목록==//
    NSArray *ar_Product = [_dic_Info valueForArrayKey:@"productList"];
    for( NSDictionary *dic in ar_Product )
    {
        [_arM_List addObject:@{@"type":@"product", @"obj":dic}];
    }
    //--------------//

    if( _arM_JoinList.count > 0 )
    {
        //참여자
        [_arM_List addObject:@{@"type":@"joiner"}];
    }

    [_tbv_List reloadData];
    [self.view layoutIfNeeded];
}

- (void)userImageTap:(UIGestureRecognizer *)gestureRecognizer
{
    [HapticHelper tap];

    NSDictionary *dic_Owner = [_dic_Info valueForDictionaryKey:@"owner"];
    UserPageViewController *vc = [kUserBoard instantiateViewControllerWithIdentifier:@"UserPageViewController"];
    vc.dic_Owner = dic_Owner;
    [self.navigationController pushViewController:vc animated:YES];
}

//MARK: TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arM_List.count;
}
//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [cell layoutIfNeeded];
//}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailTextCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailTextCell"];
    
    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return cell;
    
    NSDictionary *dic = _arM_List[indexPath.row];
    NSString *str_Type = [dic stringForKey:@"type"];
    if( [str_Type isEqualToString:@"owner"] )
    {
        OwnerInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OwnerInfoCell"];

        NSDictionary *dic_Owner = [_dic_Info valueForDictionaryKey:@"owner"];
        NSString *str_OwnerImageUrl = [dic_Owner stringForKey:@"imgUrl"];
        [cell.iv_OwnerThumb sd_setImageWithString:str_OwnerImageUrl];
        cell.iv_OwnerThumb.userInteractionEnabled = YES;
        UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userImageTap:)];
        [imageTap setNumberOfTapsRequired:1];
        [cell.iv_OwnerThumb addGestureRecognizer:imageTap];

        cell.lb_OwnerName.text = [dic_Owner stringForKey:@"name"];
        
        return cell;
    }
    else if( [str_Type isEqualToString:@"text"] )
    {
        DetailTextCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailTextCell"];
        cell.lb_Contents.text = [dic stringForKey:@"contents"];
        return cell;
    }
    else if( [str_Type isEqualToString:@"image"] )
    {
        DetailImageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailImageCell"];
        [cell initImage:dic];
//        [cell.contentView layoutIfNeeded];
//        [cell layoutIfNeeded];
        return cell;
    }
    else if( [str_Type isEqualToString:@"video"] )
    {
        DetailVideoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailVideoCell"];
        [cell initImage:dic];

        for( UIView *subView in cell.v_ContentsBg.subviews )
        {
            [subView removeFromSuperview];
        }
        
        NSString *str_FilePath = [dic stringForKey:@"filePath"];
        NSURL *url = [NSURL URLWithString:str_FilePath];

        AVPlayer *avPlayer = [AVPlayer playerWithURL:url];
        cell.playerController.view.frame = cell.v_ContentsBg.frame;
        cell.playerController.player = avPlayer;
        cell.playerController.showsPlaybackControls = YES;
        cell.playerController.player = [AVPlayer playerWithURL:url];
        [cell.v_ContentsBg addSubview:cell.playerController.view];

        return cell;
    }
    else if( [str_Type isEqualToString:@"youtube"] )
    {
        DetailYouTubeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailYouTubeCell"];

        NSDictionary *playerVars = @{
                                     @"controls" : @1,
                                     @"playsinline" : @1,
                                     @"autohide" : @1,
                                     @"showinfo" : @0,
                                     @"modestbranding" : @1,
                                     };

        [cell.v_Youtube loadWithVideoId:[dic stringForKey:@"videoId"] playerVars:playerVars];
        
        return cell;
    }
    else if( [str_Type isEqualToString:@"menu"] )
    {
        FeedCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FeedCell"];

        //조인뷰
        if( _isJoinType )
        {
            cell.v_Join.hidden = NO;
            
            cell.v_Join.tag = indexPath.row;
            UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playDetailTap:)];
            [imageTap setNumberOfTapsRequired:1];
            [cell.v_Join addGestureRecognizer:imageTap];

            NSDictionary *dic_JoinInfo = [_dic_Info valueForDictionaryKey:@"playJoin"];
            NSString *str_ImageUrl = [dic_JoinInfo stringForKey:@"coverUrl"];
            [cell.iv_JoinThumb sd_setImageWithString:str_ImageUrl];
            cell.lb_JoinTitle.text = [dic_JoinInfo stringForKey:@"title"];
            cell.lb_JoinSubTitle.text = [dic_JoinInfo stringForKey:@"name"];
            
            NSString *str_JoinCnt = [NSString stringWithFormat:@"%ld", [dic_JoinInfo integerForKey:@"playing"]];
            [cell.btn_JoinPlay setTitle:str_JoinCnt forState:UIControlStateNormal];
            cell.btn_JoinPlay.selected = YES;
        }
        else
        {
            cell.v_Join.hidden = YES;
        }
        
        
        //버튼뷰
        NSDictionary *dic = _dic_Info;
        NSString *str_IsHeart = [dic stringForKey:@"isHeart"];
        cell.btn_Heart.selected = [str_IsHeart isEqualToString:@"Y"];
        
        cell.btn_ButtonPlay.hidden = _isJoinType;
        NSDictionary *dic_Count = [dic valueForDictionaryKey:@"count"];
        NSInteger nPlayingCnt = [dic_Count integerForKey:@"playing"];
        [cell.btn_ButtonPlay setTitle:[NSString stringWithFormat:@"%ld", nPlayingCnt] forState:UIControlStateNormal];
        cell.btn_ButtonPlay.selected = [[dic stringForKey:@"isPlaying"] isEqualToString:@"Y"] ? YES : NO;
        
        //actions
        cell.btn_Heart.tag = indexPath.row;
        [cell.btn_Heart removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
        [cell.btn_Heart addTarget:self action:@selector(onLike:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btn_Comment.tag = indexPath.row;
        [cell.btn_Comment removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
        [cell.btn_Comment addTarget:self action:@selector(onComment) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btn_Share.tag = indexPath.row;
        [cell.btn_Share removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
        [cell.btn_Share addTarget:self action:@selector(onShare) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btn_ButtonPlay.tag = indexPath.row;
        [cell.btn_ButtonPlay removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
        if( cell.btn_ButtonPlay.selected == NO )
        {
            //참여하지 않은 놀이만 참여 할 수 있게
            [cell.btn_ButtonPlay addTarget:self action:@selector(onPlay) forControlEvents:UIControlEventTouchUpInside];
        }
        
        
        //하트
        NSInteger nHeartCnt = [dic_Count integerForKey:@"heart"];
        if( nHeartCnt > 0 )
        {
            //총 하트수가 0보다 클 경우에만 표시
            cell.v_Heart.hidden = NO;
            cell.lb_Heart.text = [NSString stringWithFormat:@"좋아요 %ld개", nHeartCnt];
        }
        else
        {
            cell.v_Heart.hidden = YES;
        }
        
        
        //태그
        if( _isJoinType == NO )
        {
            NSArray *ar_Tags = [Util jsonStringToArray:[dic stringForKey:@"tags"]];
            if( ar_Tags.count > 0 )
            {
                cell.cv_Tag.hidden = NO;
//                cell.lc_TagHeight.constant = 0;
                [cell initTagCell:ar_Tags withIndex:indexPath.row withParent:self];
//                [cell layoutIfNeeded];
//                [_tbv_List layoutIfNeeded];
//                [self.view layoutIfNeeded];
//                [cell.stv_Items layoutIfNeeded];
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//
//                    [self.tbv_List reloadData];
//                });
            }
            else
            {
                cell.cv_Tag.hidden = YES;
            }
        }
        else
        {
            cell.cv_Tag.hidden = YES;
        }
        
        
        //내용
        cell.v_Contents.hidden = YES;
        cell.v_ContentsMore.hidden = YES;

        
        //댓글 모두 보기
        NSInteger nCommentCnt = [dic_Count integerForKey:@"comment"];
        if( nCommentCnt > 2 )
        {
            cell.v_CommentAllShow.hidden = NO;
            cell.lb_CommentAllShow.text = [NSString stringWithFormat:@"댓글 %ld개 모두 보기", nCommentCnt];
        }
        else
        {
            cell.v_CommentAllShow.hidden = YES;
        }
        
        
        //댓글
        cell.v_Comment1.hidden = YES;
        cell.v_Comment2.hidden = YES;
        NSArray *ar_CommentList = [dic valueForArrayKey:@"commentList"];
        NSInteger nShowCommentCnt = ar_CommentList.count;
        if( nShowCommentCnt > 2 )
        {
            nShowCommentCnt = 2;
        }
        for( NSInteger i = 0; i < nShowCommentCnt; i++ )
        {
            NSDictionary *dic_Comment = ar_CommentList[i];
            NSString *str_Name = [dic_Comment stringForKey:@"name"];
            NSString *str_Contents = [NSString stringWithFormat:@"  %@", [dic_Comment stringForKey:@"comment"]];
            NSMutableAttributedString *attM = [[NSMutableAttributedString alloc] initWithString:str_Name attributes:@{NSForegroundColorAttributeName:[UIColor blackColor],
                                                                                                                      NSFontAttributeName:[UIFont systemFontOfSize:14 weight:UIFontWeightBold]}];
            NSAttributedString *att = [[NSAttributedString alloc] initWithString:str_Contents attributes:@{ NSForegroundColorAttributeName:[UIColor darkGrayColor],
                                                                                                            NSFontAttributeName:[UIFont systemFontOfSize:14 weight:UIFontWeightMedium]}];
            [attM appendAttributedString:att];
            if( i == 0 )
            {
                cell.v_Comment1.hidden = NO;
                cell.lb_Comment1.attributedText = attM;
            }
            else
            {
                cell.v_Comment2.hidden = NO;
                cell.lb_Comment2.attributedText = attM;
            }
        }
        
        
        //댓글 달기
        [cell.iv_AddCommentThumb sd_setImageWithString:[UserData sharedData].imgUrl];
        cell.btn_AddComment.tag = indexPath.row;
        [cell.btn_AddComment removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
        [cell.btn_AddComment addTarget:self action:@selector(onAddComment) forControlEvents:UIControlEventTouchUpInside];
        
        //시간
        NSString *str_Date = [NSString stringWithFormat:@"%@", [dic stringForKey:@"createDate"]];
        cell.lb_Time.text = [Util getCommentAfterTime:str_Date];
//        [cell.stv_Items layoutIfNeeded];

        return cell;
    }
    else if( [str_Type isEqualToString:@"product"] )
    {
        ProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProductCell"];
        
        NSDictionary *dic_Product = [dic valueForDictionaryKey:@"obj"];
        cell.lb_Title.text = [dic_Product stringForKey:@"productName"];
        cell.lb_Contents.text = [dic_Product stringForKey:@"productDesc"];
        NSString *str_ImageUrl = [dic_Product stringForKey:@"productImg"];
        [cell.iv_Thumb sd_setImageWithURL:[NSURL URLWithString:str_ImageUrl] placeholderImage:[UIImage imageNamed:@"ic_no_image"]];
        
        NSInteger nPrice = [dic_Product integerForKey:@"price"];
        NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
        format.numberStyle = NSNumberFormatterDecimalStyle;
        NSString *str_Price = [format stringFromNumber:@(nPrice)];
        [cell.btn_Price setTitle:[NSString stringWithFormat:@"%@원", str_Price] forState:UIControlStateNormal];
        
//        NSString *str_FileJson = [dic_Product stringForKey:@"fileData"];
//        NSData *jsonData = [str_FileJson dataUsingEncoding:NSUTF8StringEncoding];
//        NSDictionary *dic_File = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];

        return cell;
    }
    else if( [str_Type isEqualToString:@"joiner"] )
    {
        JoinerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JoinerCell"];
        cell.cv_List.delegate = self;
        cell.cv_List.dataSource = self;
        [cell.cv_List reloadData];
        cell.lc_Height.constant = cell.cv_List.contentSize.height;
        
        [cell.btn_More removeTarget:self action:NULL forControlEvents:UIControlEventAllEvents];
        [cell.btn_More addTarget:self action:@selector(onJoinerMore) forControlEvents:UIControlEventTouchUpInside];

        return cell;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    NSDictionary *dic = _arM_List[indexPath.row];
    NSString *str_Type = [dic stringForKey:@"type"];

    if( [str_Type isEqualToString:@"video"] )
    {
        DetailVideoCell *cell = (DetailVideoCell *)[tableView cellForRowAtIndexPath:indexPath];
        if( cell.playerController.player.rate != 0 && cell.playerController.player.error == nil )
        {
            //재생 중
            [cell.playerController.player pause];
        }
        else
        {
            //재생중이 아님
            [cell.playerController.player play];
        }
    }


//    NSDictionary *dic = self.ar_List[indexPath.row];
//    [self performSegueWithIdentifier:@"goDetail" sender:dic];
}


//MARK: CollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _arM_JoinList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SearchThumbCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchThumbCell" forIndexPath:indexPath];
    if( IsArrayOutOfBounds(_arM_JoinList, indexPath.row) )  return cell;
    NSDictionary *dic = _arM_JoinList[indexPath.row];
    NSString *str_ImageUrl = [dic stringForKey:@"coverUrl"];
    [cell.iv_Thumb sd_setImageWithURL:[NSURL URLWithString:str_ImageUrl] placeholderImage:[UIImage imageNamed:@"ic_no_image"]];
    cell.iv_Thumb.backgroundColor = [UIColor redColor];
    CGFloat fCellSize = (collectionView.bounds.size.width - (kCellMargin*2))/3;
    cell.lc_Width.constant = fCellSize;
    cell.lc_Height.constant = fCellSize;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    CGFloat minSize = collectionView.frame.size.width * 0.7f;//MIN(collectionView.frame.size.width, collectionView.frame.size.height);
    //    return CGSizeMake(minSize * 1.03, minSize);
    //    //    return CGSizeMake([str_Title sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:14.f]}].width + 30.f, 50.f);
    
    //    NSDictionary *dic = _arM_Corver[indexPath.row];
    //    NSInteger nWidth = [dic integerForKey:@"width"];
    //    NSInteger nHeight = [dic integerForKey:@"height"];
    //    CGFloat fRatio = kDeviceWidth/nWidth;
//    return CGSizeMake(collectionView.bounds.size.width, collectionView.bounds.size.height);
    
    CGFloat fCellSize = (collectionView.bounds.size.width - (kCellMargin*2))/3;
    return CGSizeMake(fCellSize, fCellSize);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(kCellMargin, 0, kCellMargin, 0);
    
    //    CGFloat minSize = collectionView.frame.size.width * 0.7f;//MIN(collectionView.frame.size.width, collectionView.frame.size.height);
    //
    ////    return UIEdgeInsetsMake(20, (collectionView.frame.size.width - minSize) / 2, 20, 16*self.ar_CvEventList.count);
    //    return UIEdgeInsetsMake(20, (collectionView.frame.size.width - minSize) / 2, 20, (16*self.ar_CvEventList.count) + ((collectionView.frame.size.width - minSize) / 2)/2);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [HapticHelper tap];
    
    if( IsArrayOutOfBounds(_arM_JoinList, indexPath.row) )  return;

    NSDictionary *dic = _arM_JoinList[indexPath.row];
    FeedDetailViewController *vc = [kFeedBoard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
    vc.str_PlayId = [NSString stringWithFormat:@"%ld", [dic integerForKey:@"playId"]];
    [self.navigationController pushViewController:vc animated:YES];
}

//- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    [cell layoutIfNeeded];
//}


//MARK: Action
- (void)playDetailTap:(UIGestureRecognizer *)gestureRecognizer
{
    [HapticHelper tap];
    
    NSDictionary *dic_Join = [_dic_Info valueForDictionaryKey:@"playJoin"];
    FeedDetailViewController *vc = [kFeedBoard instantiateViewControllerWithIdentifier:@"FeedDetailViewController"];
    vc.str_PlayId = [NSString stringWithFormat:@"%ld", [dic_Join integerForKey:@"playId"]];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onLike:(UIButton *)btn
{
    [HapticHelper tap];
    
    __weak __typeof(&*self)weakSelf = self;
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setValue:@([_dic_Info integerForKey:@"playId"]) forKey:@"playId"];
    [dicM_Params setValue:btn.selected?@"N":@"Y" forKey:@"status"];
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"play/heart/save" param:dicM_Params withMethod:@"POST" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            NSDictionary *dic_Result = [resulte dictionaryForKey:@"result"];
            
            //좋아요 토글 상태 업데이트
            NSString *str_Status = [dic_Result stringForKey:@"status"];
            [weakSelf.dic_Info setObject:str_Status forKey:@"isHeart"];
            
            //좋아요 카운트 업데이트
            NSMutableDictionary *dicM_Count = [NSMutableDictionary dictionaryWithDictionary:[weakSelf.dic_Info valueForDictionaryKey:@"count"]];
            NSInteger nHeartCnt = [dicM_Count integerForKey:@"heart"];
            if( [str_Status isEqualToString:@"Y"] )
            {
                nHeartCnt++;
            }
            else
            {
                nHeartCnt--;
            }
            [dicM_Count setObject:@(nHeartCnt) forKey:@"heart"];
            [weakSelf.dic_Info setObject:dicM_Count forKey:@"count"];
            
            [weakSelf updateUI];
        }
    }];
}

- (void)onComment
{
    [HapticHelper tap];

    [self performSegueWithIdentifier:@"CommentSegue" sender:nil];
}

- (void)onShare
{
    [HapticHelper tap];

    NSString *str_PlayId = @"";
    if( _isJoinType )
    {
        str_PlayId = [NSString stringWithFormat:@"%ld", [_dic_Info integerForKey:@"joinPlayId"]];
    }
    else
    {
        str_PlayId = [NSString stringWithFormat:@"%ld", [_dic_Info integerForKey:@"playId"]];
    }
    
    [self performSegueWithIdentifier:@"ShareSegue" sender:@{@"playId":str_PlayId, @"obj":_dic_Info}];
}

- (void)onAddComment
{
    [HapticHelper tap];

    [self performSegueWithIdentifier:@"AddCommentSegue" sender:nil];
}

- (void)onPlay
{
    [HapticHelper tap];

    __weak __typeof__(self) weakSelf = self;
    __block NSString *str_PlayId = str_PlayId = [NSString stringWithFormat:@"%ld", [_dic_Info integerForKey:@"playId"]];
    
    InstarPicker *instarPicker = [[InstarPicker alloc] init];
    [instarPicker setMultiCompletionHandler:^(NSArray * _Nonnull array) {
        
        if( array.count > 0 )
        {
            PlayJoinViewController *vc = [kFeedBoard instantiateViewControllerWithIdentifier:@"PlayJoinViewController"];;
            vc.str_PlayId = str_PlayId;
            vc.dic_Item = weakSelf.dic_Info;
            vc.ar_Images = array;
            [weakSelf presentViewController:vc animated:YES completion:nil];
        }
    }];
    
    YPImagePicker *pickerNavi = [instarPicker getMultiPhotoWithCount:5];
    [self presentViewController:(UINavigationController *)pickerNavi animated:YES completion:nil];
}

- (IBAction)goShowComment:(id)sender
{
    [self onComment];
}

- (void)onJoinerMore
{
    [self performSegueWithIdentifier:@"JoinerSegue" sender:nil];
}

- (IBAction)goMoreMenu:(id)sender
{
    [HapticHelper tap];
    
    __weak __typeof__(self) weakSelf = self;

    __block NSDictionary *dic = _dic_Info;
    NSDictionary *dic_Owner = [dic valueForDictionaryKey:@"owner"];
    NSInteger nOwnerId = [dic_Owner integerForKey:@"userId"];
    BOOL isMy = NO;
    if( nOwnerId == [UserData sharedData].userId )
    {
        isMy = YES;
    }
    
    SPAlertController *alert = [SPAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:SPAlertControllerStyleActionSheet];
    
    if( isMy )
    {
        SPAlertAction *action = [SPAlertAction actionWithTitle:@"삭제" style:SPAlertActionStyleDestructive
                                         handler:^(SPAlertAction * _Nonnull action) {
            [weakSelf deleteItem];
        }];
        [alert addAction:action];

        NSString *str_PlayType = [dic stringForKey:@"playType"];
        BOOL isJoinType = [str_PlayType isEqualToString:@"J"];
        if( isJoinType == NO )
        {
            action = [SPAlertAction actionWithTitle:@"수정" style:SPAlertActionStyleDefault
                                            handler:^(SPAlertAction * _Nonnull action) {
                [weakSelf modify];
            }];
            [alert addAction:action];
        }
    }
    else
    {
        SPAlertAction *action = [SPAlertAction actionWithTitle:@"신고" style:SPAlertActionStyleDestructive
                                         handler:^(SPAlertAction * _Nonnull action) {
            [weakSelf reportItem];
        }];
        [alert addAction:action];
    }
    
    SPAlertAction *action = [SPAlertAction actionWithTitle:@"링크복사" style:SPAlertActionStyleDefault
                                                    handler:^(SPAlertAction * _Nonnull action) {

        [HapticHelper tap];
        NSString *str_ShareKey = [dic stringForKey:@"shareKey"];
        //https://www.playhavea.com/p/SqqmghLk9OPWD32f?utm_source=web_button_share
        //https://www.playhavea.com/p/aHQPFNARpDuYdsug?utm_source=web_button_share
        NSString *str_Link = [NSString stringWithFormat:@"%@/p/%@?utm_source=web_button_share", kDomain, str_ShareKey];
        [UIPasteboard generalPasteboard].string = str_Link;
        [weakSelf dismissViewControllerAnimated:YES completion:^{
            [weakSelf.navigationController.view makeToast:@"링크를 클립보드에 복사했습니다."];
        }];
    }];
    [alert addAction:action];

    
    action = [SPAlertAction actionWithTitle:@"공유대상..." style:SPAlertActionStyleDefault
                                                    handler:^(SPAlertAction * _Nonnull action) {
        
        [HapticHelper tap];
        NSString *str_ShareKey = [dic stringForKey:@"shareKey"];
        NSString *str_Link = [NSString stringWithFormat:@"%@\n\n%@/p/%@?utm_source=web_button_share", [dic stringForKey:@"title"], kDomain, str_ShareKey];

        NSArray* sharedObjects=[NSArray arrayWithObjects:str_Link, nil];
        UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:sharedObjects applicationActivities:nil];
        activityViewController.popoverPresentationController.sourceView = self.view;
        [weakSelf presentViewController:activityViewController animated:YES completion:nil];
    }];
    [alert addAction:action];

    
    if( isMy )
    {
//        SPAlertAction *action = [SPAlertAction actionWithTitle:@"공유" style:SPAlertActionStyleDefault
//                                         handler:^(SPAlertAction * _Nonnull action) {
//            [HapticHelper tap];
//            [weakSelf onShare];
//        }];
//        [alert addAction:action];
    }
    else
    {
        if( [[dic stringForKey:@"isFollowing"] isEqualToString:@"Y"] )
        {
            SPAlertAction *action = [SPAlertAction actionWithTitle:@"팔로우 취소" style:SPAlertActionStyleDefault
                                             handler:^(SPAlertAction * _Nonnull action) {[self reqFollow:NO];}];
            [alert addAction:action];
        }
        else
        {
            SPAlertAction *action = [SPAlertAction actionWithTitle:@"팔로우" style:SPAlertActionStyleDefault
                                             handler:^(SPAlertAction * _Nonnull action) {[self reqFollow:YES];}];
            [alert addAction:action];
        }
    }
    
    action = [SPAlertAction actionWithTitle:@"취소" style:SPAlertActionStyleCancel
                                                    handler:^(SPAlertAction * _Nonnull action) {}];
    [alert addAction:action];
    [self presentViewController: alert animated:YES completion:^{}];
}

- (void)deleteItem
{
    __weak __typeof__(self) weakSelf = self;

    [HapticHelper tap];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        [UIAlertController showAlertInViewController:self
                                           withTitle:@""
                                             message:@"삭제 하시겠습니까?"
                                   cancelButtonTitle:@"아니요"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@[@"예"]
                                            tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                
                                                if (buttonIndex >= controller.firstOtherButtonIndex)
                                                {
                                                    NSString *str_Path = [NSString stringWithFormat:@"play/%ld", [weakSelf.dic_Info integerForKey:@"playId"]];
                                                    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
                                                    [dicM_Params setObject:@([UserData sharedData].userId) forKey:@"userId"];
                                                    
                                                    [[WebAPI sharedData] callAsyncWebAPIBlock:str_Path param:dicM_Params withMethod:@"DELETE" withBlock:^(id resulte, NSError *error) {
                                                        
                                                        [MBProgressHUD hide];
                                                        
                                                        if( error != nil )
                                                        {
                                                            return;
                                                        }
                                                        
                                                        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
                                                        if( [str_Success isEqualToString:@"success"] )
                                                        {
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshFeedListNoti" object:nil];
                                                            [weakSelf.navigationController popViewControllerAnimated:YES];
                                                        }
                                                    }];
                                                }
                                            }];
    });
}

- (void)reportItem
{
    [HapticHelper tap];

    NSDictionary *dic = _dic_Info;
    NSString *str_PlayId = [NSString stringWithFormat:@"%ld", [dic integerForKey:@"playId"]];
    
    ReportViewController *vc = [kFeedBoard instantiateViewControllerWithIdentifier:@"ReportViewController"];
    vc.str_PlayId = str_PlayId;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)reqFollow:(BOOL)isFollow
{
    __weak __typeof__(self) weakSelf = self;
    NSDictionary *dic_Owner = [_dic_Info valueForDictionaryKey:@"owner"];
    NSString *str_Path = isFollow ? @"user/follow": @"user/unfollow";
    
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@([dic_Owner integerForKey:@"userId"]) forKey:@"following"];
    [[WebAPI sharedData] callAsyncWebAPIBlock:str_Path param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            [weakSelf.view makeToast:isFollow?@"팔로우":@"팔로우 취소함"];
            [weakSelf updateJoinList];
        }
    }];
}

- (void)modify
{
    MakePlayViewController *vc = [kPlayMakeBoard instantiateViewControllerWithIdentifier:@"MakePlayViewController"];
    vc.dic_Modify = _dic_Info;
    [self presentViewController:vc animated:YES completion:^{
        
    }];
}


@end
