//
//  ReportViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/09/02.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "ReportViewController.h"
#import "ReportCell.h"

@interface ReportViewController ()
@property (nonatomic, assign) NSInteger nSelectedIdx;
@property (nonatomic, strong) NSMutableArray *arM_List;
@property (weak, nonatomic) IBOutlet UIView *v_Bg;
@property (weak, nonatomic) IBOutlet UITableView *tbv_List;
@property (weak, nonatomic) IBOutlet UIButton *btn_Next;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_ContentsHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_NextButtonBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_BgBottom;
@end

@implementation ReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _nSelectedIdx = -1;
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:_v_Bg.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    _v_Bg.layer.mask = maskLayer;

    _lc_ContentsHeight.constant = 0;
//    _lc_BgBottom.constant = kDeviceHeight * 0.7 * -1;

    [self updateList];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    _lc_NextButtonBottom.constant = 15 + [Util keyWindow].safeAreaInsets.bottom;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    _lc_BgBottom.constant = 0;
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)updateList
{
    __weak __typeof__(self) weakSelf = self;

    [[WebAPI sharedData] callAsyncWebAPIBlock:@"codes/report" param:nil withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            weakSelf.arM_List = [resulte valueForArrayKey:@"result"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.tbv_List reloadData];
            weakSelf.lc_ContentsHeight.constant = weakSelf.tbv_List.contentSize.height + 170 + [Util keyWindow].safeAreaInsets.bottom;
        });
    }];
}

- (void)reqReport
{
    __weak __typeof__(self) weakSelf = self;

    NSDictionary *dic = _arM_List[_nSelectedIdx];
    NSInteger nCode = [dic integerForKey:@"codeId"];
    
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@([_str_PlayId integerValue]) forKey:@"playId"];
    [dicM_Params setObject:@([UserData sharedData].userId) forKey:@"userId"];
    [dicM_Params setObject:@(nCode) forKey:@"reason"];

    [[WebAPI sharedData] callAsyncWebAPIBlock:@"play/report" param:dicM_Params withMethod:@"POST" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];

        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            if( self.completionBlock )
            {
                self.completionBlock(@"");
            }

            [weakSelf dismissViewControllerAnimated:YES completion:^{
                [[Util keyWindow].rootViewController.view makeToast:@"신고 되었습니다."];
            }];
        }
    }];
}


//MARK: TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    _btn_Next.selected = _nSelectedIdx > -1;
    _btn_Next.userInteractionEnabled = _btn_Next.selected;
    return _arM_List.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReportCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReportCell"];
    
    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return cell;
    
    NSDictionary *dic = _arM_List[indexPath.row];
    [cell.btn_Title setTitle:[dic stringForKey:@"codeName"] forState:UIControlStateNormal];
    
    cell.check.tag = indexPath.row;
    [cell.check addTarget:self action:@selector(onMenuSelected:) forControlEvents:UIControlEventValueChanged];

    if( indexPath.row != _nSelectedIdx )
    {
        [cell.check set_IBCheckState:@"Unchecked"];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

//    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return;
//
//    [HapticHelper tap];
//
//    _nSelectedIdx = indexPath.row;
//    [_tbv_List reloadData];
}


//MARK: Action
- (IBAction)goNext:(id)sender
{
    if( _btn_Next.selected == NO )  return;
    if( _nSelectedIdx < 0 )  return;
    
    [HapticHelper tap];
    
    __weak __typeof__(self) weakSelf = self;
    NSDictionary *dic = _arM_List[_nSelectedIdx];
    NSString *str_Msg = [NSString stringWithFormat:@"'%@' 이유로 게시물을 신고하겠습니까? 신고된 게시물은 피드나 검색 결과에서 나타나지 않습니다.", [dic stringForKey:@"codeName"]];
    [UIAlertController showAlertInViewController:self
                                       withTitle:@""
                                         message:str_Msg
                               cancelButtonTitle:@"닫기"
                          destructiveButtonTitle:nil
                               otherButtonTitles:@[@"확인"]
                                        tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                            
                                            if (buttonIndex >= controller.firstOtherButtonIndex)
                                            {
                                                [weakSelf reqReport];
                                            }
                                        }];
}

- (void)onMenuSelected:(M13Checkbox *)checkBox
{
    [HapticHelper tap];

    if( [checkBox._IBCheckState isEqualToString:@"Unchecked"] )
    {
        _nSelectedIdx = -1;
    }
    else
    {
        _nSelectedIdx = checkBox.tag;
    }
    NSLog(@"%@", checkBox._IBCheckState);
    [_tbv_List reloadData];
}

@end
