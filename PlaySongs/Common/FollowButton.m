//
//  FollowButton.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/16.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "FollowButton.h"

@implementation FollowButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.layer.cornerRadius = 6.f;
    self.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.layer.borderWidth = 0.5f;
    
    self.selected = NO;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
//    [HapticHelper tap];
    
    if( self.selected )
    {
        [self.titleLabel setFont:[UIFont systemFontOfSize:14 weight:UIFontWeightMedium]];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.backgroundColor = [UIColor colorWithHexString:@"1677F1"];
        self.layer.borderColor = [UIColor clearColor].CGColor;
        [self setTitle:@"팔로우" forState:UIControlStateNormal];
    }
    else
    {
        [self.titleLabel setFont:[UIFont systemFontOfSize:14 weight:UIFontWeightMedium]];
        [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.backgroundColor = [UIColor whiteColor];
        self.layer.borderColor = [UIColor darkGrayColor].CGColor;
        [self setTitle:@"팔로잉" forState:UIControlStateNormal];
    }
}

@end
