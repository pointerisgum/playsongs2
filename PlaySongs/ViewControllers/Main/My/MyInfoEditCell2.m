//
//  MyInfoEditCell2.m
//  PlaySongs
//
//  Created by 김영민 on 2020/09/03.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "MyInfoEditCell2.h"

@implementation MyInfoEditCell2

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.tv.textContainerInset = UIEdgeInsetsZero;
    self.tv.textContainer.lineFragmentPadding = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
