//
//  FollowListContainerViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/16.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "FollowListContainerViewController.h"
#import "FollowCell.h"
#import "UserPageViewController.h"
#import "TagPageViewController.h"

@interface FollowListContainerViewController ()

@end

@implementation FollowListContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _refreshControl = [[ODRefreshControl alloc] initInScrollView:_tbv_List];
    _refreshControl.tintColor = kEnableColor;
    _refreshControl.backgroundColor = [UIColor whiteColor];
    [_refreshControl addTarget:self action:@selector(onRefresh:) forControlEvents:UIControlEventValueChanged];
    [_tbv_List addSubview:_refreshControl];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//- (void)updateList
//{
//    __weak __typeof__(self) weakSelf = self;
//
//    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
////    [dicM_Params setValue:@(_nPage) forKey:@"page"];
////    [dicM_Params setValue:@(kLimitCount) forKey:@"limt"];
//    [dicM_Params addEntriesFromDictionary:_dic_Params];
//    [[WebAPI sharedData] callAsyncWebAPIBlock:_str_Path param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [weakSelf.refreshControl endRefreshing];
//        });
//
//        [MBProgressHUD hide];
//
//        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
//        if( [str_Success isEqualToString:@"success"] )
//        {
//
//        }
//
//        [weakSelf.tbv_List reloadData];
//    }];
//}

- (void)onRefresh:(ODRefreshControl *)sender
{
    [HapticHelper tap];
    
    [_arM_List removeAllObjects];

    __weak __typeof__(self) weakSelf = self;

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        dispatch_async(dispatch_get_main_queue(), ^{
            
            if( weakSelf.refreshBlock )
            {
                weakSelf.refreshBlock();
            }
        });

    });
}


//MARK: TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arM_List.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FollowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FollowCell"];

    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return cell;
    
    NSDictionary *dic = _arM_List[indexPath.row];
    
    if( [[dic stringForKey:@"type"] isEqualToString:@"tag"] )
    {
        NSString *str_ImageUrl = [dic stringForKey:@"coverUrl"];
        [cell.iv_Thumb sd_setImageWithURL:[NSURL URLWithString:str_ImageUrl] placeholderImage:[UIImage imageNamed:@"ic_no_image"]];
        cell.lb_Title.text = @"해시태그";
        cell.lb_Contents.text = [dic stringForKey:@"tagName"];
        cell.btn_Status.selected = [[dic stringForKey:@"isFollowing"] isEqualToString:@"Y"] ? NO : YES;
        cell.btn_Status.tag = indexPath.row;
        [cell.btn_Status addTarget:self action:@selector(onFollow:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        NSString *str_ImageUrl = [dic stringForKey:@"imgUrl"];
        [cell.iv_Thumb sd_setImageWithURL:[NSURL URLWithString:str_ImageUrl] placeholderImage:[UIImage imageNamed:@"ic_no_image"]];
        cell.lb_Title.text = [dic stringForKey:@"name"];
        cell.lb_Contents.text = [dic stringForKey:@"username"];
        cell.btn_Status.selected = [[dic stringForKey:@"isFollowing"] isEqualToString:@"Y"] ? NO : YES;
        cell.btn_Status.tag = indexPath.row;
        [cell.btn_Status addTarget:self action:@selector(onFollow:) forControlEvents:UIControlEventTouchUpInside];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    [HapticHelper tap];

    if( IsArrayOutOfBounds(_arM_List, indexPath.row) )  return;

    NSDictionary *dic = _arM_List[indexPath.row];
    if( [[dic stringForKey:@"type"] isEqualToString:@"tag"] )
    {
        TagPageViewController *vc = [kUserBoard instantiateViewControllerWithIdentifier:@"TagPageViewController"];
        vc.str_TagName = [dic stringForKey:@"tagName"];
        vc.nTagId = [dic integerForKey:@"tagId"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        UserPageViewController *vc = [kUserBoard instantiateViewControllerWithIdentifier:@"UserPageViewController"];
        vc.dic_Owner = dic;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


//MARK: Action
- (void)onFollow:(UIButton *)btn
{
    [HapticHelper tap];
    
    if( IsArrayOutOfBounds(_arM_List, btn.tag) )  return;

    __block NSMutableDictionary *dicM = _arM_List[btn.tag];
    __weak __typeof__(self) weakSelf = self;
    
    NSInteger nItemId = 0;
    NSString *str_Path = @"";
    if( [[dicM stringForKey:@"type"] isEqualToString:@"tag"] )
    {
        str_Path = btn.selected ? @"tag/follow": @"tag/unfollow";
        nItemId = [dicM integerForKey:@"tagId"];
    }
    else
    {
        str_Path = btn.selected ? @"user/follow": @"user/unfollow";
        nItemId = [dicM integerForKey:@"userId"];
    }
    
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setObject:@(nItemId) forKey:@"following"];
    [[WebAPI sharedData] callAsyncWebAPIBlock:str_Path param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        [MBProgressHUD hide];
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            [dicM setObject:btn.selected?@"Y":@"N" forKey:@"isFollowing"];
            
            if( IsArrayOutOfBounds(weakSelf.arM_List, btn.tag) == NO )
            {
                [weakSelf.arM_List replaceObjectAtIndex:btn.tag withObject:dicM];
            }
            
            [weakSelf.tbv_List reloadData];

//            if( weakSelf.isMy )
//            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    if( weakSelf.followingChangeBlock )
                    {
                        weakSelf.followingChangeBlock(dicM);
                    }
                });
//            }
        }
    }];
}

@end
