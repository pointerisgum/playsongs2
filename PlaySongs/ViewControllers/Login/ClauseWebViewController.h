//
//  ClauseWebViewController.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/06.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ClauseWebViewController : UIViewController
@property (nonatomic, strong) NSString *str_Title;
@property (nonatomic, strong) NSString *str_Url;
@end

NS_ASSUME_NONNULL_END
