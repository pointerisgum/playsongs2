//
//  DetailImageCell.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/16.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "DetailImageCell.h"

@implementation DetailImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initImage:(NSDictionary *)dic
{
    NSInteger nImageWidth = [dic integerForKey:@"width"];
    NSInteger nImageHeight = [dic integerForKey:@"height"];
    NSString *str_ImageUrl = [dic stringForKey:@"url"];
    [_iv_Thumb sd_setImageWithURL:[NSURL URLWithString:str_ImageUrl] placeholderImage:[UIImage imageNamed:@"ic_no_image"]];
    CGFloat fRatio = (kDeviceWidth / nImageWidth);
    if( isnan(fRatio) || isinf(fRatio) )
    {
        _lc_ImageHeight.constant = kDeviceWidth;
    }
    else
    {
        _lc_ImageHeight.constant = nImageHeight * fRatio;
    }
}

@end
