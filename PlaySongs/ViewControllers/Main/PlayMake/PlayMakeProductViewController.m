//
//  PlayMakeProductViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/29.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "PlayMakeProductViewController.h"
#import "PlaySongs-Swift.h"

@interface PlayMakeProductViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btn_Done;
@property (weak, nonatomic) IBOutlet UIImageView *iv_Thumb;
@property (weak, nonatomic) IBOutlet UITextField *tf_Title;
@property (weak, nonatomic) IBOutlet UITextField *tf_SubTitle;
@property (weak, nonatomic) IBOutlet UITextField *tf_Price;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_Bottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lc_ImageHeight;

@end

@implementation PlayMakeProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MBProgressHUD hide];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


- (BOOL)doneCheck
{
    if( _tf_Title.text.length > 0 && _tf_SubTitle.text.length > 0 && _tf_Price.text > 0 && _iv_Thumb.image != nil )
    {
        _btn_Done.enabled = YES;
        return YES;
    }
    else
    {
        _btn_Done.enabled = NO;
    }
    
    return NO;
}


//MARK: Noti
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    
    _lc_Bottom.constant = keyboardBounds.size.height + 20;
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    _lc_Bottom.constant = 20;
    [UIView animateWithDuration:0.25f animations:^{
        [self.view layoutIfNeeded];
    }];
}


//MARK: UITextFieldDelegate
- (void)textFieldDidChangeSelection:(UITextField *)textField
{
    [self doneCheck];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if( textField == _tf_Title )
    {
        [_tf_SubTitle becomeFirstResponder];
    }
    else if( textField == _tf_SubTitle )
    {
        [_tf_Price becomeFirstResponder];
    }
    
    return YES;
}


//MARK: Action
- (IBAction)goShowMedia:(id)sender
{
    [HapticHelper tap];

    __weak __typeof(&*self)weakSelf = self;
    
    InstarPicker *instarPicker = [[InstarPicker alloc] init];
    [instarPicker setCompletionHandler:^(UIImage * _Nonnull image) {
        
        [weakSelf doneCheck];
        
        CGFloat fRatio = weakSelf.iv_Thumb.frame.size.width/image.size.width;
        weakSelf.lc_ImageHeight.constant = image.size.height * fRatio;
        weakSelf.iv_Thumb.image = image;
    }];

    YPImagePicker *pickerNavi = [instarPicker getSinglePhoto];
    [self presentViewController:(UINavigationController *)pickerNavi animated:YES completion:nil];
}

- (IBAction)goDone:(id)sender
{
    if( [self doneCheck] == NO )  {return;}
    
    [HapticHelper tap];
    
    if( _completionBlock )
    {
        _completionBlock(@{@"type":@"product", @"image":_iv_Thumb.image, @"title":_tf_Title.text, @"subTitle":_tf_SubTitle.text, @"price":_tf_Price.text});
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
