//
//  AddCommentViewController.h
//  PlaySongs
//
//  Created by 김영민 on 2020/08/12.
//  Copyright © 2020 김영민. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddCommentViewController : UIViewController
@property (nonatomic, strong) NSString *str_PlayId;
@end

NS_ASSUME_NONNULL_END
