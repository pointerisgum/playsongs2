//
//  EmailLoginViewController.m
//  PlaySongs
//
//  Created by 김영민 on 2020/08/05.
//  Copyright © 2020 김영민. All rights reserved.
//

#import "EmailLoginViewController.h"
#import "EmailAccView.h"
#import "EmailLoginPwViewController.h"
#import "EmailJoinPwViewController.h"

@interface EmailLoginViewController ()
@property (nonatomic, strong) EmailAccView *v_AccView;
@property (weak, nonatomic) IBOutlet UIView *v_Email;
@property (weak, nonatomic) IBOutlet UIButton *btn_Status;
@property (weak, nonatomic) IBOutlet UITextField *tf_Email;
@property (weak, nonatomic) IBOutlet UILabel *lb_WarningMsg;

@end

@implementation EmailLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _v_Email.layer.cornerRadius = 8.0f;
    _v_Email.layer.borderWidth = 0.5f;
    _v_Email.layer.borderColor = kEnableBoarderColor.CGColor;

    _v_AccView = [[NSBundle mainBundle]loadNibNamed:@"EmailAccView" owner:self options:nil].firstObject;
    [_v_AccView.btn_Next setTitle:@"다음" forState:UIControlStateNormal];
    _tf_Email.inputAccessoryView = _v_AccView;
    [_v_AccView.btn_Next addTarget:self action:@selector(onNext) forControlEvents:UIControlEventTouchUpInside];
    
#ifdef DEBUG
    _tf_Email.text = @"y011@t.com";
#endif
    
//    [self validCheck];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tf_Email becomeFirstResponder];
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if( [segue.identifier isEqualToString:@"EmailLoginPwViewController"] )
    {
        EmailLoginPwViewController *vc = (EmailLoginPwViewController *)segue.destinationViewController;
        vc.str_Email = _tf_Email.text;
    }
    else if( [segue.identifier isEqualToString:@"EmailJoinPwViewController"] )
    {
        EmailJoinPwViewController *vc = (EmailJoinPwViewController *)segue.destinationViewController;
        vc.str_Email = _tf_Email.text;
    }
}


- (void)validCheck
{
    self.btn_Status.hidden = self.tf_Email.text.length <= 0;
    
    if( [Util isUsableEmail:self.tf_Email.text] )
    {
        //이메일 형식일 경우
        self.btn_Status.selected = YES;
        self.v_Email.layer.borderColor = kEnableBoarderColor.CGColor;
        self.v_AccView.btn_Next.backgroundColor = kEnableColor;
        self.v_AccView.btn_Next.enabled = YES;
        [UIView animateWithDuration:0.25 animations:^{
            self.lb_WarningMsg.alpha = NO;
        }];
    }
    else
    {
        //이메일 형식이 아닐 경우
        self.btn_Status.selected = NO;
        self.v_Email.layer.borderColor = kErrorColor.CGColor;
        self.v_AccView.btn_Next.backgroundColor = kDisableColor;
        self.v_AccView.btn_Next.enabled = NO;
        [UIView animateWithDuration:0.25 animations:^{
            self.lb_WarningMsg.alpha = YES;
        }];
    }
}


//MARK: UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        [self validCheck];
    });
    
//        //글자제한이 있는 경우 백스페이스바 예외처리
//        if( self.nMaxCount > 0 )
//        {
//            const char * _char = [string cStringUsingEncoding:NSUTF8StringEncoding];
//            int isBackSpace = strcmp(_char, "\b");
//            if (isBackSpace != -8)
//            {
//                //이건 바이트인데 기획에선 자릿수로 체크하길 원했음
//    //            NSUInteger bytes = [textField.text lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
//                NSUInteger bytes = [textField.text length];
//                if( bytes > self.nMaxCount - 1 )
//                {
//                    return NO;
//                }
//            }
//        }

    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self onNext];
    return YES;
}


//MARK: Action
- (void)onNext
{
    if( _tf_Email.text.length <= 0 )    return;
    
    [HapticHelper tap];
    
    [[NSUserDefaults standardUserDefaults] setObject:_tf_Email.text forKey:@"UserEmail"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSMutableDictionary *dicM_Params = [NSMutableDictionary dictionary];
    [dicM_Params setValue:_tf_Email.text forKey:@"email"];
    [[WebAPI sharedData] callAsyncWebAPIBlock:@"auth/validate/email" param:dicM_Params withMethod:@"GET" withBlock:^(id resulte, NSError *error) {
        
        if( error != nil )
        {
            return;
        }
        
        NSString *str_Success = [NSString stringWithFormat:@"%@", [resulte stringForKey:@"success"]];
        if( [str_Success isEqualToString:@"success"] )
        {
            //처음 가입시
            [self performSegueWithIdentifier:@"EmailJoinPwViewController" sender:nil];
        }
        else if( [str_Success isEqualToString:@"failure"] )
        {
            //기존 가입자
            [self performSegueWithIdentifier:@"EmailLoginPwViewController" sender:nil];
        }

        //        NSDictionary *dic_Result = [resulte dictionaryForKey:@"result"];
        //        LoginInfo *loginInfo = [[LoginInfo alloc] initWithData:dic_Result];
        //        NSLog(@"%ld", loginInfo.nUserId);
        //        NSLog(@"%@", loginInfo.str_UserName);
    }];
    
}

@end
